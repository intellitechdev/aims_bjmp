﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmSignatories
    Private gCon As New Clsappconfiguration()
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_Signatories_Insert", _
                    New SqlParameter("@fcName1", txtName1.Text),
                    New SqlParameter("@fcPosition1", txtPosition1.Text),
                    New SqlParameter("@fcName2", txtName2.Text),
                    New SqlParameter("@fcPosition2", txtPosition2.Text))
        MsgBox("Saved.", MsgBoxStyle.Information, "Signatories")
    End Sub

    Private Sub loadSignatories()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_Signatories_Load")
        While rd.Read = True
            txtName1.Text = rd.Item(0).ToString
            txtPosition1.Text = rd.Item(1).ToString
            txtName2.Text = rd.Item(2).ToString
            txtPosition2.Text = rd.Item(3).ToString
        End While
    End Sub

    Private Sub frmSignatories_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadSignatories()
    End Sub
End Class