Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmChecks

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmChecks_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmChecks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_savechecksprinted(gPayeeID, gCheckNumber, gCheckDate, gPayorder, gCheckAmt, _
                                       gCheckAmtwords, gPayeesAdd, gPayeesMemo, gCheckPrintMode, _
                                       gBankAccount, gBankEndBalance)

        ' rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\checkvoucher_individual.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fxKeySupplier", gPayeeID)
        rptsummary.SetParameterValue("@fcUser", gUserName)
        rptsummary.SetParameterValue("@fcCheckno", gCheckNumber)
        rptsummary.SetParameterValue("@checknumber", gCheckNumber)
        rptsummary.SetParameterValue("@checkdate", gCheckDate)
        rptsummary.SetParameterValue("@checkamt", gCheckAmt)
        rptsummary.SetParameterValue("@checkwords", gCheckAmtwords)
        rptsummary.SetParameterValue("@checkmode", gCheckPrintMode)
        rptsummary.SetParameterValue("@fcBank", gBankAccount)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

End Class