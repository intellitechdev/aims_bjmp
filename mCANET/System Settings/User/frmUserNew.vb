Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Public Class frmUserNew

    'Private ctest As New test1
    Public mode As String = "ADD"
    Public KeyUser As String = ""
    Private DBPassword As String = ""
    Private dbLogName As String = ""
    Private gCon As New Clsappconfiguration

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    'Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
    '    Dim sConfigContent As String
    '    Dim sErr As String
    '    Dim bUpdate As Boolean
    '    Dim strDBSettings As String
    '    Dim strEncryptedPwd As String
    '    Dim myConfig As New Clsappconfiguration
    '    Dim dbOK As Boolean

    '    strEncryptedPwd = Clsappconfiguration.GetEncryptedData(txtPassword.Text)
    '    strDBSettings = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & "?>" & _
    '                    vbCrLf & "<configuration>" & vbCrLf & vbNewLine & _
    '                      vbTab & "<appSettings>" & vbCrLf & vbNewLine & _
    '                                 vbTab & vbTab & "<add key=" & Chr(34) & "Server" & Chr(34) & " value=" & Chr(34) & txtServer.Text & Chr(34) & " />" & _
    '                        vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Database" & Chr(34) & " value= " & Chr(34) & txtDatabase.Text & Chr(34) & " />" & _
    '                        vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Username" & Chr(34) & " value=" & Chr(34) & txtUserName.Text & Chr(34) & " />" & _
    '                        vbCrLf & vbTab & vbTab & "<add key=" & Chr(34) & "Password" & Chr(34) & " value=" & Chr(34) & strEncryptedPwd & Chr(34) & " />" & _
    '                      vbNewLine & vbCrLf & vbTab & "</appSettings>" & vbNewLine & vbCrLf & "</configuration>"

    '    'Chr(34) & "Hello" & Chr(34)

    '    sConfigContent = GetFileContents(strCurrentFileName & ".config", sErr)
    '    If sErr = "" Then
    '        dbOK = Clsappconfiguration.ServerCheck
    '        If dbOK Then
    '            bUpdate = SaveTextToFile(strDBSettings, strCurrentFileName & ".config", sErr)
    '            If bUpdate Then
    '                MessageBox.Show("Database connection parameter has been change" & vbCrLf & _
    '                       "Please reopen the application.", "Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                End
    '            Else
    '                MessageBox.Show(sErr)
    '            End If
    '        Else
    '            MessageBox.Show("Please enter the correct parameter!", "Database Settings", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        End If
    '    Else
    '        MessageBox.Show(sErr)
    '    End If
    'End Sub
    Private Sub SetToDfault()
        mode = "ADD"
        DBPassword = ""
        dbLogName = ""
        KeyUser = ""
        txtPassword.Text = ""
        txtConfirmPswrd.Text = ""
        txtFullName.Text = ""
        txtLoginName.Text = ""
        txtDescription.Text = ""
        cboGroupName.SelectedItem = "Select Group"
    End Sub
    Private Sub LoadUserDetail(ByVal fxKeyUser As String)
        Dim sSqlCmd As String = "SELECT * FROM dbo.tUsers WHERE fxKeyUser ='" & fxKeyUser & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    txtFullName.Text = rd.Item(3).ToString
                    txtLoginName.Text = rd.Item(1).ToString
                    dbLogName = rd.Item(1).ToString
                    DBPassword = gCon.GetDecryptedData(rd.Item(2).ToString)
                    If rd.Item(4).ToString = 3 Then
                        cboGroupName.SelectedItem = "Admin"
                    Else
                        If rd.Item(4).ToString = 2 Then
                            cboGroupName.SelectedItem = "Power User"
                        Else
                            cboGroupName.SelectedItem = "User"
                        End If
                    End If
                    txtDescription.Text = rd.Item(5).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub add_User()
        Dim sSQL_CommandText As String = "SYS_User_Add "
        sSQL_CommandText &= "@LogName=" & txtLoginName.Text
        sSQL_CommandText &= ",@Pwd=" & "'" & Clsappconfiguration.GetEncryptedData(txtPassword.Text) & "'"
        sSQL_CommandText &= ",@FullName=" & "'" & txtFullName.Text & "'"
        sSQL_CommandText &= ",@Groups=" & cboGroupID.Text
        sSQL_CommandText &= ",@Descriptions=" & "'" & txtDescription.Text & "'"
        sSQL_CommandText &= ",@Created=" & " '" & CStr(Now) & "' "
        sSQL_CommandText &= ",@Creator=" & "'" & strSysCurrentFullName & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, Data.CommandType.Text, sSQL_CommandText)
        Catch
            MessageBox.Show(Err.Description, "Add User")
        End Try
    End Sub
    Private Sub load_groups()
        cboGroupName.Items.Clear()
        cboGroupID.Items.Clear()
        cboGroupName.Items.Add("Select Group")
        cboGroupID.Items.Add("None")
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "SYS_Groups_List")
                While rd.Read
                    cboGroupName.Items.Add(rd.Item("fcGroupName"))
                    cboGroupID.Items.Add(rd.Item("fxKeyGroup"))
                End While
            End Using
        Catch
            MessageBox.Show(Err.Description, "", MessageBoxButtons.OK)
        Finally
            cboGroupName.SelectedIndex = 0
            cboGroupID.SelectedIndex = 0
        End Try
    End Sub
    Private Sub EditUser(ByVal FullName As String, ByVal Password As String, ByVal Description As String, _
                            ByVal LoginName As String, ByVal fxKeyGroup As String)
        Dim EncryptedPassword = Clsappconfiguration.GetEncryptedData(Password)
        Dim sSqlCmd As String
        sSqlCmd = "UPDATE dbo.tUsers SET fcLogName ='" & LoginName & _
                                        "', fcFullName ='" & FullName & _
                                        "', fxKeyGroup ='" & fxKeyGroup & _
                                        "', fcDescription ='" & Description & "' "
        If ChckChangePass.Checked = True Then
            sSqlCmd &= ", fcPassword ='" & EncryptedPassword & "' "
        End If
        sSqlCmd &= "WHERE fxKeyUser ='" & KeyUser & "'"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
        End Try
    End Sub
    Private Function CheckIfLogInNameExist(ByVal LoginName As String)
        Dim sSqlCmd As String = "SELECT fcLogName FROM dbo.tUsers WHERE fcLogName ='" & LoginName & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    Return "Existed"
                Else
                    Return "Not Existed"
                End If
            End Using
        Catch ex As Exception
        End Try
    End Function
    Private Function GetfxKeyUser(ByVal UserFullName As String, ByVal UserLogInName As String)
        Dim sSqlCmd As String = "SELECT fxKeyUser FROM dbo.tUsers WHERE fcLogName ='" & UserLogInName & _
                                "' AND fcFullName ='" & UserFullName & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
    End Function
    Private Sub frmUserNew_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        SetToDfault()
    End Sub
    Private Sub frmUserNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim myConnection As New Clsappconfiguration
        'txtServer.Text = myConnection.Server
        'txtDatabase.Text = myConnection.Database
        Call load_groups()

        If mode = "EDIT" Then
            Me.Text = "Edit User"
            Label1.Text = "Edit User"
            LoadUserDetail(KeyUser)
        Else
            If mode = "ADD" Then
                SetToDfault()
                Me.Text = "Add User"
                Label1.Text = "New User"
            End If
        End If
    End Sub
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim Ask As String
        If txtFullName.Text = "" Or txtFullName.Text Is Nothing Then
            MsgBox("Please type the user's fullname.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No data detected.")
            txtFullName.SelectAll()
            Exit Sub
        End If
        If txtLoginName.Text = "" Or txtLoginName.Text Is Nothing Then
            MsgBox("Please type the user's Login name.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No data detected.")
            txtLoginName.SelectAll()
            Exit Sub
        End If
        If ChckChangePass.Checked = True Then
            If txtPassword.Text = "" Or txtPassword.Text Is Nothing Then
                MsgBox("Please type the user's password.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No data detected.")
                txtPassword.SelectAll()
                Exit Sub
            End If
        End If
        If cboGroupName.SelectedItem = "Select Group" Or cboGroupName.SelectedItem Is Nothing Then
            MsgBox("Please select the user's group.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No data detected.")
            Exit Sub
        End If
        If txtPassword.Text = txtConfirmPswrd.Text Then
            If mode = "ADD" Then
                '############################################### ADD mode
                If CheckIfLogInNameExist(txtLoginName.Text) = "Existed" Then
                    MsgBox("The login name already exist. Please choose another.", MsgBoxStyle.Exclamation + _
                            MsgBoxStyle.OkOnly, "Existing username detected.")
                    txtLoginName.SelectAll()
                Else
                    add_User()
                    MsgBox("User successfully added to the database.", MsgBoxStyle.Information)
                    'Ask = MsgBox("User successfully added to the database." & vbCr & "You must now configure the accessibility of modules and reports for this user.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Directing to Accessibility Control Panel")
                    AuditTrail_Save("SYSTEM SETTINGS", "User Options > Create Users > Added New User '" & txtLoginName.Text & "' | " & txtFullName.Text)
                    Call SetUserDefaultAccess()

                    'frmAccesibility.Mode = "ADD"
                    'frmAccesibility.PreFxKeyUser = GetfxKeyUser(txtFullName.Text, txtLoginName.Text)
                    'frmAccesibility.ShowDialog()
                    Me.Close()
                End If
            Else
                '############################################### EDIT mode
                If CheckIfLogInNameExist(txtLoginName.Text) = "Existed" And txtLoginName.Text <> dbLogName Then
                    MsgBox("The login name already exist. Please choose another.", MsgBoxStyle.Exclamation + _
                            MsgBoxStyle.OkOnly, "Existing username detected.")
                    txtLoginName.SelectAll()
                Else
                    EditUser(txtFullName.Text, txtPassword.Text, txtDescription.Text, txtLoginName.Text, cboGroupID.SelectedItem.ToString)
                    'Ask = MsgBox("User info successfully updated to the database." & vbCr & "Do you want to configure the accessibility of modules and reports for this user.", MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Directing to Accessibility Control Panel")
                    Ask = MessageBox.Show("User Successfully Updated!", "Edit User", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    AuditTrail_Save("SYSTEM SETTINGS", "User Options > Create Users > Update User '" & txtLoginName.Text & "' | " & txtFullName.Text)
                    If Ask = MessageBoxButtons.OK Then
                        frmAccesibility.Mode = "EDIT"
                        frmAccesibility.PreFxKeyUser = GetfxKeyUser(txtFullName.Text, txtLoginName.Text)
                        frmAccesibility.ShowDialog()
                    End If
                    Me.Close()
                End If
            End If
        Else
            MsgBox("Password did not confirmed.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
            txtPassword.SelectAll()
        End If
    End Sub

    Private Sub SetUserDefaultAccess()
        Dim mycon As New Clsappconfiguration
        SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "spu_User_AddDefaultAccess", _
                                   New SqlParameter("@fxKeyUser", getUserKey() - 1),
                                   New SqlParameter("@coid", gCompanyID()))
    End Sub

    Private Function getUserKey()
        Dim sSqlCmd As String = "SELECT top 1 fxKeyUser FROM dbo.tUsers order by fxKeyUser desc"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
            While rd.Read
                Return Val(rd.Item(0)) + 1
            End While
        End Using
    End Function

    Private Sub cboGroupName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroupName.Click
        cboGroupID.SelectedIndex = cboGroupName.SelectedIndex
    End Sub
    Private Sub cboGroupName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroupName.TextChanged
        cboGroupID.SelectedIndex = cboGroupName.SelectedIndex
    End Sub
    Private Sub txtFullName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFullName.GotFocus
        txtFullName.SelectAll()
    End Sub
    Private Sub txtLoginName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoginName.GotFocus
        txtLoginName.SelectAll()
    End Sub
    Private Sub txtPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPassword.GotFocus
        txtPassword.SelectAll()
    End Sub
    'Private Sub load_Groups()
    '    Try
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        'Dim arrayGroups As New clsParamCollection

    '        Dim cmd As New SqlCommand("SYS_Groups_List", myconnection.sqlconn)
    '        myconnection.sqlconn.Open()
    '        Dim myreader As SqlDataReader
    '        myreader = cmd.ExecuteReader()

    '        'arrayGroups.ClearParameter()w

    '        cboGroup.Items.Clear()
    '        While myreader.Read()
    '            Me.cboGroup.Items.Add(myreader.Item("fcGroupName"))
    '            'arrayGroups.AddParameter("GroupKey", myreader.Item("fcGroupID"), 1)
    '            'arrayGroups.AddParameter("GroupName", myreader.Item("fcGroupName"), 1)

    '        End While
    '        cboGroup.SelectedIndex = 0

    '        myconnection.sqlconn.Close()
    '        myreader.Close()
    '    Catch
    '        MessageBox.Show(Err.Description, "New User")
    '    End Try
    'End Sub

    'Public Function SMMR_Authentication(ByVal psUser As String, ByVal psPassword As String) As String
    '    Dim rd As SqlDataReader
    '    Dim val As String
    '    rd = SqlHelper.ExecuteReader(goConn, Data.CommandType.Text, "usp_smm_m_authentication @username ='" & psUser & "', @password = '" & psPassword & "'")
    '    Try
    '        While rd.Read
    '            val = rd.Item(0)
    '        End While
    '    Catch ex As Exception
    '        val = "0"
    '    End Try
    '    Return val
    'End Function

    'Public Function SMMR_GetMachineType() As Data.DataSet
    '    Dim sSQL_CommandText As String = "select type_desc from  dbo.smm_m_machinetype "
    '    Return SqlHelper.ExecuteDataset(goConn, Data.CommandType.Text, sSQL_CommandText)
    'End Function
    Private Sub txtConfirmPswrd_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtConfirmPswrd.GotFocus
        txtConfirmPswrd.SelectAll()
    End Sub
    Private Sub txtDescription_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescription.GotFocus
        txtDescription.SelectAll()
    End Sub
    Private Sub ChckChangePass_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChckChangePass.CheckedChanged
        If ChckChangePass.Checked = True Then
            txtPassword.Enabled = True
            txtConfirmPswrd.Enabled = True
        Else
            txtPassword.Enabled = False
            txtConfirmPswrd.Enabled = False
        End If
    End Sub
End Class

