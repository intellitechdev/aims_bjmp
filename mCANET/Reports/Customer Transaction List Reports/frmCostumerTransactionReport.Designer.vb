<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomerTransactionReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbtnTransaction1 = New System.Windows.Forms.RadioButton
        Me.rbtnTransaction4 = New System.Windows.Forms.RadioButton
        Me.rbtnTransaction3 = New System.Windows.Forms.RadioButton
        Me.rbtnTransaction2 = New System.Windows.Forms.RadioButton
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbtnTransaction1)
        Me.GroupBox1.Controls.Add(Me.rbtnTransaction4)
        Me.GroupBox1.Controls.Add(Me.rbtnTransaction3)
        Me.GroupBox1.Controls.Add(Me.rbtnTransaction2)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(177, 110)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'rbtnTransaction1
        '
        Me.rbtnTransaction1.AutoSize = True
        Me.rbtnTransaction1.Checked = True
        Me.rbtnTransaction1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnTransaction1.Location = New System.Drawing.Point(11, 10)
        Me.rbtnTransaction1.Name = "rbtnTransaction1"
        Me.rbtnTransaction1.Size = New System.Drawing.Size(147, 20)
        Me.rbtnTransaction1.TabIndex = 7
        Me.rbtnTransaction1.TabStop = True
        Me.rbtnTransaction1.Text = "All Transaction List"
        Me.rbtnTransaction1.UseVisualStyleBackColor = True
        '
        'rbtnTransaction4
        '
        Me.rbtnTransaction4.AutoSize = True
        Me.rbtnTransaction4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnTransaction4.Location = New System.Drawing.Point(11, 81)
        Me.rbtnTransaction4.Name = "rbtnTransaction4"
        Me.rbtnTransaction4.Size = New System.Drawing.Size(116, 17)
        Me.rbtnTransaction4.TabIndex = 5
        Me.rbtnTransaction4.Text = "Debit Memo List"
        Me.rbtnTransaction4.UseVisualStyleBackColor = True
        '
        'rbtnTransaction3
        '
        Me.rbtnTransaction3.AutoSize = True
        Me.rbtnTransaction3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnTransaction3.Location = New System.Drawing.Point(11, 58)
        Me.rbtnTransaction3.Name = "rbtnTransaction3"
        Me.rbtnTransaction3.Size = New System.Drawing.Size(119, 17)
        Me.rbtnTransaction3.TabIndex = 4
        Me.rbtnTransaction3.Text = "Credit Memo List"
        Me.rbtnTransaction3.UseVisualStyleBackColor = True
        '
        'rbtnTransaction2
        '
        Me.rbtnTransaction2.AutoSize = True
        Me.rbtnTransaction2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnTransaction2.Location = New System.Drawing.Point(11, 35)
        Me.rbtnTransaction2.Name = "rbtnTransaction2"
        Me.rbtnTransaction2.Size = New System.Drawing.Size(91, 17)
        Me.rbtnTransaction2.TabIndex = 3
        Me.rbtnTransaction2.Text = "Invoice List"
        Me.rbtnTransaction2.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(4, 118)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(78, 29)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "Open"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(96, 118)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(85, 29)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmCustomerTransactionReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(185, 152)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Location = New System.Drawing.Point(500, 60)
        Me.MaximizeBox = False
        Me.Name = "frmCustomerTransactionReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Customer Transaction Report"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnTransaction4 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTransaction3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTransaction2 As System.Windows.Forms.RadioButton
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents rbtnTransaction1 As System.Windows.Forms.RadioButton
End Class
