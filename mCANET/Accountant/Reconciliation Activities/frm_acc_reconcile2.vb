Public Class frm_acc_reconcile2

    Private gTrans As New clsTransactionFunctions
    Private gMaster As New modMasterFile
    Private reconid As String = Guid.NewGuid.ToString

    Private Sub frm_acc_reconcile2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call gLoadChecksandPayments(grdChkPayments, gAccountID, Me)
        Call gLoadDepositandCredits(grdDepCredits, gAccountID, Me)
        Call sLoad()
        txtdiffamt.Text = Format(CDec(pComputeAmountDifference()), "##,##0.00")
    End Sub

    Private Sub grdChkPayments_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdChkPayments.CellValidated
        Call pComputeGridAmount(grdChkPayments, txtChkAmnt, lblCntChck, 1)
    End Sub

    Private Sub grdDepCredits_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDepCredits.CellValidated
        Call pComputeGridAmount(grdDepCredits, txtDepAmt, lblCntDep, 1)
    End Sub

    Private Sub txtclrbal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtclrbal.TextChanged
        Call pComputeAmountDifference()
    End Sub

    Private Sub btnMarkAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarkAll.Click
        Call pMarkGridAll(grdChkPayments)
        Call pMarkGridAll(grdDepCredits)
    End Sub

    Private Sub btnUnmarkAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnmarkAll.Click
        Call pUnMarkGridAll(grdChkPayments)
        Call pUnMarkGridAll(grdDepCredits)
    End Sub

    Private Sub btnModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModify.Click
        gVerifyReconcile = 1
        frm_acc_reconcile.Show()
        Me.Close()
    End Sub

    Private Sub btnReconcile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReconcile.Click
        If txtdiffamt.Text = "0.00" Then
            gServiceCharge = 0
            gInterestEarned = 0
            gEndingBalance = 0
            gClearedBalance = 0
            gDifference = 0
            gReconDate = Now.Date
            gAccountID = ""
            gTrans.gSaveReconcilation(reconid, gAccountID, gReconDate, gClearedBalance, _
                                    gEndingBalance, gServiceCharge, gServiceChargeDate, _
                                    gServiceChargeAccount, gInterestEarned, gInterestEarnedDate, _
                                    gInterestEarnedAccount, txtChkAmnt.Text, txtDepAmt.Text, _
                                    txtdiffamt.Text, 1, Me)
            gTransactionsCleared(grdChkPayments)
            gTransactionsCleared(grdDepCredits)
            gTrans.gCalculateEndingBalance(gEndingBalance, gAccountID, Me)
            Me.Close()
        Else
            gReconcileDiscripancy = CDec(txtdiffamt.Text)
            gTrans.gSaveReconcilation(reconid, gAccountID, gReconDate, gClearedBalance, _
                                   gEndingBalance, gServiceCharge, gServiceChargeDate, _
                                   gServiceChargeAccount, gInterestEarned, gInterestEarnedDate, _
                                   gInterestEarnedAccount, txtChkAmnt.Text, txtDepAmt.Text, _
                                   txtdiffamt.Text, 0, Me)
            gTransactionsCleared(grdChkPayments)
            gTransactionsCleared(grdDepCredits)
            frm_acc_reconadjustment.ShowDialog()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        gTrans.gSaveReconcilation(reconid, gAccountID, gReconDate, gClearedBalance, _
                                  gEndingBalance, gServiceCharge, gServiceChargeDate, _
                                  gServiceChargeAccount, gInterestEarned, gInterestEarnedDate, _
                                  gInterestEarnedAccount, txtChkAmnt.Text, txtDepAmt.Text, _
                                  txtdiffamt.Text, 0, Me)
        Me.Close()
    End Sub

#Region "Functions/Procedures"

    Private Sub sLoad()
        lblDatePeriod.Text = gReconDate
        txtsrvcchg.Text = Format(CDec(gServiceCharge), "##,##0.00")
        txtintearnd.Text = Format(CDec(gInterestEarned), "##,##0.00")
        txtendbal.Text = Format(CDec(gEndingBalance), "##,##0.00")
        txtclrbal.Text = Format(CDec(gClearedBalance), "##,##0.00")
        txtBegBal.Text = Format(CDec(gClearedBalance), "##,##0.00")
    End Sub

    Private Function pComputeAmountDifference() As Decimal
        Dim xDifference As Decimal = (gServiceCharge + gInterestEarned + gEndingBalance) - gClearedBalance
        Return xDifference
    End Function

    Private Sub pMarkGridAll(ByRef grdView As DataGridView)
        Dim xCnt As Integer
        With grdView
            For xCnt = 0 To .Rows.Count - 1
                If .Rows(xCnt).Cells(6).Value IsNot Nothing Then
                    .Rows(xCnt).Cells(1).Value = True
                End If
            Next
        End With
        Call pComputeGridAmount(grdChkPayments, txtChkAmnt, lblCntChck)
        Call pComputeGridAmount(grdDepCredits, txtDepAmt, lblCntDep)
    End Sub

    Private Sub gTransactionsCleared(ByRef grdView As DataGridView)
        Dim xCnt As Integer
        With grdView
            For xCnt = 0 To .Rows.Count - 1
                If .Rows(xCnt).Cells(1).Value = True Then
                    If .Rows(xCnt).Cells(0).Value.ToString() IsNot Nothing Then
                        gMaster.gClearTransaction(1, reconid, .Rows(xCnt).Cells(0).Value.ToString(), Me)
                    End If
                End If
            Next
        End With
    End Sub

    Private Sub pUnMarkGridAll(ByRef grdView As DataGridView)
        Dim xCnt As Integer
        With grdView
            For xCnt = 0 To .Rows.Count - 1
                .Rows(xCnt).Cells(1).Value = False
            Next
        End With
        Call pComputeGridAmount(grdChkPayments, txtChkAmnt, lblCntChck)
        Call pComputeGridAmount(grdDepCredits, txtDepAmt, lblCntDep)
    End Sub

    Private Sub pComputeGridAmount(ByRef grdView As DataGridView, ByRef txtAmt As TextBox, ByRef lblCnt As Label, Optional ByVal xVal As Integer = 0)
        Dim xCnt, xCount As Integer
        Dim xAmount As Long = 0.0
        With grdView
            For xCnt = 0 To .Rows.Count - 1
                If xVal = 1 Then
                    If .CurrentCellAddress.X = 1 Then
                        If gTrans.gmkDefaultValues(xCnt, 1, grdView) <> Nothing Then
                            If .Rows(xCnt).Cells(1).Value = True Then
                                If gTrans.gmkDefaultValues(xCnt, 6, grdChkPayments) <> Nothing Then
                                    If .Rows(xCnt).Cells(6).Value IsNot Nothing Then
                                        xAmount += Format(CDec(.Rows(xCnt).Cells(6).Value), "##,##0.00")
                                        xCount += 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                ElseIf xVal = 0 Then
                    If gTrans.gmkDefaultValues(xCnt, 1, grdView) <> Nothing Then
                        If .Rows(xCnt).Cells(1).Value = True Then
                            If gTrans.gmkDefaultValues(xCnt, 6, grdChkPayments) <> Nothing Then
                                If .Rows(xCnt).Cells(6).Value IsNot Nothing Then
                                    xAmount += CDec(.Rows(xCnt).Cells(6).Value)
                                    xCount += 1
                                End If
                            End If
                        End If
                    End If
                End If
                lblCnt.Text = xCount
                txtAmt.Text = Format(CDec(xAmount), "##,##0.00")
            Next
        End With
    End Sub

#End Region


End Class