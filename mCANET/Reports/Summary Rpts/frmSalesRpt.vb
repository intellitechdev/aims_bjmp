Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmSalesRpt

    Private gcon As New Clsappconfiguration
    Dim levelDept As Integer = 0
    Dim VirtualListView As New ListView
    Private Sub loadClassesToLstClass()
        Dim DBClassname As String
        Dim xClassName(0) As String
        Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DBClassname = rd.Item(0).ToString
                    xClassName = Split(DBClassname, "���")
                    With LstClass.Items.Add(xClassName(0))
                        .SubItems.Add(DBClassname)
                    End With
                End While
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub LoadClassesToCboClassing()
        On Error Resume Next
        Dim xRow As Integer = 0
        loadClassesToLstClass()
        Dim Classes(50) As MTGCComboBoxItem
        Dim i As Integer = 0

        For Each li As ListViewItem In LstClass.Items
            Classes(i) = New MTGCComboBoxItem(li.Text, li.SubItems(1).Text)
            i += 1
        Next
        CboClassing.SelectedIndex = -1
        CboClassing.Items.Clear()
        CboClassing.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        CboClassing.Items.AddRange(Classes)
    End Sub
    Private Sub GetSelectedClassScope(ByVal classRealName As String)
        Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE ClassParentPath='" & classRealName & "'AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Dim xClassRef(0) As String
                    xClassRef = Split(rd.Item(0).ToString, "���")
                    With VirtualListView.Items.Add(rd.Item(0).ToString)
                        .SubItems.Add(xClassRef(1))
                    End With
                End While
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub rdPerInvoice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdPerInvoice.CheckedChanged
        If rdPerInvoice.Checked = True Then
            cboBrand.Enabled = False
            ChckClassing.Enabled = True
            ChkSubClasses.Enabled = True
            ChkSubClasses.Checked = True
        Else
            cboBrand.Enabled = True
            ChckClassing.Enabled = False
            ChckClassing.Checked = False
            CboClassing.Enabled = False
            ChkSubClasses.Enabled = False
            ChkSubClasses.Checked = False
        End If
    End Sub
    Private Sub rdPerBrand_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdPerBrand.CheckedChanged
        If rdPerInvoice.Checked = True Then
            cboBrand.Enabled = True
            cboBrand.SelectedIndex = 0
        End If
    End Sub
    Private Sub EngageClassingEvent()
        Dim diggerDept As Integer = 0
        Dim SearchVar As ListViewItem
        Dim xRefno(0) As String
        txtClassRefno.Text = CboClassing.SelectedItem.Col2
        GetLevelDept()
        ListViewScope.Items.Clear()
        With ListViewScope.Items.Add(txtClassRefno.Text)
            xRefno = Split(txtClassRefno.Text, "���")
            .SubItems.Add(xRefno(1))
        End With

        If ChkSubClasses.Checked = True Then
            Do While diggerDept <> levelDept
                For Each Item As ListViewItem In ListViewScope.Items
                    GetSelectedClassScope(Item.Text)
                Next
                For Each CollectedItem As ListViewItem In VirtualListView.Items
                    SearchVar = ListViewScope.FindItemWithText(CollectedItem.Text, False, 0)
                    If SearchVar Is Nothing Then
                        With ListViewScope.Items.Add(CollectedItem.Text)
                            .SubItems.Add(CollectedItem.SubItems(1).Text)
                        End With
                    End If
                Next
                VirtualListView.Items.Clear()
                diggerDept += 1
            Loop
        End If

        Dim sSqlDeleteCmd As String = "DELETE FROM dbo.mTemporaryClassStorage WHERE fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlDeleteCmd)
        Catch ex As Exception
        End Try
        For Each ClassItem As ListViewItem In ListViewScope.Items

            Dim sSqlSaveToTemporaryStorage As String = "INSERT INTO dbo.mTemporaryClassStorage"
            sSqlSaveToTemporaryStorage &= "(ClassName, ClassRefNo, fxKeyCompany) VALUES ('" & ClassItem.Text & _
                                            "', '" & ClassItem.SubItems(1).Text & "', '" & gCompanyID() & "')"
            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlSaveToTemporaryStorage)
            Catch ex As Exception
            End Try
        Next

    End Sub
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If rdPerInvoice.Checked = True Then
            'If ChckClassing.Checked = True Then

            'Else
            gMonth = CStr(cboMonth.SelectedIndex + 1)
            gYear = CStr(cboYear.SelectedItem)
            frmSaleReport_perInvoice.MdiParent = frmMain
            frmSaleReport_perInvoice.Show()
            'End If
        ElseIf rdPerBrand.Checked = True Then
            gBrand = verifybrand()
            'gBrand = cboBrand.SelectedItem.ToString
            gMonth = CStr(cboMonth.SelectedIndex + 1)
            gYear = CStr(cboYear.SelectedItem)
            frmSalesReport_perBrand.MdiParent = frmMain
            frmSalesReport_perBrand.Show()
        End If
    End Sub
    Private Function verifybrand() As String
        Dim sResult As String = ""

        Select Case Me.cboBrand.SelectedIndex
            Case 0
                sResult = "Hav"
            Case 1
                sResult = "D&G"
            Case 2
                sResult = "Tbox"
            Case 3
                sResult = "Cia"
            Case 4
                sResult = "Pin"
        End Select

        Return sResult
    End Function
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub GetLevelDept()
        Dim sSqlCmd As String = "SELECT classLevel FROM dbo.mClasses WHERE fxKeyCompany ='" & gCompanyID() & "'"
        Dim xLevel As Integer
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xLevel = rd.Item(0).ToString
                    If levelDept < xLevel Then
                        levelDept = xLevel
                    End If
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub frmSalesRpt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'loadBrandsInCbo()
        If frmMain.LblClassActivator.Visible = True Then
            ChckClassing.Visible = True
            CboClassing.Visible = True
            ChkSubClasses.Visible = True
        Else
            ChckClassing.Visible = False
            CboClassing.Visible = False
            ChkSubClasses.Visible = False
        End If
        LoadClassesToCboClassing()
    End Sub
    Private Sub loadBrandsInCbo()
        'Dim gcon As New Clsappconfiguration
        'Dim commandString As String = "SELECT DISTINCT fcBrand FROM dbo.mItemMaster "
        'commandString &= "WHERE fcBrand is NOT null "
        'commandString &= "AND fcBrand <> ' '"

        'Try
        '    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, commandString)
        '    While reader.Read
        '        cboBrand.Items.Add(reader.Item(0))
        '    End While
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub
    Private Sub ChckClassing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChckClassing.CheckedChanged
        If ChckClassing.Checked = True Then
            CboClassing.Enabled = True
            ChkSubClasses.Enabled = True
            If CboClassing.Items.Count <> 0 Then
                CboClassing.SelectedIndex = 0
            End If
        Else
            ChkSubClasses.Enabled = False
            CboClassing.Enabled = False
        End If
    End Sub
    Private Sub CboClassing_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CboClassing.SelectedIndexChanged
        EngageClassingEvent()
    End Sub
    Private Sub ChkSubClasses_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkSubClasses.CheckedChanged
        If CboClassing.Text <> "" Then
            EngageClassingEvent()
        End If
    End Sub
End Class