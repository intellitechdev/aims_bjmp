﻿Imports System.Data.SqlClient
Imports WPM_EDCRYPT
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBankList
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Dim List As String
    Private Sub frmBankList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BankList()
    End Sub
    Public Sub BankList()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "bankfile_ListAll",
                                    New SqlParameter("@coid", gCompanyID()))
        dgvBankList.DataSource = ds.Tables(0)
        With dgvBankList
            .Columns("PK_BankID").Visible = False
            .Columns("acnt_code").Visible = False
        End With
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub SearchDocNum(ByVal SelNum As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "BankFile_SearchBank", _
                                   New SqlParameter("@fcBankName", SelNum))
        dgvBankList.DataSource = ds.Tables(0)
        With dgvBankList
            .Columns("PK_BankID").Visible = False
            .Columns("acnt_code").Visible = False
        End With
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchDocNum(txtSearch.Text)
    End Sub

    Private Sub dgvBankList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvBankList.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.DialogResult = DialogResult.OK
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
End Class