Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmUserList

    Private gCon As New Clsappconfiguration
    Private Const COL_UserKey As Integer = 0
    Private Const COL_GroupID As Integer = 1
    Private Const COl_UserGroup As Integer = 2
    Private Const COL_UserLogName As Integer = 3
    Private Const COL_UserFullName As Integer = 4
    Private Const COL_Desc As Integer = 5
    Private Const COl_Created As Integer = 6
    Private Const COL_Creator As Integer = 7


    Private Sub frmUesrList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadUser()

    End Sub

    Private Sub gridUserSettings()
        dataGridUserList.Columns(COL_UserKey).Visible = True
        dataGridUserList.Columns(COL_GroupID).Visible = True

        dataGridUserList.Columns.Item(COl_UserGroup).HeaderText = "User Group"
        dataGridUserList.Columns.Item(COL_UserLogName).HeaderText = "Login Name"
        dataGridUserList.Columns.Item(COL_UserFullName).HeaderText = "Full Name"
        dataGridUserList.Columns.Item(COL_Desc).HeaderText = "Description"
        dataGridUserList.Columns.Item(COl_Created).HeaderText = "Date Created"
        dataGridUserList.Columns.Item(COL_Creator).HeaderText = "Created By"
    End Sub

    Private Sub loadUser()
        Dim i As Integer = 0
        Try
            dataGridUserList.DataSource = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "SYS_User_List").Tables(0)

            For i = 0 To dataGridUserList.Columns.Count - 1
                Me.dataGridUserList.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next
            Call gridUserSettings()
        Catch
            MessageBox.Show(Err.Description, "User List")
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click, DeleteToolStripMenuItem.Click

        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        If x = System.Windows.Forms.DialogResult.OK Then
            If Me.dataGridUserList.SelectedRows.Count > 0 AndAlso _
                       Not Me.dataGridUserList.SelectedRows(0).Index = _
                       Me.dataGridUserList.Rows.Count - 1 Then

                Call delete_User()
                Call loadUser()

                'Me.dataGridUserList.Rows.RemoveAt( _
                '    Me.dataGridUserList.SelectedRows(0).Index)
            End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub

    Private Sub delete_User()
        Dim sSQLCmdText As String = "SYS_User_Delete "
        sSQLCmdText &= "@RecID=" & dataGridUserList.CurrentRow.Cells(COL_UserKey).Value & ", "
        sSQLCmdText &= "@RecCurrentID=" & intSysCurrentId

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmdText)
            MessageBox.Show("Record Successfully Deleted", "Delete User")
        Catch
            MessageBox.Show(Err.Description, "Delete User")
        End Try
    End Sub

    Private Sub dataGridUserList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dataGridUserList.Click
        If Me.dataGridUserList.SelectedRows.Count > 0 AndAlso _
                       Not Me.dataGridUserList.SelectedRows(0).Index = _
                       Me.dataGridUserList.Rows.Count - 1 Then
        End If
    End Sub

    Private Sub BtnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNew.Click, NewUserToolStripMenuItem.Click
        If gUserName = "admin" Then
            frmUserNew.mode = "ADD"
            frmUserNew.KeyUser = ""
            pCallCompany(frmUserNew)
        Else
            MsgBox("pls contact your administrator to access this module...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

    Private Sub BtnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEdit.Click, EditUserToolStripMenuItem.Click
        If gUserName = "admin" Then
            frmUserNew.mode = "EDIT"
            frmUserNew.KeyUser = dataGridUserList.CurrentRow.Cells(COL_UserKey).Value
            frmUserNew.ShowDialog()
            'pCallCompany(frmUserNew)
        Else
            MsgBox("pls contact your administrator to access this module...", MsgBoxStyle.Information, Me.Text)
        End If
    End Sub

End Class