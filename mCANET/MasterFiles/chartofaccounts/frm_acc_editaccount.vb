Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_acc_editaccount

#Region "Declarations"

    Private gTrans As New clsTransactionFunctions
    Private gMaster As New modMasterFile
    Private gCon As New Clsappconfiguration

#End Region

#Region "Events"

    Private Sub frm_acc_editaccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadAccountTypes()
        GetAccountsLevel(gAccountID)
        If gAccountID <> "" Then
            Call getAccountDetails()
        Else

            chkedit_subaccount.Checked = False
            chkedit_accountinactive.Checked = False
            gTrans.gClearFormTxt(Me)
            gTrans.gClearGrpTxt(grpEditAccount)
            gTrans.gClearGrpTxt(grpOptional)
            btnCancel.Text = "Cancel"
            Dim xCnt, iCnt As Integer
            iCnt = 0
            For xCnt = 11 To Me.Controls.Count - 1
                Try
                    Me.Controls(xCnt).Text = ""
                    Me.Controls(xCnt).Enabled = True
                    iCnt += 1
                Catch ex As Exception
                End Try
            Next
            cboedit_accounttype.Text = gAcntType
        End If
    End Sub
    Private Sub GetAccountsLevel(ByVal Account_ID As String)
        Dim xItem As String
        Dim accnt_ID As String
        accnt_ID = Account_ID 'acnt_id
        Dim LoopingBreaker As String = "DisEngage"
        Dim ctr As Integer = 0
        Dim prevCnt As Integer = 0
        LstException.Items.Clear()
        LstException.Items.Add(accnt_ID)

        prevCnt = LstException.Items.Count
        Do While ctr <> prevCnt
            For Each item As ListViewItem In LstException.Items
                accnt_ID = item.Text
                Dim sSqlCmd As String = "SELECT acnt_id, acnt_name FROM dbo.mAccounts WHERE co_id ='" & gCompanyID() & "' AND acnt_subof ='" & accnt_ID & "'"
                prevCnt = LstException.Items.Count
                Try
                    Using Rd1 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                        While Rd1.Read
                            xItem = Rd1.Item(0).ToString
                            'If xItem <> "" Then
                            ' MsgBox(xItem)
                            Dim Finder As New ListViewItem
                            Finder = LstException.FindItemWithText(xItem, False, 0)
                            If Finder Is Nothing Then
                                With LstException.Items.Add(xItem)
                                    .SubItems.Add(Rd1.Item(1).ToString)
                                End With
                            End If
                            'End If
                        End While
                        ctr = LstException.Items.Count
                    End Using
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error on GetAccountsLevel")
                End Try
            Next
        Loop


    End Sub
    Private Sub frm_acc_editaccount_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        chkedit_subaccount.Checked = False
        chkedit_accountinactive.Checked = False
        gTrans.gClearFormTxt(Me)
        gTrans.gClearGrpTxt(grpEditAccount)
        gTrans.gClearGrpTxt(grpOptional)
        btnCancel.Text = "Close"
    End Sub
    Private Sub cboedit_accounttype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboedit_accounttype.SelectedIndexChanged
        lblAcntCode.Text = ""
        Call loadSegmentValue(cboedit_accounttype.SelectedItem)
        gAcntType = cboedit_accounttype.SelectedItem
    End Sub
    Private Sub cboedit_subaccountname_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboedit_subaccountname.SelectedIndexChanged
        Call loadSegmentValuefromDB(cboedit_subaccountname.SelectedItem)
    End Sub
    Private Sub btnedit_saveaccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnedit_saveaccount.Click
        If InStr(gAcntType, "BANK", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "CREDIT", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "ASSET", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "EQUITY", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "LOAN", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "LIABILITIES", CompareMethod.Text) > 0 Or _
                  InStr(gAcntType, "LIABILITY", CompareMethod.Text) > 0 Then
            'If gEnterEndingBalance <> 0 Then
            If gEditEvent <> 1 Then
                If m_accountcodevalidation(gTrans.getSegment(Me, 11)) <> "exist" Then
                    Validate_accounts()
                Else
                    MessageBox.Show("Account Code exist, please re-define your account code", "CLICKSOFTWARE:" + Me.Text)
                End If
            Else
                Validate_accounts()
            End If
            'Else
            '    MessageBox.Show("Please click the 'Enter Opening Balance button', to continue", "CLICKSOFTWARE:" + Me.Text)
            'End If
        Else
            gEndingBal = 0
            gEndingdate = "01/01/1900"

            'TODO Add data validator to Account Codes that exist during Editing
            If Me.Text = "Add New Account" Then
                If m_accountcodevalidation(gTrans.getSegment(Me, 11)) <> "exist" Then
                    Validate_accounts()
                Else
                    MessageBox.Show("Account Code exist, please re-define your account code", "CLICKSOFTWARE:" + Me.Text)
                End If
            Else
                Validate_accounts()
            End If

        End If
    End Sub
    Private Sub Validate_accounts()
        gAccountCode = gTrans.getSegment(Me, 12)
        gAccountDesc = CStr(txtedit_accountdescription.Text)

        If (IsAccountCodesExisting(gAccountCode) = True And IsAccountNamesExisting(gAccountDesc) = True) Then
            If MsgBox("Account Exists already. Do you want to overwrite it?", _
            MsgBoxStyle.OkCancel, "Save Account") = MsgBoxResult.Ok Then
                saveaccount()
            Else
                ClearForm()
            End If
        ElseIf (IsAccountCodesExisting(gAccountCode) = True And IsAccountNamesExisting(gAccountDesc) = False) Then
            If MsgBox("Account Code Exists already. Do you want to overwrite it?", _
                        MsgBoxStyle.OkCancel, "Save Account") = MsgBoxResult.Ok Then
                saveaccount()
            Else
                ClearForm()
            End If
        ElseIf (IsAccountCodesExisting(gAccountCode) = False And IsAccountNamesExisting(gAccountDesc) = True) Then
            If MsgBox("Account Name Exists already. Do you want to overwrite it?", _
                        MsgBoxStyle.OkCancel, "Save Account") = MsgBoxResult.Ok Then
                saveaccount()
            Else
                ClearForm()
            End If
        Else
            saveaccount()
        End If

    End Sub
    Private Sub saveaccount()
        Dim Account_codes As String = gTrans.getSegment(Me, 12)
        gAcntType = cboedit_accounttype.SelectedItem

        Dim xInAct, xSub As Integer
        If chkedit_accountinactive.Checked = True Then
            xInAct = 0
        Else
            xInAct = 1
        End If
        If chkedit_subaccount.Checked = True Then
            xSub = 1
        Else
            xSub = 0
        End If

        Call gMaster.gSaveAccount(txtedit_acountname.Text, xInAct, xSub, _
                                  txtedit_accountdescription.Text, _
                                  txtedit_accountnote.Text, gAccountCode, Me)


    End Sub
    Private Sub ClearForm()
        lblAcntCode.Text = "Account Code"
        MessageBox.Show("Please re-define your account code.", "CLICKSOFTWARE:" + Me.Text)
    End Sub
    Private Sub LoadSubAccounts()
        Dim cboDatatmp As New ComboBox
        Dim sSQL As String = "Select acnt_name, acnt_id FROM dbo.mAccounts WHERE co_id ='" & gCompanyID() & "' AND (acnt_id <> '" & _
                                gAccountID & "' AND (acnt_subof <> '" & gAccountID & "' OR acnt_subof is null  )) ORDER BY acnt_name"


        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)
                While rd.Read
                    Dim Finder As New ListViewItem
                    Finder = LstException.FindItemWithText(rd.Item(1).ToString, False, 0)
                    If Finder Is Nothing Then
                        cboDatatmp.Items.Add(rd.Item(0).ToString)
                    End If
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim xCnt As Integer
        cboedit_subaccountname.Items.Clear()
        For xCnt = 0 To cboDatatmp.Items.Count - 1
            cboedit_subaccountname.Items.Add(cboDatatmp.Items(xCnt))
        Next
    End Sub
    Private Sub chkedit_subaccount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkedit_subaccount.CheckedChanged
        If chkedit_subaccount.Checked = True Then
            cboedit_subaccountname.Enabled = True
            LoadSubAccounts()
            'gMaster.gLoadCboAccounts(cboedit_subaccountname)
        Else
            cboedit_subaccountname.Text = ""
            cboedit_subaccountname.Items.Clear()
            lblSubAcntCode.Text = "Account Code"
            cboedit_subaccountname.Enabled = False
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If btnCancel.Text = "Cancel" Then
            chkedit_subaccount.Checked = False
            chkedit_accountinactive.Checked = False
            gTrans.gClearFormTxt(Me)
            gTrans.gClearGrpTxt(grpEditAccount)
            gTrans.gClearGrpTxt(grpOptional)
            lblAcntCode.Text = "Account Code"
            lblSubAcntCode.Text = "Account Code"
            btnCancel.Text = "Close"
        ElseIf btnCancel.Text = "Close" Then
            chkedit_subaccount.Checked = False
            chkedit_accountinactive.Checked = False
            gTrans.gClearFormTxt(Me)
            gTrans.gClearGrpTxt(grpEditAccount)
            gTrans.gClearGrpTxt(grpOptional)
            btnCancel.Text = "Close"
            Me.Close()
        End If
    End Sub
    Private Sub btnOBalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOBalance.Click
        gAccountName = txtedit_acountname.Text
        gAccountCode = gTrans.getSegment(Me, 11)
        frm_acc_openingbalance.ShowDialog()
    End Sub

#End Region

#Region "Functions"

    Private Sub LoadAccountTypes()
        Call gMaster.gLoadCboAcntType(cboedit_accounttype)
        cboedit_accounttype.SelectedItem = gAcntType
        Call loadSegmentValue(UCase(gAcntType))
        Call gTrans.gCreateTextbox(Me, 90)
        If InStr(gAcntType, "BANK", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "CREDIT", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "ASSET", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "EQUITY", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "LOAN", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "LIABILITIES", CompareMethod.Text) > 0 Or _
            InStr(gAcntType, "LIABILITY", CompareMethod.Text) > 0 Then
            btnOBalance.Visible = True
        Else
            btnOBalance.Visible = False
        End If
    End Sub

    Private Sub loadSegmentValue(ByVal psActType As String)
        Dim sSegVal As String = gTrans.gSegmentValue(psActType)
        Dim sVal As String = ""
        Dim xCnt As Integer
        If sSegVal(sSegVal.Length - 1) = "-" Then
            For xCnt = 0 To sSegVal.Length - 2
                sVal += sSegVal(xCnt)
            Next
        Else
            sVal = sSegVal
        End If
        lblAcntCode.Text = sVal
    End Sub

    Private Sub loadSegmentValuefromDB(ByVal psAccount As String)
        Dim sSegVal As String = gTrans.gSegmentFromAccounts(psAccount)
        Dim sVal As String = ""
        Dim xCnt As Integer
        If sSegVal(sSegVal.Length - 1) = "-" Then
            For xCnt = 0 To sSegVal.Length - 2
                sVal += sSegVal(xCnt)
            Next
        Else
            If sSegVal(sSegVal.Length - 2) = "-" Then
                For xCnt = 0 To sSegVal.Length - 3
                    sVal += sSegVal(xCnt)
                Next
            Else
                sVal = sSegVal
            End If
        End If
        lblSubAcntCode.Text = sVal
    End Sub

    Private Function CheckSegmentAvailability() As String
        CheckSegmentAvailability = ""
        Dim sAcntSeg(), sAcntTypeSeg(), sAcntforSubSeg() As String
        Dim sSource, sCompare As String
        Dim sVal As String = ""

        sAcntTypeSeg = Split(lblAcntCode.Text, gTrans.gGetSegmentSeparator(1))
        sSource = sAcntTypeSeg(0)
        sAcntSeg = Split(gTrans.getSegment(Me, 11), gTrans.gGetSegmentSeparator(1))
        sCompare = sAcntSeg(0)

        If chkedit_subaccount.Checked = False Then
            If sSource(0) <> sCompare(0) Then
                MessageBox.Show("Account code should not be greater/less than the account code of its corresponding account type..", "CLICKSOFTWARE:" + Me.Text)
            Else
                Dim iCnt As Integer
                For iCnt = 0 To sAcntTypeSeg.Length - 1
                    If sAcntTypeSeg(iCnt) = sAcntSeg(iCnt) Then
                        sVal += "1"
                    Else
                        sVal += "0"
                    End If
                Next
                CheckSegmentAvailability = pResult(sVal)
            End If

        ElseIf chkedit_subaccount.Checked = True Then
            sAcntforSubSeg = Split(lblSubAcntCode.Text, gTrans.gGetSegmentSeparator(1))
            sCompare = sAcntforSubSeg(0)

            If sSource(0) <> sCompare(0) Then
                MessageBox.Show("Account code should not be greater than the account code of its corresponding account type..", "CLICKSOFTWARE:" + Me.Text)
            Else
                If sAcntforSubSeg(0) <> sAcntSeg(0) Then
                    MessageBox.Show("sub-accounts should carry the first segment of the main account.", "CLICKSOFTWARE:" + Me.Text)
                Else
                    Dim iCnt As Integer
                    For iCnt = 1 To sAcntTypeSeg.Length - 1
                        If sAcntforSubSeg(iCnt) = sAcntSeg(iCnt) Then
                            sVal += "1"
                        Else
                            sVal += "0"
                        End If
                    Next
                    CheckSegmentAvailability = pResult(sVal)
                End If
            End If
        End If
    End Function

    Private Function pResult(ByVal psResult As String) As String
        pResult = ""
        If InStr(psResult, "0", CompareMethod.Text) > 0 Then
            pResult = "True"
        Else
            pResult = "False"
        End If
    End Function

    Private Sub getAccountDetails()

        Dim acnt_type As String = ""
        Dim acnt_name As String = ""
        Dim fbactive As String = ""
        Dim acnt_sub As String = ""
        Dim acnt_subof As String = ""
        Dim acnt_desc As String = ""
        Dim acnt_note As String = ""
        Dim acnt_code As String = ""
        Dim sSQL As String

        sSQL = "usp_m_mAccounts_get "
        sSQL &= "@acntid='" & gAccountID & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQL)

                While rd.Read
                    acnt_type = rd.Item(0).ToString
                    acnt_name = rd.Item(1).ToString
                    fbactive = rd.Item(2).ToString
                    acnt_sub = rd.Item(3).ToString
                    acnt_subof = rd.Item(4).ToString
                    acnt_desc = rd.Item(5).ToString
                    acnt_note = rd.Item(6).ToString
                    acnt_code = rd.Item(7).ToString
                End While
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.cboedit_accounttype.Text = UCase(gAcntType)
        Me.txtedit_acountname.Text = acnt_name
        Me.txtedit_accountdescription.Text = acnt_desc
        Me.txtedit_accountnote.Text = acnt_note

        If fbactive = "1" Then
            Me.chkedit_accountinactive.Checked = True
        Else
            Me.chkedit_accountinactive.Checked = False
        End If

        If acnt_sub = "1" Then
            Me.chkedit_subaccount.Checked = True
            Me.cboedit_subaccountname.Enabled = True
            Me.cboedit_subaccountname.SelectedItem = acnt_subof
        Else
            Me.chkedit_subaccount.Checked = False
            Me.cboedit_subaccountname.Enabled = False
            Me.cboedit_subaccountname.Text = ""
        End If

        Dim xCnt, iCnt As Integer
        Dim sVal() As String
        sVal = Split(acnt_code, gTrans.gGetSegmentSeparator(1))
        iCnt = 0

        'Separation of account codes to each textbox"
        For xCnt = 12 To Me.Controls.Count - 1
            Try
                Me.Controls(xCnt).Text = sVal(iCnt)
                Me.Controls(xCnt).Enabled = True
                iCnt += 1
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
        Next
    End Sub

#End Region


    Private Sub btnBeginningBudget_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBeginningBudget.Click
        dlgBeginningBudget.ShowDialog()
    End Sub

    Private Function IsAccountCodesExisting(ByVal AccountCodes As String) As Boolean
        Dim sSQLCmd As String = "SELECT acnt_code FROM mAccounts "
        sSQLCmd &= "WHERE co_id = '" & gCompanyID() & "' "
        sSQLCmd &= "AND acnt_code ='" & AccountCodes & "'"
        Dim result As String = "non-existing"
        Dim IsExisting As Boolean

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    result = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If result = "non-existing" Then
            IsExisting = False
        Else
            IsExisting = True
        End If

        Return IsExisting
    End Function
    Private Function IsAccountNamesExisting(ByVal AccountNames As String) As Boolean
        Dim sSQLCmd As String = "SELECT acnt_Code FROM mAccounts "
        sSQLCmd &= "WHERE co_id = '" & gCompanyID() & "' "
        sSQLCmd &= "AND acnt_desc ='" & AccountNames & "'"
        Dim result As String = "non-existing"
        Dim IsExisting As Boolean

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    result = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        IsExisting = IIf(result = "non-existing", False, True)
        Return IsExisting
    End Function

End Class