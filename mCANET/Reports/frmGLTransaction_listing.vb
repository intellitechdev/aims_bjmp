Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmGLTransaction_listing

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'rptsummary.Refresh()
        If cboSource.SelectedItem = "Classes" Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\gl_transaactionlistingforClassing.rpt")
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\gl_transaactionlisting.rpt")
        End If
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@frmMth", cboMth1.SelectedItem)
        rptsummary.SetParameterValue("@toMth", cboMth2.SelectedItem)
        rptsummary.SetParameterValue("@year", cboYear.SelectedItem)
        rptsummary.SetParameterValue("@sortedby", cboSorted.SelectedItem)
        'rptsummary.SetParameterValue("@frmAcnt", cboAcnt1.SelectdItem)
        'rptsummary.SetParameterValue("@toAcnt", cboAcnt2.SelectedItem)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        If cboSource.SelectedItem <> "Classes" Then
            rptsummary.SetParameterValue("@source", cboSource.SelectedItem)
        Else
            rptsummary.SetParameterValue("@source", System.DBNull.Value)
        End If
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub frmGLTransaction_listing_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub


    Private Sub frmGLTransaction_listing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If frmMain.LblClassActivator.Visible = True Then
            cboSource.Items.Add("Classes")
        End If
    End Sub

    Private Sub cboSource_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSource.SelectedIndexChanged
        If cboSource.SelectedItem = "Classes" Then
            frmGLClassFilter.ShowDialog()
            If frmGLClassFilter.Result = "Cancel" Then
                cboSource.SelectedText = ""
            End If
        End If
    End Sub

    Private Sub crvRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles crvRpt.Load

    End Sub

    Private Sub btnHide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHide.Click
        If btnHide.Text = "Hide Search Pane" Then
            btnHide.Text = "Show Search Pane"
            Panel1.Visible = False
            btnHide.Top = 3
        Else
            btnHide.Top = 71
            btnHide.Text = "Hide Search Pane"
            Panel1.Visible = True
        End If
    End Sub
End Class