<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomerMasterReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvCustomerMaster = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'crvCustomerMaster
        '
        Me.crvCustomerMaster.ActiveViewIndex = -1
        Me.crvCustomerMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvCustomerMaster.DisplayGroupTree = False
        Me.crvCustomerMaster.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvCustomerMaster.Location = New System.Drawing.Point(0, 0)
        Me.crvCustomerMaster.Name = "crvCustomerMaster"
        Me.crvCustomerMaster.SelectionFormula = ""
        Me.crvCustomerMaster.ShowCloseButton = False
        Me.crvCustomerMaster.ShowGroupTreeButton = False
        Me.crvCustomerMaster.ShowRefreshButton = False
        Me.crvCustomerMaster.Size = New System.Drawing.Size(805, 560)
        Me.crvCustomerMaster.TabIndex = 0
        Me.crvCustomerMaster.ViewTimeSelectionFormula = ""
        '
        'frmCustomerMasterReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(805, 560)
        Me.Controls.Add(Me.crvCustomerMaster)
        Me.Name = "frmCustomerMasterReport"
        Me.Text = "CustomerMasterReport"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvCustomerMaster As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
