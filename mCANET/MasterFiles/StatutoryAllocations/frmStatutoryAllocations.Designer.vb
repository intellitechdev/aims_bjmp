<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStatutoryAllocations
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStatutoryAllocations))
        Me.panelTool = New System.Windows.Forms.Panel
        Me.btnDone = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.grdStatutoryAlloc = New System.Windows.Forms.DataGridView
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.contextNew = New System.Windows.Forms.ToolStripMenuItem
        Me.contextEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.contextDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripNew = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripEdit = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.SystemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.panelTool.SuspendLayout()
        CType(Me.grdStatutoryAlloc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelTool
        '
        Me.panelTool.Controls.Add(Me.btnDone)
        Me.panelTool.Controls.Add(Me.btnCancel)
        Me.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelTool.Location = New System.Drawing.Point(0, 272)
        Me.panelTool.Name = "panelTool"
        Me.panelTool.Size = New System.Drawing.Size(527, 44)
        Me.panelTool.TabIndex = 0
        '
        'btnDone
        '
        Me.btnDone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDone.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnDone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDone.Location = New System.Drawing.Point(365, 6)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(75, 30)
        Me.btnDone.TabIndex = 2
        Me.btnDone.Text = "Done"
        Me.btnDone.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(446, 6)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grdStatutoryAlloc
        '
        Me.grdStatutoryAlloc.AllowUserToAddRows = False
        Me.grdStatutoryAlloc.AllowUserToDeleteRows = False
        Me.grdStatutoryAlloc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.grdStatutoryAlloc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdStatutoryAlloc.ContextMenuStrip = Me.ContextMenuStrip1
        Me.grdStatutoryAlloc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdStatutoryAlloc.Location = New System.Drawing.Point(0, 24)
        Me.grdStatutoryAlloc.Name = "grdStatutoryAlloc"
        Me.grdStatutoryAlloc.ReadOnly = True
        Me.grdStatutoryAlloc.RowHeadersVisible = False
        Me.grdStatutoryAlloc.Size = New System.Drawing.Size(527, 248)
        Me.grdStatutoryAlloc.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.contextNew, Me.contextEdit, Me.contextDelete})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(165, 70)
        '
        'contextNew
        '
        Me.contextNew.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.contextNew.Name = "contextNew"
        Me.contextNew.Size = New System.Drawing.Size(164, 22)
        Me.contextNew.Text = "New Allocation"
        '
        'contextEdit
        '
        Me.contextEdit.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.contextEdit.Name = "contextEdit"
        Me.contextEdit.Size = New System.Drawing.Size(164, 22)
        Me.contextEdit.Text = "Edit Allocation"
        '
        'contextDelete
        '
        Me.contextDelete.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.contextDelete.Name = "contextDelete"
        Me.contextDelete.Size = New System.Drawing.Size(164, 22)
        Me.contextDelete.Text = "Delete Allocation"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.SystemToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(527, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripNew, Me.toolStripEdit, Me.toolStripDelete})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'toolStripNew
        '
        Me.toolStripNew.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.toolStripNew.Name = "toolStripNew"
        Me.toolStripNew.Size = New System.Drawing.Size(164, 22)
        Me.toolStripNew.Text = "New Allocation"
        '
        'toolStripEdit
        '
        Me.toolStripEdit.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.toolStripEdit.Name = "toolStripEdit"
        Me.toolStripEdit.Size = New System.Drawing.Size(164, 22)
        Me.toolStripEdit.Text = "Edit Allocation"
        '
        'toolStripDelete
        '
        Me.toolStripDelete.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.toolStripDelete.Name = "toolStripDelete"
        Me.toolStripDelete.Size = New System.Drawing.Size(164, 22)
        Me.toolStripDelete.Text = "Delete Allocation"
        '
        'SystemToolStripMenuItem
        '
        Me.SystemToolStripMenuItem.Name = "SystemToolStripMenuItem"
        Me.SystemToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.SystemToolStripMenuItem.Text = "Exit"
        '
        'frmStatutoryAllocations
        '
        Me.AcceptButton = Me.btnDone
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(527, 316)
        Me.Controls.Add(Me.grdStatutoryAlloc)
        Me.Controls.Add(Me.panelTool)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(483, 340)
        Me.Name = "frmStatutoryAllocations"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Statutory Allocations Masterfile"
        Me.panelTool.ResumeLayout(False)
        CType(Me.grdStatutoryAlloc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panelTool As System.Windows.Forms.Panel
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents grdStatutoryAlloc As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SystemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents contextNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents contextEdit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents contextDelete As System.Windows.Forms.ToolStripMenuItem
End Class
