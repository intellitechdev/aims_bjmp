Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmCustOpeningBalance
    Private gCon As New Clsappconfiguration

    Private Sub frmCustOpeningBalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtBalance.Text = Format(CDec(LoadBalance()), "##,##0.00")
    End Sub

    Private Function LoadBalance() As Decimal
        Dim xResult As Decimal = 0.0
        Dim sSQLCmd As String

        Try
            sSQLCmd = "select fdBalance from dbo.mCustomer00Master where fxKeyCustomer='" & gKeyCustomer & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    xResult = rd.Item(0)
                End While
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return xResult
    End Function

    Private Sub savenewbalance()
        Try
            Dim sSQLCmd As String = "UPDATE mCustomer00Master SET fdBalance ='" & CDec(txtBalance.Text) & "' where fxKeyCustomer='" & gKeyCustomer & "' "
            SqlHelper.ExecuteScalar(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call savenewbalance()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class