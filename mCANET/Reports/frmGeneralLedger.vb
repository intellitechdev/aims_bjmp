Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports CrystalDecisions.Shared

Public Class frmGeneralLedger

    Private gCon As New Clsappconfiguration
    Private rptSummary As New ReportDocument

    Private accountID As String
    Public Property GetAccountID() As String
        Get
            Return accountID
        End Get
        Set(ByVal value As String)
            accountID = value
        End Set
    End Property

    Private dateFrom As Date
    Public Property GetdateFrom() As Date
        Get
            Return dateFrom
        End Get
        Set(ByVal value As Date)
            dateFrom = value
        End Set
    End Property

    Private dateTo As Date
    Public Property GetdateTo() As Date
        Get
            Return dateTo
        End Get
        Set(ByVal value As Date)
            dateTo = value
        End Set
    End Property

    Private reference As String
    Public Property GetReference() As String
        Get
            Return reference
        End Get
        Set(ByVal value As String)
            reference = value
        End Set
    End Property

    Private subsidiary As String
    Public Property GetSubsidiary() As String
        Get
            Return subsidiary
        End Get
        Set(ByVal value As String)
            subsidiary = value
        End Set
    End Property

    Private Sub frmGeneralLedger_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptSummary.Close()
    End Sub

    Private Sub frmGeneralLedger_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitializeDataOnForm()
    End Sub

    Private Sub bgwLoadReports_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReports.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReports_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReports.RunWorkerCompleted
        loadingPic.Visible = False
        Me.crvGeneralLedger.ReportSource = rptSummary
    End Sub

    Private Sub LoadBlankReport()
        Dim reportPath As String = Application.StartupPath & "\Accounting Reports\blankReport.rpt"
        rptSummary.Load(reportPath)
        rptSummary.Refresh()
        crvGeneralLedger.ReportSource = rptSummary
    End Sub

    Private Sub LoadReport()
        Dim reportPath As String = Application.StartupPath & "\Accounting Reports\GeneralLedger.rpt"
        rptSummary.Load(reportPath)
        Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, Clsappconfiguration.GetDecryptedData(gCon.Password))
        rptSummary.Refresh()

        rptSummary.SetParameterValue("@dateTo", GetdateTo().Date)
        rptSummary.SetParameterValue("@dateFrom", GetdateFrom().Date)
        rptSummary.SetParameterValue("@companyID", gCompanyID())
        rptSummary.SetParameterValue("@acntID", IIf(GetAccountID() = "", Nothing, GetAccountID()))
        rptSummary.SetParameterValue("@documentNo", IIf(GetReference() = "", Nothing, GetReference()))
        rptSummary.SetParameterValue("@subsidiary", IIf(GetSubsidiary() = "", Nothing, GetSubsidiary()))
    End Sub

    Private Sub InitializeDataOnForm()
        InitializedDates()
        LoadBlankReport()
    End Sub

    Private Sub InitializedDates()
        GetdateTo() = Date.Now.Date
        GetdateFrom() = Date.Now.Date
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        If bgwLoadReports.IsBusy() = False Then
            loadingPic.Visible = True
            bgwLoadReports.RunWorkerAsync()
        Else
            loadingPic.Visible = False
            MessageBox.Show("User Error! The Report is currently loading.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnApplyFilters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplyFilters.Click
        With frmGeneralLedgerFilters

            .dteFrom.Value = GetdateFrom()
            .dteTo.Value = GetdateTo()
            .cboAccounts.SelectedValue = IIf(GetAccountID() = Nothing, "", GetAccountID())
            .cboSubsidiary.SelectedValue = IIf(GetSubsidiary() = Nothing, "", GetSubsidiary())
            .txtReference.Text = GetReference()

            .ShowDialog()
        End With

    End Sub
End Class