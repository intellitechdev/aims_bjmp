Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmInvoiceList
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private Sub selection(ByVal sdate As Date, ByVal sdate2 As Date)
        Dim pxSalesAccount As String = ""

        rptsummary.Load(Application.StartupPath & "\Accounting Reports\CustomerTransactionLists.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        '####### changes ##########
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@fdDateFrom", dteFrom.Value)
        rptsummary.SetParameterValue("@fdDateTo", dteTo.Value)
        If LblType.Text = "" Then
            rptsummary.SetParameterValue("@Type", DBNull.Value)
        Else
            rptsummary.SetParameterValue("@Type", LblType.Text)
        End If
        rptsummary.SetParameterValue("@OrderBy", CboOrderBy.Text)
        rptsummary.SetParameterValue("@fxCompanyName", gCompanyName)
        rptsummary.SetParameterValue("@CompanyAddress", getCompanyAddress())
        rptsummary.SetParameterValue("@transactionkind", LblTransactionKind.Text)
        rptsummary.SetParameterValue("@TransactionCoverage", gtransactionsCoveredDate)

        '##########################
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call selection(Microsoft.VisualBasic.FormatDateTime(dteFrom.Value, DateFormat.ShortDate), _
                        Microsoft.VisualBasic.FormatDateTime(dteTo.Value, DateFormat.ShortDate))
    End Sub

    Private Sub frmIncomeStatement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmInvoiceList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CboOrderBy.SelectedItem = "amount"
    End Sub

End Class