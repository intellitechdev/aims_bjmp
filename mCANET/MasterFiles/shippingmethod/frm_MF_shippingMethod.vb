Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frm_MF_shippingmethod
    Dim sKeyShippingMethod As String
    Private gcon As New Clsappconfiguration

    Public Property KeyShippingMethod()
        Get
            Return sKeyShippingMethod
        End Get
        Set(ByVal value)
            sKeyShippingMethod = value
        End Set
    End Property
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_MF_shippingmethod_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        ShippingMethod_Insert()

        With frm_cust_CreateInvoice
            m_loadShippingMethod(.cboViaName, .cboViaID)
            .cboViaName.Refresh()
            .cboViaID.Refresh()
        End With

        Me.Close()
    End Sub
    Private Sub ShippingMethod_Insert()
        Dim sSQL_CommandText As String = "usp_m_ShippingMethod_Insert "
        sSQL_CommandText &= "@fxKeyShippingMethod='" & sKeyShippingMethod & "'"
        sSQL_CommandText &= ",@fcShippingMethodName='" & txtShippingMethod.Text & "'"

        If chkStatus.Checked Then
            sSQL_CommandText &= ",@fbActive='0'"
        Else
            sSQL_CommandText &= ",@fbActive='1'"
        End If

        sSQL_CommandText &= ",@fxKeyCompany='" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, Data.CommandType.Text, sSQL_CommandText)
            MessageBox.Show("Shipping Method successfully updated", "Add Shipping Method ")
        Catch
            MessageBox.Show(Err.Description, "Add Shipping Method")
        End Try
    End Sub
End Class