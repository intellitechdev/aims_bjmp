Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Module modInvoice

    Private gCon As New Clsappconfiguration

    Private Sub navigateprevious(ByVal psInvoiceNo As Integer, ByVal psCustomer As String, _
                                 ByRef txtInvoice As TextBox, _
                                 ByRef cboCustomerName As ComboBox, _
                                 ByRef cboTaxCode As ComboBox, _
                                 ByRef txtBilling As TextBox, _
                                 ByRef txtPOno As TextBox, _
                                 ByRef cboTerms As ComboBox, _
                                 ByRef cboRep As ComboBox, _
                                 ByRef dteShip As DateTimePicker, _
                                 ByRef txtMessage As TextBox, _
                                 ByRef txtMemo As TextBox, _
                                 ByRef cboVia As ComboBox, _
                                 ByRef txtFOB As TextBox, _
                                 ByRef cboTaxName As ComboBox, _
                                 ByRef lblTaxAmt As Label, _
                                 ByRef cboSD As ComboBox, _
                                 ByRef lblSalesDiscount As Label, _
                                 ByRef cboRD As ComboBox, _
                                 ByRef lblRetailDiscount As Label, _
                                 ByRef lblPayments As Label, _
                                 ByRef lblBalance As Label, _
                                 ByRef sForm As Form)
        Dim txtInvoices As New TextBox

        Dim pdInvoice As Integer
        Dim psCustomerName As String
        Dim pdDateTrans As Date
        Dim pdDateShip As Date
        Dim pdDateDue As Date
        Dim pcBillto As String
        Dim pcShipto As String = ""
        Dim pcShipAdd As String = ""
        Dim pcPONo As String
        Dim pcFOB As String
        Dim psCustMessage As String
        Dim pcMemo As String = ""
        Dim psRepName As String = ""
        Dim psTerms As String = ""
        Dim psTax As String = ""

        Dim CustomerID As String = getCustomerID(psCustomer)
        Dim sSQLCmd As String = "usp_t_tInvoice_navigate "
        sSQLCmd &= " @fcInvoiceno='" & psInvoiceNo & "' "
        sSQLCmd &= ",@fxKeyCustomer='" & CustomerID & "' "
        sSQLCmd &= ",@fxKeyCompany=' " & gCompanyID() & "' "

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
            While rd.Read
                With rd
                    pdInvoice = .Item(0).ToString
                    psCustomerName = getCustomerName(.Item(1).ToString)
                    pdDateTrans = .Item(2).ToString
                    pdDateShip = .Item(3).ToString
                    pdDateDue = .Item(4).ToString
                    pcBillto = .Item(5).ToString
                    If .Item(6).ToString <> "" Then
                        pcShipto = getAddressName(.Item(6).ToString)
                    End If
                    If .Item(7).ToString <> "" Then
                        pcShipAdd = .Item(7).ToString
                    End If
                    pcPONo = .Item(8).ToString
                    pcFOB = .Item(9).ToString
                    psCustMessage = .Item(10).ToString
                    pcMemo = .Item(11).ToString
                    If .Item(12).ToString <> "" Then
                        psRepName = getRepName(.Item(12).ToString)
                    End If
                    If .Item(13).ToString <> "" Then
                        psTerms = getTermName(.Item(13).ToString)
                    End If
                    If .Item(14).ToString <> "" Then
                        psTax = getTaxName(.Item(14).ToString)
                    End If
                End With
            End While
        End Using

        'fxKeyTax, 
        'fxKeySalesTaxCode, fdTaxAmount, 
        'fdTotal, fdBalance, fbPrint, 
        'fbEmail, fdPayment, fdDelivryDate, 
        'fcSDCode, fdSDAmt, fcRDCode, fdRDAmt
    End Sub


End Module
