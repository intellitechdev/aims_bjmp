Public Class frm_acc_makedeposit

#Region "Declarations"

    Private gTrans As New clsTransactionFunctions

    Private psDepesotID As String

    Dim checkDate As New clsRestrictedDate

#End Region

#Region "Events"

    Private Sub frm_acc_makedeposit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_lstCustomerPayment()
        Call m_DisplayBankAccounts(cboDepositTo)
        Call m_DisplayAssetAccounts(cboAssetAct)
        txtMemo.Text = "Deposit"
        IsDateRestricted()
    End Sub

    Private Sub ts_dep_depositSlip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_dep_depositSlip.Click
        frm_dep_depositslip.ShowDialog()
    End Sub

    Private Sub btn_dep_saveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveNew.Click
        If frmClosingEntries.IsPeriodClosed(dtDeposit.Value.Date, btn_dep_saveNew) = False Then
            Call save_deposit()
            Call save_cashback()
            Call updateaccountbalance()
            Call update_accountbalancebycashback()
            Call clearform()
        End If
    End Sub

    Private Sub btn_dep_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveClose.Click
        If frmClosingEntries.IsPeriodClosed(dtDeposit.Value.Date, btn_dep_saveClose) = False Then
            Call save_deposit()
            Call save_cashback()
            Call updateaccountbalance()
            Call update_accountbalancebycashback()
            Me.Close()
        End If
    End Sub

    Private Sub gDeposits_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gDeposits.CellValidated
        With Me.gDeposits
            If .CurrentCellAddress.X = 1 Then
                If .Item(1, .CurrentRow.Index).Value = cboDepositTo.SelectedItem Then
                    MessageBox.Show("The account you entered is the account you are depositing to.", "CLICKSOFTWARE@Accounting :" + Me.Text)
                    .Item(1, .CurrentRow.Index).Value = ""
                End If
            ElseIf .CurrentCellAddress.X = 5 Then
                If .Item(5, .CurrentRow.Index).Value <> "" Then
                    .Item(5, .CurrentRow.Index).Value = Format(CDec(.Item(5, .CurrentRow.Index).Value), "#,##0.00")
                End If
            End If
        End With
        Call calculate_subtotaldeposit()
    End Sub

    Private Sub txtCBAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCBAmt.Validating
        lbl_dep_totaldeposit.Text = Format(CDec(CDec(lbl_dep_subtotaldeposit.Text) - CDec(txtCBAmt.Text)), "#,##0.00")
    End Sub

    Private Sub lbl_dep_subtotaldeposit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lbl_dep_totaldeposit.Text = Format(CDec(CDec(lbl_dep_subtotaldeposit.Text) - CDec(txtCBAmt.Text)), "#,##0.00")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboAssetAct_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssetAct.SelectedIndexChanged
        If cboAssetAct.SelectedItem = cboDepositTo.SelectedItem Then
            MessageBox.Show("The cash back account cannot be the same as the account you are depositing to.", "CLICKSOFTWARE@Accounting :" + Me.Text)
        End If
    End Sub

    Private Sub btn_dep_Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_Clear.Click
        Call clearform()
    End Sub

#End Region

#Region "Functions/Procedures"

    Private Sub load_lstCustomerPayment()
        With gDeposits
            .Columns.Clear()
            Dim cboNames As New DataGridViewComboBoxColumn
            Dim cboAccounts As New DataGridViewComboBoxColumn
            Dim txtMemo As New DataGridViewTextBoxColumn
            Dim txtchkno As New DataGridViewTextBoxColumn
            Dim cboPmtMeth As New DataGridViewComboBoxColumn
            Dim txtAmt As New DataGridViewTextBoxColumn

            Call m_DisplayNames(cboNames)
            Call m_DisplayAccounts(cboAccounts)

            With cboNames
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 200
                .HeaderText = "Received from"
                .Name = "cNames"
                .Width = 120
            End With

            With cboAccounts
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 200
                .HeaderText = "From Account"
                .Name = "cAccounts"
                .Width = 120
            End With

            With txtMemo
                .HeaderText = "Memo"
                .Name = "cMemo"
                .Width = 65
            End With

            With txtchkno
                .HeaderText = "Chk No."
                .Name = "cCheckNo"
                .Width = 100
            End With

            With cboPmtMeth
                .Items.Add("American Express")
                .Items.Add("Cash")
                .Items.Add("Check")
                .Items.Add("Master Card")
                .Items.Add("Visa")
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 150
                .HeaderText = "Pmt Meth."
                .Name = "cPayMeth"
                .Width = 130
            End With

            With txtAmt
                .HeaderText = "Amount"
                .Name = "cAmount"
                .Width = 100
            End With

            .Columns.Add(cboNames)
            .Columns.Add(cboAccounts)
            .Columns.Add(txtMemo)
            .Columns.Add(txtchkno)
            .Columns.Add(cboPmtMeth)
            .Columns.Add(txtAmt)
        End With
    End Sub

    Private Sub calculate_subtotaldeposit()
        Dim xVal As Integer = 0
        Dim xRow As Integer
        With Me.gDeposits
            For xRow = 0 To .Rows.Count - 1
                If .Item(5, xRow).Value <> "" Then
                    xVal += .Item(5, xRow).Value
                End If
            Next
            lbl_dep_subtotaldeposit.Text = Format(CDec(xVal), "#,##0.00")
        End With
    End Sub

    Private Function getnameofaccount(ByVal psAccount As String) As String
        Dim sVal As String
        Dim sAccount() As String
        sAccount = Split(psAccount, "-")
        sVal = sAccount(0)
        Return sVal
    End Function

    Private Sub save_deposit()
        Dim sDepositTo() As String
        Dim sDepID As String

        psDepesotID = Guid.NewGuid.ToString

        sDepositTo = Split(cboDepositTo.SelectedItem, "-")
        sDepID = m_GetAccountID(sDepositTo(0))

        Dim xRow As Integer
        Dim sRcvdFrm() As String
        Dim sAccount() As String

        With Me.gDeposits
            For xRow = 0 To .Rows.Count - 1
                Dim sName As String = ""
                Dim sAcnt As String = ""
                Dim sMemo As String = ""
                Dim sChkno As String = ""
                Dim sPmtMeth As String = ""
                Dim sAmount As Long = 0

                If gTrans.gmkDefaultValues(xRow, 0, gDeposits) <> Nothing Then
                    sName = .Rows(xRow).Cells("cNames").Value
                    sRcvdFrm = Split(sName, "-")
                    sName = m_GetAccountID(sRcvdFrm(0))
                End If
                If gTrans.gmkDefaultValues(xRow, 1, gDeposits) <> Nothing Then
                    sAcnt = .Rows(xRow).Cells("cAccounts").Value
                    sAccount = Split(sAcnt, "-")
                    sAcnt = m_GetAccountID(sAccount(0))
                End If
                If gTrans.gmkDefaultValues(xRow, 2, gDeposits) <> Nothing Then
                    sMemo = .Rows(xRow).Cells("cMemo").Value
                End If
                If gTrans.gmkDefaultValues(xRow, 3, gDeposits) <> Nothing Then
                    sChkno = .Rows(xRow).Cells("cCheckNo").Value
                End If
                If gTrans.gmkDefaultValues(xRow, 4, gDeposits) <> Nothing Then
                    sPmtMeth = .Rows(xRow).Cells("cPayMeth").Value
                End If
                If gTrans.gmkDefaultValues(xRow, 5, gDeposits) <> Nothing Then
                    sAmount = .Rows(xRow).Cells("cAmount").Value
                End If
                If sName <> "" Or sAcnt <> "" Or sPmtMeth <> "" Or sAmount = 0 Then
                    Call gTrans.gSaveDepositDetails(psDepesotID, sDepID, Microsoft.VisualBasic.FormatDateTime(dtDeposit.Value, DateFormat.ShortDate), _
                                                                             txtMemo.Text, sName, sAcnt, sMemo, sChkno, _
                                                                              sPmtMeth, sAmount, Me)
                Else
                    MessageBox.Show("Some of the fields are required to have a value, please re-check the fields.", "CLICKSOFTWARE@Accounting:" + Me.Text)
                End If
            Next
        End With
    End Sub

    Private Sub updateaccountbalance()
        Dim xRow As Integer
        With Me.gDeposits
            For xRow = 0 To .Rows.Count - 1
                Dim sAcnt() As String
                Dim sAccount As String = ""
                Dim sBalAmount As Long = 0
                Dim sAmount As Long = 0
                Dim sAmtBal As Long = 0

                If gTrans.gmkDefaultValues(xRow, 1, gDeposits) <> Nothing Then
                    sAccount = .Rows(xRow).Cells("cAccounts").Value
                    sAcnt = Split(sAccount, "-")
                    sAccount = m_GetAccountID(sAcnt(0))
                    sBalAmount = m_GetAccountBalanceAmount(sAccount)
                End If
                If gTrans.gmkDefaultValues(xRow, 5, gDeposits) <> Nothing Then
                    sAmount = .Rows(xRow).Cells("cAmount").Value
                End If

                If gTrans.gmkDefaultValues(xRow, 1, gDeposits) <> Nothing Then
                    sAmtBal = CDec(sBalAmount) - CDec(sAmount)
                    Call gTrans.gUpdateAccountBalanceAmount(sAccount, sAmtBal, Me)
                End If
            Next
        End With
    End Sub

    Private Sub update_accountbalancebycashback()
        If cboAssetAct.Text <> "" Then
            Dim sAcnt() As String
            Dim sAccount As String
            Dim sAmount, sAmtBal, sBalAmount As Long
            sAcnt = Split(cboAssetAct.SelectedItem, "-")
            sAccount = m_GetAccountID(sAcnt(0))
            sBalAmount = m_GetAccountBalanceAmount(sAccount)
            sAmount = txtCBAmt.Text
            sAmtBal = CDec(sBalAmount) + CDec(sAmount)
            Call gTrans.gUpdateAccountBalanceAmount(sAccount, sAmtBal, Me)
        End If
    End Sub

    Private Sub save_cashback()
        If cboAssetAct.Text <> "" Then
            Dim sAcnt() As String
            Dim sAccount As String
            sAcnt = Split(cboAssetAct.SelectedItem, "-")
            sAccount = m_GetAccountID(sAcnt(0))
            gTrans.gSaveCashBack(psDepesotID, sAccount, txtCBMemo.Text, txtCBAmt.Text, Me)
        End If
        psDepesotID = ""
    End Sub

    Private Sub clearform()
        cboDepositTo.Text = ""
        dtDeposit.Value = Now.Date
        lbl_dep_subtotaldeposit.Text = "0.00"
        lbl_dep_totaldeposit.Text = "0.00"
        txtCBAmt.Text = "0.00"
        txtCBMemo.Text = ""
        cboAssetAct.Text = ""
        With Me.gDeposits
            .Rows.Clear()
            .Columns.Clear()
        End With
        Call load_lstCustomerPayment()
    End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dtDeposit.Value) Then
            btn_dep_saveClose.Enabled = False 'true if date is NOT within range of period restricted
            btn_dep_saveNew.Enabled = False
        Else
            btn_dep_saveClose.Enabled = True 'false if date is within range of period restricted
            btn_dep_saveNew.Enabled = True
        End If
    End Sub

#End Region

    Private Sub dtDeposit_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtDeposit.ValueChanged
        IsDateRestricted()
    End Sub

    Private Sub ts_dep_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_dep_print.Click

    End Sub

    Private Sub ts_dep_depositSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_dep_depositSummary.Click

    End Sub
End Class