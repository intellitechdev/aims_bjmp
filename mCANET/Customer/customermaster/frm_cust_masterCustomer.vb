Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_cust_masterCustomer
    Private gCon As New Clsappconfiguration
    Private sKeyCustomer As String
    Private sKeyAddress As String
    Private sMode As String = ""
    Private sKey As String
    Dim sShow As String
    Private dteDateFrom As String
    Private dteDateTo As String
    Private dteFrom As String
    Private dteTo As String

    Const grd3_Key = 0
    Const grd3_Tbl = 1
    Const grd3_Col = 2
    Const grd3_Type = 3
    Const grd3_No = 4
    Const grd3_Date = 5
    Const grd3_Acct = 6
    Const grd3_Amt = 7

    Private Sub frm_vend_masterCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        With grdTransaction
            .Rows.Clear()
            .Rows.Add("Sales Orders")
            .Rows.Add("Invoices")
            ' .Rows.Add("Statement Charges")
            .Rows.Add("Sales Receipts")
            .Rows.Add("Received Payments")
            .Rows.Add("Credit Memos")
            .Rows.Add("Debit Memos")
        End With
        secPanelSettings()
        cboViewSettings()
        cboShowSettings()
        m_customerList(grdCustomer1, cboView.SelectedItem)
        If grdCustomer1.Rows.Count > 1 Then
            sKeyCustomer = grdCustomer1.Item(0, 0).Value.ToString
            gKeyCustomer = sKeyCustomer
            loadCustomerInfo()
        End If

        loadGrdCustomer2("Sales Orders")
        cboFilterBy_Load("Sales Orders", cboFilterBy)
        m_loaddteRange(dteRange)
        m_loaddteRange(dteRange2)
    End Sub

    Private Sub cboViewSettings()
        With cboView.Items
            .Add("All Customers")
            .Add("Active Customers")
            .Add("With Open Balances")
        End With

        If cboView.SelectedIndex < 0 Then
            cboView.SelectedIndex = 0
        End If
    End Sub

    Private Sub cboShowSettings()
        With cboShow.Items
            .Add("All Transactions")
            '.Add("----------------------")
            '.Add("All Sales Transactions")
            '.Add("Balance Details")
            '.Add("Payments & Credits")
            '.Add("----------------------")
            .Add("Sales Orders")
            .Add("Invoices")
            '.Add("Statement Charges")
            .Add("Sales Receipts")
            .Add("Received Payments")
            .Add("Credit Memos")
            .Add("Debit Memos")
            .Add("Journal Entries")
        End With
        If cboShow.SelectedIndex < 0 Then
            cboShow.SelectedIndex = 0
        End If
    End Sub
  
    Private Sub cboView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboView.SelectedIndexChanged
        m_customerList(grdCustomer1, cboView.SelectedItem)
    End Sub

    Private Sub tabCustomernTransaction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabCustomernTransaction.SelectedIndexChanged
        secPanelSettings()
    End Sub

    Private Sub secPanelSettings()
        If tabCustomernTransaction.SelectedIndex = 0 Then
            PanelCustomerChild.Visible = True
            sizeSettings()
        Else
            PanelCustomerChild.Visible = False
        End If
    End Sub

    Private Sub frm_cust_masterCustomer_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp

    End Sub
    Private Sub frm_vend_masterCustomer_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        sizeSettings()
    End Sub

    Private Sub SplitContainer1_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SplitContainer1.SizeChanged
        sizeSettings()
    End Sub
    Private Sub spltCustomerInfo_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SplitContainer1.SizeChanged
        sizeSettings()
    End Sub

    Private Sub sizeSettings()
        tabCustomernTransaction.Size = New Size(SplitContainer1.Panel1.Size)
        'grdCustomer1.Size = New Size(tabCustomernTransaction.Size)
        grdTransaction.Size = New Size(TabPage2.Size)

        PanelCustomerChild.Size = New Size(SplitContainer1.Panel2.Size)
        PanelCustomerChild.Location = New Point(-1, -1)

        grdCustomer2.Size = New Size(SplitContainer1.Panel2.Width - 10, SplitContainer1.Panel2.Height - 40)
        grdCustomer3.Size = New Size(spltCustomerInfo.Width - 10, spltCustomerInfo.Height - 385)
    End Sub

    Private Sub btnEditCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditCustomer.Click
        frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
        frm_cust_masterCustomerAddEdit.KeyAddress = sKeyAddress
        frm_cust_masterCustomerAddEdit.KeyCustomer = sKeyCustomer
        frm_cust_masterCustomerAddEdit.Text = "Edit Customer"
        frm_cust_masterCustomerAddEdit.Show()
    End Sub

    Private Sub btnEditNotes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditNotes.Click
        frm_cust_masterCustomerNote.KeyCustomer = sKeyCustomer
        frm_cust_masterCustomerNote.Show()
    End Sub

    Private Sub ts_NewCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_NewCustomer.Click
        frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
        frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
        frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
        frm_cust_masterCustomerAddEdit.IsNew = True
        frm_cust_masterCustomerAddEdit.Text = "Add Customer"
        frm_cust_masterCustomerAddEdit.Show()
    End Sub

    Private Sub ts_salesOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_salesOrder.Click
        'frm_cust_CreateSalesOrder.MdiParent = Me.MdiParent
        'frm_cust_CreateSalesOrder.Show()
        Dim x As New frm_cust_CreateSalesOrder
        x.Show()
    End Sub

    Private Sub ts_Invoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Invoice.Click
        'frm_cust_CreateInvoice.MdiParent = Me.MdiParent
        'frm_cust_CreateInvoice.Show()
        Dim x As New frm_cust_CreateInvoice
        x.Show()
    End Sub

    Private Sub ts_salesReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_salesReceipt.Click
        'frm_cust_CreateSalesReceipt.MdiParent = Me.MdiParent
        'frm_cust_CreateSalesReceipt.Show()
        Dim x As New frm_cust_CreateSalesReceipt
        x.Show()
    End Sub

    Private Sub ts_Charges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Charges.Click
        frm_cust_CreateStatementCharges.MdiParent = Me.MdiParent
        frm_cust_CreateStatementCharges.Show()
    End Sub

    Private Sub ts_rcvPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_rcvPayment.Click
        'frm_cust_ReceivePayment.MdiParent = Me.MdiParent
        'frm_cust_ReceivePayment.Show()
        Dim x As New frm_cust_ReceivePayment
        x.Show()
    End Sub

    Private Sub ts_creditMemo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_creditMemo.Click
        'frm_cust_CreateCreditMemos.MdiParent = Me.MdiParent
        'frm_cust_CreateCreditMemos.Show()
        Dim x As New frm_cust_CreateCreditMemos
        x.Show()
    End Sub

    Private Sub ts_Info_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Info.Click
        frm_cust_masterCustomerAddEdit.MdiParent = Me.MdiParent
        frm_cust_masterCustomerAddEdit.Text = "Edit Customer"
        frm_cust_masterCustomerAddEdit.Show()
    End Sub

    Private Sub SplitContainer1_SplitterMoved(ByVal sender As Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles SplitContainer1.SplitterMoved
        sizeSettings()
    End Sub

    Private Sub spltCustomerInfo_SplitterMoved(ByVal sender As Object, ByVal e As System.Windows.Forms.SplitterEventArgs) Handles spltCustomerInfo.SplitterMoved
        sizeSettings()
    End Sub

    Public Sub loadCustomerInfo()

        Dim sSQLCmd As String = "usp_m_customer_informationList "
        sSQLCmd &= "@fxKeyCustomer = '" & sKeyCustomer & "'"
        Try

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtCustomerName.Text = rd.Item("fcCustomerName")
                    txtCustomerType.Text = rd.Item("fcTypeName")
                    txtCompName.Text = rd.Item("fcCompanyName")
                    txtBillingAdd.Text = rd.Item("fcBillingAddress")
                    txtNotes.Text = rd.Item("fcNote")
                    txtContact.Text = rd.Item("fcContactPerson1")
                    txtPhone.Text = rd.Item("fcTelNo1")
                    txtPhone2.Text = rd.Item("fcTelNo2")
                    txtFax.Text = rd.Item("fcFaxNo")
                    txtEmail.Text = rd.Item("fcEmail")
                    txtTerms.Text = rd.Item("fcTermsName")
                    txtPriceLvl.Text = "0" ' to be done
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Customer Info.")
        End Try
    End Sub

    Private Sub grdCustomer1_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdCustomer1.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdCustomer1.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub
    Private Sub grdCustomer1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer1.Click
        ShowCustomer()
        showTransaction()
    End Sub

    Private Sub ShowCustomer()
        If Me.grdCustomer1.SelectedRows.Count > 0 Then
            sKeyCustomer = grdCustomer1.CurrentRow.Cells(0).Value.ToString
            gKeyCustomer = sKeyCustomer
            If Not grdCustomer1.CurrentRow.Cells(1).Value.ToString Is Nothing Then
                sKeyAddress = grdCustomer1.CurrentRow.Cells(1).Value.ToString
            Else
                sKeyAddress = Guid.NewGuid.ToString
            End If
            loadCustomerInfo()
        End If
    End Sub
    Private Sub grdCustomer1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer1.DoubleClick
        If Me.grdCustomer1.SelectedRows.Count > 0 AndAlso _
                                  Not Me.grdCustomer1.SelectedRows(0).Index = _
                                  Me.grdCustomer1.Rows.Count - 1 Then
            sKeyCustomer = grdCustomer1.CurrentRow.Cells(0).Value.ToString
            sKeyAddress = grdCustomer1.CurrentRow.Cells(1).Value.ToString
            frm_cust_masterCustomerAddEdit.KeyAddress = sKeyAddress
            frm_cust_masterCustomerAddEdit.KeyCustomer = sKeyCustomer
            frm_cust_masterCustomerAddEdit.Text = "Edit Customer"
            frm_cust_masterCustomerAddEdit.Show()
        End If
    End Sub

    Private Sub grdTransaction_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdTransaction.CellClick
        If Not Me.grdTransaction.CurrentRow.Index = _
               Me.grdTransaction.Rows.Count Then
            sMode = grdTransaction.CurrentRow.Cells(0).Value
            loadGrdCustomer2(sMode)
            cboFilterBy_Load(sMode, cboFilterBy)
            m_loaddteRange(dteRange)
            m_loaddteRange(dteRange2)
        End If
    End Sub

    Public Sub loadGrdCustomer2(ByVal sModes As String)

        Dim dtTableTransaction As DataTable

        Dim col1 As New DataGridViewTextBoxColumn
        Dim col2 As New DataGridViewTextBoxColumn
        Dim col3 As New DataGridViewTextBoxColumn
        Dim col4 As New DataGridViewTextBoxColumn
        Dim col5 As New DataGridViewTextBoxColumn
        Dim col6 As New DataGridViewTextBoxColumn
        Dim col7 As New DataGridViewTextBoxColumn
        Dim col8 As New DataGridViewTextBoxColumn
        Dim col9 As New DataGridViewCheckBoxColumn
        Dim col10 As New DataGridViewCheckBoxColumn
        Select Case sModes
            Case "Sales Orders"
                dtTableTransaction = m_GetSO("", dteFrom, dteTo).Tables(0)

                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeySO"
                col1.HeaderText = "KeySO"

                col2.DataPropertyName = "fcCustomerName"
                col2.HeaderText = "Customer"
                col3.DataPropertyName = "fcSONo"
                col3.HeaderText = "Ref/OR#."
                col4.DataPropertyName = "fdDateTransact"
                col4.HeaderText = "Date"
                col5.DataPropertyName = "fdTotal"
                col5.HeaderText = "Amount"
                col6.DataPropertyName = "fdBalance"
                col6.HeaderText = "Open Balance"
                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    grdCustomer2.Columns(0).Visible = False
                    grdCustomer2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    grdCustomer2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With

            Case "Invoices"
                dtTableTransaction = m_GetInvoice("", dteFrom, dteTo).Tables(0)

                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeyInvoice"
                col1.HeaderText = "KeyInvoice"

                col2.DataPropertyName = "fcCustomerName"
                col2.HeaderText = "Customer"

                col3.DataPropertyName = "fcInvoiceNo"
                col3.HeaderText = "Invoice#"

                col4.DataPropertyName = "fdDateTransact"
                col4.HeaderText = "Date"

                col5.DataPropertyName = "fdDateDue"
                col5.HeaderText = "Due Date"

                col6.DataPropertyName = "fdDateAging"
                col6.HeaderText = "Aging"

                col7.DataPropertyName = "fdTotal"
                col7.HeaderText = "Amount"

                col8.DataPropertyName = "fdBalance"
                col8.HeaderText = "Open Balance"

                col9.DataPropertyName = "fbCancelled"
                col9.HeaderText = "Cancelled"
                col9.ReadOnly = True

                col10.DataPropertyName = "fbpaid"
                col10.ReadOnly = True
                col10.HeaderText = "Paid"
                col10.Visible = False

                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    .Add(col7)
                    .Add(col8)
                    .Add(col9)
                    .Add(col10)
                    grdCustomer2.Columns(0).Visible = False
                    grdCustomer2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(4).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    grdCustomer2.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With

            Case "Statement Charges"
                '      MessageBox.Show("Statement Charges")

            Case "Sales Receipts"
                dtTableTransaction = m_GetSR("", dteFrom, dteTo).Tables(0)

                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeySR"
                col1.HeaderText = "KeySR"
                col2.DataPropertyName = "fcCustomerName"
                col2.HeaderText = "Customer"
                col3.DataPropertyName = "fcSaleNo"
                col3.HeaderText = "Ref#"
                col4.DataPropertyName = "fdDateTransact"
                col4.HeaderText = "Date"
                col5.DataPropertyName = "fcPaymentMethodName"
                col5.HeaderText = "Payment Method"
                col6.DataPropertyName = "fdTotal"
                col6.HeaderText = "Amount"

                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                End With

                grdCustomer2.Columns(0).Visible = False
                grdCustomer2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                grdCustomer2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Case "Bills Payment"
                '    MessageBox.Show("Bills Payment")

            Case "Received Payments"
                dtTableTransaction = m_GetPayment("", dteFrom, dteTo).Tables(0)

                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeyPayment"
                col1.HeaderText = "KeyPayment"
                col2.DataPropertyName = "fcCustomerName"
                col2.HeaderText = "Customer"
                col3.DataPropertyName = "fcRefNo"
                col3.HeaderText = "Ref/OR#"
                col4.DataPropertyName = "fdDate"
                col4.HeaderText = "Date"
                'col5.DataPropertyName = "fcInvoiceNo"
                'col5.HeaderText = "Invoice #"
                col6.DataPropertyName = "fdPayment"
                col6.HeaderText = "Amount"
                col7.DataPropertyName = "fnUnappliedAmt"
                col7.HeaderText = "Unapplied Amt"

                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    '.Add(col5)
                    .Add(col6)
                    .Add(col7)
                    grdCustomer2.Columns(0).Visible = False
                    grdCustomer2.Columns(3).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    'grdCustomer2.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With
            Case "Credit Memos"
                dtTableTransaction = m_GetCreditMemos("", dteFrom, dteTo).Tables(0)
                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeyCredit"
                col1.HeaderText = "KeyCredit"
                col2.DataPropertyName = "fcName"
                col2.HeaderText = "Customer"
                col3.DataPropertyName = "fcCreditNo"
                col3.HeaderText = "Credit Memo #"
                col4.DataPropertyName = "fcRefNo"
                col4.HeaderText = "Reference #"
                col5.DataPropertyName = "fdDate"
                col5.HeaderText = "Date"
                col6.DataPropertyName = "fdAmt"
                col6.HeaderText = "Amount"

                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    grdCustomer2.Columns(0).Visible = False
                    grdCustomer2.Columns(4).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ' grdCustomer2.Columns("fdAmt").Format = "##,##0.00"
                    grdCustomer2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    grdCustomer2.ReadOnly = True
                End With
            Case "Debit Memos"
                dtTableTransaction = m_GetDebitMemos("", dteFrom, dteTo).Tables(0)
                grdCustomer2.DataSource = dtTableTransaction.DefaultView
                col1.DataPropertyName = "fxKeyDebit"
                col1.HeaderText = "KeyDebit"
                col2.DataPropertyName = "fcName"
                col2.HeaderText = "Name"
                col3.DataPropertyName = "fcDebitNo"
                col3.HeaderText = "Debit Memo #"
                col4.DataPropertyName = "fcMemo"
                col4.HeaderText = "Message"
                col5.DataPropertyName = "fdDate"
                col5.HeaderText = "Date"
                col6.DataPropertyName = "fdAmt"
                col6.HeaderText = "Amount"
                With grdCustomer2.Columns
                    .Clear()
                    .Add(col1)
                    .Add(col2)
                    .Add(col3)
                    .Add(col4)
                    .Add(col5)
                    .Add(col6)
                    grdCustomer2.Columns(0).Visible = False
                    grdCustomer2.Columns(4).DefaultCellStyle.Format = "MM/dd/yyyy"
                    grdCustomer2.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ' grdCustomer2.Columns(5).DefaultCellStyle.Format = "##,##0.00"
                    grdCustomer2.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End With
            Case Else
                Exit Sub
        End Select
    End Sub

    'Private Sub grdCustomer2_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdCustomer2.CellPainting
    '    Dim sf As New StringFormat
    '    sf.Alignment = StringAlignment.Center
    '    If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdCustomer2.Rows.Count Then
    '        e.PaintBackground(e.ClipBounds, True)
    '        e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
    '        e.Handled = True
    '    End If
    'End Sub

    Private Sub grdCustomer2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer2.DoubleClick
        If Me.grdCustomer2.SelectedCells.Count > 0 Then
            ' Dim sKey As String
            sKey = grdCustomer2.CurrentRow.Cells(0).Value.ToString
            Select Case sMode
                Case "Sales Orders"
                    frm_cust_CreateSalesOrder.KeySO = sKey
                    frm_cust_CreateSalesOrder.Text = "Edit Sales Order"
                    frm_cust_CreateSalesOrder.ShowDialog()
                Case "Invoices"
                    frm_cust_CreateInvoice.KeyInvoice = sKey
                    gInvoiceKey = sKey
                    If grdCustomer2.Item(9, grdCustomer2.CurrentRow.Index).Value = True Then
                        If grdCustomer2.Item(8, grdCustomer2.CurrentRow.Index).Value = True Then
                            frm_cust_CreateInvoice.chkCancel.Checked = True
                        Else
                            frm_cust_CreateInvoice.chkCancel.Checked = False
                        End If
                        frm_cust_CreateInvoice.chkCancel.Enabled = False
                        frm_cust_CreateInvoice.btnOk.Enabled = False
                    Else
                        If grdCustomer2.Item(8, grdCustomer2.CurrentRow.Index).Value = True Then
                            frm_cust_CreateInvoice.chkCancel.Checked = True
                        Else
                            frm_cust_CreateInvoice.chkCancel.Checked = False
                        End If
                        frm_cust_CreateInvoice.chkCancel.Enabled = True
                        frm_cust_CreateInvoice.btnOk.Enabled = True

                    End If
                    frm_cust_CreateInvoice.Text = "Edit Invoice"
                    frm_cust_CreateInvoice.ShowDialog()
                Case "Sales Receipts"
                    frm_cust_CreateSalesReceipt.KeySR = sKey
                    gKeySR = sKey
                    frm_cust_CreateSalesReceipt.ShowDialog()
                    'frm_Cust_CreateSalesReciept_COOP_.sKeySR = sKey
                    'gKeySR = sKey
                    'frm_Cust_CreateSalesReciept_COOP_.ShowDialog()
                Case "Received Payments"
                    frm_cust_ReceivePayment.KeyPayment = sKey
                    frm_cust_ReceivePayment.ShowDialog()
                Case "Credit Memos"
                    frm_cust_CreateCreditMemos.KeyCreditMemo = sKey
                    gKeyCredit = sKey
                    frm_cust_CreateCreditMemos.ShowDialog()
                Case "Debit Memos"
                    frmDebitMemos.KeyDebitMemo = sKey
                    gKeyDebit = sKey
                    frmDebitMemos.ShowDialog()
            End Select
        End If
    End Sub

    Private Sub cboShow_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboShow.SelectedIndexChanged
        sShow = cboShow.SelectedItem
        gTransactionType = sShow
        cboFilterBy_Load(cboShow.SelectedItem, cboFilter)
        showTransaction()
    End Sub

    ''FOR EDIT'
    Private Sub showTransaction()
        If sKeyCustomer = "" Then
            Exit Sub
        End If
        Dim dtTransaction As DataSet
        If sShow = "All Transactions" Then
            dtTransaction = loadTransaction()
        Else
            dtTransaction = loadTransaction(sShow)
        End If
        Try
            With grdCustomer3
                .DataSource = dtTransaction.Tables(0).DefaultView
                .Columns(grd3_Key).Visible = False
                .Columns(grd3_Tbl).Visible = False
                .Columns(grd3_Col).Visible = False
                .Columns(grd3_Type).HeaderText = "Type"
                .Columns(grd3_No).HeaderText = "Inv/OR#"
                .Columns(grd3_Date).HeaderText = "Date"
                .Columns(grd3_Acct).HeaderText = "Account"
                .Columns(grd3_Amt).HeaderText = "Amount"
                .Columns(grd3_Date).DefaultCellStyle.Format = "MM/dd/yyyy"
                .Columns(grd3_Amt).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With
        Catch
            MessageBox.Show(Err.Description, "Show Transactions")
        End Try
    End Sub

    Private Function loadTransaction(Optional ByVal sType As String = "") As DataSet
        Dim sSQLCmd As String = "usp_t_transaction_listByCustomer "
        sSQLCmd &= "@fxKeyCustomer = '" & sKeyCustomer & "' "
        If sType <> "" Then
            sSQLCmd &= ",@fcType = '" & sType & "'"
        End If
        sSQLCmd &= ",@fdDateFrom = '" & dteDateFrom & "'"
        sSQLCmd &= ",@fdDateTo = '" & dteDateTo & "'"
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Transactions")
            Return Nothing
        End Try
    End Function

    Private Sub grdCustomer3_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdCustomer3.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdCustomer3.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Private Sub grdCustomer3_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer3.DoubleClick
        If grdCustomer3.CurrentRow.Index < 0 Then
            Exit Sub
        End If
        Dim sType As String = grdCustomer3.Item(grd3_Type, grdCustomer3.CurrentRow.Index).Value
        Dim sKeyTemp As String = grdCustomer3.Item(grd3_Key, grdCustomer3.CurrentRow.Index).Value.ToString
        Select Case sType
            Case "Sales Order"
                frm_cust_CreateSalesOrder.KeySO = sKeyTemp
                frm_cust_CreateSalesOrder.Text = "Edit Sales Order"
                frm_cust_CreateSalesOrder.ShowDialog()
            Case "Invoice"
                frm_cust_CreateInvoice.KeyInvoice = sKeyTemp
                gInvoiceKey = sKeyTemp
                frm_cust_CreateInvoice.Text = "Edit Invoice"
                frm_cust_CreateInvoice.ShowDialog()
            Case "Sales Receipt"
                gKeySR = sKeyTemp
                frm_cust_CreateSalesReceipt.KeySR = sKeyTemp
                frm_cust_CreateSalesReceipt.ShowDialog()
            Case "Payment"
                frm_cust_ReceivePayment.KeyPayment = sKeyTemp
                frm_cust_ReceivePayment.ShowDialog()
            Case "Credit Memo"
                gKeyCredit = sKeyTemp
                frm_cust_CreateCreditMemos.KeyCreditMemo = sKeyTemp
                frm_cust_CreateCreditMemos.ShowDialog()
            Case "Debit Memo"
                gKeyDebit = sKeyTemp
                frmDebitMemos.KeyDebitMemo = sKeyTemp
                frmDebitMemos.ShowDialog()
        End Select
        showTransaction()
        m_customerList(grdCustomer1, cboView.SelectedItem)
    End Sub

    Private Sub grdCustomer3_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCustomer3.KeyUp
        If e.KeyCode = Keys.Delete Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                deleteTrsanction()
                Call showTransaction()
            End If
        End If
    End Sub

    Private Sub deleteTrsanction()
        Dim sTable As String = grdCustomer3.Item(grd3_Tbl, grdCustomer3.CurrentRow.Index).Value
        Dim sCol As String = grdCustomer3.Item(grd3_Col, grdCustomer3.CurrentRow.Index).Value
        Dim sKey As String = grdCustomer3.Item(grd3_Key, grdCustomer3.CurrentRow.Index).Value.ToString
        ' sKey = grdCustomer3.CurrentRow.Cells(grd3_Key).Value.ToString
        Dim sSQLCmd As String = ""

        Select Case sTable
            Case "tSalesOrder"
                sSQLCmd = "DELETE FROM tSalesOrder_Item WHERE fxKeySO = '" & sKey & "'"
            Case "tInvoice"
                sSQLCmd = "DELETE FROM tInvoice_item WHERE fxKeyInvoice = '" & sKey & "'"
            Case "tCreditMemo"
                sSQLCmd = "DELETE FROM tCreditMemo_item WHERE fxKeyCredit = '" & sKey & "'"
            Case "tDebitMemo"
                sSQLCmd = "DELETE FROM tDebitMemo_item WHERE fxKeyDebit = '" & sKey & "'"
            Case "tSalesReceipt"
                sSQLCmd = "DELETE FROM tSalesReceipt_item WHERE fxKeySR = '" & sKey & "'"
        End Select

        SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)

        sSQLCmd = "DELETE FROM " & sTable
        sSQLCmd &= " WHERE " & sCol & "= '" & sKey & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully deleted.", "Delete Transaction")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Transaction")
        End Try
    End Sub

    Private Sub deleteCustomer()
        Dim sSQLCmd As String = "UPDATE mCustomer00Master SET "
        sSQLCmd &= "fbDeleted=1"
        sSQLCmd &= " WHERE fxKeyCustomer='" & sKeyCustomer & "'"
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully deleted.", "Delete Customer")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Delete Customer")
        End Try
    End Sub

    Private Sub grdCustomer1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCustomer1.KeyUp
        If e.KeyCode = Keys.Delete Then
            If MessageBox.Show("Are you sure you want to delete this record?", "Delete Supplier Rate Category", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                deleteCustomer()
                m_customerList(grdCustomer1, cboView.SelectedItem)
            End If
        End If
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
            ShowCustomer()
            showTransaction()
        End If
    End Sub
    Private Sub grdTransaction_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdTransaction.KeyUp
        If e.KeyCode = Keys.Down Or e.KeyCode = Keys.Up Then
            If Not Me.grdTransaction.CurrentRow.Index = _
                      Me.grdTransaction.Rows.Count - 1 Then
                sMode = grdTransaction.CurrentRow.Cells(0).Value
                loadGrdCustomer2(sMode)
                cboFilterBy_Load(sMode, cboFilterBy)
                m_loaddteRange(dteRange)
                m_loaddteRange(dteRange2)
            End If
        End If
    End Sub
    Private Sub cboFilterBy_Load(ByVal sMode, ByVal s)
        With s
            Select Case sMode
                Case "All Transactions"
                    .Items.Clear()
                    .Items.Add("All")
                Case "All Sales Transactions"
                    .Items.Clear()
                    .Items.Add("All")
                    .Items.Add("Open Invoices and Charges")
                    .Items.Add("Overdue Invoices and Charges")
                    .Items.Add("Paid Invoices and Charges")
                Case "Balance Details"
                    .Items.Clear()
                    .Items.Add("All")
                    .Items.Add("Open")
                Case "Payments and Credits"
                    .Items.Clear()
                    .Items.Add("All Payments and Credits")
                    .Items.Add("Unapplied Payment and Credits")
                Case "Sales Orders"
                    .Items.Clear()
                    .Items.Add("All Sales Orders")
                Case "Invoices"
                    .Items.Clear()
                    .Items.Add("All Invoices")
                Case "Sales Receipts"
                    .Items.Clear()
                    .Items.Add("All Sales Receipts")
                Case "Received Payments"
                    .Items.Clear()
                    .Items.Add("All Payment Methods")
                    PaymentMethod_Load()
                Case "Credit Memos"
                    .Items.Clear()
                    .Items.Add("All Credit Memos")
                Case "Debit Memos"
                    .Items.Clear()
                    .Items.Add("All Debit Memos")
                Case "Journal Entries"
                    .items.clear()
                    .items.add("All Journal Entries")
                Case Else
            End Select
            .SelectedIndex = 0
        End With
    End Sub
   
    Private Sub PaymentMethod_Load()

        Dim sSQLCmd As String= "SELECT fxKeyPaymentMethod, fcPaymentMethodName "
        sSQLCmd &= " FROM mCustomer03PaymentMethod WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCOmpany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcPaymentMethodName"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboFilterBy.Items.Add(rd.Item("fcPaymentMethodName").ToString)
                    cboFilter.Items.Add(rd.Item("fxKeyPaymentMethod").ToString)
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE:Load Payment Method")
        End Try
    End Sub
    Private Sub dteRange2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteRange2.SelectedIndexChanged
        getDateRange(lbldteRange2, dteRange2)
        gtransactionsCoveredDate = dteRange2.SelectedItem

        If dteRange2.SelectedIndex <> 0 Then
            If InStr(lbldteRange2.Text, "-", CompareMethod.Binary) <> 0 Then
                dteDateFrom = Convert.ToDateTime(Mid(lbldteRange2.Text, 1, InStr(lbldteRange2.Text, "-", CompareMethod.Binary) - 1))
                dteDateTo = Convert.ToDateTime(Mid(lbldteRange2.Text, InStr(lbldteRange2.Text, "-", CompareMethod.Binary) + 1, lbldteRange2.Text.Length - InStr(lbldteRange2.Text, "-", CompareMethod.Binary)))
            Else

                dteDateFrom = Convert.ToDateTime(IIf(lbldteRange2.Text = "", Nothing, lbldteRange2.Text))
                dteDateTo = Convert.ToDateTime(IIf(lbldteRange2.Text = "", Nothing, lbldteRange2.Text))

                If IsDate(dteDateTo) Then
                    Dim x As Date = DateAdd(DateInterval.Day, 1, Convert.ToDateTime(dteDateTo))
                    dteDateTo = x.ToString
                End If
            End If
        Else
            dteDateFrom = Nothing
            dteDateTo = Nothing
        End If

        showTransaction()
        gDateFrom = dteDateFrom
        gDateTo = dteDateTo
    End Sub
   
    Private Sub dteRange_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteRange.SelectedValueChanged
        getDateRange(lbldteRange, dteRange)
    End Sub


    Private Sub dteRange_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteRange.SelectedIndexChanged
        getDateRange(lbldteRange, dteRange)

        If dteRange.SelectedIndex <> 0 Then
            If InStr(lbldteRange.Text, "-", CompareMethod.Binary) <> 0 Then
                dteFrom = Convert.ToDateTime(Mid(lbldteRange.Text, 1, InStr(lbldteRange.Text, "-", CompareMethod.Binary) - 1))
                dteTo = Convert.ToDateTime(Mid(lbldteRange.Text, InStr(lbldteRange.Text, "-", CompareMethod.Binary) + 1, lbldteRange.Text.Length - InStr(lbldteRange.Text, "-", CompareMethod.Binary)))
            Else

                dteFrom = Convert.ToDateTime(IIf(lbldteRange.Text = "", Nothing, lbldteRange.Text))
                dteTo = Convert.ToDateTime(IIf(lbldteRange.Text = "", Nothing, lbldteRange.Text))

                If IsDate(dteTo) Then
                    Dim x As Date = DateAdd(DateInterval.Day, 1, Convert.ToDateTime(dteTo))
                    x = DateAdd(DateInterval.Second, -1, x)
                    dteTo = x.ToString
                End If
            End If
        Else
            dteFrom = Nothing
            dteTo = Nothing
        End If
        loadGrdCustomer2(sMode)
    End Sub

    Private Sub lnkOpeningBal_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOpeningBal.LinkClicked
        frmCustOpeningBalance.Show()
    End Sub

    Private Sub btnPrintCustomerMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintCustomerMaster.Click
        frmCustomerMasterReport.Show()
    End Sub

    Private Sub grdCustomer2_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCustomer2.CellContentClick

    End Sub

    Private Sub grdCustomer2_DockChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer2.DockChanged

    End Sub
End Class