Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_salesRepAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeySalesRep As String
    Private sKeyID As String
    Private sKeyAddress As String
    Private sInitial As String
    Private sType As String

    Public Property KeySalesRep() As String
        Get
            Return sKeySalesRep
        End Get
        Set(ByVal value As String)
            sKeySalesRep = value
        End Set
    End Property

    Private Sub frm_MF_salesRepAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Public Sub refreshForm()
        loadSalesRep()
        m_loadAllName(cboSalesRepName, cboSalesRepID, False)
        m_selectedCboValue(cboSalesRepName, cboSalesRepID, sKeySalesRep)
    End Sub

    'Private Sub selectedSalesRep()
    '    If Not sKeySalesRep = Nothing Or Not sKeySalesRep = "" Then
    '        cboSalesRepID.SelectedText = sKeySalesRep
    '        cboSalesRepID.SelectedItem = sKeySalesRep
    '        cboSalesRepName.SelectedIndex = cboSalesRepID.SelectedIndex
    '    Else
    '        cboSalesRepName.SelectedIndex = 0
    '        cboSalesRepID.SelectedIndex = 0
    '    End If
    'End Sub

    Public Sub loadSalesRep()
        Dim sSQLCmd As String = "SPU_SalesRep_File @fxKeySalesRep = '" & sKeySalesRep & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sKeyID = rd.Item("fxKeyID").ToString
                    txtSalesRepName.Text = rd.Item("fcRepName")
                    txtType.Text = rd.Item("fcSalesRepType")
                    txtInitials.Text = rd.Item("fcSalesRepInitials")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Laod Master File Type")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub getSalesRep()
        Dim sWords() As String
        Dim sString As String = cboSalesRepName.SelectedItem
        Dim ctr As Integer
        Dim sInitialTemp As String = ""
        Dim sNameTemp As String = ""

        sWords = sString.Split()

        'For ctr = 0 To (UBound(sWords) - 1)
        '    sInitialTemp &= Mid(sWords(ctr), 1, 1)
        '    sNameTemp &= sWords(ctr)
        'Next

        For ctr = 2 To UBound(sWords)
            sInitialTemp &= Mid(sWords(ctr), 1, 1)
            sNameTemp &= sWords(ctr) & " "
        Next
        sInitial = sInitialTemp.ToUpper
        sType = sWords(0)
        txtSalesRepName.Text = sNameTemp
        txtInitials.Text = sInitial
        txtType.Text = sType
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        updateSalesRep()
        m_SalesRepList(frm_acc_salesRep.chkIncludeInactive.CheckState, frm_acc_salesRep.grdSalesRep)
        m_chkSettings(frm_acc_salesRep.chkIncludeInactive, frm_acc_salesRep.SQLQuery)
        m_loadAllName(frm_cust_masterCustomerAddEdit.cboRepName, frm_cust_masterCustomerAddEdit.cboRepID, False)
        m_loadSalesRep(frm_cust_CreateInvoice.cboRepName, frm_cust_CreateInvoice.cboRepID)
        Me.Close()
    End Sub

    Private Sub updateSalesRep()
        Dim sSQLCmd As String = "SPU_SalesRep_Update "
        sSQLCmd &= "@fxKeySalesRep = '" & sKeySalesRep & "'"
        sSQLCmd &= ",@fxKeyID = '" & sKeyID & "'"
        sSQLCmd &= ",@fcSalesRepInitials = '" & txtInitials.Text & "'"
        sSQLCmd &= ",@fcSalesRepType = '" & txtType.Text & "'"
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Update Sales Rep")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Sales Rep")
        End Try
    End Sub

    Private Sub cboSalesRepName_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSalesRepName.SelectedIndexChanged
        cboSalesRepID.SelectedIndex = cboSalesRepName.SelectedIndex
        sKeyID = cboSalesRepID.SelectedItem

        If cboSalesRepName.SelectedIndex = 1 Then
            frm_MF_salesRepOption.Show()
        End If

        If cboSalesRepName.SelectedIndex <> 1 And cboSalesRepName.SelectedIndex <> 0 Then
            getSalesRep()
        Else
            txtSalesRepName.Text = ""
            txtInitials.Text = ""
            txtType.Text = ""
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim dtAddress As DataTable = m_getAddress(sKeyID, txtType.Text).Tables(0)
        Dim sKeyAddTemp As String = ""
        sKeyAddTemp = dtAddress.Rows(0)("fxKeyAddress").ToString
        'Dim rd As DataTableReader = dtAddress.CreateDataReader
        'Dim sKeyAddTemp As String = ""
        'If rd.Read Then
        '    sKeyAddTemp = rd.Item("fxKeyAddress")
        'End If
        'rd.Close()

        If txtType.Text = "Customer" Then
            frm_cust_masterCustomerAddEdit.Show()

        ElseIf txtType.Text = "Supplier" Then
            frm_vend_masterVendorAddEdit.KeySupplier = sKeyID
            frm_vend_masterVendorAddEdit.KeyAddress = sKeyAddTemp
            frm_vend_masterVendorAddEdit.Show()
        Else
            frm_MF_otherNameMasterAddEdit.KeyOtherName = sKeyID
            frm_MF_otherNameMasterAddEdit.KeyAddress = sKeyAddTemp
            frm_MF_otherNameMasterAddEdit.Show()
        End If
    End Sub

End Class