Public Class frm_acc_nametype

    Private Sub frm_acc_nametype_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call rdClear()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Call rdClear()
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
      rdValidation()
    End Sub

    Private Sub rdValidation()
        If rdSupplier.Checked = True Then
            frm_vend_masterVendorAddEdit.Show()
            Me.Close()

        ElseIf rdCustomer.Checked = True Then
            frm_cust_masterCustomerAddEdit.Show()
            Me.Close()

        ElseIf rdOther.Checked = True Then
            frm_MF_otherNameMasterAddEdit.Show()
            Me.Close()

        End If
    End Sub

    Private Sub rdClear()
        rdSupplier.Checked = False
        rdCustomer.Checked = False
        rdOther.Checked = False
    End Sub
    
End Class