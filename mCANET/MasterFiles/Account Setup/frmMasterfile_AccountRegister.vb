﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes
Imports CrystalDecisions.Shared

Public Class frmMasterfile_AccountRegister
    Private AccountKey As String
    Private EmployeeId As String
    Private fxAccountKeyRegister As String
    Private mycon As New Clsappconfiguration
    Dim Isclosed As Boolean

    Dim dv As New DataView
    Dim ds As New DataSet

#Region "Property"
    Public Property GetAccountID() As String
        Get
            Return AccountKey
        End Get
        Set(ByVal value As String)
            AccountKey = value
        End Set
    End Property

    Public Property GetfxAccountKeyRegister() As String
        Get
            Return fxAccountKeyRegister
        End Get
        Set(ByVal value As String)
            fxAccountKeyRegister = value
        End Set
    End Property
    Public Property GetEmployeeID() As String
        Get
            Return EmployeeId
        End Get
        Set(ByVal value As String)
            EmployeeId = value
        End Set
    End Property
#End Region
    Private Sub ControlSetup(ByVal mode As String)

        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.Enabled = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            Exit Sub
        End If
        If mode = "Close" Then
            Me.Close()
        End If
        If btnupdate.Text = "Update" Then
            Update_AccountRegister(fxAccountKeyRegister, txtAccountNumber.Text, AccountKey, EmployeeId, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked)
            dgvListAccountRegister.Columns.Clear()
            LoadData()
            Load_AccountRegister(txtsearch.Text)
            AuditTrail_Save("ACCOUNT REGISTER", "Update account account register " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.Enabled = False
            txtSchedule.Clear()
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            CBclose.Checked = False
            CBclose.Enabled = False
            Exit Sub
        End If
        If btnupdate.Text = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            txtSchedule.Enabled = True
            btnsave.Enabled = False
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            'txtSchedule.ReadOnly = False
            CBclose.Enabled = True
            Exit Sub
        End If
    End Sub

    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterFile_Accounts.xModule = "ACCOUNTREGISTER"
        frmMasterFile_Accounts.ShowDialog()
    End Sub

    Private Sub btnBrowseEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseEmployee.Click
        frmMember_List.ShowDialog()
    End Sub

    Private Sub btnBrowseAccountNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccountNumber.Click
        frmMasterfile_AccountNumber.GetID = 3
        frmMasterfile_AccountNumber.ShowDialog()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim AccountKey As String

        If GetAccountID <> "" Then
            AccountKey = GetAccountID
        Else
            AccountKey = Guid.NewGuid.ToString
        End If

        If txtSchedule.Text = "" Then
            txtSchedule.Text = "0.00"
        End If
        If btnsave.Text = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True
            btnBrowseAccountNumber.Enabled = True

            txtSchedule.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            'txtSchedule.Clear()
            CBclose.Checked = False
            CBclose.Enabled = True
            Exit Sub
        End If
        If btnsave.Text = "Save" Then
            If ErrMsg() = "" Then
                Add_AccountRegister(AccountKey, txtAccountNumber.Text, EmployeeId, Convert.ToDecimal(txtSchedule.Text), CBclose.Checked)
                UpdateAccountInAccounting(txtAccountNumber.Text, Date.Now)
                AuditTrail_Save("ACCOUNT REGISTER", "Create new account account register " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
                dgvListAccountRegister.Columns.Clear()
                LoadData()
                Load_AccountRegister(txtsearch.Text)
                btnupdate.Enabled = True
                btndelete.Enabled = True
                btnclose.Text = "Close"
                btnsave.Text = "New"
                btnBrowseAccount.Enabled = False
                btnBrowseEmployee.Enabled = False
                btnBrowseAccountNumber.Enabled = False

                txtSchedule.Enabled = False
                txtaccountcode.Clear()
                txtaccountname.Clear()
                txtAccountNumber.Clear()
                txtName.Clear()
                txtSchedule.Clear()
                CBclose.Checked = False
                CBclose.Enabled = False
                Exit Sub
            Else
                MsgBox(ErrMsg, MsgBoxStyle.Exclamation, "Account Register")
            End If
            'Else
            '    MessageBox.Show("Fill up form!", "Save Account Register", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        'End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            If dgvListAccountRegister.SelectedCells.Count <> 0 Then
                ControlSetup("Edit")
                Exit Sub
            End If
        End If
        'On Error Resume Next
        If btnupdate.Text = "Update" Then
            If ErrMsg() = "" Then
                ControlSetup("Update")
            Else
                MsgBox(ErrMsg, MsgBoxStyle.Exclamation, "Account Register")
            End If

            Exit Sub
        End If
    End Sub
    Private Function ErrMsg()
        Dim str As String
        If txtAccountNumber.Text = "" Then
            str = "Please select Account Number." & vbCr
        End If
        If txtaccountname.Text = "" Then
            str += "Please select Account Title." & vbCr
        End If
        If txtName.Text = "" Then
            str += "Please select Name." & vbCr
        End If
        Return str
    End Function

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
            Delete_AccountRegister(fxAccountKeyRegister)
            Dim sSQLCmd As String = "update mDocNumber Set fdDateUsed= Null where fcDocNumber='" & txtAccountNumber.Text & "' AND fcDocType='AR'"
            SqlHelper.ExecuteScalar(mycon.cnstring, CommandType.Text, sSQLCmd)
            AuditTrail_Save("ACCOUNT REGISTER", "Delete " & txtAccountNumber.Text & " | " & txtaccountname.Text & " | " & txtName.Text)
            dgvListAccountRegister.Columns.Clear()
            LoadData()
            Load_AccountRegister(txtsearch.Text)
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True

            txtSchedule.Enabled = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            CBclose.Checked = False
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Update_AccountRegister(ByVal fxKeyAccountRegister As String, ByVal fcDocNumber As String, ByVal fxKey_Account As String,
                                        ByVal fk_Employee As String, ByVal fnSchedule As Decimal, ByVal fbClosed As Boolean)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim MyGuids As New Guid(fxAccountKeyRegister)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Update",
                                      New SqlParameter("@fxKeyAccountRegister", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", txtName.Text),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed))
            MessageBox.Show("Record Succesfully Updated!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub
    Private Sub Add_AccountRegister(ByVal fxKey_Account As String, ByVal fcDocNumber As String,
                                        ByVal fk_Employee As String, ByVal fnSchedule As Double, ByVal fbClosed As Boolean)
        'Dim MyGuids As New Guid(fk_Employee)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim coid As New Guid(gCompanyID)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Insert",
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fcEmployeeNo", fk_Employee),
                                      New SqlParameter("@fcFullname", txtName.Text),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed),
                                      New SqlParameter("@coid", coid))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Add Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Delete_AccountRegister(ByVal fxKey_Account As String)
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "spu_AccountRegister_SearchSLEntry",
                                      New SqlParameter("@filter", txtAccountNumber.Text))
        If rd.Read = True Then
            MsgBox("Account Register cannot be deleted. Transaction in this SL exists.", MsgBoxStyle.Exclamation, "Account Register")
            Exit Sub
        Else
            Dim MyGuid As New Guid(fxKey_Account)
            Try
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Delete",
                                          New SqlParameter("@fxAccountKeyRegister", MyGuid))
                MessageBox.Show("Record Succesfully Deleted!")
            Catch ex As Exception
                MessageBox.Show("Error! " + ex.ToString, "Delete Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

    End Sub

    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmMasterfile_AccountRegister_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvListAccountRegister.Columns.Clear()
        LoadData()
        Load_AccountRegister(txtsearch.Text)
    End Sub

    Private Sub LoadData()
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Member_AccountRegister_ListClosed",
                                                  New SqlParameter("@Closed", CBClosedList.Checked))
        dv.Table = ds.Tables(0)
    End Sub

    Private Sub dgvListAccountRegister_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccountRegister.CellClick
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountcode.Text = Row.Cells(5).Value.ToString
            txtaccountname.Text = Row.Cells(6).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(7).Value
            CBclose.Checked = Row.Cells(8).Value
        Next
    End Sub

    Private Sub dgvListAccountRegister_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyUp
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
        Next
    End Sub
    Private Sub dgvListAccountRegister_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyDown
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxAccountKeyRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            AccountKey = Row.Cells(1).Value.ToString
            EmployeeId = Row.Cells(3).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value
            CBclose.Checked = Row.Cells(7).Value
        Next
    End Sub

    Private Sub CBClosedList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CBClosedList.CheckedChanged
        dgvListAccountRegister.Columns.Clear()
        LoadData()
        Load_AccountRegister(txtsearch.Text)
    End Sub

#Region "List"
    Public Sub ClosedList(ByVal Active As Boolean)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_ListClosed")
        dgvListAccountRegister.DataSource = ds.Tables(0)
    End Sub
    Public Sub SearchList(ByVal Search As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_Search",
                                      New SqlParameter("@Search", txtsearch.Text))
        With dgvListAccountRegister
            .DataSource = ds.Tables(0)
            .Columns(0).Visible = False
            .Columns(1).Visible = False

            .Columns(2).HeaderText = "Account Number"
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(2).Width = 100
            .Columns(2).ReadOnly = True
            .Columns(3).HeaderText = "ID"
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(3).Width = 150
            .Columns(3).ReadOnly = True
            .Columns(4).HeaderText = "Name"
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(4).ReadOnly = True
            .Columns(5).HeaderText = "Account Title"
            .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(5).ReadOnly = True
            .Columns(6).HeaderText = "Schedule"
            .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(6).ReadOnly = True
            .Columns(6).Width = 80
            .Columns(7).HeaderText = "Closed"
            .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns(7).Width = 80
        End With
    End Sub

    Private Sub Load_AccountRegister(ByVal filter As String)
        Try
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_Search",
            '                                             New SqlParameter("@Search", filter))
            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "fcEmployeeNo LIKE '%" & txtsearch.Text &
                "%' AND fcAccountName LIKE '%" & txtAccountFilter.Text &
                "&' OR Name LIKE '%" & txtsearch.Text &
                "%' AND fcAccountName LIKE '%" & txtAccountFilter.Text &
                "%' OR fcDocNumber LIKE '%" & txtsearch.Text &
                "%'AND fcAccountName LIKE '%" & txtAccountFilter.Text & "%'"
            dgvListAccountRegister.DataSource = dv

            With dgvListAccountRegister
                '.DataSource = ds.Tables(0)
                .DataSource = dv
                .Columns(0).Visible = False
                .Columns(1).Visible = False

                .Columns(2).HeaderText = "Account Number"
                .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(2).Width = 120
                .Columns(2).ReadOnly = True
                .Columns(3).HeaderText = "ID"
                .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(3).Width = 120
                .Columns(3).ReadOnly = True
                .Columns(4).HeaderText = "Name"
                .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(4).ReadOnly = True
                .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(5).HeaderText = "Code"
                .Columns(5).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(5).ReadOnly = True
                .Columns(5).Width = 80
                .Columns(6).HeaderText = "Account Title"
                .Columns(6).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(6).ReadOnly = True
                .Columns(6).Width = 180
                .Columns(7).HeaderText = "Schedule"
                .Columns(7).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(7).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(7).ReadOnly = True
                .Columns(7).Width = 100
                .Columns(8).HeaderText = "Closed"
                .Columns(8).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(8).SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns(8).Width = 50
            End With

            dv.Sort = ("Name, fcAccountName")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Load Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtsearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtsearch.TextChanged
        Load_AccountRegister(txtsearch.Text)
    End Sub

    Private Sub txtAccountFilter_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtAccountFilter.TextChanged
        Load_AccountRegister(txtsearch.Text)
    End Sub
#End Region


End Class