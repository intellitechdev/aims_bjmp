Imports WMPs_Money_Figure_Convert_to_Words
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_writechecks

#Region "Declaration"

    Private gMoneyConverter As New WPMs
    Private gTrans As New clsTransactionFunctions
    Private ExpenseAmount As Decimal
    Private ItemAmount As Decimal
    Public psEditMode As Boolean = False
    Dim checkDate As New clsRestrictedDate
    Dim gcon As New Clsappconfiguration
    Dim printGo As String
#End Region

#Region "Events"


    Private Sub frm_acc_writechecks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblchk_date.Text = Microsoft.VisualBasic.FormatDateTime(dtechk_checkdate.Value, DateFormat.ShortDate)
        IsDateRestricted()
        If psEditMode = False Then
            gCheckID = ""
            ExpenseAmount = 0
            ItemAmount = 0
            Call m_DisplayBankAccounts(cbochk_backaccount)
            Call m_cDisplayNames(cbochk_payorder)
            Call load_acc_lstexpenses()
            Call load_acc_lstitems()
            gExpenses.Visible = True
            grdLoadExpense.Visible = False
        ElseIf psEditMode = True Then
            Call m_DisplayBankAccounts(cbochk_backaccount)
            Call m_cDisplayNames(cbochk_payorder)
            grdLoadExpense.Visible = True
            gExpenses.Visible = False
            cbochk_backaccount.SelectedItem = get_bankaccount()
        End If
    End Sub

    Private Sub dtechk_checkdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtechk_checkdate.ValueChanged
        lblchk_date.Text = Microsoft.VisualBasic.FormatDateTime(dtechk_checkdate.Value, DateFormat.ShortDate)
        IsDateRestricted()
    End Sub

    Private Sub btn_chk_selectPO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_selectPO.Click
        frm_chk_selectPO.ShowDialog()
    End Sub

    Private Sub btn_chk_saveNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_saveNew.Click
        Call pSaveEvent()
        Call pSave_gExpense()
        Call pSave_gItems()
        Call clearform()
    End Sub

    Private Sub btn_chk_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_saveClose.Click
        Call pSaveEvent()
        Call pSave_gExpense()
        Call pSave_gItems()
        Me.Close()
    End Sub

    Private Sub cbochk_payorder_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbochk_payorder.SelectedIndexChanged
        Dim sName() As String
        Dim sRaw As String
        lblchk_payorder.Text = cbochk_payorder.SelectedItem
        If cbochk_payorder.SelectedItem = "" Then
            MsgBox("this section")
        Else
            If cbochk_payorder.SelectedItem <> "-------------------------------" Then
            Else
                sRaw = cbochk_payorder.SelectedItem
                sName = Split(sRaw, "-")
                'lblchk_payorder.Text = m_GetCheckName(sName(0), sName(1)) 'error
                txtAddress.Text = m_getAddressbyPayeeID(m_GetNameID(sName(0), sName(1)))
            End If
        End If
    End Sub
    Private Sub txtChkAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtChkAmt.TextChanged
        Try
            txtChkAmt.Text = Format(CDec(txtChkAmt.Text), "##,##0.00")
            lblchk_amountinwords.Text = gMoneyConverter.gGenerateCurrencyInWords(txtChkAmt.Text)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtChkAmt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtChkAmt.Validating
        lblchk_amountinwords.Text = gMoneyConverter.gGenerateCurrencyInWords(txtChkAmt.Text)
    End Sub

    Private Sub gExpenses_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gExpenses.CellValidated
        Call gCalculateExpense()
    End Sub

    Private Sub gItems_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gItems.CellValidated
        Call gCalculateItem()
    End Sub

    Private Sub btn_chk_Recalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_Recalculate.Click
        Call gCalculateExpense()
        Call gCalculateItem()
    End Sub

    Private Sub btn_chk_clearSplit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_clearSplit.Click
        Call gTrans.gGridClear(gExpenses)
        Call gTrans.gGridClear(gItems)
        With gExpenses
            .Item(1, 0).Value = txtChkAmt.Text
            ExpenseAmount = txtChkAmt.Text
            ItemAmount = 0
            tb_chkExpenses.Text = "Expenses" + "     " + "P " + Format(CDec(ExpenseAmount), "##,##0.00")
            tb_chkItems.Text = "Items" + "     " + "P " + "0.00"
        End With
    End Sub

    Private Sub cbochk_backaccount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbochk_backaccount.SelectedIndexChanged
        Dim sVal() As String
        sVal = Split(cbochk_backaccount.SelectedItem, "-")
        lblchk_endingbalance.Text = Format(CDec(m_GetBalanceAmount(sVal(0))), "##,##0.00#")
    End Sub

    Private Sub btn_chk_Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chk_Clear.Click
        Call clearform()
    End Sub

    Private Sub ts_chk_find_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_chk_find.Click
        frm_acc_find.ShowDialog()
        HighlightSelectedItem()
    End Sub

    Private Sub grdLoadExpense_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLoadExpense.CurrentCellDirtyStateChanged
        If grdLoadExpense.IsCurrentRowDirty = True Then
            computeAmount()
        End If
        HighlightSelectedItem()
    End Sub

    Private Sub grdLoadExpense_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles grdLoadExpense.RowsAdded
        calculate_amount()
    End Sub

    Private Sub grdLoadExpense_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLoadExpense.VisibleChanged
        Try
            With grdLoadExpense
                If .CurrentRow.Cells(1).Value IsNot Nothing Then
                    If .CurrentRow.Cells(8).Value IsNot Nothing Then
                        txtChkAmt.Text = .CurrentRow.Cells(8).Value
                    End If
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub grdLoadExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLoadExpense.Click
        With grdLoadExpense
            Dim xVal As Decimal = 0
            Dim xRow As Integer
            For xRow = 0 To .Rows.Count - 1
                If .Rows(xRow).Cells(1).Value = True And .Rows(xRow).Cells(1).Value IsNot Nothing Then
                    xVal += .Rows(xRow).Cells(8).Value
                End If
            Next
            txtChkAmt.Text = xVal
            If .CurrentRow.Cells(3).Value.ToString IsNot Nothing Then
                cbochk_payorder.SelectedItem = .CurrentRow.Cells(3).Value.ToString + " - " + " Supplier"
            End If

        End With
    End Sub

    Private Sub msp_accChekprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles msp_accChekprint.Click
        
        Call updateprintbills()
        Call gBillsKey()
        Dim xRow As Integer = 0
        On Error GoTo subtermited
        Dim msg As String = "You are about to print CV# " & grdLoadExpense.Item(4, grdLoadExpense.CurrentRow.Index).Value & _
                            " with the following item/s."
        For Each Item As DataGridViewRow In grdLoadExpense.Rows
            xRow = Item.Index
            If grdLoadExpense.Item("Pay", xRow).Value = True Then
                msg &= vbCr & grdLoadExpense.Item(4, xRow).Value
            End If
        Next
        Dim Response As String
        Response = MsgBox(msg & vbCr & "(We recommend that you must check first these items and the item that has been selected.)" & _
                vbCr & "Are you sure of these data and want to proceed on printing?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Warning")
        If Response = vbYes Then
            gBankAccount = cbochk_backaccount.SelectedItem
            gBankEndBalance = lblchk_endingbalance.Text
            gCheckDate = lblchk_date.Text
            gPayorder = lblchk_payorder.Text
            gCheckAmt = txtChkAmt.Text
            gCheckAmtwords = lblchk_amountinwords.Text
            gPayeesAdd = txtAddress.Text
            gPayeesMemo = txtMemo.Text
            gPreparedBy = strSysCurrentUserName
            If printGo <> "abort printing" Then
                frmCheckNo.ShowDialog()
            Else
                printGo = "go print"
            End If
        End If
subtermited:
    End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dtechk_checkdate.Value) Then
            btn_chk_saveNew.Enabled = False 'true if date is NOT within range of period restricted
            btn_chk_saveClose.Enabled = False
        Else
            btn_chk_saveNew.Enabled = True 'false if date is within range of period restricted
            btn_chk_saveClose.Enabled = True
        End If
    End Sub
#End Region
#Region "Functions/ Procedures"
    Private Sub HighlightSelectedItem()
        If grdLoadExpense.Rows.Count > 0 Then
            Dim xRow As Integer = 0
            For Each itemRow As DataGridViewRow In grdLoadExpense.Rows
                xRow = itemRow.Index
                If grdLoadExpense.Item("Pay", xRow).Value = True Then
                    grdLoadExpense.Rows(xRow).DefaultCellStyle.BackColor = Color.LightBlue
                Else
                    grdLoadExpense.Rows(xRow).DefaultCellStyle.BackColor = Color.White
                End If
            Next
        End If
    End Sub
    Private Sub computeAmount()
        If grdLoadExpense.RowCount <> 0 Then
            calculate_amount()
            btn_chk_Clear.Select()
            grdLoadExpense.Select()
        End If
    End Sub
    Private Sub updateprintbills()
        Dim xRow As Integer
        With Me.grdLoadExpense
            For xRow = 0 To .Rows.Count - 1
                If .Rows(xRow).Cells(1).Value = True And .Rows(xRow).Cells(1).Value IsNot Nothing Then
                    Call m_tagbillsasprinted(.Rows(xRow).Cells(0).Value.ToString)

                End If
            Next

        End With

    End Sub

    Private Sub getbillskey()
        Try
            With Me.grdLoadExpense
                Dim xRow As Integer
                For xRow = 0 To .Rows.Count - 1
                    If Not .Rows(xRow).Cells(0).Value Is Nothing Then
                        If .Rows(xRow).Cells(1).Value = True Then
                            gItemsKeys = .Rows(xRow).Cells(0).Value.ToString
                        End If
                    End If
                Next
            End With
        Catch ex As Exception
            MsgBox("Please use the find button to look for the payees expenses....", MsgBoxStyle.Information)
            printGo = "abort printing"
        End Try
    End Sub

    Private Sub gBillsKey()
        Try
            With Me.grdLoadExpense
                If Not .CurrentRow.Cells(0).Value Is Nothing Then
                    gItemsKeys = .CurrentRow.Cells(0).Value.ToString

                End If
            End With
        Catch ex As Exception
            MsgBox("Please use the find button to look for the payees expenses....", MsgBoxStyle.Information, Me.Text)
            printGo = "abort printing"
        End Try
    End Sub

    Public Sub load_acc_expensedetails()
        With grdLoadExpense
            .Columns.Clear()
            If Lbltemprefno.Text = "" Then
                Dim dtsBills As DataTable = m_displaybillsforchecks().Tables(0)
                .DataSource = dtsBills.DefaultView
                .Columns(0).Visible = False
                .Columns(1).Width = 50
                .Columns(2).Width = 80
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .Columns(4).Width = 80
                .Columns(5).Width = 80
                .Columns(6).Width = 80
                .Columns(7).Width = 80
                .Columns(8).Width = 80
                If frmMain.LblClassActivator.Visible = True Then
                    .Columns(9).Visible = True
                Else
                    .Columns(9).Visible = False
                End If
                .Columns(9).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Else
                Dim dtsBills As DataTable = m_displaybillsforchecks_refNo_only().Tables(0)
                .DataSource = dtsBills.DefaultView
                .Columns(0).Visible = False
                .Columns(1).Width = 50
                .Columns(2).Width = 80
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .Columns(4).Width = 80
                .Columns(5).Width = 80
                .Columns(6).Width = 80
                .Columns(7).Width = 80
                .Columns(8).Width = 80
                If frmMain.LblClassActivator.Visible = True Then
                    .Columns(9).Visible = True
                Else
                    .Columns(9).Visible = False
                End If
                .Columns(9).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            End If
        End With
        GetFxKeySupplier()
    End Sub
    Private Function GetFxKeySupplier()
        Dim sSqlCmd As String = "SPU_getFxKeySupplier "
        sSqlCmd &= "@fcSupplierName='" & gName & "' "
        sSqlCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read()
                    gfxKeySupplier = rd.Item(0).ToString
                End While
            End Using

        Catch ex As Exception

        End Try
    End Function

    Private Sub load_acc_lstexpenses()
        'With gExpenses
        '    Dim cboAccounts As New DataGridViewComboBoxColumn
        '    Dim txtAmount As New DataGridViewTextBoxColumn
        '    Dim txtMemo As New DataGridViewTextBoxColumn
        '    Dim cboCustomer As New DataGridViewComboBoxColumn
        '    Dim chkBillable As New DataGridViewCheckBoxColumn

        '    Dim dtsAccount As DataTable = m_displayAccountsDS.Tables(0)
        '    Dim dtsCustomer As DataTable = m_displaycustomerDS.Tables(0)

        '    With cboAccounts
        '        .FlatStyle = FlatStyle.Flat
        '        .HeaderText = "Accounts"
        '        .DataPropertyName = "fcDescription"
        '        .Name = "cAccounts"
        '        .DataSource = dtsAccount
        '        .DisplayMember = "fcDescription"
        '        .ValueMember = "fcDescription"
        '        .DropDownWidth = 300
        '        .AutoComplete = True
        '        .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        '        .MaxDropDownItems = 10
        '        .Resizable = DataGridViewTriState.True
        '    End With

        '    With txtAmount
        '        .HeaderText = "Amount"
        '        .Name = "cAmount"
        '        .Width = 115
        '    End With
        '    With txtMemo
        '        .HeaderText = "Memo"
        '        .Name = "cMemo"
        '        .Width = 150
        '    End With

        '    With cboCustomer
        '        .FlatStyle = FlatStyle.Flat
        '        .DropDownWidth = 200
        '        .HeaderText = "Customer/Co-Center"
        '        .DataPropertyName = "fcCustomerName"
        '        .Name = "cCustomer"
        '        .DataSource = dtsCustomer
        '        .DisplayMember = "fcCustomerName"
        '        .ValueMember = "fcCustomerName"
        '        .DropDownWidth = 300
        '        .AutoComplete = True
        '        .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        '        .MaxDropDownItems = 10
        '        .Resizable = DataGridViewTriState.True
        '    End With

        '    With chkBillable
        '        .FlatStyle = FlatStyle.Standard
        '        .HeaderText = "Billable"
        '        .Name = "cBillable"
        '        .Width = 50
        '    End With

        '    .Columns.Add(cboAccounts)
        '    .Columns.Add(txtAmount)
        '    .Columns.Add(txtMemo)
        '    .Columns.Add(cboCustomer)
        '    .Columns.Add(chkBillable)
        'End With
    End Sub

    Private Sub load_acc_lstitems()

        With gItems
            Dim cboItems As New DataGridViewComboBoxColumn
            Dim txtdescription As New DataGridViewTextBoxColumn
            Dim txtquantity As New DataGridViewTextBoxColumn
            Dim txtunit As New DataGridViewTextBoxColumn
            Dim txtcost As New DataGridViewTextBoxColumn
            Dim txtamount As New DataGridViewTextBoxColumn
            Dim cboCustomer As New DataGridViewComboBoxColumn
            Dim chkBillable As New DataGridViewCheckBoxColumn
            Dim txtponum As New DataGridViewTextBoxColumn

            Call m_DisplayItem(cboItems)
            Call m_DisplayCustomer(cboCustomer)

            With cboItems
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 200
                .HeaderText = "Item"
                .Name = "cItems"
                .Width = 100
            End With
            With txtdescription
                .HeaderText = "Description"
                .Name = "cDescriptions"
                .Width = 100
            End With
            With txtquantity
                .HeaderText = "Quantity"
                .Name = "cQuantity"
                .Width = 80
            End With
            With txtunit
                .HeaderText = "U/M"
                .Name = "cUnit"
                .Width = 20
            End With
            With txtcost
                .HeaderText = "Cost"
                .Name = "cCost"
                .Width = 100
            End With
            With txtamount
                .HeaderText = "Amount"
                .Name = "cAmount"
                .Width = 90
            End With
            With cboCustomer
                .FlatStyle = FlatStyle.Flat
                .DropDownWidth = 200
                .HeaderText = "Customer"
                .Name = "cCustomer"
                .Width = 100
            End With
            With chkBillable
                .FlatStyle = FlatStyle.Standard
                .HeaderText = "Billable"
                .Name = "cBillable"
                .Width = 50
            End With
            With txtponum
                .HeaderText = "P.O.#"
                .Name = "cPOnum"
                .Width = 50
            End With

            .Columns.Add(cboItems)
            .Columns.Add(txtdescription)
            .Columns.Add(txtunit)
            .Columns.Add(txtquantity)
            .Columns.Add(txtcost)
            .Columns.Add(txtamount)
            .Columns.Add(cboCustomer)
            .Columns.Add(chkBillable)
            .Columns.Add(txtponum)

        End With
    End Sub

    Private Sub gCalculateExpense()
        Dim xRow As Integer
        Dim xVal As Decimal = 0
        With gExpenses
            If .Item(1, .CurrentRow.Index).Value <> "" Or .Item(1, .CurrentRow.Index).Value <> "0" Then
                .Item(1, .CurrentRow.Index).Value = Format(CDec(.Item(1, .CurrentRow.Index).Value), "##,##0.00")
            Else
                .Item(1, .CurrentRow.Index).Value = "0.00"
            End If
            For xRow = 0 To .Rows.Count - 1
                xVal += .Item(1, xRow).Value
            Next
        End With
        ExpenseAmount = Format(CDec(xVal), "##,##0.00")
        tb_chkExpenses.Text = "Expenses" + "     " + "P " + Format(CDec(xVal), "##,##0.00")
        Call gCheckAmount()
    End Sub

    Private Sub gCalculateItem()
        Dim xRow As Integer
        Dim xVal As Decimal = 0
        With gItems
            If .Item(3, .CurrentRow.Index).Value <> "" Or .Item(3, .CurrentRow.Index).Value <> "0" Then
                If .Item(4, .CurrentRow.Index).Value <> "" Or .Item(4, .CurrentRow.Index).Value <> "0" Then
                    .Item(4, .CurrentRow.Index).Value = Format(CDec(.Item(4, .CurrentRow.Index).Value), "##,##0.00")
                    .Item(5, .CurrentRow.Index).Value = Format(CDec(CDec(.Item(3, .CurrentRow.Index).Value) * CDec(.Item(4, .CurrentRow.Index).Value)), "##,##0.00")
                Else
                    .Item(5, .CurrentRow.Index).Value = "0.00"
                End If
            ElseIf .Item(3, .CurrentRow.Index).Value = "" Or .Item(3, .CurrentRow.Index).Value = "0" Then
                If .Item(4, .CurrentRow.Index).Value = "" Or .Item(4, .CurrentRow.Index).Value = "0" Then
                    .Item(5, .CurrentRow.Index).Value = "0.00"
                Else
                    .Item(4, .CurrentRow.Index).Value = Format(CDec(.Item(4, .CurrentRow.Index).Value), "##,##0.00")
                    .Item(5, .CurrentRow.Index).Value = Format(CDec(CDec(.Item(4, .CurrentRow.Index).Value) * 1), "##,##0.00")
                End If
            End If
            For xRow = 0 To .Rows.Count - 1
                xVal += .Item(5, xRow).Value
            Next
        End With
        ItemAmount = Format(CDec(xVal), "##,##0.00")
        tb_chkItems.Text = "Items" + "     " + "P " + Format(CDec(xVal), "##,##0.00")
        Call gCheckAmount()
    End Sub

    Private Sub gCheckAmount()
        txtChkAmt.Text = Format(CDec(ExpenseAmount + ItemAmount), "##,##0.00")
    End Sub

    Private Function pCalculateEndBalance() As String
        Dim sVal As String
        sVal = Format(CDec(CDec(lblchk_endingbalance.Text) - CDec(txtChkAmt.Text)), "##,##0.00")
        Return sVal
    End Function

    'Private Sub gLoadHeaderforPrinting()
    '    With Me.grdLoadExpense
    '        If .CurrentRow Then
    '    End With
    'End Sub

    Private Sub pSaveEvent()
        gCheckID = ""
        Dim sVal() As String
        Dim cVal() As String
        Dim sMsg As String
        sVal = Split(cbochk_backaccount.SelectedItem, "-")
        gTrans.gCalculateEndingBalance(pCalculateEndBalance(), sVal(0), Me)
        cVal = Split(Me.cbochk_payorder.SelectedItem, "-")
        gAccountDesc = sVal(0)
        gCheckID = Guid.NewGuid.ToString
        sMsg = gTrans.gSaveCheck(gCheckID, getAccountID(sVal(0)), _
                            m_GetNameID(cVal(0), cVal(1)), lblchk_date.Text, _
                            txtChkAmt.Text, lblchk_amountinwords.Text, txtAddress.Text, lblchk_payorder.Text, _
                            txtMemo.Text)
        MessageBox.Show(sMsg, "CLICKSOFTWARE@Accounting :" + Me.Text)
    End Sub

    Private Sub pSave_gExpense()
        If tb_chkExpenses.Text <> "Expenses     P 0.00" Then
            Dim sAccount() As String
            Dim sNames() As String
            Dim x As Integer
            With Me.gExpenses
                Dim xRow As Integer
                For xRow = 0 To .Rows.Count - 1
                    If .Item(0, xRow).Value <> "" Then
                        Dim sAcnt As String = ""
                        Dim sMemo As String = ""
                        Dim sAmount As Long = 0
                        Dim sName As String = ""
                        Dim bBillable As Boolean = False
                        If gTrans.gmkDefaultValues(xRow, 0, gExpenses) <> Nothing Then
                            sAcnt = .Rows(xRow).Cells("cAccounts").Value
                            sAccount = Split(sAcnt, "-")
                            sAcnt = m_GetAccountID(sAccount(0))
                        End If
                        If gTrans.gmkDefaultValues(xRow, 1, gExpenses) <> Nothing Then
                            sAmount = .Rows(xRow).Cells("cAmount").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 2, gExpenses) <> Nothing Then
                            sMemo = .Rows(xRow).Cells("cMemo").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 3, gExpenses) <> Nothing Then
                            sName = .Rows(xRow).Cells("cCustomer").Value
                            sNames = Split(sName, "-")
                            sName = m_GetNameID(sNames(0), sNames(1))
                        End If
                        If gTrans.gmkDefaultValues(xRow, 4, gExpenses) <> Nothing Then
                            bBillable = .Rows(xRow).Cells("cBillable").Value
                            If bBillable = False Then
                                x = 0
                            Else
                                x = 1
                            End If
                        End If
                        Call gTrans.gSaveCheckExpenses(gCheckID, sAcnt, sAmount, sMemo, sName, x, Me)
                    End If
                Next
            End With
        End If
    End Sub

    Private Sub pSave_gItems()
        If tb_chkExpenses.Text <> "Items     P 0.00" Then
            Dim sItems() As String
            Dim sNames() As String
            Dim x As Integer
            With Me.gItems
                Dim xRow As Integer
                For xRow = 0 To .Rows.Count - 1
                    If .Item(0, xRow).Value <> "" Then
                        Dim sItem As String = ""
                        Dim sDescription As String = ""
                        Dim sQty As String = ""
                        Dim sUnit As Integer = 0
                        Dim sCost As Long = 0
                        Dim sAmount As Long = 0
                        Dim sName As String = ""
                        Dim bBillable As Boolean = False
                        Dim sPONumber As String = ""
                        Dim sPOID As String = ""

                        If gTrans.gmkDefaultValues(xRow, 0, gItems) <> Nothing Then
                            sItem = .Rows(xRow).Cells("cItems").Value
                            sItems = Split(sItem, "-")
                            sItem = m_GetItemByName(sItems(0))
                        End If
                        If gTrans.gmkDefaultValues(xRow, 1, gItems) <> Nothing Then
                            sDescription = .Rows(xRow).Cells("cDescriptions").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 2, gItems) <> Nothing Then
                            sQty = .Rows(xRow).Cells("cQuantity").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 3, gItems) <> Nothing Then
                            sUnit = .Rows(xRow).Cells("cUnit").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 4, gItems) <> Nothing Then
                            sCost = .Rows(xRow).Cells("cCost").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 5, gItems) <> Nothing Then
                            sAmount = .Rows(xRow).Cells("cAmount").Value
                        End If
                        If gTrans.gmkDefaultValues(xRow, 6, gItems) <> Nothing Then
                            sName = .Rows(xRow).Cells("cCustomer").Value
                            sNames = Split(sName, "-")
                            sName = m_GetNameID(sNames(0), sNames(1))
                        End If
                        If gTrans.gmkDefaultValues(xRow, 7, gItems) <> Nothing Then
                            bBillable = .Rows(xRow).Cells("cBillable").Value
                            If bBillable = False Then
                                x = 0
                            Else
                                x = 1
                            End If
                        End If
                        If gTrans.gmkDefaultValues(xRow, 8, gItems) <> Nothing Then
                            sPONumber = .Rows(xRow).Cells("cPOnum").Value
                            sPOID = m_GetPO_byPONumber(sPONumber)
                        End If

                        Call gTrans.gSaveCheckItems(gCheckID, sItem, sDescription, _
                                                    sQty, sUnit, sCost, sAmount, sName, _
                                                    x, sPOID, Me)
                    End If
                Next
            End With
        End If
    End Sub

    Private Sub clearform()
        If Me.grdLoadExpense.Visible = False Then
            lblchk_date.Text = "mm/dd/yyyy"
            lblchk_payorder.Text = ""
            lblchk_amountinwords.Text = ""
            lblchk_amountinwords.Text = "Amount in Words"
            txtChkAmt.Text = "0.00"
            txtAddress.Text = ""
            txtMemo.Text = ""
            With Me.gExpenses
                .Rows.Clear()
                .Columns.Clear()
            End With
            With Me.gItems
                .Rows.Clear()
                .Columns.Clear()
            End With
            Call load_acc_lstexpenses()
            Call load_acc_lstitems()
        ElseIf Me.grdLoadExpense.Visible = True Then
            With Me.grdLoadExpense
                .Columns.Clear()
            End With
            With Me.gExpenses
                .Rows.Clear()
                .Columns.Clear()
            End With
            With Me.gItems
                .Rows.Clear()
                .Columns.Clear()
            End With
            grdLoadExpense.Visible = False
            gExpenses.Visible = True
            Call load_acc_lstexpenses()
            Call load_acc_lstitems()
        End If
    End Sub

#End Region

    
    Private Sub FindToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FindToolStripMenuItem.Click
        frm_acc_find.ShowDialog()
    End Sub

    Private Function calculate_amount()
        With grdLoadExpense
            Dim xVal As Decimal = 0
            Dim xRow As Integer
            For xRow = 0 To .Rows.Count - 1
                If .Rows(xRow).Cells(1).Value = True And .Rows(xRow).Cells(1).Value IsNot Nothing Then
                    xVal += .Rows(xRow).Cells(6).Value
                End If
            Next
            txtChkAmt.Text = xVal
            'If .CurrentRow.Cells(3).Value.ToString IsNot Nothing Then
            '    cbochk_payorder.SelectedItem = .CurrentRow.Cells(3).Value.ToString + " - " + " Supplier"
            'End If

        End With
    End Function

    Private Sub grdLoadExpense_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdLoadExpense.CellContentClick
        'if grdLoadExpense.Item(
        calculate_amount()
    End Sub

    Private Sub grdLoadExpense_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles grdLoadExpense.RowsRemoved
        calculate_amount()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox(cbochk_payorder.SelectedText)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If grdLoadExpense.RowCount <> 0 Then
            calculate_amount()
            btn_chk_Clear.Select()
            grdLoadExpense.Select()
        End If
    End Sub

End Class