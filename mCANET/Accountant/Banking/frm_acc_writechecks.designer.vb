<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_writechecks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_writechecks))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_chk_previous = New System.Windows.Forms.ToolStripButton
        Me.ts_chk_next = New System.Windows.Forms.ToolStripButton
        Me.ts_chk_print = New System.Windows.Forms.ToolStripDropDownButton
        Me.msp_accChekprint = New System.Windows.Forms.ToolStripMenuItem
        Me.msp_accBtchprint = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_chk_find = New System.Windows.Forms.ToolStripButton
        Me.ts_chk_journal = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cbochk_backaccount = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblchk_endingbalance = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblchk_payorder = New System.Windows.Forms.TextBox
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.txtChkAmt = New System.Windows.Forms.TextBox
        Me.cbochk_payorder = New System.Windows.Forms.ComboBox
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblchk_amountinwords = New System.Windows.Forms.Label
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.lblchk_date = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label6 = New System.Windows.Forms.Label
        Me.dtechk_checkdate = New System.Windows.Forms.DateTimePicker
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Lbltemprefno = New System.Windows.Forms.Label
        Me.cTExpenses = New System.Windows.Forms.TabControl
        Me.tb_chkExpenses = New System.Windows.Forms.TabPage
        Me.grdLoadExpense = New System.Windows.Forms.DataGridView
        Me.gExpenses = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tb_chkItems = New System.Windows.Forms.TabPage
        Me.gItems = New System.Windows.Forms.DataGridView
        Me.btn_chk_showPO = New System.Windows.Forms.Button
        Me.btn_chk_receiveAll = New System.Windows.Forms.Button
        Me.btn_chk_selectPO = New System.Windows.Forms.Button
        Me.btn_chk_clearSplit = New System.Windows.Forms.Button
        Me.btn_chk_Recalculate = New System.Windows.Forms.Button
        Me.btn_chk_saveClose = New System.Windows.Forms.Button
        Me.btn_chk_saveNew = New System.Windows.Forms.Button
        Me.btn_chk_Clear = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FindToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.cTExpenses.SuspendLayout()
        Me.tb_chkExpenses.SuspendLayout()
        CType(Me.grdLoadExpense, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gExpenses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tb_chkItems.SuspendLayout()
        CType(Me.gItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_chk_previous, Me.ts_chk_next, Me.ts_chk_print, Me.ToolStripSeparator2, Me.ts_chk_find, Me.ts_chk_journal})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(811, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_chk_previous
        '
        Me.ts_chk_previous.Image = CType(resources.GetObject("ts_chk_previous.Image"), System.Drawing.Image)
        Me.ts_chk_previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_chk_previous.Name = "ts_chk_previous"
        Me.ts_chk_previous.Size = New System.Drawing.Size(84, 22)
        Me.ts_chk_previous.Text = "Previous"
        '
        'ts_chk_next
        '
        Me.ts_chk_next.Image = CType(resources.GetObject("ts_chk_next.Image"), System.Drawing.Image)
        Me.ts_chk_next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_chk_next.Name = "ts_chk_next"
        Me.ts_chk_next.Size = New System.Drawing.Size(57, 22)
        Me.ts_chk_next.Text = "Next"
        '
        'ts_chk_print
        '
        Me.ts_chk_print.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.msp_accChekprint, Me.msp_accBtchprint})
        Me.ts_chk_print.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.ts_chk_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_chk_print.Name = "ts_chk_print"
        Me.ts_chk_print.Size = New System.Drawing.Size(67, 22)
        Me.ts_chk_print.Text = "Print"
        '
        'msp_accChekprint
        '
        Me.msp_accChekprint.Name = "msp_accChekprint"
        Me.msp_accChekprint.Size = New System.Drawing.Size(146, 22)
        Me.msp_accChekprint.Text = "Preview CV"
        '
        'msp_accBtchprint
        '
        Me.msp_accBtchprint.Name = "msp_accBtchprint"
        Me.msp_accBtchprint.Size = New System.Drawing.Size(146, 22)
        Me.msp_accBtchprint.Text = "Batch Print"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ts_chk_find
        '
        Me.ts_chk_find.Image = CType(resources.GetObject("ts_chk_find.Image"), System.Drawing.Image)
        Me.ts_chk_find.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_chk_find.Name = "ts_chk_find"
        Me.ts_chk_find.Size = New System.Drawing.Size(55, 22)
        Me.ts_chk_find.Text = "Find"
        '
        'ts_chk_journal
        '
        Me.ts_chk_journal.Image = CType(resources.GetObject("ts_chk_journal.Image"), System.Drawing.Image)
        Me.ts_chk_journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_chk_journal.Name = "ts_chk_journal"
        Me.ts_chk_journal.Size = New System.Drawing.Size(75, 22)
        Me.ts_chk_journal.Text = "Journal"
        Me.ts_chk_journal.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Bank Account"
        '
        'cbochk_backaccount
        '
        Me.cbochk_backaccount.FormattingEnabled = True
        Me.cbochk_backaccount.Location = New System.Drawing.Point(107, 36)
        Me.cbochk_backaccount.Name = "cbochk_backaccount"
        Me.cbochk_backaccount.Size = New System.Drawing.Size(244, 21)
        Me.cbochk_backaccount.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(419, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Ending Balance : "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblchk_endingbalance
        '
        Me.lblchk_endingbalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblchk_endingbalance.AutoSize = True
        Me.lblchk_endingbalance.Location = New System.Drawing.Point(524, 41)
        Me.lblchk_endingbalance.Name = "lblchk_endingbalance"
        Me.lblchk_endingbalance.Size = New System.Drawing.Size(51, 13)
        Me.lblchk_endingbalance.TabIndex = 4
        Me.lblchk_endingbalance.Text = "Amount"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblchk_payorder)
        Me.Panel1.Controls.Add(Me.txtMemo)
        Me.Panel1.Controls.Add(Me.txtAddress)
        Me.Panel1.Controls.Add(Me.txtChkAmt)
        Me.Panel1.Controls.Add(Me.cbochk_payorder)
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.lblchk_amountinwords)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.lblchk_date)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.dtechk_checkdate)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Lbltemprefno)
        Me.Panel1.Location = New System.Drawing.Point(14, 69)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(783, 165)
        Me.Panel1.TabIndex = 5
        '
        'lblchk_payorder
        '
        Me.lblchk_payorder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblchk_payorder.BackColor = System.Drawing.Color.White
        Me.lblchk_payorder.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lblchk_payorder.Location = New System.Drawing.Point(134, 43)
        Me.lblchk_payorder.Name = "lblchk_payorder"
        Me.lblchk_payorder.Size = New System.Drawing.Size(357, 14)
        Me.lblchk_payorder.TabIndex = 20
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.BackColor = System.Drawing.Color.White
        Me.txtMemo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMemo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMemo.Location = New System.Drawing.Point(55, 135)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(684, 14)
        Me.txtMemo.TabIndex = 4
        '
        'txtAddress
        '
        Me.txtAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAddress.BackColor = System.Drawing.Color.White
        Me.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(69, 93)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(703, 34)
        Me.txtAddress.TabIndex = 3
        '
        'txtChkAmt
        '
        Me.txtChkAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtChkAmt.BackColor = System.Drawing.Color.White
        Me.txtChkAmt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtChkAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChkAmt.Location = New System.Drawing.Point(623, 42)
        Me.txtChkAmt.Name = "txtChkAmt"
        Me.txtChkAmt.Size = New System.Drawing.Size(149, 14)
        Me.txtChkAmt.TabIndex = 2
        Me.txtChkAmt.Text = "0.00"
        '
        'cbochk_payorder
        '
        Me.cbochk_payorder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbochk_payorder.BackColor = System.Drawing.Color.WhiteSmoke
        Me.cbochk_payorder.DropDownWidth = 200
        Me.cbochk_payorder.FormattingEnabled = True
        Me.cbochk_payorder.Location = New System.Drawing.Point(532, 38)
        Me.cbochk_payorder.Name = "cbochk_payorder"
        Me.cbochk_payorder.Size = New System.Drawing.Size(19, 21)
        Me.cbochk_payorder.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Location = New System.Drawing.Point(51, 149)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(721, 1)
        Me.Panel6.TabIndex = 19
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 135)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 13)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Memo"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 94)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(53, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Address"
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(734, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 13)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Pesos"
        '
        'lblchk_amountinwords
        '
        Me.lblchk_amountinwords.AutoSize = True
        Me.lblchk_amountinwords.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblchk_amountinwords.Location = New System.Drawing.Point(16, 67)
        Me.lblchk_amountinwords.Name = "lblchk_amountinwords"
        Me.lblchk_amountinwords.Size = New System.Drawing.Size(104, 13)
        Me.lblchk_amountinwords.TabIndex = 14
        Me.lblchk_amountinwords.Text = "Amount in Words"
        '
        'Panel5
        '
        Me.Panel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Location = New System.Drawing.Point(16, 83)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(723, 1)
        Me.Panel5.TabIndex = 13
        '
        'lblchk_date
        '
        Me.lblchk_date.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblchk_date.AutoSize = True
        Me.lblchk_date.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblchk_date.Location = New System.Drawing.Point(623, 11)
        Me.lblchk_date.Name = "lblchk_date"
        Me.lblchk_date.Size = New System.Drawing.Size(83, 13)
        Me.lblchk_date.TabIndex = 12
        Me.lblchk_date.Text = "mm/dd/yyyy"
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Location = New System.Drawing.Point(621, 58)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(151, 1)
        Me.Panel4.TabIndex = 9
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Location = New System.Drawing.Point(619, 27)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(151, 1)
        Me.Panel3.TabIndex = 8
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(132, 59)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(420, 1)
        Me.Panel2.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(607, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "P"
        '
        'dtechk_checkdate
        '
        Me.dtechk_checkdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtechk_checkdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtechk_checkdate.Location = New System.Drawing.Point(750, 7)
        Me.dtechk_checkdate.Name = "dtechk_checkdate"
        Me.dtechk_checkdate.Size = New System.Drawing.Size(19, 21)
        Me.dtechk_checkdate.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(574, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Date "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(115, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Pay to the order of"
        '
        'Lbltemprefno
        '
        Me.Lbltemprefno.AutoSize = True
        Me.Lbltemprefno.Location = New System.Drawing.Point(516, 8)
        Me.Lbltemprefno.Name = "Lbltemprefno"
        Me.Lbltemprefno.Size = New System.Drawing.Size(0, 13)
        Me.Lbltemprefno.TabIndex = 21
        Me.Lbltemprefno.Visible = False
        '
        'cTExpenses
        '
        Me.cTExpenses.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cTExpenses.Controls.Add(Me.tb_chkExpenses)
        Me.cTExpenses.Controls.Add(Me.tb_chkItems)
        Me.cTExpenses.Location = New System.Drawing.Point(13, 239)
        Me.cTExpenses.Name = "cTExpenses"
        Me.cTExpenses.SelectedIndex = 0
        Me.cTExpenses.Size = New System.Drawing.Size(788, 181)
        Me.cTExpenses.TabIndex = 6
        '
        'tb_chkExpenses
        '
        Me.tb_chkExpenses.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.tb_chkExpenses.Controls.Add(Me.grdLoadExpense)
        Me.tb_chkExpenses.Controls.Add(Me.gExpenses)
        Me.tb_chkExpenses.Location = New System.Drawing.Point(4, 22)
        Me.tb_chkExpenses.Name = "tb_chkExpenses"
        Me.tb_chkExpenses.Padding = New System.Windows.Forms.Padding(3)
        Me.tb_chkExpenses.Size = New System.Drawing.Size(780, 155)
        Me.tb_chkExpenses.TabIndex = 0
        Me.tb_chkExpenses.Text = "Expenses     P 0.00"
        '
        'grdLoadExpense
        '
        Me.grdLoadExpense.AllowUserToAddRows = False
        Me.grdLoadExpense.AllowUserToDeleteRows = False
        Me.grdLoadExpense.AllowUserToResizeRows = False
        Me.grdLoadExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdLoadExpense.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdLoadExpense.GridColor = System.Drawing.Color.Black
        Me.grdLoadExpense.Location = New System.Drawing.Point(3, 3)
        Me.grdLoadExpense.Name = "grdLoadExpense"
        Me.grdLoadExpense.Size = New System.Drawing.Size(774, 149)
        Me.grdLoadExpense.TabIndex = 1
        '
        'gExpenses
        '
        Me.gExpenses.AllowUserToAddRows = False
        Me.gExpenses.AllowUserToDeleteRows = False
        Me.gExpenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gExpenses.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8})
        Me.gExpenses.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gExpenses.GridColor = System.Drawing.Color.Black
        Me.gExpenses.Location = New System.Drawing.Point(3, 3)
        Me.gExpenses.Name = "gExpenses"
        Me.gExpenses.ReadOnly = True
        Me.gExpenses.Size = New System.Drawing.Size(774, 149)
        Me.gExpenses.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Pay"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 50
        '
        'Column2
        '
        Me.Column2.HeaderText = "Due Date"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 80
        '
        'Column3
        '
        Me.Column3.HeaderText = "Supplier"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Ref.no"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column5
        '
        Me.Column5.HeaderText = "Bill Amt"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 80
        '
        'Column6
        '
        Me.Column6.HeaderText = "Amt Due"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 80
        '
        'Column7
        '
        Me.Column7.HeaderText = "Disc Date"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 80
        '
        'Column8
        '
        Me.Column8.HeaderText = "Amt Paid"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 80
        '
        'tb_chkItems
        '
        Me.tb_chkItems.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.tb_chkItems.Controls.Add(Me.gItems)
        Me.tb_chkItems.Controls.Add(Me.btn_chk_showPO)
        Me.tb_chkItems.Controls.Add(Me.btn_chk_receiveAll)
        Me.tb_chkItems.Controls.Add(Me.btn_chk_selectPO)
        Me.tb_chkItems.Location = New System.Drawing.Point(4, 22)
        Me.tb_chkItems.Name = "tb_chkItems"
        Me.tb_chkItems.Padding = New System.Windows.Forms.Padding(3)
        Me.tb_chkItems.Size = New System.Drawing.Size(780, 155)
        Me.tb_chkItems.TabIndex = 1
        Me.tb_chkItems.Text = "Items     P 0.00"
        '
        'gItems
        '
        Me.gItems.AllowUserToOrderColumns = True
        Me.gItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gItems.GridColor = System.Drawing.Color.Black
        Me.gItems.Location = New System.Drawing.Point(5, 6)
        Me.gItems.Name = "gItems"
        Me.gItems.Size = New System.Drawing.Size(772, 116)
        Me.gItems.TabIndex = 13
        '
        'btn_chk_showPO
        '
        Me.btn_chk_showPO.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_showPO.Location = New System.Drawing.Point(171, 128)
        Me.btn_chk_showPO.Name = "btn_chk_showPO"
        Me.btn_chk_showPO.Size = New System.Drawing.Size(79, 23)
        Me.btn_chk_showPO.TabIndex = 2
        Me.btn_chk_showPO.Text = "Show PO"
        Me.btn_chk_showPO.UseVisualStyleBackColor = True
        '
        'btn_chk_receiveAll
        '
        Me.btn_chk_receiveAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_receiveAll.Location = New System.Drawing.Point(89, 128)
        Me.btn_chk_receiveAll.Name = "btn_chk_receiveAll"
        Me.btn_chk_receiveAll.Size = New System.Drawing.Size(79, 23)
        Me.btn_chk_receiveAll.TabIndex = 1
        Me.btn_chk_receiveAll.Text = "Receive All"
        Me.btn_chk_receiveAll.UseVisualStyleBackColor = True
        '
        'btn_chk_selectPO
        '
        Me.btn_chk_selectPO.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_selectPO.Location = New System.Drawing.Point(6, 128)
        Me.btn_chk_selectPO.Name = "btn_chk_selectPO"
        Me.btn_chk_selectPO.Size = New System.Drawing.Size(79, 23)
        Me.btn_chk_selectPO.TabIndex = 0
        Me.btn_chk_selectPO.Text = "Select PO"
        Me.btn_chk_selectPO.UseVisualStyleBackColor = True
        '
        'btn_chk_clearSplit
        '
        Me.btn_chk_clearSplit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_clearSplit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_chk_clearSplit.Location = New System.Drawing.Point(13, 427)
        Me.btn_chk_clearSplit.Name = "btn_chk_clearSplit"
        Me.btn_chk_clearSplit.Size = New System.Drawing.Size(108, 33)
        Me.btn_chk_clearSplit.TabIndex = 10
        Me.btn_chk_clearSplit.Text = "Clear Splits"
        Me.btn_chk_clearSplit.UseVisualStyleBackColor = True
        Me.btn_chk_clearSplit.Visible = False
        '
        'btn_chk_Recalculate
        '
        Me.btn_chk_Recalculate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_Recalculate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_chk_Recalculate.Image = CType(resources.GetObject("btn_chk_Recalculate.Image"), System.Drawing.Image)
        Me.btn_chk_Recalculate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_chk_Recalculate.Location = New System.Drawing.Point(127, 427)
        Me.btn_chk_Recalculate.Name = "btn_chk_Recalculate"
        Me.btn_chk_Recalculate.Size = New System.Drawing.Size(113, 33)
        Me.btn_chk_Recalculate.TabIndex = 11
        Me.btn_chk_Recalculate.Text = "Recalculate"
        Me.btn_chk_Recalculate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_chk_Recalculate.UseVisualStyleBackColor = True
        Me.btn_chk_Recalculate.Visible = False
        '
        'btn_chk_saveClose
        '
        Me.btn_chk_saveClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_saveClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_chk_saveClose.Image = CType(resources.GetObject("btn_chk_saveClose.Image"), System.Drawing.Image)
        Me.btn_chk_saveClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_chk_saveClose.Location = New System.Drawing.Point(425, 427)
        Me.btn_chk_saveClose.Name = "btn_chk_saveClose"
        Me.btn_chk_saveClose.Size = New System.Drawing.Size(122, 33)
        Me.btn_chk_saveClose.TabIndex = 7
        Me.btn_chk_saveClose.Text = "Save && Close"
        Me.btn_chk_saveClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_chk_saveClose.UseVisualStyleBackColor = True
        Me.btn_chk_saveClose.Visible = False
        '
        'btn_chk_saveNew
        '
        Me.btn_chk_saveNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_saveNew.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_chk_saveNew.Image = CType(resources.GetObject("btn_chk_saveNew.Image"), System.Drawing.Image)
        Me.btn_chk_saveNew.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_chk_saveNew.Location = New System.Drawing.Point(553, 427)
        Me.btn_chk_saveNew.Name = "btn_chk_saveNew"
        Me.btn_chk_saveNew.Size = New System.Drawing.Size(122, 33)
        Me.btn_chk_saveNew.TabIndex = 8
        Me.btn_chk_saveNew.Text = "Save && New"
        Me.btn_chk_saveNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_chk_saveNew.UseVisualStyleBackColor = True
        Me.btn_chk_saveNew.Visible = False
        '
        'btn_chk_Clear
        '
        Me.btn_chk_Clear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_chk_Clear.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_chk_Clear.Image = CType(resources.GetObject("btn_chk_Clear.Image"), System.Drawing.Image)
        Me.btn_chk_Clear.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_chk_Clear.Location = New System.Drawing.Point(681, 427)
        Me.btn_chk_Clear.Name = "btn_chk_Clear"
        Me.btn_chk_Clear.Size = New System.Drawing.Size(120, 33)
        Me.btn_chk_Clear.TabIndex = 9
        Me.btn_chk_Clear.Text = "Clear"
        Me.btn_chk_Clear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_chk_Clear.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FindToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(811, 24)
        Me.MenuStrip1.TabIndex = 12
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        'FindToolStripMenuItem
        '
        Me.FindToolStripMenuItem.Name = "FindToolStripMenuItem"
        Me.FindToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.FindToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
        Me.FindToolStripMenuItem.Text = "find"
        '
        'Timer1
        '
        '
        'frm_acc_writechecks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(811, 467)
        Me.Controls.Add(Me.btn_chk_Clear)
        Me.Controls.Add(Me.btn_chk_saveNew)
        Me.Controls.Add(Me.btn_chk_saveClose)
        Me.Controls.Add(Me.btn_chk_Recalculate)
        Me.Controls.Add(Me.btn_chk_clearSplit)
        Me.Controls.Add(Me.cTExpenses)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblchk_endingbalance)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbochk_backaccount)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(817, 499)
        Me.Name = "frm_acc_writechecks"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Write Checks"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.cTExpenses.ResumeLayout(False)
        Me.tb_chkExpenses.ResumeLayout(False)
        CType(Me.grdLoadExpense, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gExpenses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tb_chkItems.ResumeLayout(False)
        CType(Me.gItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_chk_previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_chk_next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_chk_print As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_chk_find As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_chk_journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents msp_accChekprint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msp_accBtchprint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbochk_backaccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblchk_endingbalance As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblchk_amountinwords As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblchk_date As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cTExpenses As System.Windows.Forms.TabControl
    Friend WithEvents tb_chkExpenses As System.Windows.Forms.TabPage
    Friend WithEvents tb_chkItems As System.Windows.Forms.TabPage
    Friend WithEvents btn_chk_showPO As System.Windows.Forms.Button
    Friend WithEvents btn_chk_receiveAll As System.Windows.Forms.Button
    Friend WithEvents btn_chk_selectPO As System.Windows.Forms.Button
    Friend WithEvents btn_chk_clearSplit As System.Windows.Forms.Button
    Friend WithEvents btn_chk_Recalculate As System.Windows.Forms.Button
    Friend WithEvents btn_chk_saveClose As System.Windows.Forms.Button
    Friend WithEvents btn_chk_saveNew As System.Windows.Forms.Button
    Friend WithEvents btn_chk_Clear As System.Windows.Forms.Button
    Friend WithEvents cbochk_payorder As System.Windows.Forms.ComboBox
    Friend WithEvents txtChkAmt As System.Windows.Forms.TextBox
    Friend WithEvents dtechk_checkdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gExpenses As System.Windows.Forms.DataGridView
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents gItems As System.Windows.Forms.DataGridView
    Friend WithEvents lblchk_payorder As System.Windows.Forms.TextBox
    Friend WithEvents grdLoadExpense As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lbltemprefno As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FindToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
