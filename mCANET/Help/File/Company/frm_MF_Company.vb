Imports System.Data.SqlClient
Imports WPM_EDCRYPT
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data

Public Class frm_MF_Company
    Private Gcon As New Clsappconfiguration
    Private gFuncs As New clsTransactionFunctions
    Private gMaster As New modMasterFile
    Private gEncrypt As New WPMED
    Private coid As String

    Private Sub btnCreateSegment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frm_segment_newEdit.ShowDialog()
    End Sub
    Private Sub frm_MF_Company_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        cboconame.Select()
        If Me.Text = "Open Previous Company" Then
            Me.AcceptButton = Me.btnClear
        Else
            Me.AcceptButton = Me.btnSave
        End If
    End Sub
    Private Sub frm_MF_Company_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gFuncs.gClearFormTxt(Me)
        cboconame.Enabled = True
        btnSegmnt.Enabled = False
        Call loadYear()
    End Sub
    Private Sub loadYear()
        With cbofiscalYr
            .Items.Clear()
            Dim xYear As Integer = Now.Year
            Dim xTmp As Integer = 20
            Dim xCnt As Integer
            For xCnt = 1 To xTmp - 10
                .Items.Add(CStr(xYear - xCnt))
            Next
            For xCnt = 1 To xTmp
                .Items.Add(CStr(xYear + xCnt))
            Next
            .Items.Add(CStr(Now.Year))
            .Sorted = True
            cbofiscalYr.SelectedItem = CStr(Now.Year)
        End With
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim WarningMsg As DialogResult
        If Me.Text = "New Company" Then
            If cboconame.Text <> "" Then
                WarningMsg = MessageBox.Show("All provided data will be lost if you stop this session." + vbCr + "Are you sure you want to stop this session?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
                If WarningMsg = Windows.Forms.DialogResult.OK Then
                    Me.Close()
                Else
                    Me.DialogResult = DialogResult.None
                End If
            End If
            'Me.btnSegment2.Visible = False
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim coid As String

        If GetCoId <> 0 Then
            coid = GetCoId
        Else
            coid = Guid.NewGuid.ToString
        End If
        If cboconame.Text = "" Then
            MessageBox.Show("You must provide a name of your project.", "UCORESOFTWARE @ Accounting System:" + Me.Text)

        ElseIf txtadminpassword.Text = "" Then
            MessageBox.Show("Please Enter your Password.", "UCORESOFTWARE @ Accounting System:" + Me.Text)
        ElseIf txtadminpassword.Text <> txtcorepass.Text Then
            MessageBox.Show("The password you entered does not match the confirmation. Please re-enter your project password", "UCORESOFTWARE @ Accounting System:" + Me.Text)
        Else
            frmMain.lblCompanyName.Text = cboconame.Text
            gCompanyName = cboconame.Text
            gMaster.saveCompany(coid, cboconame.Text, txtcolegal.Text, txtcotaxid.Text, txtcostadd.Text, txtcocityadd.Text, txtcozipadd.Text, txtcophone.Text,
                                txtcofax.Text, txtcoemail.Text, txtcosite.Text, cbocoindustry.Text, cbocororg.Text, cbofiscalYr.SelectedItem, cbofiscalmonth.SelectedItem,
                                gEncrypt.gEncryptString(txtadminpassword.Text), gCompanyName, gClassingStatus)
            MsgBox("Project Saved", vbInformation, "Saved Complete")
            gMaster.saveCompany_AccountType(coid)
            AuditTrail_Save("PROJECTS", "Created new project " & cboconame.Text & "'")
            Me.Close()
        End If
    End Sub
    Public Property GetCoId() As Integer
        Get
            Return coid
        End Get
        Set(ByVal value As Integer)
            coid = value
        End Set

    End Property



    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        If btnClear.Text = "Clear" Then
            gFuncs.gClearFormTxt(Me)
        Else
            If gOpenCompany <> 0 Then
                gCompanyName = ""
                gCompanyName = cboconame.Text
                frmMain.lblCompanyName.Text = gCompanyName
                SetAccesibility()
                AuditTrail_Save("PROJECTS", "Open project '" & gCompanyName & "'")
                mainSideMenuBar(frmMain.axbar, frmMain.cpics, frmMain.cpics1, frmMain.cpics2, frmMain.cpics3, frmMain.cpics4, frmMain.cpics5)
                frmMain.ms_file_openCompany.Enabled = False
                Me.Close()
            Else
                cboconame.Text = ""
                MessageBox.Show("You have to choose a project to use the UCORESOFTWARE Accounting System.", "UCORESOFTWARE @ Accounting System:" + Me.Text)
            End If
        End If
    End Sub
    Private Sub btnSegmnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSegmnt.Click
        frm_segment_newEdit.ShowDialog()
    End Sub
    Private Sub cboconame_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboconame.SelectedIndexChanged
        gCompanyName = ""
        If cboconame.SelectedItem <> "" Then
            gCompanyName = cboconame.SelectedItem
            'If gSegmentValidation() = False Then
            '    'Me.btnSegment2.Visible = False
            'Else
            '    'Me.btnSegment2.Visible = True
            'End If
            frm_MF_copassword.ShowDialog()
        End If
    End Sub
    Private Sub txtcoemail_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtcoemail.Validating
        'This is to validate if entered Email address is correct 10/20/2008
        Dim email As TextBox = DirectCast(sender, TextBox)
        validateEntry(email, e.Cancel)
    End Sub
    Private Sub validateEntry(ByVal valid As TextBox, ByRef cancel_event As Boolean)
        If valid.Text.Length = 0 Then
            ' Allow a zero-length string.
            cancel_event = False
        Else
            cancel_event = Not (Regex.IsMatch(valid.Text, "[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z_+])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9}$"))
        End If
        ' See if we�re going to cancel the event.
        If cancel_event Then
            ' Invalid. Set an error.            
            ErrorProvider1.SetError(valid, "Please Enter a valid Email Address")
        Else
            ' Valid. Clear any error.
            ErrorProvider1.SetError(valid, "")
        End If
    End Sub


End Class