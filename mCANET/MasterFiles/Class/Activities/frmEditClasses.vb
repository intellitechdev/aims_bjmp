Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: July 07, 2009                      ###
'                   ### Website: www.ionflux.site50.net                  ###
'                   ########################################################

Public Class frmEditClasses
#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Public SelectedClass As String
#End Region
#Region "Sub and Functions"
    Private Sub LoadClassesDetails(ByVal RefNo As String)
        Dim DBName As String
        Dim xClassName(0) As String
        Dim DBParent As String
        Dim xClassParent(0) As String
        Dim sSqlCmd As String = "SELECT classname, ClassParentPath, ClassDescription FROM dbo.mClasses WHERE ClassRefnumber='" & RefNo & _
                                    "' AND fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DBName = rd.Item(0).ToString
                    xClassName = Split(DBName, "���")
                    TxtClassName.Text = xClassName(0)

                    DBParent = rd.Item(1).ToString
                    If DBParent <> "" Then
                        xClassParent = Split(DBParent, "���")
                        txtClassParent.Text = xClassParent(0)
                        TxtRefno.Text = xClassParent(1)
                    Else
                        txtClassParent.Text = "Top Level"
                        TxtRefno.Text = ""
                    End If
                    txtDescription.Text = rd.Item(2).ToString
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Function getLevel(ByVal RefNo As String)
        Dim xLevel As String
        Dim sSqlCmd As String = "SELECT classLevel FROM dbo.mClasses WHERE ClassRefnumber='" & _
                                RefNo & "' AND fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xLevel = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        If xLevel = "" Or xLevel Is Nothing Then
            xLevel = 1
        Else
            xLevel += 1
        End If
        Return xLevel
    End Function
    Private Function Getparent(ByVal refno As String)
        Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE ClassRefnumber ='" & _
                                refno & "' AND fxKeyCompany = '" & gCompanyID() & "'"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
            Try
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            Catch ex As Exception
                Return Nothing
            End Try
        End Using
    End Function
#End Region
    Private Sub frmEditClasses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadClassesDetails(SelectedClass)
    End Sub
    Private Sub BtnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOK.Click
        If TxtClassName.Text <> "" Or TxtClassName.Text Is Nothing And txtClassParent.Text <> "" And TxtRefno.Text <> "" Then
            Dim SQLEditClass As String = "UPDATE dbo.mClasses "
            SQLEditClass &= "SET classname = '" & TxtClassName.Text & "���" & SelectedClass & "', "
            If txtClassParent.Text = "Top Level" And TxtRefno.Text = "" Then
                SQLEditClass &= "ClassLevel = '1', "
                SQLEditClass &= "ClassParentPath = '', "
            Else
                SQLEditClass &= "ClassLevel = '" & getLevel(TxtRefno.Text) & "', "
                SQLEditClass &= "ClassParentPath = '" & Getparent(TxtRefno.Text) & "', "
            End If
            SQLEditClass &= "ClassDescription = '" & txtDescription.Text & "'"
            SQLEditClass &= "WHERE ClassRefnumber = '" & SelectedClass & _
                            "' AND fxKeyCompany ='" & gCompanyID() & "'"
            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, SQLEditClass)
                TxtClassName.Text = ""
                txtDescription.Text = ""
                Me.Close()
            Catch ex As Exception
            End Try

            MsgBox("Class successfully update!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access Granted")

        End If
    End Sub
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        FrmBrowseClass.Mode = "EDIT MODE"
        FrmBrowseClass.Data1 = txtClassParent.Text
        FrmBrowseClass.Data2 = TxtRefno.Text
        FrmBrowseClass.ShowDialog()
        txtClassParent.Text = FrmBrowseClass.Data1
        TxtRefno.Text = FrmBrowseClass.Data2
        FrmBrowseClass.Dispose()
    End Sub
    Private Sub txtClassParent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtClassParent.TextChanged
        If txtClassParent.Text = "Top Level" Then
            TxtRefno.Text = ""
        End If
    End Sub
End Class