﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmSelectCheck
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        selectedCheck()
    End Sub

    Private Sub selectedCheck()
        Try
            Me.DialogResult = DialogResult.OK
        Catch ex As Exception
            MsgBox("No Check Found", vbCritical, "Warning!")
            Me.Close()
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchDocNum(txtSearch.Text)
    End Sub
    Private Sub SearchDocNum(ByVal SelNum As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        Dim i As Integer = frmGeneralJournalEntries.grdListofEntries.CurrentRow.Index
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "Check_SearchCheck1", _
                                   New SqlParameter("@FX_CheckNumber", SelNum),
                                   New SqlParameter("@BankName", frmGeneralJournalEntries.grdListofEntries.Item("Bank", i).Value.ToString),
                                   New SqlParameter("@coid", gCompanyID))
        dgvSelectCheck.DataSource = ds.Tables(0)
    End Sub

    Private Sub frmSelectCheck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ListSelNum()
    End Sub
    Private Sub ListSelNum()
        Try
            Dim i As Integer = frmGeneralJournalEntries.grdListofEntries.CurrentRow.Index
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "Check_List",
                                          New SqlParameter("@BankName", frmGeneralJournalEntries.grdListofEntries.Item("Bank", i).Value.ToString),
                                          New SqlParameter("@coid", gCompanyID))
            dgvSelectCheck.DataSource = ds.Tables(0)
        Catch ex As Exception

        End Try
       
    End Sub
End Class