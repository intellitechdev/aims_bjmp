﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmTempRebates
    Dim con As New Clsappconfiguration
    Private Sub txtSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSave.Click
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "spu_Rebate_Insert",
                    New SqlParameter("@fcParticulars", txtParticualrs.Text),
                    New SqlParameter("@fnAmount", txtAmount.Text),
                    New SqlParameter("@fnLoanAmount", txtLAmount.Text),
                    New SqlParameter("@fnRate", txtRate.Text),
                    New SqlParameter("@fnTerm", txtTerm.Text),
                    New SqlParameter("@fnUnpaid", txtUnpaidMos.Text),
                    New SqlParameter("@fnOSBal", txtOSBal.Text),
                    New SqlParameter("@fnrebate", txtRebate.Text))
            MsgBox("Saved!", MsgBoxStyle.Information)
            PrintRebate()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub PrintRebate()
        frmVoucherReport.xTransaction = "CHECK VOUCHER REBATE"
        frmVoucherReport.docnum = frmGeneralJournalEntries.txtGeneralJournalNo.Text
        frmVoucherReport.xTransID = frmGeneralJournalEntries.keyID
        frmVoucherReport.xLoan = frmGeneralJournalEntries.NormalizeValuesInDataGridView(frmGeneralJournalEntries.grdGenJournalDetails, "cLoanRef", 0)
        frmVoucherReport.StartPosition = FormStartPosition.CenterScreen
        frmVoucherReport.Show()
        Me.Close()
    End Sub
End Class