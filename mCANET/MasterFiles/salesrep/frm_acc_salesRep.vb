Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_salesRep
    Private gCon As New Clsappconfiguration
    Private sKeySalesRep As String
    Private sSQLCmdQuery As String = "SELECT * FROM mSalesRep WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property KeySalesRep() As String
        Get
            Return sKeySalesRep
        End Get
        Set(ByVal value As String)
            sKeySalesRep = value
        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_salesRepAddEdit.KeySalesRep = Guid.NewGuid.ToString
        frm_MF_salesRepAddEdit.Text = "New Sales Rep"
        frm_MF_salesRepAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_salesRepAddEdit.KeySalesRep = sKeySalesRep
        frm_MF_salesRepAddEdit.Text = "Edit Sales Rep"
        frm_MF_salesRepAddEdit.Show()
    End Sub

    Private Sub frm_acc_salesRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_SalesRepList(chkIncludeInactive.CheckState, grdSalesRep)
        gridSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_ShowInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Public Sub gridSettings()
        With grdSalesRep
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns.Item(1).HeaderText = "Active"
            .Columns.Item(2).HeaderText = "Initials"
            .Columns.Item(3).HeaderText = "Name"
            .Columns.Item(4).HeaderText = "Type"
        End With

        If chkIncludeInactive.CheckState = CheckState.Checked Then
            grdSalesRep.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdSalesRep.Columns(1).Visible = True
        End If


    End Sub

    Private Sub grdSalesRep_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSalesRep.CellClick
        If Me.grdSalesRep.SelectedCells.Count > 0  Then

            sKeySalesRep = grdSalesRep.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mSalesRep", "fxKeySalesRep", sKeySalesRep) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdSalesRep_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSalesRep.DoubleClick
        If Me.grdSalesRep.SelectedCells.Count > 0 Then
            sKeySalesRep = grdSalesRep.CurrentRow.Cells(0).Value.ToString

            frm_MF_salesRepAddEdit.Text = "Edit Sales Rep"
            frm_MF_salesRepAddEdit.KeySalesRep = sKeySalesRep
            frm_MF_salesRepAddEdit.Show()
        End If
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_SalesRepList(chkIncludeInactive.CheckState, grdSalesRep)
        gridSettings()
    End Sub

    Private Sub salesRepDelete()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mSalesRep WHERE fxKeySalesRep= '" & sKeySalesRep & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        salesRepDelete()
        refreshForm
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_ShowInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_ShowInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ShowInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mSalesRep", "fxKeySalesRep", sKeySalesRep, True)
        Else
            m_mkActive("mSalesRep", "fxKeySalesRep", sKeySalesRep, False)
        End If
        refreshForm()
    End Sub
End Class