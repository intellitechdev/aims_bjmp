Public Class frmviewallbills

    Private Sub frmviewallbills_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadbillslist()
    End Sub

    Private Sub loadbillslist()
        grdViewBills.Columns.Clear()
        With grdViewBills
            Dim cReview As New DataGridViewCheckBoxColumn
            Dim dtBill As DataTable = m_GetViewAllBills(Microsoft.VisualBasic.FormatDateTime(dtefrom.Value, DateFormat.ShortDate), _
                                                            Microsoft.VisualBasic.FormatDateTime(dtUntil.Value, DateFormat.ShortDate), False).Tables(0)
            .DataSource = dtBill.DefaultView
            .Columns(0).Visible = False
            .Columns(1).Width = 50
            .Columns(2).Width = 80
            .Columns(3).Width = 338
            .Columns(4).Width = 100
            .Columns(5).Width = 70
            .Columns(6).Width = 70
            .Columns(7).Width = 65
            .Columns(1).ReadOnly = True
            .Columns(7).ReadOnly = True
        End With
    End Sub

    Private Sub grdViewBills_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdViewBills.CellDoubleClick
        With grdViewBills
            frm_vend_EnterBills.SupplierBill = False
            frm_vend_EnterBills.Mode = 2
            frm_vend_EnterBills.KeyBill = .Item(0, .CurrentRow.Index).Value.ToString
            frm_vend_EnterBills.ShowDialog()
        End With
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        With grdViewBills
            If .RowCount > 0 Then
                If .CurrentRow.Cells(0).Value.ToString IsNot Nothing Then
                    gKeyBills = .CurrentRow.Cells(0).Value.ToString
                    frmaccntvoucherparameters.Show()
                Else
                    MsgBox("Please select a bill to preview...", MsgBoxStyle.Information, Me.Text)
                End If
            End If
        End With
    End Sub

    Private Sub dtefrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtefrom.ValueChanged
        Call loadbillslist()
    End Sub

    Private Sub dtUntil_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtUntil.ValueChanged
        Call loadbillslist()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            With Me.grdViewBills
                Dim xKeyBill As String = .CurrentRow.Cells(0).Value.ToString
                If xKeyBill <> "" Then
                    Call m_deletebills(xKeyBill)
                    Call loadbillslist()
                End If
            End With
        Catch ex As Exception
        End Try
    End Sub

End Class