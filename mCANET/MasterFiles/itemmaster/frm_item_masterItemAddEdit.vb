Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_item_masterItemAddEdit

    Private gCon As New Clsappconfiguration
    Private cls As New clsTransactionFunctions

    Private Function ValidateItemCode(ByVal CodeProvided As String)
        Dim sSqlCodeValidator As String = "SELECT fcItemCode FROM dbo.mItemCode WHERE fcItemCode ='" & _
                                            CodeProvided & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCodeValidator)
                While rd.Read
                    If rd.Item(0).ToString Is Nothing Or rd.Item(0).ToString = "" Then
                        Return "Not Existed"
                    Else
                        Return "Existed"
                    End If
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getItemCode(ByVal fxKeyItem As String)
        Dim sSqlItemCodeFinder As String = "SELECT fcBarCode,fcItemCode FROM dbo.mItemMaster"
        sSqlItemCodeFinder &= " WHERE fxKeyItem ='" & fxKeyItem & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlItemCodeFinder)
                While rd.Read
                    If rd.Item(0).ToString Is Nothing Or rd.Item(0).ToString = "" Then
                        Return rd.Item(1).ToString
                    Else
                        Return rd.Item(0).ToString
                    End If

                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getColorName(ByVal ColorCode As String)
        Dim sSqlColorFinder As String = "SELECT fcColorDescription FROM dbo.mItemColor "
        sSqlColorFinder &= "WHERE fcColorCode = '" & ColorCode & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlColorFinder)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub frm_item_masterItemAddEdit_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        gItemsKeys = ""
        Me.Text = "Add/Edit Item"
        Call cls.gClearFormTxt(Me)

    End Sub

    Private Sub frm_item_newedit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadcomboselections()
        If gItemMasterMode = "New Item" Then
            Call cls.gClearFormTxt(Me)
            gItemsKeys = ""
            Me.Text = "New Item"
            'If gItemsKeys = "" Or gItemsKeys Is Nothing Then
            '    Me.Text = "New Item"
            'Else
            '    Call loadItems()
            'End If
        ElseIf gItemMasterMode = "Edit Item" Then
            Me.Text = "Edit Item"
            Call loadItems()
        End If

    End Sub

    Private Sub cbo_item_itemGroupName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_item_itemGroupName.Click
        Call m_loadItemGroup(cbo_item_itemGroupName)
    End Sub

    Private Sub cbo_item_itemGroupName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_item_itemGroupName.SelectedIndexChanged
        Try
            If cbo_item_itemGroupName.SelectedItem = "create" Then
                frm_item_groupAddEdit.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    'Private Sub cboItemCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboItemCode.Click
    '    Call m_loadItemCode(cboItemCode)
    'End Sub

    Private Sub cboItemCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboItemCode.SelectedIndexChanged
        Dim sSQLCmd As String = "SELECT fcItemDescription FROM dbo.mItemCode WHERE  fxKeyCompany ='" & gCompanyID() & "' AND fcItemCode ='" & cboItemCode.SelectedItem.ToString & "'"
        Try

            If cboItemCode.SelectedItem = "" Then
                txtdescription.Text = ""
            End If

            If cboItemCode.SelectedItem = "create" Then
                frmItemCode.ShowDialog()
                Call m_loadItemCode(cboItemCode)
            Else

                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                    While rd.Read
                        txtdescription.Text = rd.Item(0).ToString
                    End While
                End Using
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cboCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategory.Click
        Call m_loadItemCategory(cboCategory)
    End Sub

    Private Sub cboCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedIndexChanged
        Try
            If cboCategory.SelectedItem = "create" Then
                frmItemCategory.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboColorCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboColorCode.Click
        Call m_loadItemColor(cboColorCode)
    End Sub

    Private Sub cboColorCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboColorCode.SelectedIndexChanged
        Try
            If cboColorCode.SelectedItem = "create" Then
                frmItemColor.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboSizeCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSizeCode.Click
        Call m_loadItemSize(cboSizeCode)
    End Sub

    Private Sub cboSizeCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSizeCode.SelectedIndexChanged
        Try
            If cboSizeCode.SelectedItem = "create" Then
                frmItemSize.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboUnit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnit.Click
        Call m_loadItemUnitMeasurement(cboUnit)
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        Try
            If cboUnit.SelectedItem = "create" Then
                frmUnitMeasurement.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboBrand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBrand.Click
        Call m_loadItemBrand(cboBrand)
    End Sub

    Private Sub cboBrand_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBrand.SelectedIndexChanged
        Try
            If cboBrand.SelectedItem = "create" Then
                frmBrand.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboInventoryPart_taxCodeName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTax.Click
        Call m_loadTaxCode(cboTax)
    End Sub

    Private Sub cboInventoryPart_taxCodeName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTax.SelectedIndexChanged
        Try
            If cboTax.SelectedItem = "create" Then
                frm_MF_taxAddEdit.ShowDialog()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboModel.Click
        Call m_loadItemModel(cboModel)
    End Sub

    Private Sub loadcomboselections()
        Call m_loadItemGroup(cbo_item_itemGroupName)
        Call m_loadItemCode(cboItemCode)
        Call m_loadItemCategory(cboCategory)
        Call m_loadItemColor(cboColorCode)
        Call m_loadItemSize(cboSizeCode)
        Call m_loadItemUnitMeasurement(cboUnit)
        Call m_loadItemBrand(cboBrand)
        Call m_loadItemModel(cboModel)
        Call m_loadTaxCode(cboTax)
        Call m_loadSuppliers(cboSupplier)
        Call m_DisplayAccountsAll(cboCOGs)
        Call m_DisplayAccountsAll(cboAssetAcnt)
        Call m_DisplayAccountsAll(cboIncomeAcnt)
    End Sub

    Private Sub loadItems()
        'Dim dtsItem23 As DataSet = m_getitemdetails(gItemsKeys)
        'With DGTemp
        '.DataSource = dtsItem23.Tables(0).DefaultView
        '    cbo_item_itemGroupName.SelectedItem = Convert.ToString(.Item("Group", 0).Value)
        '    cboItemCode.SelectedItem = Convert.ToString(.Item("ItemCode", 0).Value)
        '    txtdescription.Text = Convert.ToString(.Item("ItemDescription", 0).Value)
        '    cboCategory.SelectedItem = Convert.ToString(.Item("ItemCategory", 0).Value)
        '    cboBrand.SelectedItem = Convert.ToString(.Item("ItemBrand", 0).Value)
        '    cboColorCode.SelectedItem = Convert.ToString(.Item("ColorCode", 0).Value)
        '    cboSizeCode.SelectedItem = Convert.ToString(.Item("SizeCode", 0).Value)
        '    cboUnit.SelectedItem = Convert.ToString(.Item("Unit", 0).Value)
        '    cboModel.SelectedItem = Convert.ToString(.Item("Model", 0).Value)
        '    txtManPartNo.Text = Convert.ToString(.Item("ManuPart", 0).Value)
        '    txtItemdescpurchase.Text = Convert.ToString(.Item("PurchaseDesc", 0).Value)
        '    txtItemdescsales.Text = Convert.ToString(.Item("SalesDesc", 0).Value)
        '    cboCOGs.SelectedItem = Convert.ToString(.Item("COGS", 0).Value)
        '    cboSupplier.SelectedItem = Convert.ToString(.Item("Supplier", 0).Value)
        '    cboTax.SelectedItem = Convert.ToString(.Item("TAX", 0).Value)
        '    cboIncomeAcnt.SelectedItem = Convert.ToString(.Item("IncomeAcnt", 0).Value)
        '    cboAssetAcnt.SelectedItem = Convert.ToString(.Item("AssetAcnt", 0).Value)
        '    txtReorder.Text = Convert.ToString(.Item("Reorder", 0).Value)
        '    txtOnhand.Text = Convert.ToString(.Item("Onhand", 0).Value)
        '    txtSalesprice.Text = Convert.ToString(.Item("Price", 0).Value)
        '    txtCost.Text = Convert.ToString(.Item("Cost", 0).Value)
        '    txtAvgCost.Text = "0.00"
        '    txtPO.Text = "0.00"
        '    txtSO.Text = "0.00"
        'End With

        Dim dtsItem As DataTable = m_getitemdetails(gItemsKeys).Tables(0)
        Try
            Using rd As DataTableReader = dtsItem.CreateDataReader
                While rd.Read
                    With rd
                        cbo_item_itemGroupName.SelectedItem = .Item("Group").ToString

                        TxtItemCode.Text = getItemCode(gItemsKeys)
                        'cboItemCode.SelectedItem = getItemCode(gItemsKeys)
                        txtdescription.Text = .Item("ItemDescription").ToString
                        cboCategory.SelectedItem = .Item("ItemCategory").ToString
                        cboBrand.SelectedItem = .Item("ItemBrand").ToString
                        cboColorCode.SelectedItem = getColorName(.Item("ColorCode").ToString)
                        cboSizeCode.SelectedItem = .Item("SizeCode").ToString
                        cboUnit.SelectedItem = .Item("Unit").ToString
                        cboModel.SelectedItem = .Item("Model").ToString
                        txtManPartNo.Text = .Item("ManuPart").ToString
                        txtItemdescpurchase.Text = .Item("PurchaseDesc").ToString
                        txtItemdescsales.Text = .Item("SalesDesc").ToString
                        cboCOGs.SelectedItem = .Item("COGS").ToString
                        cboSupplier.SelectedItem = .Item("Supplier").ToString
                        cboTax.SelectedItem = .Item("TAX").ToString
                        cboIncomeAcnt.SelectedItem = .Item("IncomeAcnt").ToString
                        cboAssetAcnt.SelectedItem = .Item("AssetAcnt").ToString
                        txtReorder.Text = .Item("Reorder").ToString
                        txtOnhand.Text = .Item("Onhand").ToString
                        txtSalesprice.Text = .Item("Price")
                        txtCost.Text = .Item("Cost")
                        txtAvgCost.Text = "0.00"
                        txtPO.Text = "0.00"
                        txtSO.Text = "0.00"
                    End With
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btn_item_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_item_cancel.Click
        Me.Text = "Add/Edit Item"
        gItemsKeys = ""
        Call cls.gClearFormTxt(Me)
        Me.Close()
    End Sub

    Private Sub btn_dep_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveClose.Click
        'updates: Inserted Validation for Preferred Supplier. 10/17/2008
        Me.Cursor = Cursors.WaitCursor
        If Me.Text = "New Item" Then 'new Item
            Dim CodeCheck As String
            CodeCheck = ValidateItemCode(TxtItemCode.Text)
            If CodeCheck = "Existed" Then
                MsgBox("Item Code already exist." + vbCr + "Please choose another.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Existing Item Code Detected!")
                Me.Cursor = Cursors.Default
                Me.TxtItemCode.Select()
                Me.TxtItemCode.SelectAll()
                Exit Sub
            Else
                MsgBox("You've type a new itemcode." + vbCr + "Please provide the following data.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "New Item Code Detected!")
                frmItemCode.txtCode.Text = TxtItemCode.Text
                frmItemCode.ShowDialog()
                TxtItemCode.Text = ""
                TxtItemCode.Text = TxtVarrefresher
                CheckingB4Save()
            End If
        Else
            If Me.Text = "Edit Item" Then
                CheckingB4Save()
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub CheckingB4Save()
        If cbo_item_itemGroupName.SelectedIndex < 1 Then
            MessageBox.Show("Please select Item Group to proceed.", "Required Field")
            cbo_item_itemGroupName.Focus()
        ElseIf cboSupplier.SelectedItem = "" Then
            MessageBox.Show("Please select Preferred Supplier to proceed.", "Required Field")
            cboSupplier.Focus()
        Else
            If cboIncomeAcnt.SelectedItem = "" Or cboIncomeAcnt.SelectedIndex = 0 Then
                'cboIncomeAcnt.SelectedIndex = 1 
                MessageBox.Show("You must specify an Income Account to be associated with this item.", "Required Field")
                cboIncomeAcnt.Focus()
            Else
                updateItem()
                frm_item_masterItem.refreshForm()
            End If
        End If
    End Sub
    Private Function GetColorCode(ByVal ColorDesCription As String)
        Dim sSqlCmd As String = "SELECT fcColorCode FROM dbo.mItemColor WHERE fcColorDescription ='" & ColorDesCription & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub updateItem()

        Dim xval As Integer

        If chkInactive.Checked <> True Then
            xval = 1
        End If

        Dim sCogs As String = ""
        If cboCOGs.SelectedItem <> " " Then
            sCogs = getAccountID(cboCOGs.SelectedItem)
        End If

        Dim sIncome As String = ""
        If cboIncomeAcnt.SelectedItem <> " " Then
            sIncome = getAccountID(cboIncomeAcnt.SelectedItem)
        End If

        Dim sAsset As String = ""
        If cboAssetAcnt.SelectedItem <> " " Then
            sAsset = getAccountID(cboAssetAcnt.SelectedItem)
        End If

        If gItemsKeys = "" Then
            gItemsKeys = Guid.NewGuid.ToString
        End If

        Dim sBrand As String = ""
        If cboBrand.SelectedItem <> "" Then
            sBrand = getBrandID(cboBrand.SelectedItem)
        End If

        Dim sSQLCmd As String = "usp_m_item_update "
        sSQLCmd &= " @fxKeyGroupName='" & getGroupID(cbo_item_itemGroupName.SelectedItem) & "' "
        sSQLCmd &= ",@fcItemCode='" & TxtItemCode.Text & "' "
        sSQLCmd &= ",@fcDescription='" & txtdescription.Text & "' "
        sSQLCmd &= ",@fcCategory='" & cboCategory.SelectedItem & "' "
        sSQLCmd &= ",@fcBrand='" & cboBrand.SelectedItem & "' "
        ' sSQLCmd &= ",@fcColor='" & cboColorCode.SelectedItem & "' "
        sSQLCmd &= ",@ColorCode='" & GetColorCode(cboColorCode.Text) & "' "
        sSQLCmd &= ",@fcSize='" & cboSizeCode.SelectedItem & "' "
        sSQLCmd &= ",@fcUnitMeasurement='" & cboUnit.SelectedItem & "' "
        sSQLCmd &= ",@fdUnitCost='" & txtCost.Text & "' "
        sSQLCmd &= ",@fdUnitPrice='" & txtSalesprice.Text & "' "
        sSQLCmd &= ",@fcModel='" & cboModel.SelectedItem & "' "
        sSQLCmd &= ",@fdBeginBalance='" & txtOnhand.Text & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ",@fcPurchaseDesc='" & txtItemdescpurchase.Text & "' "
        sSQLCmd &= ",@fcSalesDesc='" & txtItemdescsales.Text & "' "
        sSQLCmd &= ",@fxKeyCOGs=" & IIf(sCogs = "", "NULL", "'" & sCogs & "'") & " "
        sSQLCmd &= ",@fxKeySupplier='" & getSupplierID(cboSupplier.SelectedItem) & "' "
        sSQLCmd &= ",@fxKeyTax='" & cboTax.SelectedItem & "' "
        sSQLCmd &= ",@fxKeyIncomeAcnt=" & IIf(sIncome = "", "NULL", "'" & sIncome & "'") & " "
        sSQLCmd &= ",@fxKeyAssetAcnt=" & IIf(sAsset = "", "NULL", "'" & sAsset & "'") & " "
        sSQLCmd &= ",@fdReorder='" & txtReorder.Text & "' "
        'sSQLCmd &= ",@fdAvgCost='" & txtAvgCost.Text & "' "
        'sSQLCmd &= ",@fdPOAmt='" & txtPO.Text & "' "
        'sSQLCmd &= ",@fdSOAmt='" & txtSO.Text & "' "
        sSQLCmd &= ",@fbActive='" & xval & "' "
        sSQLCmd &= ",@fcManPartNo='" & txtManPartNo.Text & "' "
        sSQLCmd &= ",@fuCreatedBy='" & gUserName & "' "
        sSQLCmd &= ",@fxKeyItem='" & gItemsKeys & "' "
        'sSQLCmd &= ",@fxKeyBrand=" & IIf(sBrand = "", "NULL", "'" & sBrand & "'")
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Update Item")
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Item")
        End Try
    End Sub

    Private Sub txtInventoryPart_reorderPoint_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtReorder.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtInventoryPart_onHand_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOnhand.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtInventoryPart_price_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSalesprice.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtInventoryPart_cost_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCost.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtInventoryPart_totalValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not numeric_Validate(e.KeyChar)
    End Sub

    Private Sub btnEnable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As New frm_item_masterItemMeasurement
        x.ShowDialog()
    End Sub

    Private Sub TxtItemCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtItemCode.TextChanged
        Dim sSQLCmd As String = "SELECT fcItemDescription FROM dbo.mItemCode WHERE  fxKeyCompany ='" & gCompanyID() & "' AND fcItemCode ='" & TxtItemCode.Text & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    txtdescription.Text = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


End Class