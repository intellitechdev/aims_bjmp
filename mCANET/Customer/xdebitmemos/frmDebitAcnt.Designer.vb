<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDebitAcnt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDebitAcnt))
        Me.cboCOS = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtPer1 = New System.Windows.Forms.TextBox
        Me.cboOutputTax = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.cboSalesAcnt = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtPercent = New System.Windows.Forms.TextBox
        Me.cboTaxAcnt = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cboCOS
        '
        Me.cboCOS.FormattingEnabled = True
        Me.cboCOS.Location = New System.Drawing.Point(171, 125)
        Me.cboCOS.Name = "cboCOS"
        Me.cboCOS.Size = New System.Drawing.Size(199, 21)
        Me.cboCOS.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 128)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 137
        Me.Label4.Text = "COS Account"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(413, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 13)
        Me.Label2.TabIndex = 133
        Me.Label2.Text = "%"
        Me.Label2.Visible = False
        '
        'txtPer1
        '
        Me.txtPer1.Location = New System.Drawing.Point(371, 99)
        Me.txtPer1.Name = "txtPer1"
        Me.txtPer1.Size = New System.Drawing.Size(40, 21)
        Me.txtPer1.TabIndex = 134
        Me.txtPer1.Text = "0.00"
        Me.txtPer1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPer1.Visible = False
        '
        'cboOutputTax
        '
        Me.cboOutputTax.FormattingEnabled = True
        Me.cboOutputTax.Location = New System.Drawing.Point(171, 99)
        Me.cboOutputTax.Name = "cboOutputTax"
        Me.cboOutputTax.Size = New System.Drawing.Size(199, 21)
        Me.cboOutputTax.TabIndex = 136
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "Store Supplies"
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(345, 152)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 29)
        Me.btnClose.TabIndex = 132
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(249, 152)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(90, 29)
        Me.btnSave.TabIndex = 131
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboSalesAcnt
        '
        Me.cboSalesAcnt.FormattingEnabled = True
        Me.cboSalesAcnt.Location = New System.Drawing.Point(171, 18)
        Me.cboSalesAcnt.Name = "cboSalesAcnt"
        Me.cboSalesAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboSalesAcnt.TabIndex = 130
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 13)
        Me.Label1.TabIndex = 129
        Me.Label1.Text = "Sales Credit Account"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(413, 75)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 125
        Me.Label24.Text = "%"
        Me.Label24.Visible = False
        '
        'txtPercent
        '
        Me.txtPercent.Location = New System.Drawing.Point(371, 72)
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtPercent.TabIndex = 126
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPercent.Visible = False
        '
        'cboTaxAcnt
        '
        Me.cboTaxAcnt.FormattingEnabled = True
        Me.cboTaxAcnt.Location = New System.Drawing.Point(171, 72)
        Me.cboTaxAcnt.Name = "cboTaxAcnt"
        Me.cboTaxAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboTaxAcnt.TabIndex = 128
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(13, 75)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(107, 13)
        Me.Label23.TabIndex = 127
        Me.Label23.Text = "Postage && Freight"
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(171, 45)
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(199, 21)
        Me.cboPayAcnt.TabIndex = 124
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(13, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(152, 13)
        Me.Label20.TabIndex = 123
        Me.Label20.Text = "Receivable Debit Account"
        '
        'frmDebitAcnt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(435, 185)
        Me.Controls.Add(Me.cboCOS)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPer1)
        Me.Controls.Add(Me.cboOutputTax)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboSalesAcnt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtPercent)
        Me.Controls.Add(Me.cboTaxAcnt)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.cboPayAcnt)
        Me.Controls.Add(Me.Label20)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDebitAcnt"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accountability"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCOS As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPer1 As System.Windows.Forms.TextBox
    Friend WithEvents cboOutputTax As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cboSalesAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtPercent As System.Windows.Forms.TextBox
    Friend WithEvents cboTaxAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
End Class
