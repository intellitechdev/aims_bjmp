Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
' GELO
Public Class frm_vend_OpenPO

    Private gCon As New Clsappconfiguration
    Public xCustomerName As String

    Public Sub createGridPOitems()
        ' Creates the DataGridView 
        Try
            With gridPOItems
                Dim colChkbox As New DataGridViewCheckBoxColumn
                Dim colDate As New DataGridViewTextBoxColumn
                Dim colPOno As New DataGridViewTextBoxColumn
                Dim colMemo As New DataGridViewTextBoxColumn
                Dim colKeyPO As New DataGridViewTextBoxColumn
                Dim retPO As Boolean = loadPOConn(MtcboSupplier.SelectedItem.Col2)

                colChkbox.Name = "chkSelect"
                colChkbox.HeaderText = ""
                colChkbox.ReadOnly = False
                colChkbox.Width = 20

                colDate.DataPropertyName = "fdDateTransact"
                colDate.HeaderText = "Date"
                colDate.DefaultCellStyle.Format = "MM/dd/yyyy"
                colDate.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                colDate.Name = "cDate"

                colPOno.DataPropertyName = "fcPoNo"
                colPOno.HeaderText = "PO No."
                colPOno.Name = "cPo"

                colMemo.DataPropertyName = "fcmemo"
                colMemo.HeaderText = "Memo"
                colMemo.Name = "cMemo"

                colKeyPO.DataPropertyName = "fxKeyPo"
                colKeyPO.HeaderText = "Key PO"
                colKeyPO.Name = "cKey"
                colKeyPO.Visible = False

                .Columns.Clear()
                .Columns.Add(colChkbox)
                .Columns.Add(colDate)
                .Columns.Add(colPOno)
                .Columns.Add(colMemo)
                .Columns.Add(colKeyPO)
            End With
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Customer Transactions")
        End Try
    End Sub

    Public Function loadPOConn(ByVal sKey As String) As Boolean
        Dim sSQLCmd As String = "usp_t_getPurchaseOrder_perSupplier "
        sSQLCmd &= "'" & MtcboSupplier.SelectedItem.Col2 & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.HasRows Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Validation")
            Return True
        End Try
    End Function
    ' Retrieving Data from Database to DataGridView
    Public Sub loadCompanyPO()
        Dim sSQLCmd As String

        sSQLCmd = "usp_t_getPurchaseOrder_perSupplier '" & MtcboSupplier.SelectedItem.Col2 & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                Dim xRow As Integer = 0
                While rd.Read
                    With gridPOItems
                        .Rows.Add()
                        .Item("cDate", xRow).Value = rd.Item(1).ToString
                        .Item("cPo", xRow).Value = rd.Item(2).ToString
                        .Item("cMemo", xRow).Value = rd.Item(3).ToString
                        .Item("cKey", xRow).Value = rd.Item(0).ToString
                    End With
                    xRow += 1
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Transactions")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub frm_vend_OpenPO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Loads Supplier Database List to MultiCombobox
        m_loadSupplierToMulticolumnCbobox(MtcboSupplier)

    End Sub

    Private Sub MtcboSupplier_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboSupplier.SelectedIndexChanged
        ' MultiComboBox Indexes
        ' Opens the Add New Supplier Form
        If MtcboSupplier.SelectedItem.Col1 = "Create Name" Then
            frm_vend_masterVendorAddEdit.ShowDialog()
            m_loadSupplierToMulticolumnCbobox(MtcboSupplier)
        Else
            ' Opens connection to Database and DataGridView
            createGridPOitems()
            loadCompanyPO()
            xCustomerName = UCase(MtcboSupplier.SelectedItem.Col1.ToString())
        End If
    End Sub



    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        ' Scanning for Selected CheckBox in DataGridView
        Dim Key As String = 0

        Try
            frm_vend_EnterBills.grdItems.Rows.Clear()
            With gridPOItems
                For Each xRow As DataGridViewRow In gridPOItems.Rows
                    If .Item("chkSelect", xRow.Index).Value = True Then
                        If Not .Item("chkSelect", xRow.Index).Value Is Nothing Then
                            Key = .Item("cKey", xRow.Index).Value
                            frm_vend_EnterBills.loadPurchaseItem(Key)
                        End If
                    End If
                Next
            End With
            frm_vend_EnterBills.cboMember.SelectedText = xCustomerName

            Me.Close()
        Catch ex As Exception
            ShowErrorMessage("btnOK_Click", "frm_vend_OpenPO", ex.Message)
        End Try
    End Sub

    Private Sub gridPOItems_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridPOItems.CurrentCellDirtyStateChanged
        If gridPOItems.IsCurrentCellDirty = True Then
            gridPOItems.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub frm_vend_OpenPO_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frm_vend_OpenPO_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

End Class