﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountSubsidiary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tab1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Add1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Delete1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Close1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.grdList1 = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Add2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Delete2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtColumn2 = New System.Windows.Forms.TextBox()
        Me.Close2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboAccount2 = New System.Windows.Forms.ComboBox()
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip()
        Me.grdList2 = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Update2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.tab1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.grdList1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        CType(Me.grdList2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'tab1
        '
        Me.tab1.Controls.Add(Me.TabPage1)
        Me.tab1.Controls.Add(Me.TabPage2)
        Me.tab1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab1.Location = New System.Drawing.Point(0, 0)
        Me.tab1.Name = "tab1"
        Me.tab1.SelectedIndex = 0
        Me.tab1.Size = New System.Drawing.Size(368, 454)
        Me.tab1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grdList1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 23)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(360, 427)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Account"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdList2)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(360, 427)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Column"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Add1ToolStripMenuItem
        '
        Me.Add1ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.Add1ToolStripMenuItem.Name = "Add1ToolStripMenuItem"
        Me.Add1ToolStripMenuItem.Size = New System.Drawing.Size(57, 32)
        Me.Add1ToolStripMenuItem.Text = "Add"
        '
        'Delete1ToolStripMenuItem
        '
        Me.Delete1ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.Delete1ToolStripMenuItem.Name = "Delete1ToolStripMenuItem"
        Me.Delete1ToolStripMenuItem.Size = New System.Drawing.Size(68, 32)
        Me.Delete1ToolStripMenuItem.Text = "Delete"
        '
        'Close1ToolStripMenuItem
        '
        Me.Close1ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Close1ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Close1ToolStripMenuItem.Name = "Close1ToolStripMenuItem"
        Me.Close1ToolStripMenuItem.Size = New System.Drawing.Size(64, 32)
        Me.Close1ToolStripMenuItem.Text = "Close"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Close1ToolStripMenuItem, Me.Add1ToolStripMenuItem, Me.Delete1ToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(3, 18)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(348, 36)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'grdList1
        '
        Me.grdList1.AllowUserToAddRows = False
        Me.grdList1.AllowUserToDeleteRows = False
        Me.grdList1.BackgroundColor = System.Drawing.Color.White
        Me.grdList1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdList1.Location = New System.Drawing.Point(3, 60)
        Me.grdList1.Name = "grdList1"
        Me.grdList1.ReadOnly = True
        Me.grdList1.Size = New System.Drawing.Size(354, 307)
        Me.grdList1.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(9, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Account"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox2.Controls.Add(Me.MenuStrip1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox2.Location = New System.Drawing.Point(3, 367)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(354, 57)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(354, 57)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'Add2ToolStripMenuItem
        '
        Me.Add2ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.Add2ToolStripMenuItem.Name = "Add2ToolStripMenuItem"
        Me.Add2ToolStripMenuItem.Size = New System.Drawing.Size(57, 32)
        Me.Add2ToolStripMenuItem.Text = "Add"
        '
        'Delete2ToolStripMenuItem
        '
        Me.Delete2ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Erase1
        Me.Delete2ToolStripMenuItem.Name = "Delete2ToolStripMenuItem"
        Me.Delete2ToolStripMenuItem.Size = New System.Drawing.Size(68, 32)
        Me.Delete2ToolStripMenuItem.Text = "Delete"
        '
        'txtColumn2
        '
        Me.txtColumn2.Location = New System.Drawing.Point(71, 45)
        Me.txtColumn2.Name = "txtColumn2"
        Me.txtColumn2.Size = New System.Drawing.Size(261, 22)
        Me.txtColumn2.TabIndex = 2
        '
        'Close2ToolStripMenuItem
        '
        Me.Close2ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Close2ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.Close2ToolStripMenuItem.Name = "Close2ToolStripMenuItem"
        Me.Close2ToolStripMenuItem.Size = New System.Drawing.Size(64, 32)
        Me.Close2ToolStripMenuItem.Text = "Close"
        '
        'cboAccount2
        '
        Me.cboAccount2.FormattingEnabled = True
        Me.cboAccount2.Location = New System.Drawing.Point(71, 16)
        Me.cboAccount2.Name = "cboAccount2"
        Me.cboAccount2.Size = New System.Drawing.Size(261, 22)
        Me.cboAccount2.TabIndex = 0
        '
        'MenuStrip2
        '
        Me.MenuStrip2.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Close2ToolStripMenuItem, Me.Add2ToolStripMenuItem, Me.Update2ToolStripMenuItem, Me.Delete2ToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(3, 18)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Size = New System.Drawing.Size(348, 36)
        Me.MenuStrip2.TabIndex = 0
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'grdList2
        '
        Me.grdList2.AllowUserToAddRows = False
        Me.grdList2.AllowUserToDeleteRows = False
        Me.grdList2.BackgroundColor = System.Drawing.Color.White
        Me.grdList2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdList2.Location = New System.Drawing.Point(3, 87)
        Me.grdList2.Name = "grdList2"
        Me.grdList2.ReadOnly = True
        Me.grdList2.Size = New System.Drawing.Size(354, 280)
        Me.grdList2.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Location = New System.Drawing.Point(9, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 14)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Account"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox3.Controls.Add(Me.MenuStrip2)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox3.Location = New System.Drawing.Point(3, 367)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(354, 57)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Location = New System.Drawing.Point(9, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 14)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Column"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.txtColumn2)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.cboAccount2)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox4.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(354, 84)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        '
        'Update2ToolStripMenuItem
        '
        Me.Update2ToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.Save
        Me.Update2ToolStripMenuItem.Name = "Update2ToolStripMenuItem"
        Me.Update2ToolStripMenuItem.Size = New System.Drawing.Size(73, 32)
        Me.Update2ToolStripMenuItem.Text = "Update"
        '
        'Button1
        '
        Me.Button1.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.Button1.Location = New System.Drawing.Point(306, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(26, 26)
        Me.Button1.TabIndex = 6
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(71, 19)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(229, 22)
        Me.TextBox1.TabIndex = 5
        '
        'frmAccountSubsidiary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(368, 454)
        Me.Controls.Add(Me.tab1)
        Me.Font = New System.Drawing.Font("Consolas", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmAccountSubsidiary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Subsidiary Account Setup"
        Me.tab1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.grdList1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        CType(Me.grdList2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tab1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grdList1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents Close1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Add1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Delete1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grdList2 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents Close2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Add2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Delete2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtColumn2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboAccount2 As System.Windows.Forms.ComboBox
    Friend WithEvents Update2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class
