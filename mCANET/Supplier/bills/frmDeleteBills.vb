﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmDeleteBills
    Public pkID As String
   
    Private Sub frmDeleteBills_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadSelectedBIll()
    End Sub
    Private Sub LoadSelectedBIll()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "loadBillstovoid", _
                                                          New SqlParameter("@pkid", pkID))
        lblAPVNO.Text = ""
        lblSupplier.Text = ""
        lblDate.Text = ""
        lblDuedate.Text = ""
        lblOAmount.Text = ""
        lblBal.Text = ""
        Dim OAmnt As String
        Dim bal As String
        Dim cvno As String
        Try
            If rd.Read() Then
                lblSupplier.Text = rd.Item(1)
                lblAPVNO.Text = rd.Item(9)
                lblDate.Text = rd.Item(6)
                lblDuedate.Text = rd.Item(8)
                OAmnt = rd.Item(7)
                bal = rd.Item(32)
                If rd.Item(30).ToString = True Then
                    CheckBox1.Checked = True
                Else
                    CheckBox1.Checked = False
                End If
                cvno = rd.Item(34).ToString
                lblnfo.Text = "The Following will be Deleted from the Database" & vbCrLf & "Check No. " + rd.Item(33).ToString & vbCrLf & cvno

            End If
            lblOAmount.TextAlign = ContentAlignment.MiddleLeft
            lblOAmount.Text = String.Format("{0:n2}", Convert.ToDecimal(OAmnt))
            lblBal.Text = String.Format("{0:n2}", Convert.ToDecimal(bal))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "error")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub VoidBills()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteBIlls", _
                                      New SqlParameter("@pkID", pkID))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub UpdateBillsVoided()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateBillsAsVoided", _
                                      New SqlParameter("@pkid", pkID), _
                                      New SqlParameter("@user", gUserName))


            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Void ERROR")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btndelete_Click(sender As System.Object, e As System.EventArgs) Handles btndelete.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you Sure you want to make this Transaction?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If x = Windows.Forms.DialogResult.Yes Then
            UpdateBillsVoided()
            VoidBills()
            MessageBox.Show("Successfully Voided")
            Me.Close()
        Else
            MessageBox.Show("Transaction has been Cancelled", "Information")
        End If
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class