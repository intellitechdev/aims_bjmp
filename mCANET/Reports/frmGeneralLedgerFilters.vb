Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmGeneralLedgerFilters
    Private gCon As New Clsappconfiguration
    Private varDateTo As Date
    Private varDateFrom As Date
    Private accountID As String
    Private subsidiary As String
    Private referenceNo As String

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub frmGeneralLedgerFilters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadAccounts()
        LoadSubsidiary()
    End Sub


    Private Sub LoadAccounts()
        Dim dtActiveAccounts As New DataTable("BankAcnt")
        Dim DR As DataRow
        Dim sSqlCmd As String = "CAS_m_GetActiveAccounts"

        dtActiveAccounts.Columns.Add("acnt_name", System.Type.GetType("System.String"))
        dtActiveAccounts.Columns.Add("acnt_id", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, sSqlCmd, _
                    New SqlParameter("@compID", gCompanyID()))

                'Add Blank Row
                DR = dtActiveAccounts.NewRow
                DR("acnt_name") = Nothing
                DR("acnt_id") = Nothing
                dtActiveAccounts.Rows.Add(DR)

                While rd.Read
                    DR = dtActiveAccounts.NewRow
                    DR("acnt_name") = rd.Item("acnt_name").ToString
                    DR("acnt_id") = rd.Item("acnt_id").ToString
                    dtActiveAccounts.Rows.Add(DR)
                End While
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        cboAccounts.Items.Clear()
        cboAccounts.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        cboAccounts.SourceDataString = New String(1) {"acnt_name", "acnt_id"}
        cboAccounts.SourceDataTable = dtActiveAccounts

    End Sub

    Private Sub LoadSubsidiary()
        Dim dtMemberList As New DataTable("MemberList")
        Dim DR As DataRow
        Dim sSqlCmd As String = "CAS_m_GetSupplierList"

        dtMemberList.Columns.Add("EmployeeNo", System.Type.GetType("System.String"))
        dtMemberList.Columns.Add("Member", System.Type.GetType("System.String"))

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, sSqlCmd, _
                    New SqlParameter("@compID", gCompanyID()))

                'Add Blank Row
                DR = dtMemberList.NewRow
                DR("EmployeeNo") = Nothing
                DR("Member") = Nothing
                dtMemberList.Rows.Add(DR)

                While rd.Read
                    DR = dtMemberList.NewRow
                    DR("EmployeeNo") = rd.Item("fcEmployeeNo").ToString
                    DR("Member") = rd.Item("fcSupplierName").ToString
                    dtMemberList.Rows.Add(DR)
                End While
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        With cboSubsidiary
            .Items.Clear()
            .LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            .SourceDataString = New String(1) {"Member", "EmployeeNo"}
            .SourceDataTable = dtMemberList
        End With

    End Sub

    Private Sub cboAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAccounts.SelectedIndexChanged
        Try
            accountID = cboAccounts.SelectedItem.Col2
        Catch ex As Exception
            accountID = Nothing
        End Try
    End Sub

    Private Sub cboSubsidiary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubsidiary.SelectedIndexChanged
        Try
            subsidiary = cboSubsidiary.SelectedItem.Col2
        Catch ex As Exception
            subsidiary = Nothing
        End Try
    End Sub

    Private Sub dteFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteFrom.ValueChanged
        varDateFrom = dteFrom.Value.Date
    End Sub

    Private Sub dteTo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteTo.ValueChanged
        varDateTo = dteTo.Value.Date
    End Sub

    Private Sub txtReference_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReference.TextChanged
        Try
            referenceNo = txtReference.Text
        Catch ex As Exception
            referenceNo = Nothing
        End Try
    End Sub

    Private Sub ApplyFilters()
        With frmGeneralLedger
            .GetdateFrom() = varDateFrom
            .GetdateTo() = varDateTo
            .GetAccountID() = IIf(accountID = Nothing, "", accountID)
            .GetSubsidiary() = IIf(subsidiary = Nothing, "", subsidiary)
            .GetReference() = referenceNo
        End With
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplyFilters.Click
        ApplyFilters()
        Close()
    End Sub


End Class