Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmItemColor

    Private gCon As New Clsappconfiguration
    Private Sub btn_dep_saveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_dep_saveClose.Click
        ' This will also prevent the user from entering an existing Item Color. 10/17/2008

        Try
            Dim sSQLCmd As String = "usp_tValidateColor "

            sSQLCmd &= "@colorCode = '" & txtCode.Text & "'"
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)

                If rd.HasRows Then
                    MessageBox.Show("Item Color is already Existing. Please Insert Another Color.", "Color Existing")
                    rd.Close()
                ElseIf txtdescription.Text = "" Then
                    MessageBox.Show("Please Enter Color Description", "Required Field")
                    txtdescription.Focus()
                    rd.Close()
                ElseIf m_saveitemcolor(txtCode.Text, txtdescription.Text, Me) = "Save Successful!" Then
                    MsgBox("Save Successful...", MsgBoxStyle.Information, Me.Text)
                    clear()
                    Me.Close()
                Else
                    MsgBox("Encountered an error, Please report this to your System Administrator.", MsgBoxStyle.Information, Me.Text)
                    clear()
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Update Other Info.")
        End Try

        'If m_saveitemcolor(txtCode.Text, txtdescription.Text, Me) = "Save Successful..." Then
        '       MsgBox("Save Successful...", MsgBoxStyle.Information, Me.Text)
        '       clear()
        '       Me.Close()
        'Else
        '       MsgBox("Encountered an error, Please report this to your System Administrator.", MsgBoxStyle.Information, Me.Text)
        '       clear()
        'End If
    End Sub

    Private Sub btn_item_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_item_cancel.Click
        clear()
        Me.Close()
    End Sub

    Private Sub clear()
        txtCode.Text = ""
        txtdescription.Text = ""
    End Sub


End Class