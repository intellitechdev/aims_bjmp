Imports Microsoft.ApplicationBlocks.Data

Public Class frm_vend_SalesTax
    Private sKeySalesTaxCode As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mSalesTaxCode WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_vend_SalesTaxAdd.Text = "New Sales Tax Code"
        frm_vend_SalesTaxAdd.KeySalesTaxCode = Guid.NewGuid.ToString
        frm_vend_SalesTaxAdd.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_vend_SalesTaxAdd.Text = "Edit Sales Tax Code"
        frm_vend_SalesTaxAdd.KeySalesTaxCode = sKeySalesTaxCode
        frm_vend_SalesTaxAdd.Show()
    End Sub

    Private Sub frm_vend_SalesTax_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_SalesTaxCodeList(chkIncludeInactive.CheckState, grd_SalesTax)
        grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_Inactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub grdSettings()
        Try
            With grd_SalesTax
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns.Item(1).HeaderText = "Active"
                .Columns.Item(2).HeaderText = "Code"
                .Columns.Item(3).HeaderText = "Description"
                .Columns.Item(4).HeaderText = "Taxable"
            End With

            If chkIncludeInactive.CheckState = CheckState.Checked Then
                grd_SalesTax.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                grd_SalesTax.Columns(1).Visible = True
            End If
        Catch
            MessageBox.Show(Err.Description, "Sales Tax Grid")
        End Try
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_SalesTaxCodeList(chkIncludeInactive.CheckState, grd_SalesTax)
        grdSettings()
    End Sub

    Private Sub grd_SalesTax_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grd_SalesTax.CellClick
        If Me.grd_SalesTax.SelectedCells.Count > 0 Then
            sKeySalesTaxCode = grd_SalesTax.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mSalesTaxCode", "fxKeySalesTaxCode", sKeySalesTaxCode) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grd_SalesTax_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grd_SalesTax.DoubleClick
        If Me.grd_SalesTax.SelectedCells.Count > 0 Then
            sKeySalesTaxCode = grd_SalesTax.CurrentRow.Cells(0).Value.ToString

            frm_vend_SalesTaxAdd.Text = "Edit Sales Tax Code"
            frm_vend_SalesTaxAdd.KeySalesTaxCode = sKeySalesTaxCode
            frm_vend_SalesTaxAdd.Show()
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        deleteSalesTaxCode()
        refreshForm()
    End Sub

    Private Sub deleteSalesTaxCode()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mSalesTaxCode WHERE fxKeySalesTaxCode = '" & sKeySalesTaxCode & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            Catch ex As Exception
                MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Delete Sales Tax Code")
            End Try
        End If

    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_Inactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_Inactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Inactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mSalesTaxCode", "fxKeySalesTaxCode", sKeySalesTaxCode, True)
        Else
            m_mkActive("mSalesTaxCode", "fxKeySalesTaxCode", sKeySalesTaxCode, False)
        End If
        refreshForm
    End Sub
End Class