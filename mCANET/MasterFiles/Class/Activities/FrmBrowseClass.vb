Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: July 07, 2009                      ###
'                   ### Website: www.ionflux.site50.net                  ###
'                   ########################################################

Public Class FrmBrowseClass
#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Dim LevelDept As Integer
    Public Data1 As String
    Public Data2 As String
    Public Mode As String
    Private ClassDesciptionForToolTip As String
    Private ClassParentForToolTip As String
    Private ClassNameForToolTip As String
#End Region
#Region "Functions and Subs"
    Private Sub getClassDetailsForToolTip(ByVal ClassReferenceNumber As String)
        Dim dbParent As String
        Dim xParent(0) As String
        Dim dbName As String
        Dim xName(0) As String
        ClassNameForToolTip = ""
        Dim sSqlGetDetail As String = _
                    "SELECT ClassDescription, ClassParentPath, classname FROM dbo.mClasses WHERE ClassRefnumber = '" & _
                    ClassReferenceNumber & "'AND fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlGetDetail)
                While rd.Read
                    ClassDesciptionForToolTip = rd.Item(0).ToString
                    dbParent = rd.Item(1).ToString
                    If dbParent Is Nothing Or dbParent = "" Or dbParent = " " Then
                        ClassParentForToolTip = "N/A"
                    Else
                        xParent = Split(dbParent, "���")
                        ClassParentForToolTip = xParent(0)
                    End If
                    dbName = rd.Item(2).ToString
                    xName = Split(dbName, "���")
                    ClassNameForToolTip = xName(0)

                End While
            End Using
        Catch ex As Exception

        End Try
        If ClassNameForToolTip Is Nothing Or ClassNameForToolTip = "" Or ClassNameForToolTip = " " Then
            ClassDesciptionForToolTip = "Class on Top or first Level"
            ClassParentForToolTip = "N/A"
            ClassNameForToolTip = "Top Level"
        End If
    End Sub
    Private Function GetClasses() As DataSet
        Dim sSqlCmd As String = "SELECT * FROM dbo.mClasses where fxKeyCompany = '" & gCompanyID() & "'"
        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub LoadClass()
        With DGView
            Dim DGVClasses As DataTable = GetClasses().Tables(0)
            .DataSource = DGVClasses.DefaultView
        End With
    End Sub
    Private Sub getLevelDept()
        DGView.SelectAll()
        Dim xRow As Integer = 0
        For Each selectedRow As DataGridViewRow In DGView.SelectedRows
            xRow = selectedRow.Index
            Dim RowLeveldept As Integer = DGView.Item(3, xRow).Value
            'MsgBox(RowLeveldept)
            If RowLeveldept > LevelDept Then
                LevelDept = RowLeveldept
            End If
        Next
        DGView.Refresh()
    End Sub
    Private Function GetNode(ByVal text As String, ByVal parentCollection As TreeNodeCollection) As TreeNode
        Dim ret As TreeNode
        Dim child As TreeNode
        For Each child In parentCollection 'step through the parentcollection
            If child.Text = text Then
                ret = child
            ElseIf child.GetNodeCount(False) > 0 Then ' if there is child items then call this function recursively
                ret = GetNode(text, child.Nodes)
            End If
            If Not ret Is Nothing Then Exit For 'if something was found, exit out of the for loop
        Next
        Return ret
    End Function
    Private Sub EvaluateNodeName()
        DGView.SelectAll()
        Dim xRow As Integer = 0
        Dim UncleanName As String
        Dim ClassName() As String
        For Each selectedRow As DataGridViewRow In DGView.SelectedRows
            xRow = selectedRow.Index
            TreeClass.SelectedNode = GetNode(DGView.Item(2, xRow).Value, TreeClass.Nodes)
            If TreeClass.SelectedNode Is Nothing Then
                'DeleteClass(DGView.Item(0, xRow).Value)
            Else
                UncleanName = TreeClass.SelectedNode.Text
                ClassName = Split(UncleanName, "���")
                TreeClass.SelectedNode.Text = ClassName(0)
            End If
        Next
        DGView.Refresh()
        TreeClass.CollapseAll()
    End Sub
    Private Function getNewRefno()
        Dim sSqlCmd As String = "select right(newid(),9)"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub RemoveClassFromTree(ByVal ClassRealName As String)
        TreeClass.SelectedNode = GetNode(ClassRealName, TreeClass.Nodes)
        TreeClass.SelectedNode.Remove()
    End Sub
    Private Sub ClassFromGridToTree(ByVal Mode As String)
        Dim diggerDept As Integer = 1
        Dim xRow As Integer = 0
        TreeClass.Nodes.Clear()
        If Mode = "ADD MODE" Then
            TreeClass.Nodes.Add("Top Level")
            TreeClass.SelectedNode = GetNode("Top Level", TreeClass.Nodes)
            TreeClass.SelectedNode.Tag = getNewRefno()
        Else
            If Mode = "BROWSE MODE" Then

            Else
                TreeClass.Nodes.Add("Top Level")
                TreeClass.SelectedNode = GetNode("Top Level", TreeClass.Nodes)
                TreeClass.SelectedNode.Tag = frmEditClasses.SelectedClass
            End If
        End If
        Do While diggerDept <= LevelDept
            If diggerDept = 1 Then
                'establishing first level
                DGView.SelectAll()
                For Each selectedRow As DataGridViewRow In DGView.SelectedRows
                    xRow = selectedRow.Index
                    If diggerDept = DGView.Item(3, xRow).Value Then
                        TreeClass.Nodes.Add(DGView.Item(2, xRow).Value)
                        TreeClass.SelectedNode = GetNode(DGView.Item(2, xRow).Value, TreeClass.Nodes)
                        TreeClass.SelectedNode.Tag = DGView.Item(0, xRow).Value
                    End If
                Next
                xRow = 0
                DGView.Refresh()
                '#############################
            Else
                DGView.SelectAll()
                For Each SelectedRow As DataGridViewRow In DGView.SelectedRows
                    xRow = SelectedRow.Index

                    If diggerDept = DGView.Item(3, xRow).Value Then
                        TreeClass.SelectedNode = GetNode(DGView.Item(4, xRow).Value, TreeClass.Nodes)
                        If TreeClass.SelectedNode Is Nothing Then
                            TreeClass.Nodes.Add(DGView.Item(0, xRow).Value)
                        Else
                            TreeClass.SelectedNode.Nodes.Add(DGView.Item(2, xRow).Value)
                            TreeClass.SelectedNode = GetNode(DGView.Item(2, xRow).Value, TreeClass.Nodes)
                            TreeClass.SelectedNode.Tag = DGView.Item(0, xRow).Value
                        End If
                    End If
                Next
                DGView.Refresh()
            End If
            diggerDept += 1
        Loop

        If Mode = "EDIT MODE" Then
            RemoveClassFromTree(frmEditClasses.TxtClassName.Text & "���" & frmEditClasses.SelectedClass)
        End If

        EvaluateNodeName()
    End Sub
#End Region
    Private Sub FrmBrowseClass_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadClass()
        getLevelDept()
        ClassFromGridToTree(Mode)
        TreeClass.SelectedNode = GetNode("Top Level", TreeClass.Nodes)

    End Sub
    Private Sub TreeClass_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeClass.MouseHover
        On Error GoTo subTerminated
        getClassDetailsForToolTip(TreeClass.SelectedNode.Tag)
        If TreeClass.SelectedNode.Tag <> "" Then
            TreeClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TreeClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If TreeClass.SelectedNode.GetNodeCount(True) = -1 Then
            MsgBox("Please select a class!" + vbCr + "If you want to create a class in the first level," + vbCr + "Select Top Level.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
            TreeClass.Select()
        Else
            Data1 = TreeClass.SelectedNode.Text
            If Mode = "EDIT MODE" Then
                If TreeClass.SelectedNode.Text = "Top Level" Then
                    Data2 = ""
                Else
                    Data2 = TreeClass.SelectedNode.Tag
                End If
            Else
                Data2 = TreeClass.SelectedNode.Tag
            End If
            Me.Close()

        End If
    End Sub
    Private Sub TreeClass_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeClass.AfterSelect
        btnOk.Enabled = True
        On Error GoTo subTerminated
        getClassDetailsForToolTip(TreeClass.SelectedNode.Tag)
        If TreeClass.SelectedNode.Tag <> "" Then
            TreeClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TreeClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub TreeClass_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeClass.NodeMouseDoubleClick
        If TreeClass.SelectedNode.GetNodeCount(True) = -1 Then
            MsgBox("Please select a class!" + vbCr + "If you want to create a class in the first level," + vbCr + "Select Top Level.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
            TreeClass.Select()
        Else
            Data1 = TreeClass.SelectedNode.Text
            If Mode = "EDIT MODE" Then
                If TreeClass.SelectedNode.Text = "Top Level" Then
                    Data2 = ""
                Else
                    Data2 = TreeClass.SelectedNode.Tag
                End If
            Else
                Data2 = TreeClass.SelectedNode.Tag
            End If
            Me.Close()

        End If
    End Sub
End Class