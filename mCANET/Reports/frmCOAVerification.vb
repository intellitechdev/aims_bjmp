﻿Public Class frmCOAVerification


    Private SQLHelper As New NewSQLHelper
    Public Property IsLoadByCOA As Boolean = False
    Public Property ParentCOAForm As frmChartOfAccounts

    Private Sub SetPositionToRight()
        Dim MainWindowWidth As Integer = frmMain.Bounds.Width
        Dim MainWindowHieght As Integer = frmMain.Bounds.Height
        Dim XPosition As Integer = ((MainWindowWidth / 3) * 2) + 10
        Dim YPosition As Integer = (MainWindowHieght / 2) - (Me.Bounds.Height / 2)

        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New System.Drawing.Point(XPosition, YPosition)
    End Sub
    Public Sub LoadAccounts()
        Try
            Dim SQLStr As String = "SELECT acnt_id, acnt_desc FROM mAccounts WHERE fcLevel > 1 AND acnt_subof IS NULL AND co_id = '" & gCompanyID() & "' ORDER BY acnt_desc"

            LstAcnts.DisplayMember = "acnt_desc"
            LstAcnts.ValueMember = "acnt_id"
            LstAcnts.DataSource = SQLHelper.ExecuteDataset(SQLStr).Tables(0).DefaultView

            If LstAcnts.Items.Count = 0 Then
                Me.Close()
            End If
        Catch ex As Exception
            ShowErrorMessage("LoadAccounts", Me.Name, ex.Message)
        End Try
    End Sub

    Private Sub frmCOAVerification_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadAccounts()
        If IsLoadByCOA = True Then
            SetPositionToRight()
        End If
    End Sub
    Private Sub btnAcnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcnt.Click
        Me.Close()
    End Sub
    Private Sub frmCOAVerification_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.7
    End Sub
    Private Sub frmCOAVerification_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
    Private Sub LstAcnts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LstAcnts.DoubleClick
        If LstAcnts.SelectedIndex <> -1 Then
            If IsLoadByCOA = False Then
                Dim xForm As New frmChartOfAccounts
                xForm.AcntToBeZoom = LstAcnts.SelectedValue.ToString
                xForm.DisplayCOAVerificationForm = True
                pCallCompany(xForm)
                Me.Close()
            Else
                ParentCOAForm.ZoomToAccount(LstAcnts.SelectedValue.ToString)
            End If
        End If
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        LoadAccounts()
    End Sub
End Class