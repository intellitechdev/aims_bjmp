Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAEDDoctype
    Private Sub AEDDoctype_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadDocType()
    End Sub
    Private Sub LoadDocType()
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("Masterfile_LoadDoctype", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "fcDocName")
            Me.grdDoctype.DataSource = ds
            Me.grdDoctype.DataMember = "fcDocName"
            Me.grdDoctype.Columns(0).Width = 80
            grdDoctype.Columns(0).Name = "Initial"
            grdDoctype.Columns(1).Width = 250
            grdDoctype.Columns(1).Name = "Document Name"
            Me.grdDoctype.ReadOnly = True
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Document Type")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub AddEdit(ByVal Initial As String, ByVal DocName As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "usp_m_DocumentType_InsertUpdate", _
                New SqlParameter("@Initial", Initial), _
                New SqlParameter("@DocName", DocName))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub deleteDoctype(ByVal Dinitial As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "usp_m_DocumentType_Delete", _
                                      New SqlParameter("@inital", Dinitial))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteDoctype")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If Me.btnClose.Text = "Cancel" Then
            Me.txtName.Clear()
            Me.txtInitial.Clear()
            Me.btnEdit.Enabled = True
            Me.btnDelete.Enabled = True
            btnNew.Enabled = True
            btnNew.Text = "New"
            btnClose.Text = "Close"
            LoadDocType()
        Else
            Me.Close()
        End If
    End Sub
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If Me.btnNew.Text = "New" Then
            Me.txtInitial.Enabled = True
            Me.txtName.Enabled = True
            Me.txtName.Clear()
            Me.txtInitial.Clear()
            Me.btnNew.Text = "Update"
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            Me.btnClose.Text = "Cancel"
            'Call AddEdit(Me.txtInitial.Text, Me.txtName.Text)
        ElseIf Me.btnNew.Text = "Update" Then
            If txtInitial.Text = "" Or txtName.Text = "" Then
                MessageBox.Show("Please fill up the Fields", "Information", MessageBoxButtons.OK)
            Else
                Call AddEdit(Me.txtInitial.Text, Me.txtName.Text)
                btnNew.Text = "New"
                Me.txtInitial.Enabled = False
                Me.txtName.Enabled = False
                Me.btnClose.Text = "Close"
                Me.btnEdit.Enabled = True
                Me.btnDelete.Enabled = True
                Me.txtName.Clear()
                Me.txtInitial.Clear()
                Call LoadDocType()
                MessageBox.Show("Successfully added", "INFORMATION", MessageBoxButtons.OK)
            End If
        End If
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Me.grdDoctype.RowCount = 0 Then
            MessageBox.Show("There is no Data Display", "User ERROR", MessageBoxButtons.OK)
        Else
            Dim initial As String
            Try
                initial = Me.grdDoctype.CurrentRow.Cells(0).Value
                Dim x As New DialogResult
                x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                If x = System.Windows.Forms.DialogResult.OK Then
                    Call deleteDoctype(initial)
                    Call LoadDocType()
                ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
                    MessageBox.Show("Cancelled", "Information", MessageBoxButtons.OK)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error Deleting Data")
            End Try
        End If
    End Sub
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Me.grdDoctype.RowCount = 0 Then
            MessageBox.Show("There is no Data Display", "User ERROR", MessageBoxButtons.OK)
        ElseIf Me.grdDoctype.CurrentRow.Cells("Initial").Value = Nothing Then
            MessageBox.Show("Please Select the Row you want to Edit", "Information", MessageBoxButtons.OK)
        Else
            Me.btnNew.Text = "Update"
            Me.btnEdit.Enabled = False
            Me.btnDelete.Enabled = False
            Me.btnClose.Text = "Cancel"
            Me.txtInitial.Enabled = True
            Me.txtName.Enabled = True
            Me.txtInitial.Text = Me.grdDoctype.CurrentRow.Cells("Initial").Value
            Me.txtName.Text = Me.grdDoctype.CurrentRow.Cells("Document Name").Value
        End If
    End Sub
End Class