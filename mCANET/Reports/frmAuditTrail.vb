﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient

Public Class frmAuditTrail
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Dim pkuser As Integer

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\AuditTrail.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fdDate", dtpTrialBalance.Text)
        rptsummary.SetParameterValue("@fkUser", pkuser)
        rptsummary.SetParameterValue("@fdTo", dtpTo.Text)
        'rptsummary.SetParameterValue("@KeyAccount", acnt_id)
        'rptsummary.SetParameterValue("@fdDate", GetLastDayOfMonth(dtpTrialBalance.Text))
        'rptsummary.SetParameterValue("@prevMonth", GetLastDayOfPreviousMonth(dtpTrialBalance.Text))
        'rptsummary.SetParameterValue("@code", acnt_code)
        'rptsummary.SetParameterValue("@acnt_name", acnt_name)
    End Sub
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        LoadReport()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub frmBalanceSheetv2_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rptsummary.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        'AuditTrail_Save("REPORTS", "View Audit Trail Report > " & dtpTrialBalance.Text)
        If BackgroundWorker1.IsBusy = False Then
            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub frmAuditTrail_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadDoctype()
    End Sub

    Private Sub LoadDoctype()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_AuditTrail_Users_Select", con.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "User")
            With cboUser
                .ValueMember = "fxKeyUser"
                .DisplayMember = "fcFullName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "All"
            End With
            con.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cboUser_TextChanged(sender As System.Object, e As System.EventArgs) Handles cboUser.TextChanged
        pkuser = cboUser.SelectedValue
    End Sub
End Class