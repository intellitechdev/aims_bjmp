﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBirthdayBonus

    Private jvKeyID As String
    Private con As New Clsappconfiguration

    Dim Account As String
    Dim nRow As Integer

    Private Sub frmBirthdayBonus_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadAccountsCredit()
        LoadAccountsDebit()
    End Sub
    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        grdDetails.Rows.Clear()
        txtDebit.Text = "0.00"
        txtCredit.Text = "0.00"
        txtDocnum.Clear()
        Me.Close()
    End Sub
    Private Sub AddDetailItem(ByVal cID As String,
                             ByVal cName As String,
                             ByVal cAccountRef As String,
                             ByVal cAccountCode As String,
                             ByVal cAccountTitle As String,
                             ByVal Debit As Decimal,
                             ByVal Credit As Decimal,
                             ByVal cAccounts As String,
                             ByVal cMemID As String)
        Try
            Dim row As String() =
             {cID, cName, cAccountRef, cAccountCode, cAccountTitle, Format(CDec(Debit), "##,##0.00"), Format(CDec(Credit), "##,##0.00"), cAccounts, cMemID}

            Dim nRowIndex As Integer
            With grdDetails

                .Rows.Add(row)
                '.ClearSelection()
                nRowIndex = .Rows.Count - 1
                .FirstDisplayedScrollingRowIndex = nRowIndex
                '.Rows.Add()
            End With
        Catch
            MessageBox.Show(Err.ToString, "List..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
#Region "Birth Day Bonus Entry"
    Public Sub generateEntry()
        Try
            Dim vindex As Integer = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "spu_BirthdayBonus_GenerateEntry",
                                                              New SqlParameter("@bdatefrom", dtdateFrom.Value.Date),
                                                              New SqlParameter("@bdateto", dtDateTo.Value.Date),
                                                              New SqlParameter("@bonusamt", txtAmount.Text),
                                                              New SqlParameter("@acctcode", cboAccount.SelectedValue.ToString))
            If rd.HasRows Then
                While rd.Read
                    AddDetailItem(rd.Item("ID No.").ToString, rd.Item("Name").ToString, rd.Item("Acct Ref.").ToString, rd.Item("Code").ToString, rd.Item("Account Title").ToString, "0.00", rd.Item("Credit").ToString, rd.Item("acnt_id").ToString, rd.Item("pk_Employee").ToString)
                    'grdDetails.Rows.Add()
                    'With grdDetails.Rows(vindex)
                    '    .Cells("cID").Value = rd("ID No.").ToString
                    '    .Cells("cName").Value = rd("Name").ToString
                    '    .Cells("cAccountCode").Value = rd("Code").ToString
                    '    .Cells("cAccountTitle").Value = rd("Account Title").ToString
                    '    .Cells("Debit").Value = "0.00"
                    '    .Cells("Credit").Value = rd("Credit")
                    '    .Cells("cAccounts").Value = rd("acnt_id").ToString
                    '    .Cells("cMemID").Value = rd("pk_Employee").ToString
                    '    .Cells("cAccountRef").Value = rd("Acct Ref.").ToString
                    '    .Cells("fxKey").Value = ""
                    '    .Cells("cLoanRef").Value = " "
                    'End With
                    'vindex += 1
                End While
                computetotal()

                Dim ds As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "spu_BirthdayBonus_GenerateEntry_debit",
                                                              New SqlParameter("@accntDebit", cmbDebitAcct.SelectedValue.ToString))
                If ds.HasRows Then
                    While ds.Read
                        AddDetailItem("", "", "", ds.Item("acnt_code").ToString, ds.Item("acnt_name").ToString, txtCredit.Text, "0.00", ds.Item("acnt_id").ToString, "")
                        'grdDetails.Rows.Add()
                        'With grdDetails.Rows(vindex)
                        '    .Cells("cID").Value = ""
                        '    .Cells("cName").Value = ""
                        '    .Cells("cAccountCode").Value = ds("acnt_code").ToString
                        '    .Cells("cAccountTitle").Value = ds("acnt_name").ToString
                        '    .Cells("Debit").Value = frmGeneralJournalEntries.txtTotalCredit.Text.ToString
                        '    .Cells("Credit").Value = ""
                        '    .Cells("cAccounts").Value = ds("acnt_id").ToString
                        '    .Cells("cMemID").Value = ""
                        '    .Cells("cAccountRef").Value = ""
                        '    .Cells("fxKey").Value = ""
                        '    .Cells("cLoanRef").Value = " "
                        'End With
                        'vindex += 1
                    End While
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        If txtDocnum.Text = "" Then
            MessageBox.Show("Select Document first...!")
        ElseIf txtAmount.Text = "0.00" Then
            MessageBox.Show("Invalid Amount...!")
        Else
            Call generateEntry()
            Call computetotal()
            btnCreate.Enabled = False
            btnSave.Enabled = True
            lblCount.Text = "Total Entries : " + (grdDetails.RowCount - 1).ToString
        End If
    End Sub

    Private Sub LoadAccountsCredit()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "spu_BirthdayBonus_LoadAccount")
        With cboAccount
            .DataSource = ds.Tables(0)
            .DisplayMember = "acct"
            .ValueMember = "fcAccountCode"
        End With
    End Sub
    Private Sub LoadAccountsDebit()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "spu_BirthdayBonus_LoadAccount_Debit")
        With cmbDebitAcct
            .DataSource = ds.Tables(0)
            .DisplayMember = "acct"
            .ValueMember = "acnt_code"
        End With
    End Sub

    Private Sub btnDocnum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocnum.Click
        frmJVno.ShowDialog()
        If frmJVno.DialogResult = DialogResult.OK Then
            txtDocnum.Text = frmJVno.listDocNumber.SelectedItems.Item(0).Text
        End If
    End Sub
    Private Sub computetotal()
        Dim tdebit As Decimal
        Dim tcredit As Decimal
        Try
            For Each xRow As DataGridViewRow In grdDetails.Rows
                Dim totdebit As Object = grdDetails.Item("debit", xRow.Index).Value
                Dim totcredit As Object = grdDetails.Item("credit", xRow.Index).Value

                If totdebit IsNot Nothing Then
                    tdebit += totdebit
                End If

                If totcredit IsNot Nothing Then
                    tcredit += totcredit
                End If
            Next
            txtDebit.Text = Format(CDec(tdebit), "##,##0.00")
            txtCredit.Text = Format(CDec(tcredit), "##,##0.00")
        Catch ex As InvalidCastException
            MessageBox.Show("You have entered an invalid data. Please recheck your entries.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()

                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If grdDetails.Rows.Count = 0 Then
            MessageBox.Show("Generate Entry first to save..!")
        Else
            If bgwProcess.IsBusy = False Then
                bgwProcess.WorkerReportsProgress = True
                ProgressBar1.Visible = True
                bgwProcess.RunWorkerAsync()
            Else
                bgwProcess.CancelAsync()
                ProgressBar1.Visible = True
                bgwProcess.WorkerReportsProgress = True
                bgwProcess.RunWorkerAsync()
            End If
        End If
    End Sub
    Public Sub SaveBirthdayBunosEntry()
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            Dim companyId As String = gCompanyID()
            Dim journalNo As String = txtDocnum.Text
            Dim transDate As String = dtpDateSaved.Value.Date
            Dim memo As String = txtMemo.Text
            Dim totalAmount As String = txtCredit.Text
            Dim user As String = frmMain.currentUser.Text
            Dim subsidiaryID As String = NormalizeValuesInDataGridView(grdDetails, "cMemID", 0)
            Dim doctype As String = "JV"
            Dim isCancelled As Boolean = False

            'SAVE ENTRY HEADER/DETAILS
            Call SaveCurrentJournalEntry_Header(GetJVKeyID(), companyId, journalNo, transDate, totalAmount, user, doctype, isCancelled)
            Call SaveCurrentJournalEntry_Details()

        Catch ex As ArgumentOutOfRangeException
            MessageBox.Show("User Error! There is nothing to save.", "Transaction Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialNo As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd.Item("maxSerialNo")
            Else
                serialNo = 0
            End If
        End Using

        Return serialNo
    End Function
    Private Sub SaveCurrentJournalEntry_Header(ByVal jvKeyID As String, ByVal companyID As String, ByVal journalNo As String, ByVal transdate As Date, _
    ByVal total As Decimal, ByVal user As String, ByVal doctype As String, ByVal isCancelled As String)

        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(dtpDateSaved.Value.Date, doctype)
        Dim guid As New Guid(jvKeyID)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "CAS_t_GeneralJournal_Header_InsertUpdate", _
                    New SqlParameter("@fxKeyJVNo", guid),
                    New SqlParameter("@companyID", companyID),
                    New SqlParameter("@fdTransDate", transdate),
                    New SqlParameter("@fdTotAmt", total),
                    New SqlParameter("@user", user),
                    New SqlParameter("@doctypeInitial", doctype),
                    New SqlParameter("@maxSerialNo", maxSerialNo),
                    New SqlParameter("@fiEntryNo", journalNo),
                    New SqlParameter("@fbIsCancelled", isCancelled),
                    New SqlParameter("@fbIsPosted", 0),
                    New SqlParameter("@fdDatePrepared", dtpDateSaved.Text),
                    New SqlParameter("@FkCollector", ""))

            Call UpdateDocDate()
        Catch ex As Exception
            MsgBox("Can't save the transaction. Please notify the system developer if you seen this message.", MsgBoxStyle.Exclamation, "Transaction Entries")
            Exit Sub
        End Try
    End Sub
    Private Sub SaveCurrentJournalEntry_Details()
        Dim aRow As Integer
        Try
            For xRow As Integer = 0 To grdDetails.RowCount - 1
                nRow = xRow
                aRow = grdDetails.RowCount - 1
                Dim journalNo As String = txtDocnum.Text
                Dim sAccount As String = IIf(IsDBNull(grdDetails.Item("cAccounts", xRow).Value), Nothing, grdDetails.Item("cAccounts", xRow).Value.ToString())
                Dim jvDetailRecordID As String = NormalizeValuesInDataGridView(grdDetails, "key", xRow)
                Dim dDebit As Decimal = grdDetails.Item("debit", xRow).Value
                Dim dCredit As Decimal = grdDetails.Item("credit", xRow).Value
                Dim sMemo As String = txtMemo.Text
                Dim sNameID As String = grdDetails.Item("fxkey", xRow).Value
                Dim pkJournalID As String = GetJVKeyID()
                Dim dtTransact As Date = dtpDateSaved.Value.Date
                Dim AccountRef As String = NormalizeValuesInDataGridView(grdDetails, "refno", xRow)

                Account = sAccount
                If jvDetailRecordID = "" Then
                    jvDetailRecordID = System.Guid.NewGuid.ToString()
                End If

                If sNameID <> "" Then
                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save", _
                                      New SqlParameter("@fiEntryNo", journalNo), _
                                      New SqlParameter("@fxKeyAccount", sAccount), _
                                      New SqlParameter("@fdDebit", dDebit), _
                                      New SqlParameter("@fdCredit", dCredit), _
                                      New SqlParameter("@fcMemo", sMemo), _
                                      New SqlParameter("@fxKeyNameID", sNameID), _
                                      New SqlParameter("@fxKey", jvDetailRecordID), _
                                      New SqlParameter("@fk_JVHeader", GetJVKeyID()), _
                                      New SqlParameter("@transDate", dtTransact), _
                                      New SqlParameter("@fcLoanRef", ""), _
                                      New SqlParameter("@fcAccRef", AccountRef))
                    con.sqlconn.Close()
                Else
                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "usp_t_tJVEntry_details_save1", _
                                      New SqlParameter("@fiEntryNo", journalNo), _
                                      New SqlParameter("@fxKeyAccount", sAccount), _
                                      New SqlParameter("@fdDebit", dDebit), _
                                      New SqlParameter("@fdCredit", dCredit), _
                                      New SqlParameter("@fcMemo", sMemo), _
                                      New SqlParameter("@fxKey", jvDetailRecordID), _
                                      New SqlParameter("@fk_JVHeader", GetJVKeyID()), _
                                      New SqlParameter("@transDate", dtTransact), _
                                      New SqlParameter("@fcLoanRef", ""), _
                                      New SqlParameter("@fcAccRef", AccountRef))
                    con.sqlconn.Close()

                    'lblStatus.Text = "----Saving" & grdDetails.Item("memID", xRow).Value & " | " & grdDetails.Item("memName", xRow).Value
                End If
                Dim percent As Integer = (nRow / aRow) * 100
                bgwProcess.ReportProgress(percent)
            Next
            ''DELETE zero line
            Dim sSQLCmd As String = "DELETE FROM tJVEntry_items WHERE fdDebit = 0 AND fdCredit = 0"
            SqlHelper.ExecuteScalar(con.cnstring, CommandType.Text, sSQLCmd)

            'lblStatus.Text = ""
        Catch ex As Exception
            MsgBox("Can't save the transaction. Please notify the system developer if you seen this message.", MsgBoxStyle.Exclamation, "Transaction Entries")
            Exit Sub
        End Try

    End Sub
    Private Sub UpdateDocDate()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "usp_m_DocumentNumber_Update", _
                    New SqlParameter("@fcDocNumber", txtDocnum.Text),
                    New SqlParameter("@fcDoctype", "JOURNAL VOUCHER"),
                    New SqlParameter("@coid", gCompanyID()))
        Catch ex As Exception
            MsgBox("Error on updating document number " & txtDocnum.Text & ". Please notify the system developer if you seen this message.")
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        grdDetails.Rows.Clear()
        txtDebit.Text = "0.00"
        txtCredit.Text = "0.00"
        txtDocnum.Clear()
        lblCount.Text = ""
    End Sub

    Private Sub bgwProcess_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcess.DoWork
        SaveBirthdayBunosEntry()
    End Sub

    Private Sub bgwProcess_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwProcess.ProgressChanged
        Me.ProgressBar1.Value = e.ProgressPercentage
        lblStatus.Text = e.ProgressPercentage & "%" & " --Saving Line " & nRow & " | " & grdDetails.Item("mID", nRow).Value & " | " & grdDetails.Item("mName", nRow).Value
    End Sub

    Private Sub bgwProcess_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcess.RunWorkerCompleted
        MessageBox.Show("Data save successfully ! ", "Birthday Bunos", MessageBoxButtons.OK, MessageBoxIcon.Information)
        lblStatus.Text = ""
        ProgressBar1.Visible = False
        btnCreate.Enabled = True
        grdDetails.Rows.Clear()
        txtDebit.Text = "0.00"
        txtCredit.Text = "0.00"
        txtDocnum.Clear()
        lblCount.Text = ""
    End Sub
End Class