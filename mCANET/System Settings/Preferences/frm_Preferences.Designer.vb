<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Preferences
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Preferences))
        Me.grdPreferences = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnDefault = New System.Windows.Forms.Button
        Me.tabMyPreference = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.pnlSpellingMyPref = New System.Windows.Forms.Panel
        Me.GroupBox42 = New System.Windows.Forms.GroupBox
        Me.CheckBox18 = New System.Windows.Forms.CheckBox
        Me.CheckBox17 = New System.Windows.Forms.CheckBox
        Me.CheckBox16 = New System.Windows.Forms.CheckBox
        Me.CheckBox15 = New System.Windows.Forms.CheckBox
        Me.CheckBox14 = New System.Windows.Forms.CheckBox
        Me.chkSpelAlways = New System.Windows.Forms.CheckBox
        Me.Label106 = New System.Windows.Forms.Label
        Me.pnlServiceMyPref = New System.Windows.Forms.Panel
        Me.GroupBox40 = New System.Windows.Forms.GroupBox
        Me.chkServiceDontClose = New System.Windows.Forms.CheckBox
        Me.chkServiceOptionToSave = New System.Windows.Forms.CheckBox
        Me.Label96 = New System.Windows.Forms.Label
        Me.pnlSalesMyPref = New System.Windows.Forms.Panel
        Me.GroupBox32 = New System.Windows.Forms.GroupBox
        Me.RadioButton10 = New System.Windows.Forms.RadioButton
        Me.RadioButton11 = New System.Windows.Forms.RadioButton
        Me.RadioButton12 = New System.Windows.Forms.RadioButton
        Me.Label80 = New System.Windows.Forms.Label
        Me.pnlReportsMyPref = New System.Windows.Forms.Panel
        Me.GroupBox26 = New System.Windows.Forms.GroupBox
        Me.rbtnPatterns = New System.Windows.Forms.RadioButton
        Me.rbtn2D = New System.Windows.Forms.RadioButton
        Me.GroupBox27 = New System.Windows.Forms.GroupBox
        Me.rbtnDontRefresh = New System.Windows.Forms.RadioButton
        Me.Label76 = New System.Windows.Forms.Label
        Me.rbtnAutoRefresh = New System.Windows.Forms.RadioButton
        Me.rbtnPromtRefresh = New System.Windows.Forms.RadioButton
        Me.chkPromptModifyReport = New System.Windows.Forms.CheckBox
        Me.Label75 = New System.Windows.Forms.Label
        Me.pnlGeneralMyPref = New System.Windows.Forms.Panel
        Me.GroupBox18 = New System.Windows.Forms.GroupBox
        Me.RadioButton7 = New System.Windows.Forms.RadioButton
        Me.RadioButton5 = New System.Windows.Forms.RadioButton
        Me.RadioButton6 = New System.Windows.Forms.RadioButton
        Me.GroupBox17 = New System.Windows.Forms.GroupBox
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.GroupBox16 = New System.Windows.Forms.GroupBox
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.CheckBox13 = New System.Windows.Forms.CheckBox
        Me.CheckBox12 = New System.Windows.Forms.CheckBox
        Me.CheckBox11 = New System.Windows.Forms.CheckBox
        Me.CheckBox10 = New System.Windows.Forms.CheckBox
        Me.CheckBox9 = New System.Windows.Forms.CheckBox
        Me.CheckBox8 = New System.Windows.Forms.CheckBox
        Me.CheckBox7 = New System.Windows.Forms.CheckBox
        Me.CheckBox5 = New System.Windows.Forms.CheckBox
        Me.CheckBox4 = New System.Windows.Forms.CheckBox
        Me.CheckBox6 = New System.Windows.Forms.CheckBox
        Me.Label38 = New System.Windows.Forms.Label
        Me.pnlFinanceChargeMyPref = New System.Windows.Forms.Panel
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.pnlDesktopMyPref = New System.Windows.Forms.Panel
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.btnSound = New System.Windows.Forms.Button
        Me.btnDisplay = New System.Windows.Forms.Button
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.cboColor = New System.Windows.Forms.ComboBox
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.chkShowHomePage = New System.Windows.Forms.CheckBox
        Me.rbtnKeepPrevious = New System.Windows.Forms.RadioButton
        Me.rbtnDontSave = New System.Windows.Forms.RadioButton
        Me.rbtnSaveCurrent = New System.Windows.Forms.RadioButton
        Me.rbtnSaveWhenClose = New System.Windows.Forms.RadioButton
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.rbtnMulWindw = New System.Windows.Forms.RadioButton
        Me.rbtnOneWindw = New System.Windows.Forms.RadioButton
        Me.pnlCheckingMyPref = New System.Windows.Forms.Panel
        Me.Label28 = New System.Windows.Forms.Label
        Me.grpCheckingMyPref = New System.Windows.Forms.GroupBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cboAccforMkDeposits = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cboAccforSalesTx = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboAccforPayBills = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.cboAccforCheck = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.chkMkDeposits = New System.Windows.Forms.CheckBox
        Me.chkPaySalesTax = New System.Windows.Forms.CheckBox
        Me.chkPayBills = New System.Windows.Forms.CheckBox
        Me.chkWriteChecks = New System.Windows.Forms.CheckBox
        Me.pnlBillMyPref = New System.Windows.Forms.Panel
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblBill = New System.Windows.Forms.Label
        Me.lblBill2 = New System.Windows.Forms.Label
        Me.pnlAccountingMyPref = New System.Windows.Forms.Panel
        Me.lblCaptionAcctg = New System.Windows.Forms.Label
        Me.chkAcctgAutoFill = New System.Windows.Forms.CheckBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.pnlTimeCoPref = New System.Windows.Forms.Panel
        Me.cboTimeDays = New System.Windows.Forms.ComboBox
        Me.GroupBox43 = New System.Windows.Forms.GroupBox
        Me.rbtnTimeNo = New System.Windows.Forms.RadioButton
        Me.rbtnTimeYes = New System.Windows.Forms.RadioButton
        Me.Label107 = New System.Windows.Forms.Label
        Me.Label108 = New System.Windows.Forms.Label
        Me.pnlTaxCoPref = New System.Windows.Forms.Panel
        Me.grdTaxCategory = New System.Windows.Forms.DataGridView
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkHowTo = New System.Windows.Forms.LinkLabel
        Me.rbtnTaxNo = New System.Windows.Forms.RadioButton
        Me.rbtnTaxYes = New System.Windows.Forms.RadioButton
        Me.Label109 = New System.Windows.Forms.Label
        Me.Label110 = New System.Windows.Forms.Label
        Me.pnlSpellingCoPref = New System.Windows.Forms.Panel
        Me.Label103 = New System.Windows.Forms.Label
        Me.Label104 = New System.Windows.Forms.Label
        Me.Label105 = New System.Windows.Forms.Label
        Me.pnlServiceCoPref = New System.Windows.Forms.Panel
        Me.chkServiceAllow = New System.Windows.Forms.CheckBox
        Me.rbtnServiceAlways = New System.Windows.Forms.RadioButton
        Me.rbtnServiceAuto = New System.Windows.Forms.RadioButton
        Me.Label102 = New System.Windows.Forms.Label
        Me.Label100 = New System.Windows.Forms.Label
        Me.Label97 = New System.Windows.Forms.Label
        Me.pnlSendCoPref = New System.Windows.Forms.Panel
        Me.btnSendSpelling = New System.Windows.Forms.Button
        Me.txtSendBody = New System.Windows.Forms.TextBox
        Me.txtSendSubject = New System.Windows.Forms.TextBox
        Me.Label92 = New System.Windows.Forms.Label
        Me.txtSendBcc = New System.Windows.Forms.TextBox
        Me.Label99 = New System.Windows.Forms.Label
        Me.cboSendNameSeq = New System.Windows.Forms.ComboBox
        Me.cboSendDearTo = New System.Windows.Forms.ComboBox
        Me.cboSendDefault = New System.Windows.Forms.ComboBox
        Me.Label98 = New System.Windows.Forms.Label
        Me.Label101 = New System.Windows.Forms.Label
        Me.pnlSTaxCoPref = New System.Windows.Forms.Panel
        Me.GroupBox39 = New System.Windows.Forms.GroupBox
        Me.rbtnSTaxAnnually = New System.Windows.Forms.RadioButton
        Me.rbtnSTaxQuarterly = New System.Windows.Forms.RadioButton
        Me.rbtnSTaxMonthly = New System.Windows.Forms.RadioButton
        Me.GroupBox38 = New System.Windows.Forms.GroupBox
        Me.rbtnSTaxCashBasis = New System.Windows.Forms.RadioButton
        Me.rbtnSTaxAccrualBasis = New System.Windows.Forms.RadioButton
        Me.GroupBox37 = New System.Windows.Forms.GroupBox
        Me.cboSTaxTxItem = New System.Windows.Forms.ComboBox
        Me.cboSTaxNonTxItem = New System.Windows.Forms.ComboBox
        Me.chkSTaxT = New System.Windows.Forms.CheckBox
        Me.Label91 = New System.Windows.Forms.Label
        Me.Label89 = New System.Windows.Forms.Label
        Me.Label90 = New System.Windows.Forms.Label
        Me.rbtnSTaxNO = New System.Windows.Forms.RadioButton
        Me.rbtnSTaxYes = New System.Windows.Forms.RadioButton
        Me.Label88 = New System.Windows.Forms.Label
        Me.GroupBox41 = New System.Windows.Forms.GroupBox
        Me.btnSTaxAdd = New System.Windows.Forms.Button
        Me.lnkSTaxSample = New System.Windows.Forms.LinkLabel
        Me.Label93 = New System.Windows.Forms.Label
        Me.cboSTaxItem = New System.Windows.Forms.ComboBox
        Me.Label94 = New System.Windows.Forms.Label
        Me.Label95 = New System.Windows.Forms.Label
        Me.pnlSalesCoPref = New System.Windows.Forms.Panel
        Me.GroupBox36 = New System.Windows.Forms.GroupBox
        Me.chkUseUndepositFunds = New System.Windows.Forms.CheckBox
        Me.chkAutoCalcPay = New System.Windows.Forms.CheckBox
        Me.chkAutoPay = New System.Windows.Forms.CheckBox
        Me.GroupBox35 = New System.Windows.Forms.GroupBox
        Me.cboPickList = New System.Windows.Forms.ComboBox
        Me.cboPackingSlip = New System.Windows.Forms.ComboBox
        Me.Label87 = New System.Windows.Forms.Label
        Me.Label86 = New System.Windows.Forms.Label
        Me.Label85 = New System.Windows.Forms.Label
        Me.chkDontPrintZeroSO = New System.Windows.Forms.CheckBox
        Me.chkWarnDupSO = New System.Windows.Forms.CheckBox
        Me.chkSO = New System.Windows.Forms.CheckBox
        Me.GroupBox34 = New System.Windows.Forms.GroupBox
        Me.chkPriceLvl = New System.Windows.Forms.CheckBox
        Me.GroupBox33 = New System.Windows.Forms.GroupBox
        Me.cboInvoicePackSlip = New System.Windows.Forms.ComboBox
        Me.Label84 = New System.Windows.Forms.Label
        Me.txtPercentage = New System.Windows.Forms.TextBox
        Me.Label83 = New System.Windows.Forms.Label
        Me.chkXpenseAsIncome = New System.Windows.Forms.CheckBox
        Me.GroupBox31 = New System.Windows.Forms.GroupBox
        Me.chkWarnDupInvoice = New System.Windows.Forms.CheckBox
        Me.txtFOB = New System.Windows.Forms.TextBox
        Me.Label82 = New System.Windows.Forms.Label
        Me.cboShipMethod = New System.Windows.Forms.ComboBox
        Me.Label81 = New System.Windows.Forms.Label
        Me.Label79 = New System.Windows.Forms.Label
        Me.pnlReportsCoPref = New System.Windows.Forms.Panel
        Me.btnFormat = New System.Windows.Forms.Button
        Me.GroupBox30 = New System.Windows.Forms.GroupBox
        Me.btnClassify = New System.Windows.Forms.Button
        Me.Label77 = New System.Windows.Forms.Label
        Me.GroupBox29 = New System.Windows.Forms.GroupBox
        Me.rbtnDescription = New System.Windows.Forms.RadioButton
        Me.rbtnNameDescription = New System.Windows.Forms.RadioButton
        Me.rbtnName = New System.Windows.Forms.RadioButton
        Me.GroupBox25 = New System.Windows.Forms.GroupBox
        Me.rbtnAgeTrans = New System.Windows.Forms.RadioButton
        Me.rbtnAgeDue = New System.Windows.Forms.RadioButton
        Me.GroupBox28 = New System.Windows.Forms.GroupBox
        Me.rbtnCash = New System.Windows.Forms.RadioButton
        Me.rbtnAccrual = New System.Windows.Forms.RadioButton
        Me.Label78 = New System.Windows.Forms.Label
        Me.pnlRemindersCoPref = New System.Windows.Forms.Panel
        Me.Panel13 = New System.Windows.Forms.Panel
        Me.Label74 = New System.Windows.Forms.Label
        Me.rbtnToDoDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnToDoList = New System.Windows.Forms.RadioButton
        Me.rbtnToDoSummary = New System.Windows.Forms.RadioButton
        Me.Panel12 = New System.Windows.Forms.Panel
        Me.Label72 = New System.Windows.Forms.Label
        Me.rbtnPODontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnPOList = New System.Windows.Forms.RadioButton
        Me.rbtnPOSummary = New System.Windows.Forms.RadioButton
        Me.Panel11 = New System.Windows.Forms.Panel
        Me.Label73 = New System.Windows.Forms.Label
        Me.rbtnMoneyDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnMoneyList = New System.Windows.Forms.RadioButton
        Me.rbtnMoneySummary = New System.Windows.Forms.RadioButton
        Me.Panel10 = New System.Windows.Forms.Panel
        Me.Label70 = New System.Windows.Forms.Label
        Me.Label71 = New System.Windows.Forms.Label
        Me.rbtnTransDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnTransList = New System.Windows.Forms.RadioButton
        Me.rbtnTransSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysAfterDueDteTrans = New System.Windows.Forms.TextBox
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.Label68 = New System.Windows.Forms.Label
        Me.Label69 = New System.Windows.Forms.Label
        Me.rbtnBillsDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnBillsList = New System.Windows.Forms.RadioButton
        Me.rbtnBillsSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysAfterDueDteBills = New System.Windows.Forms.TextBox
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Label67 = New System.Windows.Forms.Label
        Me.rbtnAssemblyDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnAssemblyList = New System.Windows.Forms.RadioButton
        Me.rbtnAssemblySummary = New System.Windows.Forms.RadioButton
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Label66 = New System.Windows.Forms.Label
        Me.rbtnInventoryDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnInventoryList = New System.Windows.Forms.RadioButton
        Me.rbtnInventorySummary = New System.Windows.Forms.RadioButton
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Label64 = New System.Windows.Forms.Label
        Me.rbtnSODontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnSOList = New System.Windows.Forms.RadioButton
        Me.rbtnSOSummary = New System.Windows.Forms.RadioButton
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Label65 = New System.Windows.Forms.Label
        Me.rbtnSalesRctDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnSalesRctList = New System.Windows.Forms.RadioButton
        Me.rbtnSalesRctSummary = New System.Windows.Forms.RadioButton
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label62 = New System.Windows.Forms.Label
        Me.Label63 = New System.Windows.Forms.Label
        Me.rbtnOverdueInvoiceDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnOverdueInvoiceList = New System.Windows.Forms.RadioButton
        Me.rbtnOverdueInvoiceSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysAfterDueDte = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label60 = New System.Windows.Forms.Label
        Me.Label61 = New System.Windows.Forms.Label
        Me.rbtnInvoiceDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnInvoiceList = New System.Windows.Forms.RadioButton
        Me.rbtnInvoiceSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysBeforeInvoiceDte = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.rbtnPChkDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnPChkList = New System.Windows.Forms.RadioButton
        Me.rbtnPChkSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysBeforePyChkDte = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label57 = New System.Windows.Forms.Label
        Me.rbtnChkDontRmd = New System.Windows.Forms.RadioButton
        Me.rbtnChkList = New System.Windows.Forms.RadioButton
        Me.rbtnChkSummary = New System.Windows.Forms.RadioButton
        Me.txtDaysBeforeChkDte = New System.Windows.Forms.TextBox
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label58 = New System.Windows.Forms.Label
        Me.Label59 = New System.Windows.Forms.Label
        Me.pnlJobsCoPref = New System.Windows.Forms.Panel
        Me.chkDontPrintZero = New System.Windows.Forms.CheckBox
        Me.chkWarnDupEstimates = New System.Windows.Forms.CheckBox
        Me.GroupBox23 = New System.Windows.Forms.GroupBox
        Me.rbtnNotProgressInvoice = New System.Windows.Forms.RadioButton
        Me.rbtnProgressInvoice = New System.Windows.Forms.RadioButton
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.txtNotAward = New System.Windows.Forms.TextBox
        Me.Label50 = New System.Windows.Forms.Label
        Me.txtClose = New System.Windows.Forms.TextBox
        Me.Label49 = New System.Windows.Forms.Label
        Me.txtProgress = New System.Windows.Forms.TextBox
        Me.Label48 = New System.Windows.Forms.Label
        Me.txtAward = New System.Windows.Forms.TextBox
        Me.Label46 = New System.Windows.Forms.Label
        Me.txtPending = New System.Windows.Forms.TextBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.GroupBox24 = New System.Windows.Forms.GroupBox
        Me.sbtnNotCreateEstimates = New System.Windows.Forms.RadioButton
        Me.rbtnCreateEstimate = New System.Windows.Forms.RadioButton
        Me.Label47 = New System.Windows.Forms.Label
        Me.pnlItemsCoPref = New System.Windows.Forms.Panel
        Me.GroupBox20 = New System.Windows.Forms.GroupBox
        Me.btnEnable = New System.Windows.Forms.Button
        Me.GroupBox19 = New System.Windows.Forms.GroupBox
        Me.rbtnQtyAvailable = New System.Windows.Forms.RadioButton
        Me.rbtnQtyOnHand = New System.Windows.Forms.RadioButton
        Me.chkWarnNoInventory = New System.Windows.Forms.CheckBox
        Me.chkQtySO = New System.Windows.Forms.CheckBox
        Me.chkQtyRsv = New System.Windows.Forms.CheckBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.lnkQty = New System.Windows.Forms.LinkLabel
        Me.Label42 = New System.Windows.Forms.Label
        Me.chkWarnDuplicatePO = New System.Windows.Forms.CheckBox
        Me.chkActive = New System.Windows.Forms.CheckBox
        Me.Label43 = New System.Windows.Forms.Label
        Me.pnlIntegratedApplicationsCoPref = New System.Windows.Forms.Panel
        Me.Label41 = New System.Windows.Forms.Label
        Me.GroupBox22 = New System.Windows.Forms.GroupBox
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnProperties = New System.Windows.Forms.Button
        Me.lstApplication = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.chkNotifyUser = New System.Windows.Forms.CheckBox
        Me.chkDontAllow = New System.Windows.Forms.CheckBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.pnlGeneralCoPref = New System.Windows.Forms.Panel
        Me.chkNvrUpdateName = New System.Windows.Forms.CheckBox
        Me.GroupBox21 = New System.Windows.Forms.GroupBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.rbtnMinutes = New System.Windows.Forms.RadioButton
        Me.rbtnDecimal = New System.Windows.Forms.RadioButton
        Me.Label39 = New System.Windows.Forms.Label
        Me.chkShwFourDigits = New System.Windows.Forms.CheckBox
        Me.pnlFinanceChargeCoPref = New System.Windows.Forms.Panel
        Me.chkMarkToBePrint = New System.Windows.Forms.CheckBox
        Me.GroupBox15 = New System.Windows.Forms.GroupBox
        Me.rbtnInvoice = New System.Windows.Forms.RadioButton
        Me.rbtnDueDate = New System.Windows.Forms.RadioButton
        Me.Label37 = New System.Windows.Forms.Label
        Me.chkAssessFCharge = New System.Windows.Forms.CheckBox
        Me.cboFChargeAcc = New System.Windows.Forms.ComboBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtGracePeriodDay = New System.Windows.Forms.TextBox
        Me.txtMinFinanceCharge = New System.Windows.Forms.TextBox
        Me.txtAnnualInterest = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.pnlDesktopCoPref = New System.Windows.Forms.Panel
        Me.Label22 = New System.Windows.Forms.Label
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.lblTimeTracking = New System.Windows.Forms.Label
        Me.btnTimeTracking = New System.Windows.Forms.Button
        Me.lblPayroll = New System.Windows.Forms.Label
        Me.btnPayroll = New System.Windows.Forms.Button
        Me.lblInventory = New System.Windows.Forms.Label
        Me.btnInventory = New System.Windows.Forms.Button
        Me.lblSalesOrder = New System.Windows.Forms.Label
        Me.btnSalesOrder = New System.Windows.Forms.Button
        Me.lblSalesTax = New System.Windows.Forms.Label
        Me.btnSalesTax = New System.Windows.Forms.Button
        Me.lblEstimates = New System.Windows.Forms.Label
        Me.btnEstimates = New System.Windows.Forms.Button
        Me.Label21 = New System.Windows.Forms.Label
        Me.linkRemove = New System.Windows.Forms.LinkLabel
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.chkEnterBills = New System.Windows.Forms.CheckBox
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.chkStatements = New System.Windows.Forms.CheckBox
        Me.chkSalesReceipt = New System.Windows.Forms.CheckBox
        Me.chkInvoice = New System.Windows.Forms.CheckBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.pnlCheckingCoPref = New System.Windows.Forms.Panel
        Me.lblChecking = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.cboAcc2 = New System.Windows.Forms.ComboBox
        Me.cboAcc1 = New System.Windows.Forms.ComboBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.chkPayroll = New System.Windows.Forms.CheckBox
        Me.chkOpenPayChk = New System.Windows.Forms.CheckBox
        Me.chkAutoFill = New System.Windows.Forms.CheckBox
        Me.chkWarn = New System.Windows.Forms.CheckBox
        Me.chkStartPayee = New System.Windows.Forms.CheckBox
        Me.chkChangeDate = New System.Windows.Forms.CheckBox
        Me.chkPrintName = New System.Windows.Forms.CheckBox
        Me.pnlBillCoPref = New System.Windows.Forms.Panel
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.cboAcct = New System.Windows.Forms.ComboBox
        Me.chkAutomaticDiscount = New System.Windows.Forms.CheckBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.chkWarnDuplicate = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtDueDays = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.pnlAccountingCoPref = New System.Windows.Forms.Panel
        Me.Label25 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnSetPwd = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDayinFuture = New System.Windows.Forms.TextBox
        Me.txtDayinPast = New System.Windows.Forms.TextBox
        Me.chkWarnFuture = New System.Windows.Forms.CheckBox
        Me.chkWarnPast = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.chkPrompt = New System.Windows.Forms.CheckBox
        Me.chkUseCls = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkReqAcc = New System.Windows.Forms.CheckBox
        Me.chkShowLowest = New System.Windows.Forms.CheckBox
        Me.chkUseAccNo = New System.Windows.Forms.CheckBox
        CType(Me.grdPreferences, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMyPreference.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.pnlSpellingMyPref.SuspendLayout()
        Me.GroupBox42.SuspendLayout()
        Me.pnlServiceMyPref.SuspendLayout()
        Me.GroupBox40.SuspendLayout()
        Me.pnlSalesMyPref.SuspendLayout()
        Me.GroupBox32.SuspendLayout()
        Me.pnlReportsMyPref.SuspendLayout()
        Me.GroupBox26.SuspendLayout()
        Me.GroupBox27.SuspendLayout()
        Me.pnlGeneralMyPref.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.pnlFinanceChargeMyPref.SuspendLayout()
        Me.pnlDesktopMyPref.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCheckingMyPref.SuspendLayout()
        Me.grpCheckingMyPref.SuspendLayout()
        Me.pnlBillMyPref.SuspendLayout()
        Me.pnlAccountingMyPref.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.pnlTimeCoPref.SuspendLayout()
        Me.GroupBox43.SuspendLayout()
        Me.pnlTaxCoPref.SuspendLayout()
        CType(Me.grdTaxCategory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSpellingCoPref.SuspendLayout()
        Me.pnlServiceCoPref.SuspendLayout()
        Me.pnlSendCoPref.SuspendLayout()
        Me.pnlSTaxCoPref.SuspendLayout()
        Me.GroupBox39.SuspendLayout()
        Me.GroupBox38.SuspendLayout()
        Me.GroupBox37.SuspendLayout()
        Me.GroupBox41.SuspendLayout()
        Me.pnlSalesCoPref.SuspendLayout()
        Me.GroupBox36.SuspendLayout()
        Me.GroupBox35.SuspendLayout()
        Me.GroupBox34.SuspendLayout()
        Me.GroupBox33.SuspendLayout()
        Me.GroupBox31.SuspendLayout()
        Me.pnlReportsCoPref.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.GroupBox29.SuspendLayout()
        Me.GroupBox25.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.pnlRemindersCoPref.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlJobsCoPref.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.pnlItemsCoPref.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        Me.pnlIntegratedApplicationsCoPref.SuspendLayout()
        Me.GroupBox22.SuspendLayout()
        Me.pnlGeneralCoPref.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        Me.pnlFinanceChargeCoPref.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.pnlDesktopCoPref.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.pnlCheckingCoPref.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.pnlBillCoPref.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.pnlAccountingCoPref.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdPreferences
        '
        Me.grdPreferences.AllowUserToAddRows = False
        Me.grdPreferences.AllowUserToDeleteRows = False
        Me.grdPreferences.AllowUserToResizeRows = False
        Me.grdPreferences.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdPreferences.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grdPreferences.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grdPreferences.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPreferences.ColumnHeadersVisible = False
        Me.grdPreferences.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1})
        Me.grdPreferences.Location = New System.Drawing.Point(8, 8)
        Me.grdPreferences.Name = "grdPreferences"
        Me.grdPreferences.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.grdPreferences.RowHeadersVisible = False
        Me.grdPreferences.Size = New System.Drawing.Size(190, 398)
        Me.grdPreferences.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = ""
        Me.Column1.Name = "Column1"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(17, 423)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(98, 423)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnDefault
        '
        Me.btnDefault.Location = New System.Drawing.Point(532, 423)
        Me.btnDefault.Name = "btnDefault"
        Me.btnDefault.Size = New System.Drawing.Size(75, 23)
        Me.btnDefault.TabIndex = 4
        Me.btnDefault.Text = "&Default"
        Me.btnDefault.UseVisualStyleBackColor = True
        '
        'tabMyPreference
        '
        Me.tabMyPreference.Controls.Add(Me.TabPage1)
        Me.tabMyPreference.Controls.Add(Me.TabPage2)
        Me.tabMyPreference.Location = New System.Drawing.Point(204, 4)
        Me.tabMyPreference.Name = "tabMyPreference"
        Me.tabMyPreference.SelectedIndex = 0
        Me.tabMyPreference.Size = New System.Drawing.Size(443, 406)
        Me.tabMyPreference.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.pnlSpellingMyPref)
        Me.TabPage1.Controls.Add(Me.pnlServiceMyPref)
        Me.TabPage1.Controls.Add(Me.pnlSalesMyPref)
        Me.TabPage1.Controls.Add(Me.pnlReportsMyPref)
        Me.TabPage1.Controls.Add(Me.pnlGeneralMyPref)
        Me.TabPage1.Controls.Add(Me.pnlFinanceChargeMyPref)
        Me.TabPage1.Controls.Add(Me.pnlDesktopMyPref)
        Me.TabPage1.Controls.Add(Me.pnlCheckingMyPref)
        Me.TabPage1.Controls.Add(Me.pnlBillMyPref)
        Me.TabPage1.Controls.Add(Me.pnlAccountingMyPref)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(435, 380)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "My Preference"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'pnlSpellingMyPref
        '
        Me.pnlSpellingMyPref.Controls.Add(Me.GroupBox42)
        Me.pnlSpellingMyPref.Controls.Add(Me.chkSpelAlways)
        Me.pnlSpellingMyPref.Controls.Add(Me.Label106)
        Me.pnlSpellingMyPref.Location = New System.Drawing.Point(73, 252)
        Me.pnlSpellingMyPref.Name = "pnlSpellingMyPref"
        Me.pnlSpellingMyPref.Size = New System.Drawing.Size(413, 307)
        Me.pnlSpellingMyPref.TabIndex = 21
        '
        'GroupBox42
        '
        Me.GroupBox42.Controls.Add(Me.CheckBox18)
        Me.GroupBox42.Controls.Add(Me.CheckBox17)
        Me.GroupBox42.Controls.Add(Me.CheckBox16)
        Me.GroupBox42.Controls.Add(Me.CheckBox15)
        Me.GroupBox42.Controls.Add(Me.CheckBox14)
        Me.GroupBox42.Location = New System.Drawing.Point(19, 91)
        Me.GroupBox42.Name = "GroupBox42"
        Me.GroupBox42.Size = New System.Drawing.Size(374, 137)
        Me.GroupBox42.TabIndex = 9
        Me.GroupBox42.TabStop = False
        Me.GroupBox42.Text = "Ignore words with:"
        '
        'CheckBox18
        '
        Me.CheckBox18.AutoSize = True
        Me.CheckBox18.Location = New System.Drawing.Point(9, 111)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(156, 17)
        Me.CheckBox18.TabIndex = 13
        Me.CheckBox18.Text = "Mi&xed case (e.g. ""myClick"")"
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'CheckBox17
        '
        Me.CheckBox17.AutoSize = True
        Me.CheckBox17.Location = New System.Drawing.Point(9, 88)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(196, 17)
        Me.CheckBox17.TabIndex = 12
        Me.CheckBox17.Text = "All &UPPERCASE leters (e.g. ""ASAP"")"
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'CheckBox16
        '
        Me.CheckBox16.AutoSize = True
        Me.CheckBox16.Location = New System.Drawing.Point(9, 65)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(239, 17)
        Me.CheckBox16.TabIndex = 11
        Me.CheckBox16.Text = "Capitalized &first letter (e.g. ""Click Software"")"
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'CheckBox15
        '
        Me.CheckBox15.AutoSize = True
        Me.CheckBox15.Location = New System.Drawing.Point(9, 42)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(194, 17)
        Me.CheckBox15.TabIndex = 10
        Me.CheckBox15.Text = "&Numbers (e.g. ""12345"" and ""v1.0"")"
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'CheckBox14
        '
        Me.CheckBox14.AutoSize = True
        Me.CheckBox14.Location = New System.Drawing.Point(9, 19)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(307, 17)
        Me.CheckBox14.TabIndex = 9
        Me.CheckBox14.Text = "&Interenet addresses (e.g. ""http://www.myclicktech.com/"")"
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'chkSpelAlways
        '
        Me.chkSpelAlways.AutoSize = True
        Me.chkSpelAlways.Location = New System.Drawing.Point(19, 57)
        Me.chkSpelAlways.Name = "chkSpelAlways"
        Me.chkSpelAlways.Size = New System.Drawing.Size(375, 17)
        Me.chkSpelAlways.TabIndex = 8
        Me.chkSpelAlways.Text = "&Always check spelling before printing, saving or sending supported forms"
        Me.chkSpelAlways.UseVisualStyleBackColor = True
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label106.Location = New System.Drawing.Point(16, 14)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(57, 16)
        Me.Label106.TabIndex = 7
        Me.Label106.Text = "Spelling"
        '
        'pnlServiceMyPref
        '
        Me.pnlServiceMyPref.Controls.Add(Me.GroupBox40)
        Me.pnlServiceMyPref.Controls.Add(Me.Label96)
        Me.pnlServiceMyPref.Location = New System.Drawing.Point(101, 229)
        Me.pnlServiceMyPref.Name = "pnlServiceMyPref"
        Me.pnlServiceMyPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlServiceMyPref.TabIndex = 12
        '
        'GroupBox40
        '
        Me.GroupBox40.Controls.Add(Me.chkServiceDontClose)
        Me.GroupBox40.Controls.Add(Me.chkServiceOptionToSave)
        Me.GroupBox40.Location = New System.Drawing.Point(9, 59)
        Me.GroupBox40.Name = "GroupBox40"
        Me.GroupBox40.Size = New System.Drawing.Size(410, 99)
        Me.GroupBox40.TabIndex = 17
        Me.GroupBox40.TabStop = False
        Me.GroupBox40.Text = "Settings"
        '
        'chkServiceDontClose
        '
        Me.chkServiceDontClose.Location = New System.Drawing.Point(10, 42)
        Me.chkServiceDontClose.Name = "chkServiceDontClose"
        Me.chkServiceDontClose.Size = New System.Drawing.Size(391, 31)
        Me.chkServiceDontClose.TabIndex = 1
        Me.chkServiceDontClose.Text = "&If Accounting System is run by my browser, don't close it after Web Connect is d" & _
            "one."
        Me.chkServiceDontClose.UseVisualStyleBackColor = True
        '
        'chkServiceOptionToSave
        '
        Me.chkServiceOptionToSave.AutoSize = True
        Me.chkServiceOptionToSave.Location = New System.Drawing.Point(10, 23)
        Me.chkServiceOptionToSave.Name = "chkServiceOptionToSave"
        Me.chkServiceOptionToSave.Size = New System.Drawing.Size(391, 17)
        Me.chkServiceOptionToSave.TabIndex = 0
        Me.chkServiceOptionToSave.Text = "&Give me the option of saving a file whenever I download Web connect data."
        Me.chkServiceOptionToSave.UseVisualStyleBackColor = True
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label96.Location = New System.Drawing.Point(16, 11)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(132, 16)
        Me.Label96.TabIndex = 7
        Me.Label96.Text = "Service Connection"
        '
        'pnlSalesMyPref
        '
        Me.pnlSalesMyPref.Controls.Add(Me.GroupBox32)
        Me.pnlSalesMyPref.Controls.Add(Me.Label80)
        Me.pnlSalesMyPref.Location = New System.Drawing.Point(120, 203)
        Me.pnlSalesMyPref.Name = "pnlSalesMyPref"
        Me.pnlSalesMyPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlSalesMyPref.TabIndex = 11
        '
        'GroupBox32
        '
        Me.GroupBox32.Controls.Add(Me.RadioButton10)
        Me.GroupBox32.Controls.Add(Me.RadioButton11)
        Me.GroupBox32.Controls.Add(Me.RadioButton12)
        Me.GroupBox32.Location = New System.Drawing.Point(9, 59)
        Me.GroupBox32.Name = "GroupBox32"
        Me.GroupBox32.Size = New System.Drawing.Size(410, 99)
        Me.GroupBox32.TabIndex = 17
        Me.GroupBox32.TabStop = False
        Me.GroupBox32.Text = "Add available time/costs to invoices for the selected Jobs"
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Location = New System.Drawing.Point(33, 71)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(97, 17)
        Me.RadioButton10.TabIndex = 16
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "Ask what to do"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Location = New System.Drawing.Point(33, 48)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(92, 17)
        Me.RadioButton11.TabIndex = 14
        Me.RadioButton11.TabStop = True
        Me.RadioButton11.Text = "Don't add any"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Location = New System.Drawing.Point(33, 25)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(162, 17)
        Me.RadioButton12.TabIndex = 13
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "Prompt for time/costs to add"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label80.Location = New System.Drawing.Point(16, 11)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(130, 16)
        Me.Label80.TabIndex = 7
        Me.Label80.Text = "Sales && Customers"
        '
        'pnlReportsMyPref
        '
        Me.pnlReportsMyPref.Controls.Add(Me.GroupBox26)
        Me.pnlReportsMyPref.Controls.Add(Me.GroupBox27)
        Me.pnlReportsMyPref.Controls.Add(Me.chkPromptModifyReport)
        Me.pnlReportsMyPref.Controls.Add(Me.Label75)
        Me.pnlReportsMyPref.Location = New System.Drawing.Point(154, 178)
        Me.pnlReportsMyPref.Name = "pnlReportsMyPref"
        Me.pnlReportsMyPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlReportsMyPref.TabIndex = 10
        '
        'GroupBox26
        '
        Me.GroupBox26.Controls.Add(Me.rbtnPatterns)
        Me.GroupBox26.Controls.Add(Me.rbtn2D)
        Me.GroupBox26.Location = New System.Drawing.Point(9, 226)
        Me.GroupBox26.Name = "GroupBox26"
        Me.GroupBox26.Size = New System.Drawing.Size(409, 49)
        Me.GroupBox26.TabIndex = 18
        Me.GroupBox26.TabStop = False
        Me.GroupBox26.Text = "Graphs Only"
        '
        'rbtnPatterns
        '
        Me.rbtnPatterns.AutoSize = True
        Me.rbtnPatterns.Location = New System.Drawing.Point(197, 20)
        Me.rbtnPatterns.Name = "rbtnPatterns"
        Me.rbtnPatterns.Size = New System.Drawing.Size(87, 17)
        Me.rbtnPatterns.TabIndex = 14
        Me.rbtnPatterns.TabStop = True
        Me.rbtnPatterns.Text = "Use &patterns"
        Me.rbtnPatterns.UseVisualStyleBackColor = True
        '
        'rbtn2D
        '
        Me.rbtn2D.AutoSize = True
        Me.rbtn2D.Location = New System.Drawing.Point(9, 21)
        Me.rbtn2D.Name = "rbtn2D"
        Me.rbtn2D.Size = New System.Drawing.Size(153, 17)
        Me.rbtn2D.TabIndex = 13
        Me.rbtn2D.TabStop = True
        Me.rbtn2D.Text = "Draw graphs in &2D (faster)"
        Me.rbtn2D.UseVisualStyleBackColor = True
        '
        'GroupBox27
        '
        Me.GroupBox27.Controls.Add(Me.rbtnDontRefresh)
        Me.GroupBox27.Controls.Add(Me.Label76)
        Me.GroupBox27.Controls.Add(Me.rbtnAutoRefresh)
        Me.GroupBox27.Controls.Add(Me.rbtnPromtRefresh)
        Me.GroupBox27.Location = New System.Drawing.Point(8, 86)
        Me.GroupBox27.Name = "GroupBox27"
        Me.GroupBox27.Size = New System.Drawing.Size(410, 126)
        Me.GroupBox27.TabIndex = 17
        Me.GroupBox27.TabStop = False
        Me.GroupBox27.Text = "Reports and Graphs"
        '
        'rbtnDontRefresh
        '
        Me.rbtnDontRefresh.AutoSize = True
        Me.rbtnDontRefresh.Location = New System.Drawing.Point(26, 91)
        Me.rbtnDontRefresh.Name = "rbtnDontRefresh"
        Me.rbtnDontRefresh.Size = New System.Drawing.Size(88, 17)
        Me.rbtnDontRefresh.TabIndex = 16
        Me.rbtnDontRefresh.TabStop = True
        Me.rbtnDontRefresh.Text = "Don't re&fresh"
        Me.rbtnDontRefresh.UseVisualStyleBackColor = True
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(30, 25)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(244, 13)
        Me.Label76.TabIndex = 15
        Me.Label76.Text = "When a report or a graph needs to be refreshed:"
        '
        'rbtnAutoRefresh
        '
        Me.rbtnAutoRefresh.AutoSize = True
        Me.rbtnAutoRefresh.Location = New System.Drawing.Point(26, 68)
        Me.rbtnAutoRefresh.Name = "rbtnAutoRefresh"
        Me.rbtnAutoRefresh.Size = New System.Drawing.Size(129, 17)
        Me.rbtnAutoRefresh.TabIndex = 14
        Me.rbtnAutoRefresh.TabStop = True
        Me.rbtnAutoRefresh.Text = "Refresh &automatically"
        Me.rbtnAutoRefresh.UseVisualStyleBackColor = True
        '
        'rbtnPromtRefresh
        '
        Me.rbtnPromtRefresh.AutoSize = True
        Me.rbtnPromtRefresh.Location = New System.Drawing.Point(26, 45)
        Me.rbtnPromtRefresh.Name = "rbtnPromtRefresh"
        Me.rbtnPromtRefresh.Size = New System.Drawing.Size(127, 17)
        Me.rbtnPromtRefresh.TabIndex = 13
        Me.rbtnPromtRefresh.TabStop = True
        Me.rbtnPromtRefresh.Text = "Pr&ompt me to refresh"
        Me.rbtnPromtRefresh.UseVisualStyleBackColor = True
        '
        'chkPromptModifyReport
        '
        Me.chkPromptModifyReport.AutoSize = True
        Me.chkPromptModifyReport.Location = New System.Drawing.Point(18, 47)
        Me.chkPromptModifyReport.Name = "chkPromptModifyReport"
        Me.chkPromptModifyReport.Size = New System.Drawing.Size(314, 17)
        Me.chkPromptModifyReport.TabIndex = 8
        Me.chkPromptModifyReport.Text = "Prompt me to modify &report options before opening a report"
        Me.chkPromptModifyReport.UseVisualStyleBackColor = True
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label75.Location = New System.Drawing.Point(16, 11)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(124, 16)
        Me.Label75.TabIndex = 7
        Me.Label75.Text = "Reports && Graphs"
        '
        'pnlGeneralMyPref
        '
        Me.pnlGeneralMyPref.Controls.Add(Me.GroupBox18)
        Me.pnlGeneralMyPref.Controls.Add(Me.GroupBox17)
        Me.pnlGeneralMyPref.Controls.Add(Me.GroupBox16)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox12)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox11)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox10)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox9)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox8)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox7)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox5)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox4)
        Me.pnlGeneralMyPref.Controls.Add(Me.CheckBox6)
        Me.pnlGeneralMyPref.Controls.Add(Me.Label38)
        Me.pnlGeneralMyPref.Location = New System.Drawing.Point(188, 155)
        Me.pnlGeneralMyPref.Name = "pnlGeneralMyPref"
        Me.pnlGeneralMyPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlGeneralMyPref.TabIndex = 9
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.RadioButton7)
        Me.GroupBox18.Controls.Add(Me.RadioButton5)
        Me.GroupBox18.Controls.Add(Me.RadioButton6)
        Me.GroupBox18.Location = New System.Drawing.Point(9, 326)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(409, 45)
        Me.GroupBox18.TabIndex = 19
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "Keep Custom Item Information When Changing Item in Transactions"
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Location = New System.Drawing.Point(227, 21)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(54, 17)
        Me.RadioButton7.TabIndex = 15
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Never"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Location = New System.Drawing.Point(122, 20)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(59, 17)
        Me.RadioButton5.TabIndex = 14
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "Always"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Location = New System.Drawing.Point(9, 21)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(42, 17)
        Me.RadioButton6.TabIndex = 13
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Ask"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.RadioButton3)
        Me.GroupBox17.Controls.Add(Me.RadioButton4)
        Me.GroupBox17.Location = New System.Drawing.Point(10, 274)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(409, 49)
        Me.GroupBox17.TabIndex = 18
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Default Date to Use for New Transactions"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(197, 20)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(199, 17)
        Me.RadioButton3.TabIndex = 14
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Use the last entered date as default"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(9, 21)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(157, 17)
        Me.RadioButton4.TabIndex = 13
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Use today's date as default"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.RadioButton2)
        Me.GroupBox16.Controls.Add(Me.RadioButton1)
        Me.GroupBox16.Controls.Add(Me.CheckBox13)
        Me.GroupBox16.Location = New System.Drawing.Point(9, 169)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(410, 100)
        Me.GroupBox16.TabIndex = 17
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Automatically Recall Information"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(26, 68)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(262, 17)
        Me.RadioButton2.TabIndex = 14
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Pre-fill accounts for vendor based on past entries"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(26, 45)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(260, 17)
        Me.RadioButton1.TabIndex = 13
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Automatically recall &last transaction for this name"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'CheckBox13
        '
        Me.CheckBox13.AutoSize = True
        Me.CheckBox13.Location = New System.Drawing.Point(10, 23)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(286, 17)
        Me.CheckBox13.TabIndex = 12
        Me.CheckBox13.Text = "A&utomatically recall account or transaction information"
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'CheckBox12
        '
        Me.CheckBox12.Location = New System.Drawing.Point(230, 122)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(182, 40)
        Me.CheckBox12.TabIndex = 16
        Me.CheckBox12.Text = "&Warn when deleting a transaction or  unused list item"
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'CheckBox11
        '
        Me.CheckBox11.AutoSize = True
        Me.CheckBox11.Location = New System.Drawing.Point(230, 97)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(170, 17)
        Me.CheckBox11.TabIndex = 15
        Me.CheckBox11.Text = "&Show ToolTips for clipped text"
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        Me.CheckBox10.Location = New System.Drawing.Point(230, 55)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(182, 40)
        Me.CheckBox10.TabIndex = 14
        Me.CheckBox10.Text = "Turn off &pop-up messages for products  and services"
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.Location = New System.Drawing.Point(230, 36)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(182, 17)
        Me.CheckBox9.TabIndex = 13
        Me.CheckBox9.Text = "Bring back all &one time messages"
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.Location = New System.Drawing.Point(19, 145)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(182, 17)
        Me.CheckBox8.TabIndex = 12
        Me.CheckBox8.Text = "Warn when &editing a transaction"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.Location = New System.Drawing.Point(19, 121)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(183, 17)
        Me.CheckBox7.TabIndex = 11
        Me.CheckBox7.Text = "A&utomatically place decimal point"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(19, 97)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(193, 17)
        Me.CheckBox5.TabIndex = 10
        Me.CheckBox5.Text = "Beep when recording a &transaction"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.Location = New System.Drawing.Point(19, 60)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(202, 30)
        Me.CheckBox4.TabIndex = 9
        Me.CheckBox4.Text = "&Automatically open drop-down lists when typing "
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Location = New System.Drawing.Point(19, 36)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(202, 17)
        Me.CheckBox6.TabIndex = 8
        Me.CheckBox6.Text = "Pressing Enter moves between &fields"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(16, 11)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(57, 16)
        Me.Label38.TabIndex = 7
        Me.Label38.Text = "General"
        '
        'pnlFinanceChargeMyPref
        '
        Me.pnlFinanceChargeMyPref.Controls.Add(Me.Label29)
        Me.pnlFinanceChargeMyPref.Controls.Add(Me.Label30)
        Me.pnlFinanceChargeMyPref.Controls.Add(Me.Label31)
        Me.pnlFinanceChargeMyPref.Location = New System.Drawing.Point(215, 128)
        Me.pnlFinanceChargeMyPref.Name = "pnlFinanceChargeMyPref"
        Me.pnlFinanceChargeMyPref.Size = New System.Drawing.Size(394, 152)
        Me.pnlFinanceChargeMyPref.TabIndex = 8
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(16, 14)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(106, 16)
        Me.Label29.TabIndex = 7
        Me.Label29.Text = "Finance Charge"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(16, 57)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(269, 13)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "There are no personal preferences for Finance Charge"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(16, 81)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(334, 13)
        Me.Label31.TabIndex = 2
        Me.Label31.Text = "Please click the ""Company Preferences"" tab above for more options."
        '
        'pnlDesktopMyPref
        '
        Me.pnlDesktopMyPref.Controls.Add(Me.Label23)
        Me.pnlDesktopMyPref.Controls.Add(Me.GroupBox11)
        Me.pnlDesktopMyPref.Controls.Add(Me.GroupBox10)
        Me.pnlDesktopMyPref.Controls.Add(Me.GroupBox9)
        Me.pnlDesktopMyPref.Controls.Add(Me.GroupBox8)
        Me.pnlDesktopMyPref.Location = New System.Drawing.Point(254, 103)
        Me.pnlDesktopMyPref.Name = "pnlDesktopMyPref"
        Me.pnlDesktopMyPref.Size = New System.Drawing.Size(436, 380)
        Me.pnlDesktopMyPref.TabIndex = 5
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(16, 7)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(96, 16)
        Me.Label23.TabIndex = 6
        Me.Label23.Text = "Desktop View"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.btnSound)
        Me.GroupBox11.Controls.Add(Me.btnDisplay)
        Me.GroupBox11.Location = New System.Drawing.Point(6, 291)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(420, 68)
        Me.GroupBox11.TabIndex = 3
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Windows Settings"
        '
        'btnSound
        '
        Me.btnSound.Location = New System.Drawing.Point(224, 29)
        Me.btnSound.Name = "btnSound"
        Me.btnSound.Size = New System.Drawing.Size(75, 23)
        Me.btnSound.TabIndex = 1
        Me.btnSound.Text = "Sou&nds"
        Me.btnSound.UseVisualStyleBackColor = True
        '
        'btnDisplay
        '
        Me.btnDisplay.Location = New System.Drawing.Point(31, 29)
        Me.btnDisplay.Name = "btnDisplay"
        Me.btnDisplay.Size = New System.Drawing.Size(75, 23)
        Me.btnDisplay.TabIndex = 0
        Me.btnDisplay.Text = "D&isplay"
        Me.btnDisplay.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.cboColor)
        Me.GroupBox10.Location = New System.Drawing.Point(3, 224)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(423, 62)
        Me.GroupBox10.TabIndex = 2
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Color Scheme"
        '
        'cboColor
        '
        Me.cboColor.FormattingEnabled = True
        Me.cboColor.Location = New System.Drawing.Point(31, 22)
        Me.cboColor.Name = "cboColor"
        Me.cboColor.Size = New System.Drawing.Size(268, 21)
        Me.cboColor.TabIndex = 0
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.chkShowHomePage)
        Me.GroupBox9.Controls.Add(Me.rbtnKeepPrevious)
        Me.GroupBox9.Controls.Add(Me.rbtnDontSave)
        Me.GroupBox9.Controls.Add(Me.rbtnSaveCurrent)
        Me.GroupBox9.Controls.Add(Me.rbtnSaveWhenClose)
        Me.GroupBox9.Location = New System.Drawing.Point(6, 102)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(420, 117)
        Me.GroupBox9.TabIndex = 1
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Desktop"
        '
        'chkShowHomePage
        '
        Me.chkShowHomePage.AutoSize = True
        Me.chkShowHomePage.Location = New System.Drawing.Point(31, 80)
        Me.chkShowHomePage.Name = "chkShowHomePage"
        Me.chkShowHomePage.Size = New System.Drawing.Size(251, 17)
        Me.chkShowHomePage.TabIndex = 5
        Me.chkShowHomePage.Text = "Show Home page when opening a company file"
        Me.chkShowHomePage.UseVisualStyleBackColor = True
        '
        'rbtnKeepPrevious
        '
        Me.rbtnKeepPrevious.AutoSize = True
        Me.rbtnKeepPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.rbtnKeepPrevious.Location = New System.Drawing.Point(224, 45)
        Me.rbtnKeepPrevious.Name = "rbtnKeepPrevious"
        Me.rbtnKeepPrevious.Size = New System.Drawing.Size(174, 17)
        Me.rbtnKeepPrevious.TabIndex = 4
        Me.rbtnKeepPrevious.TabStop = True
        Me.rbtnKeepPrevious.Text = "Keep &previously saved desktop"
        Me.rbtnKeepPrevious.UseVisualStyleBackColor = True
        '
        'rbtnDontSave
        '
        Me.rbtnDontSave.AutoSize = True
        Me.rbtnDontSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.rbtnDontSave.Location = New System.Drawing.Point(224, 22)
        Me.rbtnDontSave.Name = "rbtnDontSave"
        Me.rbtnDontSave.Size = New System.Drawing.Size(136, 17)
        Me.rbtnDontSave.TabIndex = 3
        Me.rbtnDontSave.TabStop = True
        Me.rbtnDontSave.Text = "Don't save the des&ktop"
        Me.rbtnDontSave.UseVisualStyleBackColor = True
        '
        'rbtnSaveCurrent
        '
        Me.rbtnSaveCurrent.AutoSize = True
        Me.rbtnSaveCurrent.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.rbtnSaveCurrent.Location = New System.Drawing.Point(31, 45)
        Me.rbtnSaveCurrent.Name = "rbtnSaveCurrent"
        Me.rbtnSaveCurrent.Size = New System.Drawing.Size(128, 17)
        Me.rbtnSaveCurrent.TabIndex = 2
        Me.rbtnSaveCurrent.TabStop = True
        Me.rbtnSaveCurrent.Text = "Save cu&rrent desktop"
        Me.rbtnSaveCurrent.UseVisualStyleBackColor = True
        '
        'rbtnSaveWhenClose
        '
        Me.rbtnSaveWhenClose.AutoSize = True
        Me.rbtnSaveWhenClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.rbtnSaveWhenClose.Location = New System.Drawing.Point(31, 22)
        Me.rbtnSaveWhenClose.Name = "rbtnSaveWhenClose"
        Me.rbtnSaveWhenClose.Size = New System.Drawing.Size(159, 17)
        Me.rbtnSaveWhenClose.TabIndex = 1
        Me.rbtnSaveWhenClose.TabStop = True
        Me.rbtnSaveWhenClose.Text = "S&ave when closing company"
        Me.rbtnSaveWhenClose.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.PictureBox2)
        Me.GroupBox8.Controls.Add(Me.PictureBox1)
        Me.GroupBox8.Controls.Add(Me.rbtnMulWindw)
        Me.GroupBox8.Controls.Add(Me.rbtnOneWindw)
        Me.GroupBox8.Location = New System.Drawing.Point(6, 32)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(420, 65)
        Me.GroupBox8.TabIndex = 0
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "View"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(322, 31)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(27, 19)
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(113, 29)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(27, 19)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'rbtnMulWindw
        '
        Me.rbtnMulWindw.AutoSize = True
        Me.rbtnMulWindw.Location = New System.Drawing.Point(224, 29)
        Me.rbtnMulWindw.Name = "rbtnMulWindw"
        Me.rbtnMulWindw.Size = New System.Drawing.Size(102, 17)
        Me.rbtnMulWindw.TabIndex = 1
        Me.rbtnMulWindw.TabStop = True
        Me.rbtnMulWindw.Text = "M&ultiple Window"
        Me.rbtnMulWindw.UseVisualStyleBackColor = True
        '
        'rbtnOneWindw
        '
        Me.rbtnOneWindw.AutoSize = True
        Me.rbtnOneWindw.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.rbtnOneWindw.Location = New System.Drawing.Point(31, 31)
        Me.rbtnOneWindw.Name = "rbtnOneWindw"
        Me.rbtnOneWindw.Size = New System.Drawing.Size(86, 17)
        Me.rbtnOneWindw.TabIndex = 0
        Me.rbtnOneWindw.TabStop = True
        Me.rbtnOneWindw.Text = "&One Window"
        Me.rbtnOneWindw.UseVisualStyleBackColor = True
        '
        'pnlCheckingMyPref
        '
        Me.pnlCheckingMyPref.Controls.Add(Me.Label28)
        Me.pnlCheckingMyPref.Controls.Add(Me.grpCheckingMyPref)
        Me.pnlCheckingMyPref.Location = New System.Drawing.Point(305, 66)
        Me.pnlCheckingMyPref.Name = "pnlCheckingMyPref"
        Me.pnlCheckingMyPref.Size = New System.Drawing.Size(429, 287)
        Me.pnlCheckingMyPref.TabIndex = 7
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(16, 14)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(65, 16)
        Me.Label28.TabIndex = 7
        Me.Label28.Text = "Checking"
        '
        'grpCheckingMyPref
        '
        Me.grpCheckingMyPref.Controls.Add(Me.Label14)
        Me.grpCheckingMyPref.Controls.Add(Me.cboAccforMkDeposits)
        Me.grpCheckingMyPref.Controls.Add(Me.Label15)
        Me.grpCheckingMyPref.Controls.Add(Me.cboAccforSalesTx)
        Me.grpCheckingMyPref.Controls.Add(Me.Label13)
        Me.grpCheckingMyPref.Controls.Add(Me.cboAccforPayBills)
        Me.grpCheckingMyPref.Controls.Add(Me.Label12)
        Me.grpCheckingMyPref.Controls.Add(Me.cboAccforCheck)
        Me.grpCheckingMyPref.Controls.Add(Me.Label11)
        Me.grpCheckingMyPref.Controls.Add(Me.Label10)
        Me.grpCheckingMyPref.Controls.Add(Me.Label9)
        Me.grpCheckingMyPref.Controls.Add(Me.Label7)
        Me.grpCheckingMyPref.Controls.Add(Me.chkMkDeposits)
        Me.grpCheckingMyPref.Controls.Add(Me.chkPaySalesTax)
        Me.grpCheckingMyPref.Controls.Add(Me.chkPayBills)
        Me.grpCheckingMyPref.Controls.Add(Me.chkWriteChecks)
        Me.grpCheckingMyPref.Location = New System.Drawing.Point(8, 82)
        Me.grpCheckingMyPref.Name = "grpCheckingMyPref"
        Me.grpCheckingMyPref.Size = New System.Drawing.Size(414, 150)
        Me.grpCheckingMyPref.TabIndex = 4
        Me.grpCheckingMyPref.TabStop = False
        Me.grpCheckingMyPref.Text = "Select Default Accounts to Use"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(363, 117)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "account"
        '
        'cboAccforMkDeposits
        '
        Me.cboAccforMkDeposits.FormattingEnabled = True
        Me.cboAccforMkDeposits.Location = New System.Drawing.Point(209, 113)
        Me.cboAccforMkDeposits.Name = "cboAccforMkDeposits"
        Me.cboAccforMkDeposits.Size = New System.Drawing.Size(148, 21)
        Me.cboAccforMkDeposits.TabIndex = 14
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(363, 89)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 13)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "account"
        '
        'cboAccforSalesTx
        '
        Me.cboAccforSalesTx.FormattingEnabled = True
        Me.cboAccforSalesTx.Location = New System.Drawing.Point(209, 85)
        Me.cboAccforSalesTx.Name = "cboAccforSalesTx"
        Me.cboAccforSalesTx.Size = New System.Drawing.Size(148, 21)
        Me.cboAccforSalesTx.TabIndex = 12
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(363, 59)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 13)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "account"
        '
        'cboAccforPayBills
        '
        Me.cboAccforPayBills.FormattingEnabled = True
        Me.cboAccforPayBills.Location = New System.Drawing.Point(209, 55)
        Me.cboAccforPayBills.Name = "cboAccforPayBills"
        Me.cboAccforPayBills.Size = New System.Drawing.Size(148, 21)
        Me.cboAccforPayBills.TabIndex = 10
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(363, 31)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "account"
        '
        'cboAccforCheck
        '
        Me.cboAccforCheck.FormattingEnabled = True
        Me.cboAccforCheck.Location = New System.Drawing.Point(209, 27)
        Me.cboAccforCheck.Name = "cboAccforCheck"
        Me.cboAccforCheck.Size = New System.Drawing.Size(148, 21)
        Me.cboAccforCheck.TabIndex = 8
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(151, 117)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(52, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "form with"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(151, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "form with"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(151, 59)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "form with"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(151, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "form with"
        '
        'chkMkDeposits
        '
        Me.chkMkDeposits.AutoSize = True
        Me.chkMkDeposits.Location = New System.Drawing.Point(10, 116)
        Me.chkMkDeposits.Name = "chkMkDeposits"
        Me.chkMkDeposits.Size = New System.Drawing.Size(143, 17)
        Me.chkMkDeposits.TabIndex = 3
        Me.chkMkDeposits.Text = "Open the Make Deposits"
        Me.chkMkDeposits.UseVisualStyleBackColor = True
        '
        'chkPaySalesTax
        '
        Me.chkPaySalesTax.AutoSize = True
        Me.chkPaySalesTax.Location = New System.Drawing.Point(10, 87)
        Me.chkPaySalesTax.Name = "chkPaySalesTax"
        Me.chkPaySalesTax.Size = New System.Drawing.Size(141, 17)
        Me.chkPaySalesTax.TabIndex = 2
        Me.chkPaySalesTax.Text = "Open the Pay Sales Tax"
        Me.chkPaySalesTax.UseVisualStyleBackColor = True
        '
        'chkPayBills
        '
        Me.chkPayBills.AutoSize = True
        Me.chkPayBills.Location = New System.Drawing.Point(10, 58)
        Me.chkPayBills.Name = "chkPayBills"
        Me.chkPayBills.Size = New System.Drawing.Size(112, 17)
        Me.chkPayBills.TabIndex = 1
        Me.chkPayBills.Text = "Open the Pay Bills"
        Me.chkPayBills.UseVisualStyleBackColor = True
        '
        'chkWriteChecks
        '
        Me.chkWriteChecks.AutoSize = True
        Me.chkWriteChecks.Location = New System.Drawing.Point(10, 29)
        Me.chkWriteChecks.Name = "chkWriteChecks"
        Me.chkWriteChecks.Size = New System.Drawing.Size(132, 17)
        Me.chkWriteChecks.TabIndex = 0
        Me.chkWriteChecks.Text = "Open the Write Cheks"
        Me.chkWriteChecks.UseVisualStyleBackColor = True
        '
        'pnlBillMyPref
        '
        Me.pnlBillMyPref.Controls.Add(Me.lblCaption)
        Me.pnlBillMyPref.Controls.Add(Me.lblBill)
        Me.pnlBillMyPref.Controls.Add(Me.lblBill2)
        Me.pnlBillMyPref.Location = New System.Drawing.Point(321, 39)
        Me.pnlBillMyPref.Name = "pnlBillMyPref"
        Me.pnlBillMyPref.Size = New System.Drawing.Size(394, 152)
        Me.pnlBillMyPref.TabIndex = 3
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.Location = New System.Drawing.Point(16, 14)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(231, 16)
        Me.lblCaption.TabIndex = 7
        Me.lblCaption.Text = "Bills/Integrated Applications/Jobs"
        '
        'lblBill
        '
        Me.lblBill.AutoSize = True
        Me.lblBill.Location = New System.Drawing.Point(16, 57)
        Me.lblBill.Name = "lblBill"
        Me.lblBill.Size = New System.Drawing.Size(215, 13)
        Me.lblBill.TabIndex = 1
        Me.lblBill.Text = "There are no personal preferences for Bills."
        '
        'lblBill2
        '
        Me.lblBill2.AutoSize = True
        Me.lblBill2.Location = New System.Drawing.Point(16, 81)
        Me.lblBill2.Name = "lblBill2"
        Me.lblBill2.Size = New System.Drawing.Size(334, 13)
        Me.lblBill2.TabIndex = 2
        Me.lblBill2.Text = "Please click the ""Company Preferences"" tab above for more options."
        '
        'pnlAccountingMyPref
        '
        Me.pnlAccountingMyPref.Controls.Add(Me.lblCaptionAcctg)
        Me.pnlAccountingMyPref.Controls.Add(Me.chkAcctgAutoFill)
        Me.pnlAccountingMyPref.Location = New System.Drawing.Point(343, 6)
        Me.pnlAccountingMyPref.Name = "pnlAccountingMyPref"
        Me.pnlAccountingMyPref.Size = New System.Drawing.Size(394, 152)
        Me.pnlAccountingMyPref.TabIndex = 6
        '
        'lblCaptionAcctg
        '
        Me.lblCaptionAcctg.AutoSize = True
        Me.lblCaptionAcctg.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaptionAcctg.Location = New System.Drawing.Point(16, 14)
        Me.lblCaptionAcctg.Name = "lblCaptionAcctg"
        Me.lblCaptionAcctg.Size = New System.Drawing.Size(239, 16)
        Me.lblCaptionAcctg.TabIndex = 7
        Me.lblCaptionAcctg.Text = "Accounting/Reminders/Send Forms"
        '
        'chkAcctgAutoFill
        '
        Me.chkAcctgAutoFill.Location = New System.Drawing.Point(19, 60)
        Me.chkAcctgAutoFill.Name = "chkAcctgAutoFill"
        Me.chkAcctgAutoFill.Size = New System.Drawing.Size(325, 33)
        Me.chkAcctgAutoFill.TabIndex = 0
        Me.chkAcctgAutoFill.Text = "Auto check the ""To be emailed"" checkbox if customer's preferred Send Method is Em" & _
            "ail"
        Me.chkAcctgAutoFill.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.pnlTimeCoPref)
        Me.TabPage2.Controls.Add(Me.pnlTaxCoPref)
        Me.TabPage2.Controls.Add(Me.pnlSpellingCoPref)
        Me.TabPage2.Controls.Add(Me.pnlServiceCoPref)
        Me.TabPage2.Controls.Add(Me.pnlSendCoPref)
        Me.TabPage2.Controls.Add(Me.pnlSTaxCoPref)
        Me.TabPage2.Controls.Add(Me.pnlSalesCoPref)
        Me.TabPage2.Controls.Add(Me.pnlReportsCoPref)
        Me.TabPage2.Controls.Add(Me.pnlRemindersCoPref)
        Me.TabPage2.Controls.Add(Me.pnlJobsCoPref)
        Me.TabPage2.Controls.Add(Me.pnlItemsCoPref)
        Me.TabPage2.Controls.Add(Me.pnlIntegratedApplicationsCoPref)
        Me.TabPage2.Controls.Add(Me.pnlGeneralCoPref)
        Me.TabPage2.Controls.Add(Me.pnlFinanceChargeCoPref)
        Me.TabPage2.Controls.Add(Me.pnlDesktopCoPref)
        Me.TabPage2.Controls.Add(Me.pnlCheckingCoPref)
        Me.TabPage2.Controls.Add(Me.pnlBillCoPref)
        Me.TabPage2.Controls.Add(Me.pnlAccountingCoPref)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(435, 380)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Company Preference"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'pnlTimeCoPref
        '
        Me.pnlTimeCoPref.Controls.Add(Me.cboTimeDays)
        Me.pnlTimeCoPref.Controls.Add(Me.GroupBox43)
        Me.pnlTimeCoPref.Controls.Add(Me.Label107)
        Me.pnlTimeCoPref.Controls.Add(Me.Label108)
        Me.pnlTimeCoPref.Location = New System.Drawing.Point(5, 356)
        Me.pnlTimeCoPref.Name = "pnlTimeCoPref"
        Me.pnlTimeCoPref.Size = New System.Drawing.Size(394, 202)
        Me.pnlTimeCoPref.TabIndex = 21
        '
        'cboTimeDays
        '
        Me.cboTimeDays.FormattingEnabled = True
        Me.cboTimeDays.Location = New System.Drawing.Point(137, 137)
        Me.cboTimeDays.Name = "cboTimeDays"
        Me.cboTimeDays.Size = New System.Drawing.Size(153, 21)
        Me.cboTimeDays.TabIndex = 9
        '
        'GroupBox43
        '
        Me.GroupBox43.Controls.Add(Me.rbtnTimeNo)
        Me.GroupBox43.Controls.Add(Me.rbtnTimeYes)
        Me.GroupBox43.Location = New System.Drawing.Point(19, 46)
        Me.GroupBox43.Name = "GroupBox43"
        Me.GroupBox43.Size = New System.Drawing.Size(271, 73)
        Me.GroupBox43.TabIndex = 8
        Me.GroupBox43.TabStop = False
        Me.GroupBox43.Text = "Do You Track Time?"
        '
        'rbtnTimeNo
        '
        Me.rbtnTimeNo.AutoSize = True
        Me.rbtnTimeNo.Location = New System.Drawing.Point(154, 32)
        Me.rbtnTimeNo.Name = "rbtnTimeNo"
        Me.rbtnTimeNo.Size = New System.Drawing.Size(38, 17)
        Me.rbtnTimeNo.TabIndex = 1
        Me.rbtnTimeNo.TabStop = True
        Me.rbtnTimeNo.Text = "&No"
        Me.rbtnTimeNo.UseVisualStyleBackColor = True
        '
        'rbtnTimeYes
        '
        Me.rbtnTimeYes.AutoSize = True
        Me.rbtnTimeYes.Location = New System.Drawing.Point(25, 32)
        Me.rbtnTimeYes.Name = "rbtnTimeYes"
        Me.rbtnTimeYes.Size = New System.Drawing.Size(42, 17)
        Me.rbtnTimeYes.TabIndex = 0
        Me.rbtnTimeYes.TabStop = True
        Me.rbtnTimeYes.Text = "&Yes"
        Me.rbtnTimeYes.UseVisualStyleBackColor = True
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(16, 14)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(95, 16)
        Me.Label107.TabIndex = 7
        Me.Label107.Text = "Time Tracking"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(16, 144)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(116, 13)
        Me.Label108.TabIndex = 1
        Me.Label108.Text = "&First day of work week"
        '
        'pnlTaxCoPref
        '
        Me.pnlTaxCoPref.Controls.Add(Me.grdTaxCategory)
        Me.pnlTaxCoPref.Controls.Add(Me.lnkHowTo)
        Me.pnlTaxCoPref.Controls.Add(Me.rbtnTaxNo)
        Me.pnlTaxCoPref.Controls.Add(Me.rbtnTaxYes)
        Me.pnlTaxCoPref.Controls.Add(Me.Label109)
        Me.pnlTaxCoPref.Controls.Add(Me.Label110)
        Me.pnlTaxCoPref.Location = New System.Drawing.Point(19, 338)
        Me.pnlTaxCoPref.Name = "pnlTaxCoPref"
        Me.pnlTaxCoPref.Size = New System.Drawing.Size(394, 373)
        Me.pnlTaxCoPref.TabIndex = 22
        '
        'grdTaxCategory
        '
        Me.grdTaxCategory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdTaxCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdTaxCategory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4})
        Me.grdTaxCategory.Location = New System.Drawing.Point(19, 100)
        Me.grdTaxCategory.Name = "grdTaxCategory"
        Me.grdTaxCategory.Size = New System.Drawing.Size(365, 266)
        Me.grdTaxCategory.TabIndex = 9
        '
        'Column2
        '
        Me.Column2.HeaderText = "1099 Category"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Account"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Threshold"
        Me.Column4.Name = "Column4"
        '
        'lnkHowTo
        '
        Me.lnkHowTo.AutoSize = True
        Me.lnkHowTo.Location = New System.Drawing.Point(16, 77)
        Me.lnkHowTo.Name = "lnkHowTo"
        Me.lnkHowTo.Size = New System.Drawing.Size(277, 13)
        Me.lnkHowTo.TabIndex = 8
        Me.lnkHowTo.TabStop = True
        Me.lnkHowTo.Text = "How to map accounts and thresholds to 1099 categories"
        '
        'rbtnTaxNo
        '
        Me.rbtnTaxNo.AutoSize = True
        Me.rbtnTaxNo.Location = New System.Drawing.Point(268, 44)
        Me.rbtnTaxNo.Name = "rbtnTaxNo"
        Me.rbtnTaxNo.Size = New System.Drawing.Size(38, 17)
        Me.rbtnTaxNo.TabIndex = 1
        Me.rbtnTaxNo.TabStop = True
        Me.rbtnTaxNo.Text = "&No"
        Me.rbtnTaxNo.UseVisualStyleBackColor = True
        '
        'rbtnTaxYes
        '
        Me.rbtnTaxYes.AutoSize = True
        Me.rbtnTaxYes.Location = New System.Drawing.Point(168, 44)
        Me.rbtnTaxYes.Name = "rbtnTaxYes"
        Me.rbtnTaxYes.Size = New System.Drawing.Size(42, 17)
        Me.rbtnTaxYes.TabIndex = 0
        Me.rbtnTaxYes.TabStop = True
        Me.rbtnTaxYes.Text = "&Yes"
        Me.rbtnTaxYes.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(16, 14)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(30, 16)
        Me.Label109.TabIndex = 7
        Me.Label109.Text = "Tax"
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(16, 46)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(149, 13)
        Me.Label110.TabIndex = 1
        Me.Label110.Text = "Do you file 1099-MISC forms?"
        '
        'pnlSpellingCoPref
        '
        Me.pnlSpellingCoPref.Controls.Add(Me.Label103)
        Me.pnlSpellingCoPref.Controls.Add(Me.Label104)
        Me.pnlSpellingCoPref.Controls.Add(Me.Label105)
        Me.pnlSpellingCoPref.Location = New System.Drawing.Point(36, 325)
        Me.pnlSpellingCoPref.Name = "pnlSpellingCoPref"
        Me.pnlSpellingCoPref.Size = New System.Drawing.Size(394, 152)
        Me.pnlSpellingCoPref.TabIndex = 20
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label103.Location = New System.Drawing.Point(16, 14)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(57, 16)
        Me.Label103.TabIndex = 7
        Me.Label103.Text = "Spelling"
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(16, 57)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(234, 13)
        Me.Label104.TabIndex = 1
        Me.Label104.Text = "There are no personal preferences for Spelling."
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(16, 81)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(303, 13)
        Me.Label105.TabIndex = 2
        Me.Label105.Text = "Please click the ""My Preferences"" tab above for more options."
        '
        'pnlServiceCoPref
        '
        Me.pnlServiceCoPref.Controls.Add(Me.chkServiceAllow)
        Me.pnlServiceCoPref.Controls.Add(Me.rbtnServiceAlways)
        Me.pnlServiceCoPref.Controls.Add(Me.rbtnServiceAuto)
        Me.pnlServiceCoPref.Controls.Add(Me.Label102)
        Me.pnlServiceCoPref.Controls.Add(Me.Label100)
        Me.pnlServiceCoPref.Controls.Add(Me.Label97)
        Me.pnlServiceCoPref.Location = New System.Drawing.Point(62, 303)
        Me.pnlServiceCoPref.Name = "pnlServiceCoPref"
        Me.pnlServiceCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlServiceCoPref.TabIndex = 19
        '
        'chkServiceAllow
        '
        Me.chkServiceAllow.AutoSize = True
        Me.chkServiceAllow.Location = New System.Drawing.Point(19, 191)
        Me.chkServiceAllow.Name = "chkServiceAllow"
        Me.chkServiceAllow.Size = New System.Drawing.Size(276, 17)
        Me.chkServiceAllow.TabIndex = 12
        Me.chkServiceAllow.Text = "A&llow background  downloading of service messages"
        Me.chkServiceAllow.UseVisualStyleBackColor = True
        '
        'rbtnServiceAlways
        '
        Me.rbtnServiceAlways.AutoSize = True
        Me.rbtnServiceAlways.Location = New System.Drawing.Point(19, 142)
        Me.rbtnServiceAlways.Name = "rbtnServiceAlways"
        Me.rbtnServiceAlways.Size = New System.Drawing.Size(243, 17)
        Me.rbtnServiceAlways.TabIndex = 11
        Me.rbtnServiceAlways.TabStop = True
        Me.rbtnServiceAlways.Text = "&Always ask for a password before connecting"
        Me.rbtnServiceAlways.UseVisualStyleBackColor = True
        '
        'rbtnServiceAuto
        '
        Me.rbtnServiceAuto.AutoSize = True
        Me.rbtnServiceAuto.Location = New System.Drawing.Point(19, 118)
        Me.rbtnServiceAuto.Name = "rbtnServiceAuto"
        Me.rbtnServiceAuto.Size = New System.Drawing.Size(277, 17)
        Me.rbtnServiceAuto.TabIndex = 10
        Me.rbtnServiceAuto.TabStop = True
        Me.rbtnServiceAuto.Text = "A&utomatically connect without asking for a password"
        Me.rbtnServiceAuto.UseVisualStyleBackColor = True
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Location = New System.Drawing.Point(16, 95)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(265, 13)
        Me.Label102.TabIndex = 9
        Me.Label102.Text = "The following apply to all Accounting System services."
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(16, 60)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(373, 13)
        Me.Label100.TabIndex = 8
        Me.Label100.Text = "How do you want to handle your connection to Accounting System services?"
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label97.Location = New System.Drawing.Point(16, 11)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(132, 16)
        Me.Label97.TabIndex = 7
        Me.Label97.Text = "Service Connection"
        '
        'pnlSendCoPref
        '
        Me.pnlSendCoPref.Controls.Add(Me.btnSendSpelling)
        Me.pnlSendCoPref.Controls.Add(Me.txtSendBody)
        Me.pnlSendCoPref.Controls.Add(Me.txtSendSubject)
        Me.pnlSendCoPref.Controls.Add(Me.Label92)
        Me.pnlSendCoPref.Controls.Add(Me.txtSendBcc)
        Me.pnlSendCoPref.Controls.Add(Me.Label99)
        Me.pnlSendCoPref.Controls.Add(Me.cboSendNameSeq)
        Me.pnlSendCoPref.Controls.Add(Me.cboSendDearTo)
        Me.pnlSendCoPref.Controls.Add(Me.cboSendDefault)
        Me.pnlSendCoPref.Controls.Add(Me.Label98)
        Me.pnlSendCoPref.Controls.Add(Me.Label101)
        Me.pnlSendCoPref.Location = New System.Drawing.Point(90, 282)
        Me.pnlSendCoPref.Name = "pnlSendCoPref"
        Me.pnlSendCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlSendCoPref.TabIndex = 18
        '
        'btnSendSpelling
        '
        Me.btnSendSpelling.Location = New System.Drawing.Point(340, 312)
        Me.btnSendSpelling.Name = "btnSendSpelling"
        Me.btnSendSpelling.Size = New System.Drawing.Size(75, 23)
        Me.btnSendSpelling.TabIndex = 31
        Me.btnSendSpelling.Text = "&Spellin&g"
        Me.btnSendSpelling.UseVisualStyleBackColor = True
        '
        'txtSendBody
        '
        Me.txtSendBody.Location = New System.Drawing.Point(19, 153)
        Me.txtSendBody.Multiline = True
        Me.txtSendBody.Name = "txtSendBody"
        Me.txtSendBody.Size = New System.Drawing.Size(396, 148)
        Me.txtSendBody.TabIndex = 30
        Me.txtSendBody.Text = "Your invoice is attached.  Please remit payment at your earliest convenience." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
            "Thank you for your business - we appreciate it very much." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Sincerely," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Compa" & _
            "ny Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtSendSubject
        '
        Me.txtSendSubject.Location = New System.Drawing.Point(128, 123)
        Me.txtSendSubject.Name = "txtSendSubject"
        Me.txtSendSubject.Size = New System.Drawing.Size(287, 21)
        Me.txtSendSubject.TabIndex = 29
        Me.txtSendSubject.Text = "Invoice from Company Name"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(17, 129)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(43, 13)
        Me.Label92.TabIndex = 28
        Me.Label92.Text = "&Subject"
        '
        'txtSendBcc
        '
        Me.txtSendBcc.Location = New System.Drawing.Point(128, 98)
        Me.txtSendBcc.Name = "txtSendBcc"
        Me.txtSendBcc.Size = New System.Drawing.Size(287, 21)
        Me.txtSendBcc.TabIndex = 27
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(17, 104)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(23, 13)
        Me.Label99.TabIndex = 26
        Me.Label99.Text = "&Bcc"
        '
        'cboSendNameSeq
        '
        Me.cboSendNameSeq.FormattingEnabled = True
        Me.cboSendNameSeq.Items.AddRange(New Object() {"<First>", "<First> <Last>", "<Mr./Miss/Mrs.> <Last>"})
        Me.cboSendNameSeq.Location = New System.Drawing.Point(128, 63)
        Me.cboSendNameSeq.Name = "cboSendNameSeq"
        Me.cboSendNameSeq.Size = New System.Drawing.Size(183, 21)
        Me.cboSendNameSeq.TabIndex = 25
        '
        'cboSendDearTo
        '
        Me.cboSendDearTo.FormattingEnabled = True
        Me.cboSendDearTo.Items.AddRange(New Object() {"Dear", "To"})
        Me.cboSendDearTo.Location = New System.Drawing.Point(20, 63)
        Me.cboSendDearTo.Name = "cboSendDearTo"
        Me.cboSendDearTo.Size = New System.Drawing.Size(100, 21)
        Me.cboSendDearTo.TabIndex = 8
        '
        'cboSendDefault
        '
        Me.cboSendDefault.FormattingEnabled = True
        Me.cboSendDefault.Location = New System.Drawing.Point(128, 36)
        Me.cboSendDefault.Name = "cboSendDefault"
        Me.cboSendDefault.Size = New System.Drawing.Size(183, 21)
        Me.cboSendDefault.TabIndex = 24
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(17, 39)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(98, 13)
        Me.Label98.TabIndex = 18
        Me.Label98.Text = "C&hange default for"
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label101.Location = New System.Drawing.Point(16, 11)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(82, 16)
        Me.Label101.TabIndex = 7
        Me.Label101.Text = "Send Forms"
        '
        'pnlSTaxCoPref
        '
        Me.pnlSTaxCoPref.Controls.Add(Me.GroupBox39)
        Me.pnlSTaxCoPref.Controls.Add(Me.GroupBox38)
        Me.pnlSTaxCoPref.Controls.Add(Me.GroupBox37)
        Me.pnlSTaxCoPref.Controls.Add(Me.rbtnSTaxNO)
        Me.pnlSTaxCoPref.Controls.Add(Me.rbtnSTaxYes)
        Me.pnlSTaxCoPref.Controls.Add(Me.Label88)
        Me.pnlSTaxCoPref.Controls.Add(Me.GroupBox41)
        Me.pnlSTaxCoPref.Controls.Add(Me.Label95)
        Me.pnlSTaxCoPref.Location = New System.Drawing.Point(122, 261)
        Me.pnlSTaxCoPref.Name = "pnlSTaxCoPref"
        Me.pnlSTaxCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlSTaxCoPref.TabIndex = 17
        '
        'GroupBox39
        '
        Me.GroupBox39.Controls.Add(Me.rbtnSTaxAnnually)
        Me.GroupBox39.Controls.Add(Me.rbtnSTaxQuarterly)
        Me.GroupBox39.Controls.Add(Me.rbtnSTaxMonthly)
        Me.GroupBox39.Location = New System.Drawing.Point(237, 283)
        Me.GroupBox39.Name = "GroupBox39"
        Me.GroupBox39.Size = New System.Drawing.Size(191, 92)
        Me.GroupBox39.TabIndex = 23
        Me.GroupBox39.TabStop = False
        Me.GroupBox39.Text = "When do you pay Sales Tax?"
        '
        'rbtnSTaxAnnually
        '
        Me.rbtnSTaxAnnually.AutoSize = True
        Me.rbtnSTaxAnnually.Location = New System.Drawing.Point(8, 64)
        Me.rbtnSTaxAnnually.Name = "rbtnSTaxAnnually"
        Me.rbtnSTaxAnnually.Size = New System.Drawing.Size(66, 17)
        Me.rbtnSTaxAnnually.TabIndex = 2
        Me.rbtnSTaxAnnually.TabStop = True
        Me.rbtnSTaxAnnually.Text = "&Annually"
        Me.rbtnSTaxAnnually.UseVisualStyleBackColor = True
        '
        'rbtnSTaxQuarterly
        '
        Me.rbtnSTaxQuarterly.AutoSize = True
        Me.rbtnSTaxQuarterly.Location = New System.Drawing.Point(8, 44)
        Me.rbtnSTaxQuarterly.Name = "rbtnSTaxQuarterly"
        Me.rbtnSTaxQuarterly.Size = New System.Drawing.Size(71, 17)
        Me.rbtnSTaxQuarterly.TabIndex = 1
        Me.rbtnSTaxQuarterly.TabStop = True
        Me.rbtnSTaxQuarterly.Text = "&Quarterly"
        Me.rbtnSTaxQuarterly.UseVisualStyleBackColor = True
        '
        'rbtnSTaxMonthly
        '
        Me.rbtnSTaxMonthly.AutoSize = True
        Me.rbtnSTaxMonthly.Location = New System.Drawing.Point(8, 25)
        Me.rbtnSTaxMonthly.Name = "rbtnSTaxMonthly"
        Me.rbtnSTaxMonthly.Size = New System.Drawing.Size(63, 17)
        Me.rbtnSTaxMonthly.TabIndex = 0
        Me.rbtnSTaxMonthly.TabStop = True
        Me.rbtnSTaxMonthly.Text = "M&onthly"
        Me.rbtnSTaxMonthly.UseVisualStyleBackColor = True
        '
        'GroupBox38
        '
        Me.GroupBox38.Controls.Add(Me.rbtnSTaxCashBasis)
        Me.GroupBox38.Controls.Add(Me.rbtnSTaxAccrualBasis)
        Me.GroupBox38.Location = New System.Drawing.Point(10, 283)
        Me.GroupBox38.Name = "GroupBox38"
        Me.GroupBox38.Size = New System.Drawing.Size(222, 92)
        Me.GroupBox38.TabIndex = 22
        Me.GroupBox38.TabStop = False
        Me.GroupBox38.Text = "When do you owe Sales Tax?"
        '
        'rbtnSTaxCashBasis
        '
        Me.rbtnSTaxCashBasis.AutoSize = True
        Me.rbtnSTaxCashBasis.Location = New System.Drawing.Point(8, 50)
        Me.rbtnSTaxCashBasis.Name = "rbtnSTaxCashBasis"
        Me.rbtnSTaxCashBasis.Size = New System.Drawing.Size(206, 17)
        Me.rbtnSTaxCashBasis.TabIndex = 1
        Me.rbtnSTaxCashBasis.TabStop = True
        Me.rbtnSTaxCashBasis.Text = "&Upon receipt of payment (Cash Basis)"
        Me.rbtnSTaxCashBasis.UseVisualStyleBackColor = True
        '
        'rbtnSTaxAccrualBasis
        '
        Me.rbtnSTaxAccrualBasis.AutoSize = True
        Me.rbtnSTaxAccrualBasis.Location = New System.Drawing.Point(8, 25)
        Me.rbtnSTaxAccrualBasis.Name = "rbtnSTaxAccrualBasis"
        Me.rbtnSTaxAccrualBasis.Size = New System.Drawing.Size(186, 17)
        Me.rbtnSTaxAccrualBasis.TabIndex = 0
        Me.rbtnSTaxAccrualBasis.TabStop = True
        Me.rbtnSTaxAccrualBasis.Text = "A&s of Invoice date (Accrual Basis)"
        Me.rbtnSTaxAccrualBasis.UseVisualStyleBackColor = True
        '
        'GroupBox37
        '
        Me.GroupBox37.Controls.Add(Me.cboSTaxTxItem)
        Me.GroupBox37.Controls.Add(Me.cboSTaxNonTxItem)
        Me.GroupBox37.Controls.Add(Me.chkSTaxT)
        Me.GroupBox37.Controls.Add(Me.Label91)
        Me.GroupBox37.Controls.Add(Me.Label89)
        Me.GroupBox37.Controls.Add(Me.Label90)
        Me.GroupBox37.Location = New System.Drawing.Point(10, 180)
        Me.GroupBox37.Name = "GroupBox37"
        Me.GroupBox37.Size = New System.Drawing.Size(418, 97)
        Me.GroupBox37.TabIndex = 21
        Me.GroupBox37.TabStop = False
        Me.GroupBox37.Text = "Assign Sales Tax Codes"
        '
        'cboSTaxTxItem
        '
        Me.cboSTaxTxItem.FormattingEnabled = True
        Me.cboSTaxTxItem.Location = New System.Drawing.Point(103, 43)
        Me.cboSTaxTxItem.Name = "cboSTaxTxItem"
        Me.cboSTaxTxItem.Size = New System.Drawing.Size(61, 21)
        Me.cboSTaxTxItem.TabIndex = 8
        '
        'cboSTaxNonTxItem
        '
        Me.cboSTaxNonTxItem.FormattingEnabled = True
        Me.cboSTaxNonTxItem.Location = New System.Drawing.Point(331, 43)
        Me.cboSTaxNonTxItem.Name = "cboSTaxNonTxItem"
        Me.cboSTaxNonTxItem.Size = New System.Drawing.Size(61, 21)
        Me.cboSTaxNonTxItem.TabIndex = 7
        '
        'chkSTaxT
        '
        Me.chkSTaxT.AutoSize = True
        Me.chkSTaxT.Location = New System.Drawing.Point(9, 72)
        Me.chkSTaxT.Name = "chkSTaxT"
        Me.chkSTaxT.Size = New System.Drawing.Size(312, 17)
        Me.chkSTaxT.TabIndex = 4
        Me.chkSTaxT.Text = "Identify taxable amounts as ""&T"" for ""Taxable"" when printing"
        Me.chkSTaxT.UseVisualStyleBackColor = True
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(6, 46)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(94, 13)
        Me.Label91.TabIndex = 6
        Me.Label91.Text = "Ta&xable item code"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(213, 48)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(115, 13)
        Me.Label89.TabIndex = 2
        Me.Label89.Text = "Non-taxa&ble item code"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(5, 22)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(364, 13)
        Me.Label90.TabIndex = 0
        Me.Label90.Text = "Sales tax codes determine if the items you sell are taxable or non-taxable."
        '
        'rbtnSTaxNO
        '
        Me.rbtnSTaxNO.AutoSize = True
        Me.rbtnSTaxNO.Location = New System.Drawing.Point(226, 37)
        Me.rbtnSTaxNO.Name = "rbtnSTaxNO"
        Me.rbtnSTaxNO.Size = New System.Drawing.Size(38, 17)
        Me.rbtnSTaxNO.TabIndex = 20
        Me.rbtnSTaxNO.TabStop = True
        Me.rbtnSTaxNO.Text = "&No"
        Me.rbtnSTaxNO.UseVisualStyleBackColor = True
        '
        'rbtnSTaxYes
        '
        Me.rbtnSTaxYes.AutoSize = True
        Me.rbtnSTaxYes.Location = New System.Drawing.Point(164, 37)
        Me.rbtnSTaxYes.Name = "rbtnSTaxYes"
        Me.rbtnSTaxYes.Size = New System.Drawing.Size(42, 17)
        Me.rbtnSTaxYes.TabIndex = 19
        Me.rbtnSTaxYes.TabStop = True
        Me.rbtnSTaxYes.Text = "&Yes"
        Me.rbtnSTaxYes.UseVisualStyleBackColor = True
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(17, 39)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(131, 13)
        Me.Label88.TabIndex = 18
        Me.Label88.Text = "Do you charge Sales Tax?"
        '
        'GroupBox41
        '
        Me.GroupBox41.Controls.Add(Me.btnSTaxAdd)
        Me.GroupBox41.Controls.Add(Me.lnkSTaxSample)
        Me.GroupBox41.Controls.Add(Me.Label93)
        Me.GroupBox41.Controls.Add(Me.cboSTaxItem)
        Me.GroupBox41.Controls.Add(Me.Label94)
        Me.GroupBox41.Location = New System.Drawing.Point(10, 64)
        Me.GroupBox41.Name = "GroupBox41"
        Me.GroupBox41.Size = New System.Drawing.Size(418, 112)
        Me.GroupBox41.TabIndex = 17
        Me.GroupBox41.TabStop = False
        Me.GroupBox41.Text = "Setup Sales Tax Items"
        '
        'btnSTaxAdd
        '
        Me.btnSTaxAdd.Location = New System.Drawing.Point(8, 77)
        Me.btnSTaxAdd.Name = "btnSTaxAdd"
        Me.btnSTaxAdd.Size = New System.Drawing.Size(118, 23)
        Me.btnSTaxAdd.TabIndex = 6
        Me.btnSTaxAdd.Text = "A&dd Sales Tax item"
        Me.btnSTaxAdd.UseVisualStyleBackColor = True
        '
        'lnkSTaxSample
        '
        Me.lnkSTaxSample.AutoSize = True
        Me.lnkSTaxSample.Location = New System.Drawing.Point(7, 41)
        Me.lnkSTaxSample.Name = "lnkSTaxSample"
        Me.lnkSTaxSample.Size = New System.Drawing.Size(81, 13)
        Me.lnkSTaxSample.TabIndex = 5
        Me.lnkSTaxSample.TabStop = True
        Me.lnkSTaxSample.Text = "Show Examples"
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(227, 59)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(169, 13)
        Me.Label93.TabIndex = 2
        Me.Label93.Text = "Your most common Sal&es Tax item"
        '
        'cboSTaxItem
        '
        Me.cboSTaxItem.FormattingEnabled = True
        Me.cboSTaxItem.Location = New System.Drawing.Point(230, 80)
        Me.cboSTaxItem.Name = "cboSTaxItem"
        Me.cboSTaxItem.Size = New System.Drawing.Size(162, 21)
        Me.cboSTaxItem.TabIndex = 1
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(5, 22)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(321, 13)
        Me.Label94.TabIndex = 0
        Me.Label94.Text = "Add a Sales Tax item for each district where you collect Sales Tax"
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label95.Location = New System.Drawing.Point(16, 11)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(68, 16)
        Me.Label95.TabIndex = 7
        Me.Label95.Text = "Sales Tax"
        '
        'pnlSalesCoPref
        '
        Me.pnlSalesCoPref.Controls.Add(Me.GroupBox36)
        Me.pnlSalesCoPref.Controls.Add(Me.GroupBox35)
        Me.pnlSalesCoPref.Controls.Add(Me.GroupBox34)
        Me.pnlSalesCoPref.Controls.Add(Me.GroupBox33)
        Me.pnlSalesCoPref.Controls.Add(Me.GroupBox31)
        Me.pnlSalesCoPref.Controls.Add(Me.Label79)
        Me.pnlSalesCoPref.Location = New System.Drawing.Point(154, 240)
        Me.pnlSalesCoPref.Name = "pnlSalesCoPref"
        Me.pnlSalesCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlSalesCoPref.TabIndex = 16
        '
        'GroupBox36
        '
        Me.GroupBox36.Controls.Add(Me.chkUseUndepositFunds)
        Me.GroupBox36.Controls.Add(Me.chkAutoCalcPay)
        Me.GroupBox36.Controls.Add(Me.chkAutoPay)
        Me.GroupBox36.Location = New System.Drawing.Point(7, 295)
        Me.GroupBox36.Name = "GroupBox36"
        Me.GroupBox36.Size = New System.Drawing.Size(421, 71)
        Me.GroupBox36.TabIndex = 21
        Me.GroupBox36.TabStop = False
        Me.GroupBox36.Text = "Receive Payments"
        '
        'chkUseUndepositFunds
        '
        Me.chkUseUndepositFunds.Location = New System.Drawing.Point(251, 22)
        Me.chkUseUndepositFunds.Name = "chkUseUndepositFunds"
        Me.chkUseUndepositFunds.Size = New System.Drawing.Size(164, 38)
        Me.chkUseUndepositFunds.TabIndex = 6
        Me.chkUseUndepositFunds.Text = "Use Undepos&ited Funds as a default deposit to account"
        Me.chkUseUndepositFunds.UseVisualStyleBackColor = True
        '
        'chkAutoCalcPay
        '
        Me.chkAutoCalcPay.Location = New System.Drawing.Point(122, 22)
        Me.chkAutoCalcPay.Name = "chkAutoCalcPay"
        Me.chkAutoCalcPay.Size = New System.Drawing.Size(126, 38)
        Me.chkAutoCalcPay.TabIndex = 5
        Me.chkAutoCalcPay.Text = "Automaticall&y calculate payments"
        Me.chkAutoCalcPay.UseVisualStyleBackColor = True
        '
        'chkAutoPay
        '
        Me.chkAutoPay.Location = New System.Drawing.Point(3, 22)
        Me.chkAutoPay.Name = "chkAutoPay"
        Me.chkAutoPay.Size = New System.Drawing.Size(141, 38)
        Me.chkAutoPay.TabIndex = 4
        Me.chkAutoPay.Text = "&Automatically apply payments"
        Me.chkAutoPay.UseVisualStyleBackColor = True
        '
        'GroupBox35
        '
        Me.GroupBox35.Controls.Add(Me.cboPickList)
        Me.GroupBox35.Controls.Add(Me.cboPackingSlip)
        Me.GroupBox35.Controls.Add(Me.Label87)
        Me.GroupBox35.Controls.Add(Me.Label86)
        Me.GroupBox35.Controls.Add(Me.Label85)
        Me.GroupBox35.Controls.Add(Me.chkDontPrintZeroSO)
        Me.GroupBox35.Controls.Add(Me.chkWarnDupSO)
        Me.GroupBox35.Controls.Add(Me.chkSO)
        Me.GroupBox35.Location = New System.Drawing.Point(235, 98)
        Me.GroupBox35.Name = "GroupBox35"
        Me.GroupBox35.Size = New System.Drawing.Size(192, 191)
        Me.GroupBox35.TabIndex = 20
        Me.GroupBox35.TabStop = False
        Me.GroupBox35.Text = "Sales Order"
        '
        'cboPickList
        '
        Me.cboPickList.FormattingEnabled = True
        Me.cboPickList.Location = New System.Drawing.Point(73, 158)
        Me.cboPickList.Name = "cboPickList"
        Me.cboPickList.Size = New System.Drawing.Size(114, 21)
        Me.cboPickList.TabIndex = 11
        '
        'cboPackingSlip
        '
        Me.cboPackingSlip.FormattingEnabled = True
        Me.cboPackingSlip.Location = New System.Drawing.Point(73, 133)
        Me.cboPackingSlip.Name = "cboPackingSlip"
        Me.cboPackingSlip.Size = New System.Drawing.Size(114, 21)
        Me.cboPackingSlip.TabIndex = 10
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(3, 161)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(44, 13)
        Me.Label87.TabIndex = 9
        Me.Label87.Text = "Pic&k List"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(4, 136)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(66, 13)
        Me.Label86.TabIndex = 8
        Me.Label86.Text = "Packin&g Slip:"
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(3, 111)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(109, 13)
        Me.Label85.TabIndex = 7
        Me.Label85.Text = "Choose template for:"
        '
        'chkDontPrintZeroSO
        '
        Me.chkDontPrintZeroSO.Location = New System.Drawing.Point(25, 74)
        Me.chkDontPrintZeroSO.Name = "chkDontPrintZeroSO"
        Me.chkDontPrintZeroSO.Size = New System.Drawing.Size(137, 33)
        Me.chkDontPrintZeroSO.TabIndex = 6
        Me.chkDontPrintZeroSO.Text = "Dont print items with &zero amount"
        Me.chkDontPrintZeroSO.UseVisualStyleBackColor = True
        '
        'chkWarnDupSO
        '
        Me.chkWarnDupSO.Location = New System.Drawing.Point(25, 40)
        Me.chkWarnDupSO.Name = "chkWarnDupSO"
        Me.chkWarnDupSO.Size = New System.Drawing.Size(150, 33)
        Me.chkWarnDupSO.TabIndex = 5
        Me.chkWarnDupSO.Text = "Warn about duplicate Sales Order &No."
        Me.chkWarnDupSO.UseVisualStyleBackColor = True
        '
        'chkSO
        '
        Me.chkSO.AutoSize = True
        Me.chkSO.Location = New System.Drawing.Point(6, 19)
        Me.chkSO.Name = "chkSO"
        Me.chkSO.Size = New System.Drawing.Size(127, 17)
        Me.chkSO.TabIndex = 4
        Me.chkSO.Text = "&Enables Sales Orders"
        Me.chkSO.UseVisualStyleBackColor = True
        '
        'GroupBox34
        '
        Me.GroupBox34.Controls.Add(Me.chkPriceLvl)
        Me.GroupBox34.Location = New System.Drawing.Point(235, 30)
        Me.GroupBox34.Name = "GroupBox34"
        Me.GroupBox34.Size = New System.Drawing.Size(193, 58)
        Me.GroupBox34.TabIndex = 19
        Me.GroupBox34.TabStop = False
        Me.GroupBox34.Text = "Price Levels"
        '
        'chkPriceLvl
        '
        Me.chkPriceLvl.AutoSize = True
        Me.chkPriceLvl.Location = New System.Drawing.Point(6, 30)
        Me.chkPriceLvl.Name = "chkPriceLvl"
        Me.chkPriceLvl.Size = New System.Drawing.Size(100, 17)
        Me.chkPriceLvl.TabIndex = 4
        Me.chkPriceLvl.Text = "&Use price levels"
        Me.chkPriceLvl.UseVisualStyleBackColor = True
        '
        'GroupBox33
        '
        Me.GroupBox33.Controls.Add(Me.cboInvoicePackSlip)
        Me.GroupBox33.Controls.Add(Me.Label84)
        Me.GroupBox33.Controls.Add(Me.txtPercentage)
        Me.GroupBox33.Controls.Add(Me.Label83)
        Me.GroupBox33.Controls.Add(Me.chkXpenseAsIncome)
        Me.GroupBox33.Location = New System.Drawing.Point(7, 147)
        Me.GroupBox33.Name = "GroupBox33"
        Me.GroupBox33.Size = New System.Drawing.Size(224, 142)
        Me.GroupBox33.TabIndex = 18
        Me.GroupBox33.TabStop = False
        Me.GroupBox33.Text = "Miscellaneous"
        '
        'cboInvoicePackSlip
        '
        Me.cboInvoicePackSlip.FormattingEnabled = True
        Me.cboInvoicePackSlip.Location = New System.Drawing.Point(8, 109)
        Me.cboInvoicePackSlip.Name = "cboInvoicePackSlip"
        Me.cboInvoicePackSlip.Size = New System.Drawing.Size(205, 21)
        Me.cboInvoicePackSlip.TabIndex = 8
        '
        'Label84
        '
        Me.Label84.Location = New System.Drawing.Point(5, 74)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(149, 26)
        Me.Label84.TabIndex = 7
        Me.Label84.Text = "Choose &template for Invoice packing slip:"
        '
        'txtPercentage
        '
        Me.txtPercentage.Location = New System.Drawing.Point(143, 42)
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Size = New System.Drawing.Size(70, 21)
        Me.txtPercentage.TabIndex = 6
        Me.txtPercentage.Text = "0.0%"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(5, 45)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(138, 13)
        Me.Label83.TabIndex = 5
        Me.Label83.Text = "Default Markup &Percentage"
        '
        'chkXpenseAsIncome
        '
        Me.chkXpenseAsIncome.AutoSize = True
        Me.chkXpenseAsIncome.Location = New System.Drawing.Point(9, 20)
        Me.chkXpenseAsIncome.Name = "chkXpenseAsIncome"
        Me.chkXpenseAsIncome.Size = New System.Drawing.Size(207, 17)
        Me.chkXpenseAsIncome.TabIndex = 4
        Me.chkXpenseAsIncome.Text = "Track &reimbursed expenses as income"
        Me.chkXpenseAsIncome.UseVisualStyleBackColor = True
        '
        'GroupBox31
        '
        Me.GroupBox31.Controls.Add(Me.chkWarnDupInvoice)
        Me.GroupBox31.Controls.Add(Me.txtFOB)
        Me.GroupBox31.Controls.Add(Me.Label82)
        Me.GroupBox31.Controls.Add(Me.cboShipMethod)
        Me.GroupBox31.Controls.Add(Me.Label81)
        Me.GroupBox31.Location = New System.Drawing.Point(7, 30)
        Me.GroupBox31.Name = "GroupBox31"
        Me.GroupBox31.Size = New System.Drawing.Size(224, 112)
        Me.GroupBox31.TabIndex = 17
        Me.GroupBox31.TabStop = False
        Me.GroupBox31.Text = "Sales Forms"
        '
        'chkWarnDupInvoice
        '
        Me.chkWarnDupInvoice.AutoSize = True
        Me.chkWarnDupInvoice.Location = New System.Drawing.Point(8, 86)
        Me.chkWarnDupInvoice.Name = "chkWarnDupInvoice"
        Me.chkWarnDupInvoice.Size = New System.Drawing.Size(209, 17)
        Me.chkWarnDupInvoice.TabIndex = 4
        Me.chkWarnDupInvoice.Text = "&Warn about duplicate invoice numbers"
        Me.chkWarnDupInvoice.UseVisualStyleBackColor = True
        '
        'txtFOB
        '
        Me.txtFOB.Location = New System.Drawing.Point(88, 56)
        Me.txtFOB.Name = "txtFOB"
        Me.txtFOB.Size = New System.Drawing.Size(125, 21)
        Me.txtFOB.TabIndex = 3
        '
        'Label82
        '
        Me.Label82.Location = New System.Drawing.Point(6, 61)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(67, 19)
        Me.Label82.TabIndex = 2
        Me.Label82.Text = "Usual &FOB"
        '
        'cboShipMethod
        '
        Me.cboShipMethod.FormattingEnabled = True
        Me.cboShipMethod.Location = New System.Drawing.Point(88, 28)
        Me.cboShipMethod.Name = "cboShipMethod"
        Me.cboShipMethod.Size = New System.Drawing.Size(125, 21)
        Me.cboShipMethod.TabIndex = 1
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(5, 22)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(98, 26)
        Me.Label81.TabIndex = 0
        Me.Label81.Text = "Usual &Shipping Method"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(16, 11)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(130, 16)
        Me.Label79.TabIndex = 7
        Me.Label79.Text = "Sales && Customers"
        '
        'pnlReportsCoPref
        '
        Me.pnlReportsCoPref.Controls.Add(Me.btnFormat)
        Me.pnlReportsCoPref.Controls.Add(Me.GroupBox30)
        Me.pnlReportsCoPref.Controls.Add(Me.GroupBox29)
        Me.pnlReportsCoPref.Controls.Add(Me.GroupBox25)
        Me.pnlReportsCoPref.Controls.Add(Me.GroupBox28)
        Me.pnlReportsCoPref.Controls.Add(Me.Label78)
        Me.pnlReportsCoPref.Location = New System.Drawing.Point(175, 219)
        Me.pnlReportsCoPref.Name = "pnlReportsCoPref"
        Me.pnlReportsCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlReportsCoPref.TabIndex = 15
        '
        'btnFormat
        '
        Me.btnFormat.Location = New System.Drawing.Point(9, 269)
        Me.btnFormat.Name = "btnFormat"
        Me.btnFormat.Size = New System.Drawing.Size(68, 23)
        Me.btnFormat.TabIndex = 21
        Me.btnFormat.Text = "&Format"
        Me.btnFormat.UseVisualStyleBackColor = True
        '
        'GroupBox30
        '
        Me.GroupBox30.Controls.Add(Me.btnClassify)
        Me.GroupBox30.Controls.Add(Me.Label77)
        Me.GroupBox30.Location = New System.Drawing.Point(226, 159)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Size = New System.Drawing.Size(191, 92)
        Me.GroupBox30.TabIndex = 20
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "Statements of Cash Flows"
        '
        'btnClassify
        '
        Me.btnClassify.Location = New System.Drawing.Point(51, 63)
        Me.btnClassify.Name = "btnClassify"
        Me.btnClassify.Size = New System.Drawing.Size(88, 23)
        Me.btnClassify.TabIndex = 16
        Me.btnClassify.Text = "Class&ify Cash"
        Me.btnClassify.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.Location = New System.Drawing.Point(10, 20)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(175, 42)
        Me.Label77.TabIndex = 15
        Me.Label77.Text = "Click here to assign accounts  to sections of the Statement of Cash Flows:"
        '
        'GroupBox29
        '
        Me.GroupBox29.Controls.Add(Me.rbtnDescription)
        Me.GroupBox29.Controls.Add(Me.rbtnNameDescription)
        Me.GroupBox29.Controls.Add(Me.rbtnName)
        Me.GroupBox29.Location = New System.Drawing.Point(9, 159)
        Me.GroupBox29.Name = "GroupBox29"
        Me.GroupBox29.Size = New System.Drawing.Size(191, 92)
        Me.GroupBox29.TabIndex = 19
        Me.GroupBox29.TabStop = False
        Me.GroupBox29.Text = "Reports - Show Accounts By:"
        '
        'rbtnDescription
        '
        Me.rbtnDescription.AutoSize = True
        Me.rbtnDescription.Location = New System.Drawing.Point(25, 44)
        Me.rbtnDescription.Name = "rbtnDescription"
        Me.rbtnDescription.Size = New System.Drawing.Size(96, 17)
        Me.rbtnDescription.TabIndex = 15
        Me.rbtnDescription.TabStop = True
        Me.rbtnDescription.Text = "Decription &only"
        Me.rbtnDescription.UseVisualStyleBackColor = True
        '
        'rbtnNameDescription
        '
        Me.rbtnNameDescription.AutoSize = True
        Me.rbtnNameDescription.Location = New System.Drawing.Point(25, 65)
        Me.rbtnNameDescription.Name = "rbtnNameDescription"
        Me.rbtnNameDescription.Size = New System.Drawing.Size(129, 17)
        Me.rbtnNameDescription.TabIndex = 14
        Me.rbtnNameDescription.TabStop = True
        Me.rbtnNameDescription.Text = "Nam&e and Description"
        Me.rbtnNameDescription.UseVisualStyleBackColor = True
        '
        'rbtnName
        '
        Me.rbtnName.AutoSize = True
        Me.rbtnName.Location = New System.Drawing.Point(25, 21)
        Me.rbtnName.Name = "rbtnName"
        Me.rbtnName.Size = New System.Drawing.Size(75, 17)
        Me.rbtnName.TabIndex = 13
        Me.rbtnName.TabStop = True
        Me.rbtnName.Text = "&Name only"
        Me.rbtnName.UseVisualStyleBackColor = True
        '
        'GroupBox25
        '
        Me.GroupBox25.Controls.Add(Me.rbtnAgeTrans)
        Me.GroupBox25.Controls.Add(Me.rbtnAgeDue)
        Me.GroupBox25.Location = New System.Drawing.Point(9, 97)
        Me.GroupBox25.Name = "GroupBox25"
        Me.GroupBox25.Size = New System.Drawing.Size(409, 49)
        Me.GroupBox25.TabIndex = 18
        Me.GroupBox25.TabStop = False
        Me.GroupBox25.Text = "Aging Reports"
        '
        'rbtnAgeTrans
        '
        Me.rbtnAgeTrans.AutoSize = True
        Me.rbtnAgeTrans.Location = New System.Drawing.Point(197, 20)
        Me.rbtnAgeTrans.Name = "rbtnAgeTrans"
        Me.rbtnAgeTrans.Size = New System.Drawing.Size(151, 17)
        Me.rbtnAgeTrans.TabIndex = 14
        Me.rbtnAgeTrans.TabStop = True
        Me.rbtnAgeTrans.Text = "Age from &transaction date"
        Me.rbtnAgeTrans.UseVisualStyleBackColor = True
        '
        'rbtnAgeDue
        '
        Me.rbtnAgeDue.AutoSize = True
        Me.rbtnAgeDue.Location = New System.Drawing.Point(25, 21)
        Me.rbtnAgeDue.Name = "rbtnAgeDue"
        Me.rbtnAgeDue.Size = New System.Drawing.Size(115, 17)
        Me.rbtnAgeDue.TabIndex = 13
        Me.rbtnAgeDue.TabStop = True
        Me.rbtnAgeDue.Text = "Age from &due date"
        Me.rbtnAgeDue.UseVisualStyleBackColor = True
        '
        'GroupBox28
        '
        Me.GroupBox28.Controls.Add(Me.rbtnCash)
        Me.GroupBox28.Controls.Add(Me.rbtnAccrual)
        Me.GroupBox28.Location = New System.Drawing.Point(8, 34)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Size = New System.Drawing.Size(410, 51)
        Me.GroupBox28.TabIndex = 17
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "Summary Reports Basis"
        '
        'rbtnCash
        '
        Me.rbtnCash.AutoSize = True
        Me.rbtnCash.Location = New System.Drawing.Point(198, 23)
        Me.rbtnCash.Name = "rbtnCash"
        Me.rbtnCash.Size = New System.Drawing.Size(49, 17)
        Me.rbtnCash.TabIndex = 14
        Me.rbtnCash.TabStop = True
        Me.rbtnCash.Text = "Ca&sh"
        Me.rbtnCash.UseVisualStyleBackColor = True
        '
        'rbtnAccrual
        '
        Me.rbtnAccrual.AutoSize = True
        Me.rbtnAccrual.Location = New System.Drawing.Point(26, 23)
        Me.rbtnAccrual.Name = "rbtnAccrual"
        Me.rbtnAccrual.Size = New System.Drawing.Size(60, 17)
        Me.rbtnAccrual.TabIndex = 13
        Me.rbtnAccrual.TabStop = True
        Me.rbtnAccrual.Text = "&Accrual"
        Me.rbtnAccrual.UseVisualStyleBackColor = True
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label78.Location = New System.Drawing.Point(16, 11)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(124, 16)
        Me.Label78.TabIndex = 7
        Me.Label78.Text = "Reports && Graphs"
        '
        'pnlRemindersCoPref
        '
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel13)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel12)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel11)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel10)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel9)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel8)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel7)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel6)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel5)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel4)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel3)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel2)
        Me.pnlRemindersCoPref.Controls.Add(Me.Panel1)
        Me.pnlRemindersCoPref.Controls.Add(Me.Label53)
        Me.pnlRemindersCoPref.Controls.Add(Me.Label27)
        Me.pnlRemindersCoPref.Controls.Add(Me.Label58)
        Me.pnlRemindersCoPref.Controls.Add(Me.Label59)
        Me.pnlRemindersCoPref.Location = New System.Drawing.Point(199, 197)
        Me.pnlRemindersCoPref.Name = "pnlRemindersCoPref"
        Me.pnlRemindersCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlRemindersCoPref.TabIndex = 14
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label74)
        Me.Panel13.Controls.Add(Me.rbtnToDoDontRmd)
        Me.Panel13.Controls.Add(Me.rbtnToDoList)
        Me.Panel13.Controls.Add(Me.rbtnToDoSummary)
        Me.Panel13.Location = New System.Drawing.Point(3, 340)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(436, 22)
        Me.Panel13.TabIndex = 47
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(6, 4)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(66, 13)
        Me.Label74.TabIndex = 33
        Me.Label74.Text = "&To Do Notes"
        Me.Label74.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnToDoDontRmd
        '
        Me.rbtnToDoDontRmd.AutoSize = True
        Me.rbtnToDoDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnToDoDontRmd.Name = "rbtnToDoDontRmd"
        Me.rbtnToDoDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnToDoDontRmd.TabIndex = 32
        Me.rbtnToDoDontRmd.TabStop = True
        Me.rbtnToDoDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnToDoList
        '
        Me.rbtnToDoList.AutoSize = True
        Me.rbtnToDoList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnToDoList.Name = "rbtnToDoList"
        Me.rbtnToDoList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnToDoList.TabIndex = 31
        Me.rbtnToDoList.TabStop = True
        Me.rbtnToDoList.UseVisualStyleBackColor = True
        '
        'rbtnToDoSummary
        '
        Me.rbtnToDoSummary.AutoSize = True
        Me.rbtnToDoSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnToDoSummary.Name = "rbtnToDoSummary"
        Me.rbtnToDoSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnToDoSummary.TabIndex = 30
        Me.rbtnToDoSummary.TabStop = True
        Me.rbtnToDoSummary.UseVisualStyleBackColor = True
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label72)
        Me.Panel12.Controls.Add(Me.rbtnPODontRmd)
        Me.Panel12.Controls.Add(Me.rbtnPOList)
        Me.Panel12.Controls.Add(Me.rbtnPOSummary)
        Me.Panel12.Location = New System.Drawing.Point(3, 318)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(436, 22)
        Me.Panel12.TabIndex = 46
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(6, 4)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(125, 13)
        Me.Label72.TabIndex = 33
        Me.Label72.Text = "P&urchase Orders to Print"
        Me.Label72.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnPODontRmd
        '
        Me.rbtnPODontRmd.AutoSize = True
        Me.rbtnPODontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnPODontRmd.Name = "rbtnPODontRmd"
        Me.rbtnPODontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPODontRmd.TabIndex = 32
        Me.rbtnPODontRmd.TabStop = True
        Me.rbtnPODontRmd.UseVisualStyleBackColor = True
        '
        'rbtnPOList
        '
        Me.rbtnPOList.AutoSize = True
        Me.rbtnPOList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnPOList.Name = "rbtnPOList"
        Me.rbtnPOList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPOList.TabIndex = 31
        Me.rbtnPOList.TabStop = True
        Me.rbtnPOList.UseVisualStyleBackColor = True
        '
        'rbtnPOSummary
        '
        Me.rbtnPOSummary.AutoSize = True
        Me.rbtnPOSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnPOSummary.Name = "rbtnPOSummary"
        Me.rbtnPOSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPOSummary.TabIndex = 30
        Me.rbtnPOSummary.TabStop = True
        Me.rbtnPOSummary.UseVisualStyleBackColor = True
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Label73)
        Me.Panel11.Controls.Add(Me.rbtnMoneyDontRmd)
        Me.Panel11.Controls.Add(Me.rbtnMoneyList)
        Me.Panel11.Controls.Add(Me.rbtnMoneySummary)
        Me.Panel11.Location = New System.Drawing.Point(3, 296)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(436, 22)
        Me.Panel11.TabIndex = 45
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(6, 4)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(91, 13)
        Me.Label73.TabIndex = 33
        Me.Label73.Text = "Mo&ney to Deposit"
        Me.Label73.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnMoneyDontRmd
        '
        Me.rbtnMoneyDontRmd.AutoSize = True
        Me.rbtnMoneyDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnMoneyDontRmd.Name = "rbtnMoneyDontRmd"
        Me.rbtnMoneyDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnMoneyDontRmd.TabIndex = 32
        Me.rbtnMoneyDontRmd.TabStop = True
        Me.rbtnMoneyDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnMoneyList
        '
        Me.rbtnMoneyList.AutoSize = True
        Me.rbtnMoneyList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnMoneyList.Name = "rbtnMoneyList"
        Me.rbtnMoneyList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnMoneyList.TabIndex = 31
        Me.rbtnMoneyList.TabStop = True
        Me.rbtnMoneyList.UseVisualStyleBackColor = True
        '
        'rbtnMoneySummary
        '
        Me.rbtnMoneySummary.AutoSize = True
        Me.rbtnMoneySummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnMoneySummary.Name = "rbtnMoneySummary"
        Me.rbtnMoneySummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnMoneySummary.TabIndex = 30
        Me.rbtnMoneySummary.TabStop = True
        Me.rbtnMoneySummary.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Label70)
        Me.Panel10.Controls.Add(Me.Label71)
        Me.Panel10.Controls.Add(Me.rbtnTransDontRmd)
        Me.Panel10.Controls.Add(Me.rbtnTransList)
        Me.Panel10.Controls.Add(Me.rbtnTransSummary)
        Me.Panel10.Controls.Add(Me.txtDaysAfterDueDteTrans)
        Me.Panel10.Location = New System.Drawing.Point(3, 265)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(436, 31)
        Me.Panel10.TabIndex = 44
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(288, 4)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(115, 13)
        Me.Label70.TabIndex = 34
        Me.Label70.Text = "days before due date."
        Me.Label70.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label71
        '
        Me.Label71.Location = New System.Drawing.Point(6, 4)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(114, 27)
        Me.Label71.TabIndex = 33
        Me.Label71.Text = "Memori&zed Transactions Due"
        '
        'rbtnTransDontRmd
        '
        Me.rbtnTransDontRmd.AutoSize = True
        Me.rbtnTransDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnTransDontRmd.Name = "rbtnTransDontRmd"
        Me.rbtnTransDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnTransDontRmd.TabIndex = 32
        Me.rbtnTransDontRmd.TabStop = True
        Me.rbtnTransDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnTransList
        '
        Me.rbtnTransList.AutoSize = True
        Me.rbtnTransList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnTransList.Name = "rbtnTransList"
        Me.rbtnTransList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnTransList.TabIndex = 31
        Me.rbtnTransList.TabStop = True
        Me.rbtnTransList.UseVisualStyleBackColor = True
        '
        'rbtnTransSummary
        '
        Me.rbtnTransSummary.AutoSize = True
        Me.rbtnTransSummary.Location = New System.Drawing.Point(131, 3)
        Me.rbtnTransSummary.Name = "rbtnTransSummary"
        Me.rbtnTransSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnTransSummary.TabIndex = 30
        Me.rbtnTransSummary.TabStop = True
        Me.rbtnTransSummary.UseVisualStyleBackColor = True
        '
        'txtDaysAfterDueDteTrans
        '
        Me.txtDaysAfterDueDteTrans.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysAfterDueDteTrans.Name = "txtDaysAfterDueDteTrans"
        Me.txtDaysAfterDueDteTrans.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysAfterDueDteTrans.TabIndex = 29
        Me.txtDaysAfterDueDteTrans.Text = "5"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label68)
        Me.Panel9.Controls.Add(Me.Label69)
        Me.Panel9.Controls.Add(Me.rbtnBillsDontRmd)
        Me.Panel9.Controls.Add(Me.rbtnBillsList)
        Me.Panel9.Controls.Add(Me.rbtnBillsSummary)
        Me.Panel9.Controls.Add(Me.txtDaysAfterDueDteBills)
        Me.Panel9.Location = New System.Drawing.Point(3, 243)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(436, 22)
        Me.Panel9.TabIndex = 43
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(288, 4)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(115, 13)
        Me.Label68.TabIndex = 34
        Me.Label68.Text = "days before due date."
        Me.Label68.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(6, 4)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(58, 13)
        Me.Label69.TabIndex = 33
        Me.Label69.Text = "&Bills to Pay"
        Me.Label69.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnBillsDontRmd
        '
        Me.rbtnBillsDontRmd.AutoSize = True
        Me.rbtnBillsDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnBillsDontRmd.Name = "rbtnBillsDontRmd"
        Me.rbtnBillsDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnBillsDontRmd.TabIndex = 32
        Me.rbtnBillsDontRmd.TabStop = True
        Me.rbtnBillsDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnBillsList
        '
        Me.rbtnBillsList.AutoSize = True
        Me.rbtnBillsList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnBillsList.Name = "rbtnBillsList"
        Me.rbtnBillsList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnBillsList.TabIndex = 31
        Me.rbtnBillsList.TabStop = True
        Me.rbtnBillsList.UseVisualStyleBackColor = True
        '
        'rbtnBillsSummary
        '
        Me.rbtnBillsSummary.AutoSize = True
        Me.rbtnBillsSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnBillsSummary.Name = "rbtnBillsSummary"
        Me.rbtnBillsSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnBillsSummary.TabIndex = 30
        Me.rbtnBillsSummary.TabStop = True
        Me.rbtnBillsSummary.UseVisualStyleBackColor = True
        '
        'txtDaysAfterDueDteBills
        '
        Me.txtDaysAfterDueDteBills.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysAfterDueDteBills.Name = "txtDaysAfterDueDteBills"
        Me.txtDaysAfterDueDteBills.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysAfterDueDteBills.TabIndex = 29
        Me.txtDaysAfterDueDteBills.Text = "10"
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label67)
        Me.Panel8.Controls.Add(Me.rbtnAssemblyDontRmd)
        Me.Panel8.Controls.Add(Me.rbtnAssemblyList)
        Me.Panel8.Controls.Add(Me.rbtnAssemblySummary)
        Me.Panel8.Location = New System.Drawing.Point(3, 221)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(436, 22)
        Me.Panel8.TabIndex = 42
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(6, 4)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(115, 13)
        Me.Label67.TabIndex = 33
        Me.Label67.Text = "&Assembly Item to Build"
        Me.Label67.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnAssemblyDontRmd
        '
        Me.rbtnAssemblyDontRmd.AutoSize = True
        Me.rbtnAssemblyDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnAssemblyDontRmd.Name = "rbtnAssemblyDontRmd"
        Me.rbtnAssemblyDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnAssemblyDontRmd.TabIndex = 32
        Me.rbtnAssemblyDontRmd.TabStop = True
        Me.rbtnAssemblyDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnAssemblyList
        '
        Me.rbtnAssemblyList.AutoSize = True
        Me.rbtnAssemblyList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnAssemblyList.Name = "rbtnAssemblyList"
        Me.rbtnAssemblyList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnAssemblyList.TabIndex = 31
        Me.rbtnAssemblyList.TabStop = True
        Me.rbtnAssemblyList.UseVisualStyleBackColor = True
        '
        'rbtnAssemblySummary
        '
        Me.rbtnAssemblySummary.AutoSize = True
        Me.rbtnAssemblySummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnAssemblySummary.Name = "rbtnAssemblySummary"
        Me.rbtnAssemblySummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnAssemblySummary.TabIndex = 30
        Me.rbtnAssemblySummary.TabStop = True
        Me.rbtnAssemblySummary.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label66)
        Me.Panel7.Controls.Add(Me.rbtnInventoryDontRmd)
        Me.Panel7.Controls.Add(Me.rbtnInventoryList)
        Me.Panel7.Controls.Add(Me.rbtnInventorySummary)
        Me.Panel7.Location = New System.Drawing.Point(3, 199)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(436, 22)
        Me.Panel7.TabIndex = 41
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(6, 4)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(110, 13)
        Me.Label66.TabIndex = 33
        Me.Label66.Text = "In&ventory to Reorder"
        Me.Label66.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnInventoryDontRmd
        '
        Me.rbtnInventoryDontRmd.AutoSize = True
        Me.rbtnInventoryDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnInventoryDontRmd.Name = "rbtnInventoryDontRmd"
        Me.rbtnInventoryDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInventoryDontRmd.TabIndex = 32
        Me.rbtnInventoryDontRmd.TabStop = True
        Me.rbtnInventoryDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnInventoryList
        '
        Me.rbtnInventoryList.AutoSize = True
        Me.rbtnInventoryList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnInventoryList.Name = "rbtnInventoryList"
        Me.rbtnInventoryList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInventoryList.TabIndex = 31
        Me.rbtnInventoryList.TabStop = True
        Me.rbtnInventoryList.UseVisualStyleBackColor = True
        '
        'rbtnInventorySummary
        '
        Me.rbtnInventorySummary.AutoSize = True
        Me.rbtnInventorySummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnInventorySummary.Name = "rbtnInventorySummary"
        Me.rbtnInventorySummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInventorySummary.TabIndex = 30
        Me.rbtnInventorySummary.TabStop = True
        Me.rbtnInventorySummary.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label64)
        Me.Panel6.Controls.Add(Me.rbtnSODontRmd)
        Me.Panel6.Controls.Add(Me.rbtnSOList)
        Me.Panel6.Controls.Add(Me.rbtnSOSummary)
        Me.Panel6.Location = New System.Drawing.Point(3, 177)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(436, 22)
        Me.Panel6.TabIndex = 40
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(6, 4)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(106, 13)
        Me.Label64.TabIndex = 33
        Me.Label64.Text = "&Sales Orders to Print"
        Me.Label64.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnSODontRmd
        '
        Me.rbtnSODontRmd.AutoSize = True
        Me.rbtnSODontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnSODontRmd.Name = "rbtnSODontRmd"
        Me.rbtnSODontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSODontRmd.TabIndex = 32
        Me.rbtnSODontRmd.TabStop = True
        Me.rbtnSODontRmd.UseVisualStyleBackColor = True
        '
        'rbtnSOList
        '
        Me.rbtnSOList.AutoSize = True
        Me.rbtnSOList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnSOList.Name = "rbtnSOList"
        Me.rbtnSOList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSOList.TabIndex = 31
        Me.rbtnSOList.TabStop = True
        Me.rbtnSOList.UseVisualStyleBackColor = True
        '
        'rbtnSOSummary
        '
        Me.rbtnSOSummary.AutoSize = True
        Me.rbtnSOSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnSOSummary.Name = "rbtnSOSummary"
        Me.rbtnSOSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSOSummary.TabIndex = 30
        Me.rbtnSOSummary.TabStop = True
        Me.rbtnSOSummary.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label65)
        Me.Panel5.Controls.Add(Me.rbtnSalesRctDontRmd)
        Me.Panel5.Controls.Add(Me.rbtnSalesRctList)
        Me.Panel5.Controls.Add(Me.rbtnSalesRctSummary)
        Me.Panel5.Location = New System.Drawing.Point(3, 155)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(436, 22)
        Me.Panel5.TabIndex = 39
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(6, 4)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(114, 13)
        Me.Label65.TabIndex = 33
        Me.Label65.Text = "Sales &Receipts to Print"
        Me.Label65.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnSalesRctDontRmd
        '
        Me.rbtnSalesRctDontRmd.AutoSize = True
        Me.rbtnSalesRctDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnSalesRctDontRmd.Name = "rbtnSalesRctDontRmd"
        Me.rbtnSalesRctDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSalesRctDontRmd.TabIndex = 32
        Me.rbtnSalesRctDontRmd.TabStop = True
        Me.rbtnSalesRctDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnSalesRctList
        '
        Me.rbtnSalesRctList.AutoSize = True
        Me.rbtnSalesRctList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnSalesRctList.Name = "rbtnSalesRctList"
        Me.rbtnSalesRctList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSalesRctList.TabIndex = 31
        Me.rbtnSalesRctList.TabStop = True
        Me.rbtnSalesRctList.UseVisualStyleBackColor = True
        '
        'rbtnSalesRctSummary
        '
        Me.rbtnSalesRctSummary.AutoSize = True
        Me.rbtnSalesRctSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnSalesRctSummary.Name = "rbtnSalesRctSummary"
        Me.rbtnSalesRctSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnSalesRctSummary.TabIndex = 30
        Me.rbtnSalesRctSummary.TabStop = True
        Me.rbtnSalesRctSummary.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label62)
        Me.Panel4.Controls.Add(Me.Label63)
        Me.Panel4.Controls.Add(Me.rbtnOverdueInvoiceDontRmd)
        Me.Panel4.Controls.Add(Me.rbtnOverdueInvoiceList)
        Me.Panel4.Controls.Add(Me.rbtnOverdueInvoiceSummary)
        Me.Panel4.Controls.Add(Me.txtDaysAfterDueDte)
        Me.Panel4.Location = New System.Drawing.Point(3, 133)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(436, 22)
        Me.Panel4.TabIndex = 38
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(288, 4)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(107, 13)
        Me.Label62.TabIndex = 34
        Me.Label62.Text = "days after due date."
        Me.Label62.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(6, 4)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(92, 13)
        Me.Label63.TabIndex = 33
        Me.Label63.Text = "&Overdue Invoices"
        Me.Label63.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnOverdueInvoiceDontRmd
        '
        Me.rbtnOverdueInvoiceDontRmd.AutoSize = True
        Me.rbtnOverdueInvoiceDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnOverdueInvoiceDontRmd.Name = "rbtnOverdueInvoiceDontRmd"
        Me.rbtnOverdueInvoiceDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnOverdueInvoiceDontRmd.TabIndex = 32
        Me.rbtnOverdueInvoiceDontRmd.TabStop = True
        Me.rbtnOverdueInvoiceDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnOverdueInvoiceList
        '
        Me.rbtnOverdueInvoiceList.AutoSize = True
        Me.rbtnOverdueInvoiceList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnOverdueInvoiceList.Name = "rbtnOverdueInvoiceList"
        Me.rbtnOverdueInvoiceList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnOverdueInvoiceList.TabIndex = 31
        Me.rbtnOverdueInvoiceList.TabStop = True
        Me.rbtnOverdueInvoiceList.UseVisualStyleBackColor = True
        '
        'rbtnOverdueInvoiceSummary
        '
        Me.rbtnOverdueInvoiceSummary.AutoSize = True
        Me.rbtnOverdueInvoiceSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnOverdueInvoiceSummary.Name = "rbtnOverdueInvoiceSummary"
        Me.rbtnOverdueInvoiceSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnOverdueInvoiceSummary.TabIndex = 30
        Me.rbtnOverdueInvoiceSummary.TabStop = True
        Me.rbtnOverdueInvoiceSummary.UseVisualStyleBackColor = True
        '
        'txtDaysAfterDueDte
        '
        Me.txtDaysAfterDueDte.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysAfterDueDte.Name = "txtDaysAfterDueDte"
        Me.txtDaysAfterDueDte.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysAfterDueDte.TabIndex = 29
        Me.txtDaysAfterDueDte.Text = "5"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label60)
        Me.Panel3.Controls.Add(Me.Label61)
        Me.Panel3.Controls.Add(Me.rbtnInvoiceDontRmd)
        Me.Panel3.Controls.Add(Me.rbtnInvoiceList)
        Me.Panel3.Controls.Add(Me.rbtnInvoiceSummary)
        Me.Panel3.Controls.Add(Me.txtDaysBeforeInvoiceDte)
        Me.Panel3.Location = New System.Drawing.Point(3, 107)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(436, 29)
        Me.Panel3.TabIndex = 37
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(288, 4)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(130, 13)
        Me.Label60.TabIndex = 34
        Me.Label60.Text = "days before invoice date."
        Me.Label60.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label61
        '
        Me.Label61.Location = New System.Drawing.Point(6, -2)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(110, 31)
        Me.Label61.TabIndex = 33
        Me.Label61.Text = "&Invoice/Credit Memos to Print"
        '
        'rbtnInvoiceDontRmd
        '
        Me.rbtnInvoiceDontRmd.AutoSize = True
        Me.rbtnInvoiceDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnInvoiceDontRmd.Name = "rbtnInvoiceDontRmd"
        Me.rbtnInvoiceDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInvoiceDontRmd.TabIndex = 32
        Me.rbtnInvoiceDontRmd.TabStop = True
        Me.rbtnInvoiceDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnInvoiceList
        '
        Me.rbtnInvoiceList.AutoSize = True
        Me.rbtnInvoiceList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnInvoiceList.Name = "rbtnInvoiceList"
        Me.rbtnInvoiceList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInvoiceList.TabIndex = 31
        Me.rbtnInvoiceList.TabStop = True
        Me.rbtnInvoiceList.UseVisualStyleBackColor = True
        '
        'rbtnInvoiceSummary
        '
        Me.rbtnInvoiceSummary.AutoSize = True
        Me.rbtnInvoiceSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnInvoiceSummary.Name = "rbtnInvoiceSummary"
        Me.rbtnInvoiceSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnInvoiceSummary.TabIndex = 30
        Me.rbtnInvoiceSummary.TabStop = True
        Me.rbtnInvoiceSummary.UseVisualStyleBackColor = True
        '
        'txtDaysBeforeInvoiceDte
        '
        Me.txtDaysBeforeInvoiceDte.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysBeforeInvoiceDte.Name = "txtDaysBeforeInvoiceDte"
        Me.txtDaysBeforeInvoiceDte.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysBeforeInvoiceDte.TabIndex = 29
        Me.txtDaysBeforeInvoiceDte.Text = "5"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label54)
        Me.Panel2.Controls.Add(Me.Label55)
        Me.Panel2.Controls.Add(Me.rbtnPChkDontRmd)
        Me.Panel2.Controls.Add(Me.rbtnPChkList)
        Me.Panel2.Controls.Add(Me.rbtnPChkSummary)
        Me.Panel2.Controls.Add(Me.txtDaysBeforePyChkDte)
        Me.Panel2.Location = New System.Drawing.Point(3, 85)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(436, 22)
        Me.Panel2.TabIndex = 36
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(288, 4)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(142, 13)
        Me.Label54.TabIndex = 34
        Me.Label54.Text = "days before paycheck date."
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(6, 4)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(95, 13)
        Me.Label55.TabIndex = 33
        Me.Label55.Text = "&Paychecks to Print"
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnPChkDontRmd
        '
        Me.rbtnPChkDontRmd.AutoSize = True
        Me.rbtnPChkDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnPChkDontRmd.Name = "rbtnPChkDontRmd"
        Me.rbtnPChkDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPChkDontRmd.TabIndex = 32
        Me.rbtnPChkDontRmd.TabStop = True
        Me.rbtnPChkDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnPChkList
        '
        Me.rbtnPChkList.AutoSize = True
        Me.rbtnPChkList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnPChkList.Name = "rbtnPChkList"
        Me.rbtnPChkList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPChkList.TabIndex = 31
        Me.rbtnPChkList.TabStop = True
        Me.rbtnPChkList.UseVisualStyleBackColor = True
        '
        'rbtnPChkSummary
        '
        Me.rbtnPChkSummary.AutoSize = True
        Me.rbtnPChkSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnPChkSummary.Name = "rbtnPChkSummary"
        Me.rbtnPChkSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnPChkSummary.TabIndex = 30
        Me.rbtnPChkSummary.TabStop = True
        Me.rbtnPChkSummary.UseVisualStyleBackColor = True
        '
        'txtDaysBeforePyChkDte
        '
        Me.txtDaysBeforePyChkDte.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysBeforePyChkDte.Name = "txtDaysBeforePyChkDte"
        Me.txtDaysBeforePyChkDte.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysBeforePyChkDte.TabIndex = 29
        Me.txtDaysBeforePyChkDte.Text = "5"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label56)
        Me.Panel1.Controls.Add(Me.Label57)
        Me.Panel1.Controls.Add(Me.rbtnChkDontRmd)
        Me.Panel1.Controls.Add(Me.rbtnChkList)
        Me.Panel1.Controls.Add(Me.rbtnChkSummary)
        Me.Panel1.Controls.Add(Me.txtDaysBeforeChkDte)
        Me.Panel1.Location = New System.Drawing.Point(3, 63)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(423, 22)
        Me.Panel1.TabIndex = 35
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(288, 4)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(124, 13)
        Me.Label56.TabIndex = 34
        Me.Label56.Text = "days before check date."
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(6, 4)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(79, 13)
        Me.Label57.TabIndex = 33
        Me.Label57.Text = "Chec&ks to Print"
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'rbtnChkDontRmd
        '
        Me.rbtnChkDontRmd.AutoSize = True
        Me.rbtnChkDontRmd.Location = New System.Drawing.Point(219, 4)
        Me.rbtnChkDontRmd.Name = "rbtnChkDontRmd"
        Me.rbtnChkDontRmd.Size = New System.Drawing.Size(14, 13)
        Me.rbtnChkDontRmd.TabIndex = 32
        Me.rbtnChkDontRmd.TabStop = True
        Me.rbtnChkDontRmd.UseVisualStyleBackColor = True
        '
        'rbtnChkList
        '
        Me.rbtnChkList.AutoSize = True
        Me.rbtnChkList.Location = New System.Drawing.Point(175, 4)
        Me.rbtnChkList.Name = "rbtnChkList"
        Me.rbtnChkList.Size = New System.Drawing.Size(14, 13)
        Me.rbtnChkList.TabIndex = 31
        Me.rbtnChkList.TabStop = True
        Me.rbtnChkList.UseVisualStyleBackColor = True
        '
        'rbtnChkSummary
        '
        Me.rbtnChkSummary.AutoSize = True
        Me.rbtnChkSummary.Location = New System.Drawing.Point(131, 4)
        Me.rbtnChkSummary.Name = "rbtnChkSummary"
        Me.rbtnChkSummary.Size = New System.Drawing.Size(14, 13)
        Me.rbtnChkSummary.TabIndex = 30
        Me.rbtnChkSummary.TabStop = True
        Me.rbtnChkSummary.UseVisualStyleBackColor = True
        '
        'txtDaysBeforeChkDte
        '
        Me.txtDaysBeforeChkDte.Location = New System.Drawing.Point(253, 1)
        Me.txtDaysBeforeChkDte.Name = "txtDaysBeforeChkDte"
        Me.txtDaysBeforeChkDte.Size = New System.Drawing.Size(32, 21)
        Me.txtDaysBeforeChkDte.TabIndex = 29
        Me.txtDaysBeforeChkDte.Text = "5"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(199, 27)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(64, 33)
        Me.Label53.TabIndex = 34
        Me.Label53.Text = "Don't Remind Me"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(166, 27)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 33)
        Me.Label27.TabIndex = 33
        Me.Label27.Text = "Show List"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(114, 27)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(53, 33)
        Me.Label58.TabIndex = 18
        Me.Label58.Text = "Show Summary"
        Me.Label58.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(11, 11)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(76, 16)
        Me.Label59.TabIndex = 7
        Me.Label59.Text = "Reminders"
        '
        'pnlJobsCoPref
        '
        Me.pnlJobsCoPref.Controls.Add(Me.chkDontPrintZero)
        Me.pnlJobsCoPref.Controls.Add(Me.chkWarnDupEstimates)
        Me.pnlJobsCoPref.Controls.Add(Me.GroupBox23)
        Me.pnlJobsCoPref.Controls.Add(Me.Label52)
        Me.pnlJobsCoPref.Controls.Add(Me.Label51)
        Me.pnlJobsCoPref.Controls.Add(Me.txtNotAward)
        Me.pnlJobsCoPref.Controls.Add(Me.Label50)
        Me.pnlJobsCoPref.Controls.Add(Me.txtClose)
        Me.pnlJobsCoPref.Controls.Add(Me.Label49)
        Me.pnlJobsCoPref.Controls.Add(Me.txtProgress)
        Me.pnlJobsCoPref.Controls.Add(Me.Label48)
        Me.pnlJobsCoPref.Controls.Add(Me.txtAward)
        Me.pnlJobsCoPref.Controls.Add(Me.Label46)
        Me.pnlJobsCoPref.Controls.Add(Me.txtPending)
        Me.pnlJobsCoPref.Controls.Add(Me.Label45)
        Me.pnlJobsCoPref.Controls.Add(Me.GroupBox24)
        Me.pnlJobsCoPref.Controls.Add(Me.Label47)
        Me.pnlJobsCoPref.Location = New System.Drawing.Point(228, 176)
        Me.pnlJobsCoPref.Name = "pnlJobsCoPref"
        Me.pnlJobsCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlJobsCoPref.TabIndex = 13
        '
        'chkDontPrintZero
        '
        Me.chkDontPrintZero.AutoSize = True
        Me.chkDontPrintZero.Location = New System.Drawing.Point(198, 312)
        Me.chkDontPrintZero.Name = "chkDontPrintZero"
        Me.chkDontPrintZero.Size = New System.Drawing.Size(217, 17)
        Me.chkDontPrintZero.TabIndex = 32
        Me.chkDontPrintZero.Text = "Don't print items that have &zero amount"
        Me.chkDontPrintZero.UseVisualStyleBackColor = True
        '
        'chkWarnDupEstimates
        '
        Me.chkWarnDupEstimates.AutoSize = True
        Me.chkWarnDupEstimates.Location = New System.Drawing.Point(198, 237)
        Me.chkWarnDupEstimates.Name = "chkWarnDupEstimates"
        Me.chkWarnDupEstimates.Size = New System.Drawing.Size(217, 17)
        Me.chkWarnDupEstimates.TabIndex = 31
        Me.chkWarnDupEstimates.Text = "&Warn about duplicate estimate numbers"
        Me.chkWarnDupEstimates.UseVisualStyleBackColor = True
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.rbtnNotProgressInvoice)
        Me.GroupBox23.Controls.Add(Me.rbtnProgressInvoice)
        Me.GroupBox23.Location = New System.Drawing.Point(19, 278)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(168, 66)
        Me.GroupBox23.TabIndex = 30
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "Do You Do Progress Invoicing?"
        '
        'rbtnNotProgressInvoice
        '
        Me.rbtnNotProgressInvoice.AutoSize = True
        Me.rbtnNotProgressInvoice.Location = New System.Drawing.Point(84, 33)
        Me.rbtnNotProgressInvoice.Name = "rbtnNotProgressInvoice"
        Me.rbtnNotProgressInvoice.Size = New System.Drawing.Size(38, 17)
        Me.rbtnNotProgressInvoice.TabIndex = 25
        Me.rbtnNotProgressInvoice.TabStop = True
        Me.rbtnNotProgressInvoice.Text = "N&o"
        Me.rbtnNotProgressInvoice.UseVisualStyleBackColor = True
        '
        'rbtnProgressInvoice
        '
        Me.rbtnProgressInvoice.AutoSize = True
        Me.rbtnProgressInvoice.Location = New System.Drawing.Point(24, 32)
        Me.rbtnProgressInvoice.Name = "rbtnProgressInvoice"
        Me.rbtnProgressInvoice.Size = New System.Drawing.Size(42, 17)
        Me.rbtnProgressInvoice.TabIndex = 24
        Me.rbtnProgressInvoice.TabStop = True
        Me.rbtnProgressInvoice.Text = "Ye&s"
        Me.rbtnProgressInvoice.UseVisualStyleBackColor = True
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(238, 83)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(151, 49)
        Me.Label52.TabIndex = 29
        Me.Label52.Text = "For example: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You could enter ""Submitted "" for Pending." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(238, 46)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(184, 29)
        Me.Label51.TabIndex = 28
        Me.Label51.Text = "Enter the job status descriptions you prefer for your business. "
        '
        'txtNotAward
        '
        Me.txtNotAward.Location = New System.Drawing.Point(96, 159)
        Me.txtNotAward.Name = "txtNotAward"
        Me.txtNotAward.Size = New System.Drawing.Size(130, 21)
        Me.txtNotAward.TabIndex = 27
        Me.txtNotAward.Text = "Not awarded"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(16, 161)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(70, 13)
        Me.Label50.TabIndex = 26
        Me.Label50.Text = "No&t Awarded"
        '
        'txtClose
        '
        Me.txtClose.Location = New System.Drawing.Point(96, 130)
        Me.txtClose.Name = "txtClose"
        Me.txtClose.Size = New System.Drawing.Size(130, 21)
        Me.txtClose.TabIndex = 25
        Me.txtClose.Text = "Closed"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(16, 132)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(39, 13)
        Me.Label49.TabIndex = 24
        Me.Label49.Text = "C&losed"
        '
        'txtProgress
        '
        Me.txtProgress.Location = New System.Drawing.Point(96, 101)
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.Size = New System.Drawing.Size(130, 21)
        Me.txtProgress.TabIndex = 23
        Me.txtProgress.Text = "In progress"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(16, 106)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(62, 13)
        Me.Label48.TabIndex = 22
        Me.Label48.Text = "&In Progress"
        '
        'txtAward
        '
        Me.txtAward.Location = New System.Drawing.Point(96, 72)
        Me.txtAward.Name = "txtAward"
        Me.txtAward.Size = New System.Drawing.Size(130, 21)
        Me.txtAward.TabIndex = 21
        Me.txtAward.Text = "Award"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(16, 74)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(50, 13)
        Me.Label46.TabIndex = 20
        Me.Label46.Text = "&Awarded"
        '
        'txtPending
        '
        Me.txtPending.Location = New System.Drawing.Point(96, 43)
        Me.txtPending.Name = "txtPending"
        Me.txtPending.Size = New System.Drawing.Size(130, 21)
        Me.txtPending.TabIndex = 19
        Me.txtPending.Text = "Pending"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(16, 46)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(45, 13)
        Me.Label45.TabIndex = 18
        Me.Label45.Text = "&Pending"
        '
        'GroupBox24
        '
        Me.GroupBox24.Controls.Add(Me.sbtnNotCreateEstimates)
        Me.GroupBox24.Controls.Add(Me.rbtnCreateEstimate)
        Me.GroupBox24.Location = New System.Drawing.Point(19, 201)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Size = New System.Drawing.Size(168, 66)
        Me.GroupBox24.TabIndex = 17
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "Do You Create Estimates?"
        '
        'sbtnNotCreateEstimates
        '
        Me.sbtnNotCreateEstimates.AutoSize = True
        Me.sbtnNotCreateEstimates.Location = New System.Drawing.Point(84, 33)
        Me.sbtnNotCreateEstimates.Name = "sbtnNotCreateEstimates"
        Me.sbtnNotCreateEstimates.Size = New System.Drawing.Size(38, 17)
        Me.sbtnNotCreateEstimates.TabIndex = 25
        Me.sbtnNotCreateEstimates.TabStop = True
        Me.sbtnNotCreateEstimates.Text = "&No"
        Me.sbtnNotCreateEstimates.UseVisualStyleBackColor = True
        '
        'rbtnCreateEstimate
        '
        Me.rbtnCreateEstimate.AutoSize = True
        Me.rbtnCreateEstimate.Location = New System.Drawing.Point(24, 32)
        Me.rbtnCreateEstimate.Name = "rbtnCreateEstimate"
        Me.rbtnCreateEstimate.Size = New System.Drawing.Size(42, 17)
        Me.rbtnCreateEstimate.TabIndex = 24
        Me.rbtnCreateEstimate.TabStop = True
        Me.rbtnCreateEstimate.Text = "&Yes"
        Me.rbtnCreateEstimate.UseVisualStyleBackColor = True
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(16, 11)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(119, 16)
        Me.Label47.TabIndex = 7
        Me.Label47.Text = "Jobs && Estimates"
        '
        'pnlItemsCoPref
        '
        Me.pnlItemsCoPref.Controls.Add(Me.GroupBox20)
        Me.pnlItemsCoPref.Controls.Add(Me.GroupBox19)
        Me.pnlItemsCoPref.Controls.Add(Me.Label43)
        Me.pnlItemsCoPref.Location = New System.Drawing.Point(260, 155)
        Me.pnlItemsCoPref.Name = "pnlItemsCoPref"
        Me.pnlItemsCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlItemsCoPref.TabIndex = 12
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.btnEnable)
        Me.GroupBox20.Location = New System.Drawing.Point(10, 274)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(410, 67)
        Me.GroupBox20.TabIndex = 18
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "Unit of Measure"
        '
        'btnEnable
        '
        Me.btnEnable.Location = New System.Drawing.Point(33, 27)
        Me.btnEnable.Name = "btnEnable"
        Me.btnEnable.Size = New System.Drawing.Size(75, 23)
        Me.btnEnable.TabIndex = 2
        Me.btnEnable.Text = "Enable"
        Me.btnEnable.UseVisualStyleBackColor = True
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.rbtnQtyAvailable)
        Me.GroupBox19.Controls.Add(Me.rbtnQtyOnHand)
        Me.GroupBox19.Controls.Add(Me.chkWarnNoInventory)
        Me.GroupBox19.Controls.Add(Me.chkQtySO)
        Me.GroupBox19.Controls.Add(Me.chkQtyRsv)
        Me.GroupBox19.Controls.Add(Me.Label44)
        Me.GroupBox19.Controls.Add(Me.lnkQty)
        Me.GroupBox19.Controls.Add(Me.Label42)
        Me.GroupBox19.Controls.Add(Me.chkWarnDuplicatePO)
        Me.GroupBox19.Controls.Add(Me.chkActive)
        Me.GroupBox19.Location = New System.Drawing.Point(10, 41)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(410, 227)
        Me.GroupBox19.TabIndex = 17
        Me.GroupBox19.TabStop = False
        Me.GroupBox19.Text = "Purchase Orders && Inventory"
        '
        'rbtnQtyAvailable
        '
        Me.rbtnQtyAvailable.AutoSize = True
        Me.rbtnQtyAvailable.Location = New System.Drawing.Point(33, 190)
        Me.rbtnQtyAvailable.Name = "rbtnQtyAvailable"
        Me.rbtnQtyAvailable.Size = New System.Drawing.Size(333, 17)
        Me.rbtnQtyAvailable.TabIndex = 25
        Me.rbtnQtyAvailable.TabStop = True
        Me.rbtnQtyAvailable.Text = "When the quantity I want to sell exceeds the Quantity A&vailable"
        Me.rbtnQtyAvailable.UseVisualStyleBackColor = True
        '
        'rbtnQtyOnHand
        '
        Me.rbtnQtyOnHand.AutoSize = True
        Me.rbtnQtyOnHand.Location = New System.Drawing.Point(33, 169)
        Me.rbtnQtyOnHand.Name = "rbtnQtyOnHand"
        Me.rbtnQtyOnHand.Size = New System.Drawing.Size(330, 17)
        Me.rbtnQtyOnHand.TabIndex = 24
        Me.rbtnQtyOnHand.TabStop = True
        Me.rbtnQtyOnHand.Text = "When the quantity I want to sell exceeds the Quantity on &Hand"
        Me.rbtnQtyOnHand.UseVisualStyleBackColor = True
        '
        'chkWarnNoInventory
        '
        Me.chkWarnNoInventory.AutoSize = True
        Me.chkWarnNoInventory.Location = New System.Drawing.Point(10, 146)
        Me.chkWarnNoInventory.Name = "chkWarnNoInventory"
        Me.chkWarnNoInventory.Size = New System.Drawing.Size(199, 17)
        Me.chkWarnNoInventory.TabIndex = 23
        Me.chkWarnNoInventory.Text = "Warn if not &enough inventory to sell"
        Me.chkWarnNoInventory.UseVisualStyleBackColor = True
        '
        'chkQtySO
        '
        Me.chkQtySO.AutoSize = True
        Me.chkQtySO.Location = New System.Drawing.Point(236, 103)
        Me.chkQtySO.Name = "chkQtySO"
        Me.chkQtySO.Size = New System.Drawing.Size(147, 17)
        Me.chkQtySO.TabIndex = 22
        Me.chkQtySO.Text = "Quantity on sales &orders "
        Me.chkQtySO.UseVisualStyleBackColor = True
        '
        'chkQtyRsv
        '
        Me.chkQtyRsv.AutoSize = True
        Me.chkQtyRsv.Location = New System.Drawing.Point(33, 103)
        Me.chkQtyRsv.Name = "chkQtyRsv"
        Me.chkQtyRsv.Size = New System.Drawing.Size(202, 17)
        Me.chkQtyRsv.TabIndex = 21
        Me.chkQtyRsv.Text = "Quantity reserved for pending &builds"
        Me.chkQtyRsv.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(214, 81)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(131, 13)
        Me.Label44.TabIndex = 20
        Me.Label44.Text = "for my inventory, deduct:"
        '
        'lnkQty
        '
        Me.lnkQty.AutoSize = True
        Me.lnkQty.Location = New System.Drawing.Point(119, 81)
        Me.lnkQty.Name = "lnkQty"
        Me.lnkQty.Size = New System.Drawing.Size(95, 13)
        Me.lnkQty.TabIndex = 19
        Me.lnkQty.TabStop = True
        Me.lnkQty.Text = "Quantity Available"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(30, 81)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(91, 13)
        Me.Label42.TabIndex = 18
        Me.Label42.Text = "When calculating "
        '
        'chkWarnDuplicatePO
        '
        Me.chkWarnDuplicatePO.AutoSize = True
        Me.chkWarnDuplicatePO.Location = New System.Drawing.Point(10, 54)
        Me.chkWarnDuplicatePO.Name = "chkWarnDuplicatePO"
        Me.chkWarnDuplicatePO.Size = New System.Drawing.Size(249, 17)
        Me.chkWarnDuplicatePO.TabIndex = 9
        Me.chkWarnDuplicatePO.Text = "Warn about duplicate purchase orders &number"
        Me.chkWarnDuplicatePO.UseVisualStyleBackColor = True
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Location = New System.Drawing.Point(10, 31)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(227, 17)
        Me.chkActive.TabIndex = 8
        Me.chkActive.Text = "&Inventory and purchase orders are active"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(16, 11)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(129, 16)
        Me.Label43.TabIndex = 7
        Me.Label43.Text = "Items && Inventory"
        '
        'pnlIntegratedApplicationsCoPref
        '
        Me.pnlIntegratedApplicationsCoPref.Controls.Add(Me.Label41)
        Me.pnlIntegratedApplicationsCoPref.Controls.Add(Me.GroupBox22)
        Me.pnlIntegratedApplicationsCoPref.Controls.Add(Me.chkNotifyUser)
        Me.pnlIntegratedApplicationsCoPref.Controls.Add(Me.chkDontAllow)
        Me.pnlIntegratedApplicationsCoPref.Controls.Add(Me.Label26)
        Me.pnlIntegratedApplicationsCoPref.Location = New System.Drawing.Point(283, 134)
        Me.pnlIntegratedApplicationsCoPref.Name = "pnlIntegratedApplicationsCoPref"
        Me.pnlIntegratedApplicationsCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlIntegratedApplicationsCoPref.TabIndex = 11
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(16, 44)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(386, 37)
        Me.Label41.TabIndex = 18
        Me.Label41.Text = "You can manage all applications that interact with this Accounting System company" & _
            " file here."
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.btnRemove)
        Me.GroupBox22.Controls.Add(Me.btnProperties)
        Me.GroupBox22.Controls.Add(Me.lstApplication)
        Me.GroupBox22.Location = New System.Drawing.Point(9, 133)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(410, 215)
        Me.GroupBox22.TabIndex = 17
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "Applications that have previously requested access to this company file"
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(314, 65)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Text = "&Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnProperties
        '
        Me.btnProperties.Location = New System.Drawing.Point(314, 31)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.Size = New System.Drawing.Size(75, 23)
        Me.btnProperties.TabIndex = 2
        Me.btnProperties.Text = "&Properties"
        Me.btnProperties.UseVisualStyleBackColor = True
        '
        'lstApplication
        '
        Me.lstApplication.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lstApplication.Location = New System.Drawing.Point(10, 20)
        Me.lstApplication.Name = "lstApplication"
        Me.lstApplication.Size = New System.Drawing.Size(291, 189)
        Me.lstApplication.TabIndex = 0
        Me.lstApplication.UseCompatibleStateImageBehavior = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Allow Acc"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Application Name"
        Me.ColumnHeader2.Width = 100
        '
        'chkNotifyUser
        '
        Me.chkNotifyUser.AutoSize = True
        Me.chkNotifyUser.Location = New System.Drawing.Point(19, 107)
        Me.chkNotifyUser.Name = "chkNotifyUser"
        Me.chkNotifyUser.Size = New System.Drawing.Size(367, 17)
        Me.chkNotifyUser.TabIndex = 9
        Me.chkNotifyUser.Text = "&Notifythe user before running application whose certificate has expired"
        Me.chkNotifyUser.UseVisualStyleBackColor = True
        '
        'chkDontAllow
        '
        Me.chkDontAllow.AutoSize = True
        Me.chkDontAllow.Location = New System.Drawing.Point(19, 84)
        Me.chkDontAllow.Name = "chkDontAllow"
        Me.chkDontAllow.Size = New System.Drawing.Size(289, 17)
        Me.chkDontAllow.TabIndex = 8
        Me.chkDontAllow.Text = "Don'&t allow any applications to access this company file"
        Me.chkDontAllow.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(16, 11)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(162, 16)
        Me.Label26.TabIndex = 7
        Me.Label26.Text = "Integrated Applications"
        '
        'pnlGeneralCoPref
        '
        Me.pnlGeneralCoPref.Controls.Add(Me.chkNvrUpdateName)
        Me.pnlGeneralCoPref.Controls.Add(Me.GroupBox21)
        Me.pnlGeneralCoPref.Controls.Add(Me.Label39)
        Me.pnlGeneralCoPref.Controls.Add(Me.chkShwFourDigits)
        Me.pnlGeneralCoPref.Location = New System.Drawing.Point(304, 113)
        Me.pnlGeneralCoPref.Name = "pnlGeneralCoPref"
        Me.pnlGeneralCoPref.Size = New System.Drawing.Size(432, 381)
        Me.pnlGeneralCoPref.TabIndex = 10
        '
        'chkNvrUpdateName
        '
        Me.chkNvrUpdateName.AutoSize = True
        Me.chkNvrUpdateName.Location = New System.Drawing.Point(19, 192)
        Me.chkNvrUpdateName.Name = "chkNvrUpdateName"
        Me.chkNvrUpdateName.Size = New System.Drawing.Size(322, 17)
        Me.chkNvrUpdateName.TabIndex = 18
        Me.chkNvrUpdateName.Text = "Never &update the name information when saving transactions"
        Me.chkNvrUpdateName.UseVisualStyleBackColor = True
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.Label40)
        Me.GroupBox21.Controls.Add(Me.rbtnMinutes)
        Me.GroupBox21.Controls.Add(Me.rbtnDecimal)
        Me.GroupBox21.Location = New System.Drawing.Point(11, 48)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Size = New System.Drawing.Size(410, 100)
        Me.GroupBox21.TabIndex = 17
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "Time Format"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(23, 22)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(142, 13)
        Me.Label40.TabIndex = 15
        Me.Label40.Text = "Show portions of an hour as"
        '
        'rbtnMinutes
        '
        Me.rbtnMinutes.AutoSize = True
        Me.rbtnMinutes.Location = New System.Drawing.Point(26, 72)
        Me.rbtnMinutes.Name = "rbtnMinutes"
        Me.rbtnMinutes.Size = New System.Drawing.Size(101, 17)
        Me.rbtnMinutes.TabIndex = 14
        Me.rbtnMinutes.TabStop = True
        Me.rbtnMinutes.Text = "Mi&nutes (10:12)"
        Me.rbtnMinutes.UseVisualStyleBackColor = True
        '
        'rbtnDecimal
        '
        Me.rbtnDecimal.AutoSize = True
        Me.rbtnDecimal.Location = New System.Drawing.Point(26, 49)
        Me.rbtnDecimal.Name = "rbtnDecimal"
        Me.rbtnDecimal.Size = New System.Drawing.Size(100, 17)
        Me.rbtnDecimal.TabIndex = 13
        Me.rbtnDecimal.TabStop = True
        Me.rbtnDecimal.Text = "Dec&imal (10.20)"
        Me.rbtnDecimal.UseVisualStyleBackColor = True
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(16, 11)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(57, 16)
        Me.Label39.TabIndex = 7
        Me.Label39.Text = "General"
        '
        'chkShwFourDigits
        '
        Me.chkShwFourDigits.AutoSize = True
        Me.chkShwFourDigits.Location = New System.Drawing.Point(19, 167)
        Me.chkShwFourDigits.Name = "chkShwFourDigits"
        Me.chkShwFourDigits.Size = New System.Drawing.Size(204, 17)
        Me.chkShwFourDigits.TabIndex = 12
        Me.chkShwFourDigits.Text = "Always show &years as 4 digits (1999)"
        Me.chkShwFourDigits.UseVisualStyleBackColor = True
        '
        'pnlFinanceChargeCoPref
        '
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.chkMarkToBePrint)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.GroupBox15)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.chkAssessFCharge)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.cboFChargeAcc)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.Label36)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.txtGracePeriodDay)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.txtMinFinanceCharge)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.txtAnnualInterest)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.Label35)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.Label34)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.Label33)
        Me.pnlFinanceChargeCoPref.Controls.Add(Me.Label32)
        Me.pnlFinanceChargeCoPref.Location = New System.Drawing.Point(315, 89)
        Me.pnlFinanceChargeCoPref.Name = "pnlFinanceChargeCoPref"
        Me.pnlFinanceChargeCoPref.Size = New System.Drawing.Size(394, 341)
        Me.pnlFinanceChargeCoPref.TabIndex = 9
        '
        'chkMarkToBePrint
        '
        Me.chkMarkToBePrint.AutoSize = True
        Me.chkMarkToBePrint.Location = New System.Drawing.Point(19, 294)
        Me.chkMarkToBePrint.Name = "chkMarkToBePrint"
        Me.chkMarkToBePrint.Size = New System.Drawing.Size(236, 17)
        Me.chkMarkToBePrint.TabIndex = 18
        Me.chkMarkToBePrint.Text = "Mark finance charge Invoice ""To be Printed"""
        Me.chkMarkToBePrint.UseVisualStyleBackColor = True
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.rbtnInvoice)
        Me.GroupBox15.Controls.Add(Me.rbtnDueDate)
        Me.GroupBox15.Controls.Add(Me.Label37)
        Me.GroupBox15.Location = New System.Drawing.Point(19, 209)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(280, 71)
        Me.GroupBox15.TabIndex = 17
        Me.GroupBox15.TabStop = False
        '
        'rbtnInvoice
        '
        Me.rbtnInvoice.AutoSize = True
        Me.rbtnInvoice.Location = New System.Drawing.Point(141, 43)
        Me.rbtnInvoice.Name = "rbtnInvoice"
        Me.rbtnInvoice.Size = New System.Drawing.Size(114, 17)
        Me.rbtnInvoice.TabIndex = 13
        Me.rbtnInvoice.TabStop = True
        Me.rbtnInvoice.Text = "&Invoice/Billed Date"
        Me.rbtnInvoice.UseVisualStyleBackColor = True
        '
        'rbtnDueDate
        '
        Me.rbtnDueDate.AutoSize = True
        Me.rbtnDueDate.Location = New System.Drawing.Point(21, 40)
        Me.rbtnDueDate.Name = "rbtnDueDate"
        Me.rbtnDueDate.Size = New System.Drawing.Size(70, 17)
        Me.rbtnDueDate.TabIndex = 12
        Me.rbtnDueDate.TabStop = True
        Me.rbtnDueDate.Text = "D&ue Date"
        Me.rbtnDueDate.UseVisualStyleBackColor = True
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(18, 15)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(117, 13)
        Me.Label37.TabIndex = 11
        Me.Label37.Text = "Calculate charges from"
        '
        'chkAssessFCharge
        '
        Me.chkAssessFCharge.AutoSize = True
        Me.chkAssessFCharge.Location = New System.Drawing.Point(19, 186)
        Me.chkAssessFCharge.Name = "chkAssessFCharge"
        Me.chkAssessFCharge.Size = New System.Drawing.Size(275, 17)
        Me.chkAssessFCharge.TabIndex = 16
        Me.chkAssessFCharge.Text = "Assess finance charges on overdue &finance charges"
        Me.chkAssessFCharge.UseVisualStyleBackColor = True
        '
        'cboFChargeAcc
        '
        Me.cboFChargeAcc.FormattingEnabled = True
        Me.cboFChargeAcc.Location = New System.Drawing.Point(160, 135)
        Me.cboFChargeAcc.Name = "cboFChargeAcc"
        Me.cboFChargeAcc.Size = New System.Drawing.Size(201, 21)
        Me.cboFChargeAcc.TabIndex = 15
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(16, 138)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(124, 13)
        Me.Label36.TabIndex = 14
        Me.Label36.Text = "Finance Charge &Account"
        '
        'txtGracePeriodDay
        '
        Me.txtGracePeriodDay.Location = New System.Drawing.Point(160, 91)
        Me.txtGracePeriodDay.Name = "txtGracePeriodDay"
        Me.txtGracePeriodDay.Size = New System.Drawing.Size(139, 21)
        Me.txtGracePeriodDay.TabIndex = 13
        Me.txtGracePeriodDay.Text = "0"
        '
        'txtMinFinanceCharge
        '
        Me.txtMinFinanceCharge.Location = New System.Drawing.Point(160, 65)
        Me.txtMinFinanceCharge.Name = "txtMinFinanceCharge"
        Me.txtMinFinanceCharge.Size = New System.Drawing.Size(139, 21)
        Me.txtMinFinanceCharge.TabIndex = 12
        Me.txtMinFinanceCharge.Text = "0.00"
        '
        'txtAnnualInterest
        '
        Me.txtAnnualInterest.Location = New System.Drawing.Point(160, 39)
        Me.txtAnnualInterest.Name = "txtAnnualInterest"
        Me.txtAnnualInterest.Size = New System.Drawing.Size(139, 21)
        Me.txtAnnualInterest.TabIndex = 11
        Me.txtAnnualInterest.Text = "0.0%"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(16, 92)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(102, 13)
        Me.Label35.TabIndex = 10
        Me.Label35.Text = "&Grace Period (days)"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(16, 68)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(123, 13)
        Me.Label34.TabIndex = 9
        Me.Label34.Text = "Minimun Fi&nance Charge"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(16, 44)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(130, 13)
        Me.Label33.TabIndex = 8
        Me.Label33.Text = "Annual Interest &Rate (%)"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(16, 14)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(106, 16)
        Me.Label32.TabIndex = 7
        Me.Label32.Text = "Finance Charge"
        '
        'pnlDesktopCoPref
        '
        Me.pnlDesktopCoPref.Controls.Add(Me.Label22)
        Me.pnlDesktopCoPref.Controls.Add(Me.GroupBox14)
        Me.pnlDesktopCoPref.Controls.Add(Me.linkRemove)
        Me.pnlDesktopCoPref.Controls.Add(Me.GroupBox13)
        Me.pnlDesktopCoPref.Controls.Add(Me.GroupBox12)
        Me.pnlDesktopCoPref.Controls.Add(Me.Label20)
        Me.pnlDesktopCoPref.Location = New System.Drawing.Point(337, 72)
        Me.pnlDesktopCoPref.Name = "pnlDesktopCoPref"
        Me.pnlDesktopCoPref.Size = New System.Drawing.Size(436, 380)
        Me.pnlDesktopCoPref.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(16, 6)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(96, 16)
        Me.Label22.TabIndex = 5
        Me.Label22.Text = "Desktop View"
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.lblTimeTracking)
        Me.GroupBox14.Controls.Add(Me.btnTimeTracking)
        Me.GroupBox14.Controls.Add(Me.lblPayroll)
        Me.GroupBox14.Controls.Add(Me.btnPayroll)
        Me.GroupBox14.Controls.Add(Me.lblInventory)
        Me.GroupBox14.Controls.Add(Me.btnInventory)
        Me.GroupBox14.Controls.Add(Me.lblSalesOrder)
        Me.GroupBox14.Controls.Add(Me.btnSalesOrder)
        Me.GroupBox14.Controls.Add(Me.lblSalesTax)
        Me.GroupBox14.Controls.Add(Me.btnSalesTax)
        Me.GroupBox14.Controls.Add(Me.lblEstimates)
        Me.GroupBox14.Controls.Add(Me.btnEstimates)
        Me.GroupBox14.Controls.Add(Me.Label21)
        Me.GroupBox14.Location = New System.Drawing.Point(7, 203)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(423, 162)
        Me.GroupBox14.TabIndex = 4
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Related Preferences"
        '
        'lblTimeTracking
        '
        Me.lblTimeTracking.AutoSize = True
        Me.lblTimeTracking.Location = New System.Drawing.Point(307, 126)
        Me.lblTimeTracking.Name = "lblTimeTracking"
        Me.lblTimeTracking.Size = New System.Drawing.Size(27, 13)
        Me.lblTimeTracking.TabIndex = 12
        Me.lblTimeTracking.Text = "(on)"
        '
        'btnTimeTracking
        '
        Me.btnTimeTracking.Location = New System.Drawing.Point(213, 121)
        Me.btnTimeTracking.Name = "btnTimeTracking"
        Me.btnTimeTracking.Size = New System.Drawing.Size(93, 23)
        Me.btnTimeTracking.TabIndex = 11
        Me.btnTimeTracking.Text = "Time Tracking"
        Me.btnTimeTracking.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTimeTracking.UseVisualStyleBackColor = True
        '
        'lblPayroll
        '
        Me.lblPayroll.AutoSize = True
        Me.lblPayroll.Location = New System.Drawing.Point(307, 103)
        Me.lblPayroll.Name = "lblPayroll"
        Me.lblPayroll.Size = New System.Drawing.Size(27, 13)
        Me.lblPayroll.TabIndex = 10
        Me.lblPayroll.Text = "(on)"
        '
        'btnPayroll
        '
        Me.btnPayroll.Location = New System.Drawing.Point(213, 98)
        Me.btnPayroll.Name = "btnPayroll"
        Me.btnPayroll.Size = New System.Drawing.Size(93, 23)
        Me.btnPayroll.TabIndex = 9
        Me.btnPayroll.Text = "Payroll"
        Me.btnPayroll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPayroll.UseVisualStyleBackColor = True
        '
        'lblInventory
        '
        Me.lblInventory.AutoSize = True
        Me.lblInventory.Location = New System.Drawing.Point(307, 80)
        Me.lblInventory.Name = "lblInventory"
        Me.lblInventory.Size = New System.Drawing.Size(27, 13)
        Me.lblInventory.TabIndex = 8
        Me.lblInventory.Text = "(on)"
        '
        'btnInventory
        '
        Me.btnInventory.Location = New System.Drawing.Point(213, 75)
        Me.btnInventory.Name = "btnInventory"
        Me.btnInventory.Size = New System.Drawing.Size(93, 23)
        Me.btnInventory.TabIndex = 7
        Me.btnInventory.Text = "Inventory"
        Me.btnInventory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnInventory.UseVisualStyleBackColor = True
        '
        'lblSalesOrder
        '
        Me.lblSalesOrder.AutoSize = True
        Me.lblSalesOrder.Location = New System.Drawing.Point(152, 126)
        Me.lblSalesOrder.Name = "lblSalesOrder"
        Me.lblSalesOrder.Size = New System.Drawing.Size(27, 13)
        Me.lblSalesOrder.TabIndex = 6
        Me.lblSalesOrder.Text = "(on)"
        '
        'btnSalesOrder
        '
        Me.btnSalesOrder.Location = New System.Drawing.Point(56, 121)
        Me.btnSalesOrder.Name = "btnSalesOrder"
        Me.btnSalesOrder.Size = New System.Drawing.Size(93, 23)
        Me.btnSalesOrder.TabIndex = 5
        Me.btnSalesOrder.Text = "Sales Order"
        Me.btnSalesOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalesOrder.UseVisualStyleBackColor = True
        '
        'lblSalesTax
        '
        Me.lblSalesTax.AutoSize = True
        Me.lblSalesTax.Location = New System.Drawing.Point(152, 103)
        Me.lblSalesTax.Name = "lblSalesTax"
        Me.lblSalesTax.Size = New System.Drawing.Size(27, 13)
        Me.lblSalesTax.TabIndex = 4
        Me.lblSalesTax.Text = "(on)"
        '
        'btnSalesTax
        '
        Me.btnSalesTax.Location = New System.Drawing.Point(56, 98)
        Me.btnSalesTax.Name = "btnSalesTax"
        Me.btnSalesTax.Size = New System.Drawing.Size(93, 23)
        Me.btnSalesTax.TabIndex = 3
        Me.btnSalesTax.Text = "Sales Tax"
        Me.btnSalesTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalesTax.UseVisualStyleBackColor = True
        '
        'lblEstimates
        '
        Me.lblEstimates.AutoSize = True
        Me.lblEstimates.Location = New System.Drawing.Point(152, 80)
        Me.lblEstimates.Name = "lblEstimates"
        Me.lblEstimates.Size = New System.Drawing.Size(27, 13)
        Me.lblEstimates.TabIndex = 2
        Me.lblEstimates.Text = "(on)"
        '
        'btnEstimates
        '
        Me.btnEstimates.Location = New System.Drawing.Point(56, 75)
        Me.btnEstimates.Name = "btnEstimates"
        Me.btnEstimates.Size = New System.Drawing.Size(93, 23)
        Me.btnEstimates.TabIndex = 1
        Me.btnEstimates.Text = "Estimates"
        Me.btnEstimates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEstimates.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(53, 25)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(318, 47)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "To show or hide feature icons on the Home page, you need to turn the features on " & _
            "or off. Click the feature name below to view its prefernces."
        '
        'linkRemove
        '
        Me.linkRemove.AutoSize = True
        Me.linkRemove.Location = New System.Drawing.Point(98, 169)
        Me.linkRemove.Name = "linkRemove"
        Me.linkRemove.Size = New System.Drawing.Size(259, 13)
        Me.linkRemove.TabIndex = 3
        Me.linkRemove.TabStop = True
        Me.linkRemove.Text = "*How do I remove this feature from the home page?"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.chkEnterBills)
        Me.GroupBox13.Location = New System.Drawing.Point(220, 56)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(210, 100)
        Me.GroupBox13.TabIndex = 2
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Vendors"
        '
        'chkEnterBills
        '
        Me.chkEnterBills.AutoSize = True
        Me.chkEnterBills.Location = New System.Drawing.Point(18, 26)
        Me.chkEnterBills.Name = "chkEnterBills"
        Me.chkEnterBills.Size = New System.Drawing.Size(140, 17)
        Me.chkEnterBills.TabIndex = 1
        Me.chkEnterBills.Text = "Enter Bills and Pay Bills*"
        Me.chkEnterBills.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.chkStatements)
        Me.GroupBox12.Controls.Add(Me.chkSalesReceipt)
        Me.GroupBox12.Controls.Add(Me.chkInvoice)
        Me.GroupBox12.Location = New System.Drawing.Point(6, 56)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(210, 100)
        Me.GroupBox12.TabIndex = 1
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Customers"
        '
        'chkStatements
        '
        Me.chkStatements.AutoSize = True
        Me.chkStatements.Location = New System.Drawing.Point(13, 72)
        Me.chkStatements.Name = "chkStatements"
        Me.chkStatements.Size = New System.Drawing.Size(198, 17)
        Me.chkStatements.TabIndex = 2
        Me.chkStatements.Text = "Statements and Statement Charges"
        Me.chkStatements.UseVisualStyleBackColor = True
        '
        'chkSalesReceipt
        '
        Me.chkSalesReceipt.AutoSize = True
        Me.chkSalesReceipt.Location = New System.Drawing.Point(13, 49)
        Me.chkSalesReceipt.Name = "chkSalesReceipt"
        Me.chkSalesReceipt.Size = New System.Drawing.Size(95, 17)
        Me.chkSalesReceipt.TabIndex = 1
        Me.chkSalesReceipt.Text = "Sales Receipts"
        Me.chkSalesReceipt.UseVisualStyleBackColor = True
        '
        'chkInvoice
        '
        Me.chkInvoice.AutoSize = True
        Me.chkInvoice.Location = New System.Drawing.Point(13, 26)
        Me.chkInvoice.Name = "chkInvoice"
        Me.chkInvoice.Size = New System.Drawing.Size(72, 17)
        Me.chkInvoice.TabIndex = 0
        Me.chkInvoice.Text = "Invoices*"
        Me.chkInvoice.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(16, 34)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(283, 13)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Select the features you want to show on the Home page."
        '
        'pnlCheckingCoPref
        '
        Me.pnlCheckingCoPref.Controls.Add(Me.lblChecking)
        Me.pnlCheckingCoPref.Controls.Add(Me.GroupBox7)
        Me.pnlCheckingCoPref.Controls.Add(Me.chkAutoFill)
        Me.pnlCheckingCoPref.Controls.Add(Me.chkWarn)
        Me.pnlCheckingCoPref.Controls.Add(Me.chkStartPayee)
        Me.pnlCheckingCoPref.Controls.Add(Me.chkChangeDate)
        Me.pnlCheckingCoPref.Controls.Add(Me.chkPrintName)
        Me.pnlCheckingCoPref.Location = New System.Drawing.Point(355, 49)
        Me.pnlCheckingCoPref.Name = "pnlCheckingCoPref"
        Me.pnlCheckingCoPref.Size = New System.Drawing.Size(436, 380)
        Me.pnlCheckingCoPref.TabIndex = 2
        '
        'lblChecking
        '
        Me.lblChecking.AutoSize = True
        Me.lblChecking.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChecking.Location = New System.Drawing.Point(12, 10)
        Me.lblChecking.Name = "lblChecking"
        Me.lblChecking.Size = New System.Drawing.Size(65, 16)
        Me.lblChecking.TabIndex = 6
        Me.lblChecking.Text = "Checking"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.cboAcc2)
        Me.GroupBox7.Controls.Add(Me.cboAcc1)
        Me.GroupBox7.Controls.Add(Me.Label19)
        Me.GroupBox7.Controls.Add(Me.Label18)
        Me.GroupBox7.Controls.Add(Me.Label17)
        Me.GroupBox7.Controls.Add(Me.Label16)
        Me.GroupBox7.Controls.Add(Me.chkPayroll)
        Me.GroupBox7.Controls.Add(Me.chkOpenPayChk)
        Me.GroupBox7.Location = New System.Drawing.Point(7, 179)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(421, 91)
        Me.GroupBox7.TabIndex = 5
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Select Defaults Accounts to Use"
        '
        'cboAcc2
        '
        Me.cboAcc2.FormattingEnabled = True
        Me.cboAcc2.Location = New System.Drawing.Point(228, 56)
        Me.cboAcc2.Name = "cboAcc2"
        Me.cboAcc2.Size = New System.Drawing.Size(140, 21)
        Me.cboAcc2.TabIndex = 12
        '
        'cboAcc1
        '
        Me.cboAcc1.FormattingEnabled = True
        Me.cboAcc1.Location = New System.Drawing.Point(228, 23)
        Me.cboAcc1.Name = "cboAcc1"
        Me.cboAcc1.Size = New System.Drawing.Size(140, 21)
        Me.cboAcc1.TabIndex = 11
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(369, 59)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(45, 13)
        Me.Label19.TabIndex = 10
        Me.Label19.Text = "account"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(370, 26)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(45, 13)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "account"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(176, 59)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(52, 13)
        Me.Label17.TabIndex = 8
        Me.Label17.Text = "form with"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(175, 26)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(52, 13)
        Me.Label16.TabIndex = 7
        Me.Label16.Text = "form with"
        '
        'chkPayroll
        '
        Me.chkPayroll.AutoSize = True
        Me.chkPayroll.Location = New System.Drawing.Point(6, 58)
        Me.chkPayroll.Name = "chkPayroll"
        Me.chkPayroll.Size = New System.Drawing.Size(172, 17)
        Me.chkPayroll.TabIndex = 6
        Me.chkPayroll.Text = "Open the Pay Payroll Liabilities"
        Me.chkPayroll.UseVisualStyleBackColor = True
        '
        'chkOpenPayChk
        '
        Me.chkOpenPayChk.AutoSize = True
        Me.chkOpenPayChk.Location = New System.Drawing.Point(6, 25)
        Me.chkOpenPayChk.Name = "chkOpenPayChk"
        Me.chkOpenPayChk.Size = New System.Drawing.Size(171, 17)
        Me.chkOpenPayChk.TabIndex = 5
        Me.chkOpenPayChk.Text = "Open the Created Pay Checks"
        Me.chkOpenPayChk.UseVisualStyleBackColor = True
        '
        'chkAutoFill
        '
        Me.chkAutoFill.AutoSize = True
        Me.chkAutoFill.Location = New System.Drawing.Point(14, 148)
        Me.chkAutoFill.Name = "chkAutoFill"
        Me.chkAutoFill.Size = New System.Drawing.Size(244, 17)
        Me.chkAutoFill.TabIndex = 4
        Me.chkAutoFill.Text = "&Autofill payee account number in check memo"
        Me.chkAutoFill.UseVisualStyleBackColor = True
        '
        'chkWarn
        '
        Me.chkWarn.AutoSize = True
        Me.chkWarn.Location = New System.Drawing.Point(14, 123)
        Me.chkWarn.Name = "chkWarn"
        Me.chkWarn.Size = New System.Drawing.Size(203, 17)
        Me.chkWarn.TabIndex = 3
        Me.chkWarn.Text = "&Warn about duplicate check numbers"
        Me.chkWarn.UseVisualStyleBackColor = True
        '
        'chkStartPayee
        '
        Me.chkStartPayee.AutoSize = True
        Me.chkStartPayee.Location = New System.Drawing.Point(14, 98)
        Me.chkStartPayee.Name = "chkStartPayee"
        Me.chkStartPayee.Size = New System.Drawing.Size(174, 17)
        Me.chkStartPayee.TabIndex = 2
        Me.chkStartPayee.Text = "&Start with payee field on check"
        Me.chkStartPayee.UseVisualStyleBackColor = True
        '
        'chkChangeDate
        '
        Me.chkChangeDate.AutoSize = True
        Me.chkChangeDate.Location = New System.Drawing.Point(14, 73)
        Me.chkChangeDate.Name = "chkChangeDate"
        Me.chkChangeDate.Size = New System.Drawing.Size(224, 17)
        Me.chkChangeDate.TabIndex = 1
        Me.chkChangeDate.Text = "Change check date when check is &printed"
        Me.chkChangeDate.UseVisualStyleBackColor = True
        '
        'chkPrintName
        '
        Me.chkPrintName.AutoSize = True
        Me.chkPrintName.Location = New System.Drawing.Point(14, 48)
        Me.chkPrintName.Name = "chkPrintName"
        Me.chkPrintName.Size = New System.Drawing.Size(175, 17)
        Me.chkPrintName.TabIndex = 0
        Me.chkPrintName.Text = "Print account name on &voucher"
        Me.chkPrintName.UseVisualStyleBackColor = True
        '
        'pnlBillCoPref
        '
        Me.pnlBillCoPref.Controls.Add(Me.Label24)
        Me.pnlBillCoPref.Controls.Add(Me.GroupBox6)
        Me.pnlBillCoPref.Controls.Add(Me.GroupBox5)
        Me.pnlBillCoPref.Controls.Add(Me.GroupBox4)
        Me.pnlBillCoPref.Location = New System.Drawing.Point(377, 26)
        Me.pnlBillCoPref.Name = "pnlBillCoPref"
        Me.pnlBillCoPref.Size = New System.Drawing.Size(436, 380)
        Me.pnlBillCoPref.TabIndex = 1
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(10, 10)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(32, 16)
        Me.Label24.TabIndex = 6
        Me.Label24.Text = "Bills"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.CheckBox3)
        Me.GroupBox6.Location = New System.Drawing.Point(4, 226)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(425, 73)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Zipingo"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(9, 34)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(149, 17)
        Me.CheckBox3.TabIndex = 3
        Me.CheckBox3.Text = "Use &Zipingo rating system"
        Me.CheckBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cboAcct)
        Me.GroupBox5.Controls.Add(Me.chkAutomaticDiscount)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Location = New System.Drawing.Point(4, 138)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(425, 73)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Paying Bills"
        '
        'cboAcct
        '
        Me.cboAcct.FormattingEnabled = True
        Me.cboAcct.Location = New System.Drawing.Point(140, 41)
        Me.cboAcct.Name = "cboAcct"
        Me.cboAcct.Size = New System.Drawing.Size(121, 21)
        Me.cboAcct.TabIndex = 4
        '
        'chkAutomaticDiscount
        '
        Me.chkAutomaticDiscount.AutoSize = True
        Me.chkAutomaticDiscount.Location = New System.Drawing.Point(9, 20)
        Me.chkAutomaticDiscount.Name = "chkAutomaticDiscount"
        Me.chkAutomaticDiscount.Size = New System.Drawing.Size(214, 17)
        Me.chkAutomaticDiscount.TabIndex = 3
        Me.chkAutomaticDiscount.Text = "&Automatically use discounts and credits"
        Me.chkAutomaticDiscount.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Default Di&scount Account"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkWarnDuplicate)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.txtDueDays)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Location = New System.Drawing.Point(4, 50)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(425, 73)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Entering Bills"
        '
        'chkWarnDuplicate
        '
        Me.chkWarnDuplicate.AutoSize = True
        Me.chkWarnDuplicate.Location = New System.Drawing.Point(9, 47)
        Me.chkWarnDuplicate.Name = "chkWarnDuplicate"
        Me.chkWarnDuplicate.Size = New System.Drawing.Size(273, 17)
        Me.chkWarnDuplicate.TabIndex = 3
        Me.chkWarnDuplicate.Text = "&Warn about duplicate bill number from same vendor"
        Me.chkWarnDuplicate.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(117, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "days after receipt"
        '
        'txtDueDays
        '
        Me.txtDueDays.Location = New System.Drawing.Point(76, 20)
        Me.txtDueDays.Name = "txtDueDays"
        Me.txtDueDays.Size = New System.Drawing.Size(35, 21)
        Me.txtDueDays.TabIndex = 1
        Me.txtDueDays.Text = "10"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Bills are due"
        '
        'pnlAccountingCoPref
        '
        Me.pnlAccountingCoPref.Controls.Add(Me.Label25)
        Me.pnlAccountingCoPref.Controls.Add(Me.GroupBox3)
        Me.pnlAccountingCoPref.Controls.Add(Me.GroupBox2)
        Me.pnlAccountingCoPref.Controls.Add(Me.CheckBox2)
        Me.pnlAccountingCoPref.Controls.Add(Me.CheckBox1)
        Me.pnlAccountingCoPref.Controls.Add(Me.chkPrompt)
        Me.pnlAccountingCoPref.Controls.Add(Me.chkUseCls)
        Me.pnlAccountingCoPref.Controls.Add(Me.GroupBox1)
        Me.pnlAccountingCoPref.Location = New System.Drawing.Point(397, 6)
        Me.pnlAccountingCoPref.Name = "pnlAccountingCoPref"
        Me.pnlAccountingCoPref.Size = New System.Drawing.Size(436, 380)
        Me.pnlAccountingCoPref.TabIndex = 0
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(10, 8)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(81, 16)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Accounting"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnSetPwd)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 296)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(416, 72)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Closing Dates"
        '
        'btnSetPwd
        '
        Me.btnSetPwd.Location = New System.Drawing.Point(16, 42)
        Me.btnSetPwd.Name = "btnSetPwd"
        Me.btnSetPwd.Size = New System.Drawing.Size(144, 23)
        Me.btnSetPwd.TabIndex = 2
        Me.btnSetPwd.Text = "Set Date/Password"
        Me.btnSetPwd.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(221, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "(not set)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(190, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Date through which books are closed  "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtDayinFuture)
        Me.GroupBox2.Controls.Add(Me.txtDayinPast)
        Me.GroupBox2.Controls.Add(Me.chkWarnFuture)
        Me.GroupBox2.Controls.Add(Me.chkWarnPast)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 212)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 79)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Date Warnings"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(221, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "day(s) in the future."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(221, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "day(s) in the past."
        '
        'txtDayinFuture
        '
        Me.txtDayinFuture.Location = New System.Drawing.Point(167, 40)
        Me.txtDayinFuture.Name = "txtDayinFuture"
        Me.txtDayinFuture.Size = New System.Drawing.Size(33, 21)
        Me.txtDayinFuture.TabIndex = 3
        Me.txtDayinFuture.Text = "30"
        '
        'txtDayinPast
        '
        Me.txtDayinPast.Location = New System.Drawing.Point(167, 17)
        Me.txtDayinPast.Name = "txtDayinPast"
        Me.txtDayinPast.Size = New System.Drawing.Size(33, 21)
        Me.txtDayinPast.TabIndex = 2
        Me.txtDayinPast.Text = "90"
        '
        'chkWarnFuture
        '
        Me.chkWarnFuture.AutoSize = True
        Me.chkWarnFuture.Location = New System.Drawing.Point(16, 42)
        Me.chkWarnFuture.Name = "chkWarnFuture"
        Me.chkWarnFuture.Size = New System.Drawing.Size(142, 17)
        Me.chkWarnFuture.TabIndex = 1
        Me.chkWarnFuture.Text = "Warn if transactions are"
        Me.chkWarnFuture.UseVisualStyleBackColor = True
        '
        'chkWarnPast
        '
        Me.chkWarnPast.AutoSize = True
        Me.chkWarnPast.Location = New System.Drawing.Point(16, 19)
        Me.chkWarnPast.Name = "chkWarnPast"
        Me.chkWarnPast.Size = New System.Drawing.Size(142, 17)
        Me.chkWarnPast.TabIndex = 0
        Me.chkWarnPast.Text = "Warn if transactions are"
        Me.chkWarnPast.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(22, 184)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(288, 17)
        Me.CheckBox2.TabIndex = 4
        Me.CheckBox2.Text = "&Warn when posting a transaction to Retained Earnings"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(22, 161)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(266, 17)
        Me.CheckBox1.TabIndex = 3
        Me.CheckBox1.Text = "A&utomatically assign general journal entry number"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'chkPrompt
        '
        Me.chkPrompt.AutoSize = True
        Me.chkPrompt.Location = New System.Drawing.Point(39, 138)
        Me.chkPrompt.Name = "chkPrompt"
        Me.chkPrompt.Size = New System.Drawing.Size(143, 17)
        Me.chkPrompt.TabIndex = 2
        Me.chkPrompt.Text = "P&rompt to assign classes"
        Me.chkPrompt.UseVisualStyleBackColor = True
        '
        'chkUseCls
        '
        Me.chkUseCls.AutoSize = True
        Me.chkUseCls.Location = New System.Drawing.Point(22, 115)
        Me.chkUseCls.Name = "chkUseCls"
        Me.chkUseCls.Size = New System.Drawing.Size(111, 17)
        Me.chkUseCls.TabIndex = 1
        Me.chkUseCls.Text = "Use cla&ss tracking"
        Me.chkUseCls.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkReqAcc)
        Me.GroupBox1.Controls.Add(Me.chkShowLowest)
        Me.GroupBox1.Controls.Add(Me.chkUseAccNo)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 68)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Accounts"
        '
        'chkReqAcc
        '
        Me.chkReqAcc.AutoSize = True
        Me.chkReqAcc.Location = New System.Drawing.Point(224, 19)
        Me.chkReqAcc.Name = "chkReqAcc"
        Me.chkReqAcc.Size = New System.Drawing.Size(109, 17)
        Me.chkReqAcc.TabIndex = 2
        Me.chkReqAcc.Text = "Require &accounts"
        Me.chkReqAcc.UseVisualStyleBackColor = True
        '
        'chkShowLowest
        '
        Me.chkShowLowest.AutoSize = True
        Me.chkShowLowest.Location = New System.Drawing.Point(16, 42)
        Me.chkShowLowest.Name = "chkShowLowest"
        Me.chkShowLowest.Size = New System.Drawing.Size(167, 17)
        Me.chkShowLowest.TabIndex = 1
        Me.chkShowLowest.Text = "Show &lowest subaccount only"
        Me.chkShowLowest.UseVisualStyleBackColor = True
        '
        'chkUseAccNo
        '
        Me.chkUseAccNo.AutoSize = True
        Me.chkUseAccNo.Location = New System.Drawing.Point(16, 19)
        Me.chkUseAccNo.Name = "chkUseAccNo"
        Me.chkUseAccNo.Size = New System.Drawing.Size(129, 17)
        Me.chkUseAccNo.TabIndex = 0
        Me.chkUseAccNo.Text = "Use account &numbers"
        Me.chkUseAccNo.UseVisualStyleBackColor = True
        '
        'frm_Preferences
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(651, 462)
        Me.Controls.Add(Me.tabMyPreference)
        Me.Controls.Add(Me.btnDefault)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.grdPreferences)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_Preferences"
        Me.Text = "Preferences"
        CType(Me.grdPreferences, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMyPreference.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.pnlSpellingMyPref.ResumeLayout(False)
        Me.pnlSpellingMyPref.PerformLayout()
        Me.GroupBox42.ResumeLayout(False)
        Me.GroupBox42.PerformLayout()
        Me.pnlServiceMyPref.ResumeLayout(False)
        Me.pnlServiceMyPref.PerformLayout()
        Me.GroupBox40.ResumeLayout(False)
        Me.GroupBox40.PerformLayout()
        Me.pnlSalesMyPref.ResumeLayout(False)
        Me.pnlSalesMyPref.PerformLayout()
        Me.GroupBox32.ResumeLayout(False)
        Me.GroupBox32.PerformLayout()
        Me.pnlReportsMyPref.ResumeLayout(False)
        Me.pnlReportsMyPref.PerformLayout()
        Me.GroupBox26.ResumeLayout(False)
        Me.GroupBox26.PerformLayout()
        Me.GroupBox27.ResumeLayout(False)
        Me.GroupBox27.PerformLayout()
        Me.pnlGeneralMyPref.ResumeLayout(False)
        Me.pnlGeneralMyPref.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.pnlFinanceChargeMyPref.ResumeLayout(False)
        Me.pnlFinanceChargeMyPref.PerformLayout()
        Me.pnlDesktopMyPref.ResumeLayout(False)
        Me.pnlDesktopMyPref.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCheckingMyPref.ResumeLayout(False)
        Me.pnlCheckingMyPref.PerformLayout()
        Me.grpCheckingMyPref.ResumeLayout(False)
        Me.grpCheckingMyPref.PerformLayout()
        Me.pnlBillMyPref.ResumeLayout(False)
        Me.pnlBillMyPref.PerformLayout()
        Me.pnlAccountingMyPref.ResumeLayout(False)
        Me.pnlAccountingMyPref.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.pnlTimeCoPref.ResumeLayout(False)
        Me.pnlTimeCoPref.PerformLayout()
        Me.GroupBox43.ResumeLayout(False)
        Me.GroupBox43.PerformLayout()
        Me.pnlTaxCoPref.ResumeLayout(False)
        Me.pnlTaxCoPref.PerformLayout()
        CType(Me.grdTaxCategory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSpellingCoPref.ResumeLayout(False)
        Me.pnlSpellingCoPref.PerformLayout()
        Me.pnlServiceCoPref.ResumeLayout(False)
        Me.pnlServiceCoPref.PerformLayout()
        Me.pnlSendCoPref.ResumeLayout(False)
        Me.pnlSendCoPref.PerformLayout()
        Me.pnlSTaxCoPref.ResumeLayout(False)
        Me.pnlSTaxCoPref.PerformLayout()
        Me.GroupBox39.ResumeLayout(False)
        Me.GroupBox39.PerformLayout()
        Me.GroupBox38.ResumeLayout(False)
        Me.GroupBox38.PerformLayout()
        Me.GroupBox37.ResumeLayout(False)
        Me.GroupBox37.PerformLayout()
        Me.GroupBox41.ResumeLayout(False)
        Me.GroupBox41.PerformLayout()
        Me.pnlSalesCoPref.ResumeLayout(False)
        Me.pnlSalesCoPref.PerformLayout()
        Me.GroupBox36.ResumeLayout(False)
        Me.GroupBox35.ResumeLayout(False)
        Me.GroupBox35.PerformLayout()
        Me.GroupBox34.ResumeLayout(False)
        Me.GroupBox34.PerformLayout()
        Me.GroupBox33.ResumeLayout(False)
        Me.GroupBox33.PerformLayout()
        Me.GroupBox31.ResumeLayout(False)
        Me.GroupBox31.PerformLayout()
        Me.pnlReportsCoPref.ResumeLayout(False)
        Me.pnlReportsCoPref.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox29.ResumeLayout(False)
        Me.GroupBox29.PerformLayout()
        Me.GroupBox25.ResumeLayout(False)
        Me.GroupBox25.PerformLayout()
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox28.PerformLayout()
        Me.pnlRemindersCoPref.ResumeLayout(False)
        Me.pnlRemindersCoPref.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pnlJobsCoPref.ResumeLayout(False)
        Me.pnlJobsCoPref.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.pnlItemsCoPref.ResumeLayout(False)
        Me.pnlItemsCoPref.PerformLayout()
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        Me.pnlIntegratedApplicationsCoPref.ResumeLayout(False)
        Me.pnlIntegratedApplicationsCoPref.PerformLayout()
        Me.GroupBox22.ResumeLayout(False)
        Me.pnlGeneralCoPref.ResumeLayout(False)
        Me.pnlGeneralCoPref.PerformLayout()
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        Me.pnlFinanceChargeCoPref.ResumeLayout(False)
        Me.pnlFinanceChargeCoPref.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.pnlDesktopCoPref.ResumeLayout(False)
        Me.pnlDesktopCoPref.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.pnlCheckingCoPref.ResumeLayout(False)
        Me.pnlCheckingCoPref.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.pnlBillCoPref.ResumeLayout(False)
        Me.pnlBillCoPref.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.pnlAccountingCoPref.ResumeLayout(False)
        Me.pnlAccountingCoPref.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdPreferences As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDefault As System.Windows.Forms.Button
    Friend WithEvents tabMyPreference As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents chkAcctgAutoFill As System.Windows.Forms.CheckBox
    Friend WithEvents pnlAccountingCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkUseAccNo As System.Windows.Forms.CheckBox
    Friend WithEvents chkReqAcc As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLowest As System.Windows.Forms.CheckBox
    Friend WithEvents chkUseCls As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrompt As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkWarnFuture As System.Windows.Forms.CheckBox
    Friend WithEvents chkWarnPast As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSetPwd As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDayinPast As System.Windows.Forms.TextBox
    Friend WithEvents txtDayinFuture As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblBill As System.Windows.Forms.Label
    Friend WithEvents lblBill2 As System.Windows.Forms.Label
    Friend WithEvents pnlBillCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkWarnDuplicate As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDueDays As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cboAcct As System.Windows.Forms.ComboBox
    Friend WithEvents chkAutomaticDiscount As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents pnlBillMyPref As System.Windows.Forms.Panel
    Friend WithEvents grpCheckingMyPref As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents chkMkDeposits As System.Windows.Forms.CheckBox
    Friend WithEvents chkPaySalesTax As System.Windows.Forms.CheckBox
    Friend WithEvents chkPayBills As System.Windows.Forms.CheckBox
    Friend WithEvents chkWriteChecks As System.Windows.Forms.CheckBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboAccforPayBills As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboAccforCheck As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboAccforMkDeposits As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboAccforSalesTx As System.Windows.Forms.ComboBox
    Friend WithEvents pnlCheckingCoPref As System.Windows.Forms.Panel
    Friend WithEvents chkPrintName As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoFill As System.Windows.Forms.CheckBox
    Friend WithEvents chkWarn As System.Windows.Forms.CheckBox
    Friend WithEvents chkStartPayee As System.Windows.Forms.CheckBox
    Friend WithEvents chkChangeDate As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents chkPayroll As System.Windows.Forms.CheckBox
    Friend WithEvents chkOpenPayChk As System.Windows.Forms.CheckBox
    Friend WithEvents cboAcc1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboAcc2 As System.Windows.Forms.ComboBox
    Friend WithEvents pnlDesktopMyPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnMulWindw As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnOneWindw As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnKeepPrevious As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDontSave As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSaveCurrent As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSaveWhenClose As System.Windows.Forms.RadioButton
    Friend WithEvents chkShowHomePage As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSound As System.Windows.Forms.Button
    Friend WithEvents btnDisplay As System.Windows.Forms.Button
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents cboColor As System.Windows.Forms.ComboBox
    Friend WithEvents pnlDesktopCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEnterBills As System.Windows.Forms.CheckBox
    Friend WithEvents chkStatements As System.Windows.Forms.CheckBox
    Friend WithEvents chkSalesReceipt As System.Windows.Forms.CheckBox
    Friend WithEvents chkInvoice As System.Windows.Forms.CheckBox
    Friend WithEvents linkRemove As System.Windows.Forms.LinkLabel
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lblEstimates As System.Windows.Forms.Label
    Friend WithEvents btnEstimates As System.Windows.Forms.Button
    Friend WithEvents lblSalesOrder As System.Windows.Forms.Label
    Friend WithEvents btnSalesOrder As System.Windows.Forms.Button
    Friend WithEvents lblSalesTax As System.Windows.Forms.Label
    Friend WithEvents btnSalesTax As System.Windows.Forms.Button
    Friend WithEvents lblTimeTracking As System.Windows.Forms.Label
    Friend WithEvents btnTimeTracking As System.Windows.Forms.Button
    Friend WithEvents lblPayroll As System.Windows.Forms.Label
    Friend WithEvents btnPayroll As System.Windows.Forms.Button
    Friend WithEvents lblInventory As System.Windows.Forms.Label
    Friend WithEvents btnInventory As System.Windows.Forms.Button
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblChecking As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents pnlAccountingMyPref As System.Windows.Forms.Panel
    Friend WithEvents lblCaptionAcctg As System.Windows.Forms.Label
    Friend WithEvents pnlCheckingMyPref As System.Windows.Forms.Panel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents pnlFinanceChargeMyPref As System.Windows.Forms.Panel
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents pnlFinanceChargeCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtAnnualInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtGracePeriodDay As System.Windows.Forms.TextBox
    Friend WithEvents txtMinFinanceCharge As System.Windows.Forms.TextBox
    Friend WithEvents cboFChargeAcc As System.Windows.Forms.ComboBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents chkAssessFCharge As System.Windows.Forms.CheckBox
    Friend WithEvents chkMarkToBePrint As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDueDate As System.Windows.Forms.RadioButton
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents pnlGeneralMyPref As System.Windows.Forms.Panel
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox13 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents pnlGeneralCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox21 As System.Windows.Forms.GroupBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents rbtnMinutes As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDecimal As System.Windows.Forms.RadioButton
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents chkShwFourDigits As System.Windows.Forms.CheckBox
    Friend WithEvents chkNvrUpdateName As System.Windows.Forms.CheckBox
    Friend WithEvents pnlIntegratedApplicationsCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents GroupBox22 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnProperties As System.Windows.Forms.Button
    Friend WithEvents lstApplication As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkNotifyUser As System.Windows.Forms.CheckBox
    Friend WithEvents chkDontAllow As System.Windows.Forms.CheckBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents pnlItemsCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents GroupBox19 As System.Windows.Forms.GroupBox
    Friend WithEvents chkWarnDuplicatePO As System.Windows.Forms.CheckBox
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents chkQtySO As System.Windows.Forms.CheckBox
    Friend WithEvents chkQtyRsv As System.Windows.Forms.CheckBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents lnkQty As System.Windows.Forms.LinkLabel
    Friend WithEvents chkWarnNoInventory As System.Windows.Forms.CheckBox
    Friend WithEvents rbtnQtyAvailable As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnQtyOnHand As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox20 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEnable As System.Windows.Forms.Button
    Friend WithEvents pnlJobsCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox24 As System.Windows.Forms.GroupBox
    Friend WithEvents sbtnNotCreateEstimates As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCreateEstimate As System.Windows.Forms.RadioButton
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtNotAward As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtClose As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtProgress As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtAward As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtPending As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents GroupBox23 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnNotProgressInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnProgressInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents chkWarnDupEstimates As System.Windows.Forms.CheckBox
    Friend WithEvents chkDontPrintZero As System.Windows.Forms.CheckBox
    Friend WithEvents pnlRemindersCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents rbtnPChkDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnPChkList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnPChkSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysBeforePyChkDte As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents rbtnChkDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnChkList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnChkSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysBeforeChkDte As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents rbtnOverdueInvoiceDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnOverdueInvoiceList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnOverdueInvoiceSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysAfterDueDte As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents rbtnInvoiceDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnInvoiceList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnInvoiceSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysBeforeInvoiceDte As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents rbtnSalesRctDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSalesRctList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSalesRctSummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents rbtnSODontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSOList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSOSummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents rbtnAssemblyDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnAssemblyList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnAssemblySummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents rbtnInventoryDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnInventoryList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnInventorySummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents rbtnBillsDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnBillsList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnBillsSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysAfterDueDteBills As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents rbtnTransDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTransList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTransSummary As System.Windows.Forms.RadioButton
    Friend WithEvents txtDaysAfterDueDteTrans As System.Windows.Forms.TextBox
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents rbtnPODontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnPOList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnPOSummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents rbtnMoneyDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnMoneyList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnMoneySummary As System.Windows.Forms.RadioButton
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents rbtnToDoDontRmd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnToDoList As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnToDoSummary As System.Windows.Forms.RadioButton
    Friend WithEvents pnlReportsMyPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox26 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnPatterns As System.Windows.Forms.RadioButton
    Friend WithEvents rbtn2D As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox27 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnAutoRefresh As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnPromtRefresh As System.Windows.Forms.RadioButton
    Friend WithEvents chkPromptModifyReport As System.Windows.Forms.CheckBox
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents rbtnDontRefresh As System.Windows.Forms.RadioButton
    Friend WithEvents pnlReportsCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox25 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnAgeTrans As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnAgeDue As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox28 As System.Windows.Forms.GroupBox
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents rbtnCash As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnAccrual As System.Windows.Forms.RadioButton
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents GroupBox30 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox29 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnDescription As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnNameDescription As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnName As System.Windows.Forms.RadioButton
    Friend WithEvents btnFormat As System.Windows.Forms.Button
    Friend WithEvents btnClassify As System.Windows.Forms.Button
    Friend WithEvents pnlSalesMyPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox32 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton10 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton11 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton12 As System.Windows.Forms.RadioButton
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents pnlSalesCoPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox31 As System.Windows.Forms.GroupBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents cboShipMethod As System.Windows.Forms.ComboBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents chkWarnDupInvoice As System.Windows.Forms.CheckBox
    Friend WithEvents txtFOB As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox33 As System.Windows.Forms.GroupBox
    Friend WithEvents chkXpenseAsIncome As System.Windows.Forms.CheckBox
    Friend WithEvents txtPercentage As System.Windows.Forms.TextBox
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents cboInvoicePackSlip As System.Windows.Forms.ComboBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents GroupBox34 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPriceLvl As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox35 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSO As System.Windows.Forms.CheckBox
    Friend WithEvents chkWarnDupSO As System.Windows.Forms.CheckBox
    Friend WithEvents chkDontPrintZeroSO As System.Windows.Forms.CheckBox
    Friend WithEvents cboPickList As System.Windows.Forms.ComboBox
    Friend WithEvents cboPackingSlip As System.Windows.Forms.ComboBox
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents GroupBox36 As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoPay As System.Windows.Forms.CheckBox
    Friend WithEvents chkUseUndepositFunds As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoCalcPay As System.Windows.Forms.CheckBox
    Friend WithEvents pnlSTaxCoPref As System.Windows.Forms.Panel
    Friend WithEvents rbtnSTaxNO As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSTaxYes As System.Windows.Forms.RadioButton
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents GroupBox41 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSTaxT As System.Windows.Forms.CheckBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents cboSTaxItem As System.Windows.Forms.ComboBox
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents btnSTaxAdd As System.Windows.Forms.Button
    Friend WithEvents lnkSTaxSample As System.Windows.Forms.LinkLabel
    Friend WithEvents GroupBox37 As System.Windows.Forms.GroupBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents cboSTaxTxItem As System.Windows.Forms.ComboBox
    Friend WithEvents cboSTaxNonTxItem As System.Windows.Forms.ComboBox
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents GroupBox38 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnSTaxCashBasis As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSTaxAccrualBasis As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox39 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnSTaxAnnually As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSTaxQuarterly As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnSTaxMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents pnlSendCoPref As System.Windows.Forms.Panel
    Friend WithEvents cboSendDearTo As System.Windows.Forms.ComboBox
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents cboSendDefault As System.Windows.Forms.ComboBox
    Friend WithEvents txtSendBcc As System.Windows.Forms.TextBox
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents cboSendNameSeq As System.Windows.Forms.ComboBox
    Friend WithEvents btnSendSpelling As System.Windows.Forms.Button
    Friend WithEvents txtSendBody As System.Windows.Forms.TextBox
    Friend WithEvents txtSendSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents pnlServiceMyPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox40 As System.Windows.Forms.GroupBox
    Friend WithEvents chkServiceDontClose As System.Windows.Forms.CheckBox
    Friend WithEvents chkServiceOptionToSave As System.Windows.Forms.CheckBox
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents pnlServiceCoPref As System.Windows.Forms.Panel
    Friend WithEvents rbtnServiceAlways As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnServiceAuto As System.Windows.Forms.RadioButton
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents chkServiceAllow As System.Windows.Forms.CheckBox
    Friend WithEvents pnlSpellingCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents pnlSpellingMyPref As System.Windows.Forms.Panel
    Friend WithEvents GroupBox42 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox14 As System.Windows.Forms.CheckBox
    Friend WithEvents chkSpelAlways As System.Windows.Forms.CheckBox
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox15 As System.Windows.Forms.CheckBox
    Friend WithEvents pnlTimeCoPref As System.Windows.Forms.Panel
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents cboTimeDays As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox43 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnTimeNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTimeYes As System.Windows.Forms.RadioButton
    Friend WithEvents pnlTaxCoPref As System.Windows.Forms.Panel
    Friend WithEvents lnkHowTo As System.Windows.Forms.LinkLabel
    Friend WithEvents rbtnTaxNo As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnTaxYes As System.Windows.Forms.RadioButton
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents grdTaxCategory As System.Windows.Forms.DataGridView
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
