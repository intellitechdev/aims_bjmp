<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateSalesOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_CreateSalesOrder))
        Me.txtSONo = New System.Windows.Forms.TextBox
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.cboShipToAddName = New System.Windows.Forms.ComboBox
        Me.cboTaxCodeName = New System.Windows.Forms.ComboBox
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.cboTaxCodeID = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtPONo = New System.Windows.Forms.TextBox
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Email = New System.Windows.Forms.ToolStripButton
        Me.ts_Find = New System.Windows.Forms.ToolStripButton
        Me.ts_Journal = New System.Windows.Forms.ToolStripButton
        Me.ts_History = New System.Windows.Forms.ToolStripButton
        Me.ts_Spelling = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_Preview = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_printShippingLbl = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_PrintEnvelope = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Next = New System.Windows.Forms.ToolStripButton
        Me.ts_Previous = New System.Windows.Forms.ToolStripButton
        Me.txtCustomerMsg = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnPreview = New System.Windows.Forms.Button
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.txtShippingAdd = New System.Windows.Forms.TextBox
        Me.grdSO = New System.Windows.Forms.DataGridView
        Me.cboShipToAddID = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteTransDate = New System.Windows.Forms.DateTimePicker
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtBillingAdd = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.chkClose = New System.Windows.Forms.CheckBox
        CType(Me.grdSO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtSONo
        '
        Me.txtSONo.Location = New System.Drawing.Point(512, 91)
        Me.txtSONo.Name = "txtSONo"
        Me.txtSONo.Size = New System.Drawing.Size(161, 21)
        Me.txtSONo.TabIndex = 72
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Location = New System.Drawing.Point(671, 409)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(154, 13)
        Me.lblTaxAmt.TabIndex = 103
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnRefresh
        '
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(98, 511)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(87, 23)
        Me.btnRefresh.TabIndex = 102
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(744, 488)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 23)
        Me.btnClose.TabIndex = 101
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(653, 488)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 23)
        Me.btnSave.TabIndex = 100
        Me.btnSave.Text = "S&ave "
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboTaxName
        '
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(441, 406)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(132, 21)
        Me.cboTaxName.TabIndex = 99
        '
        'cboShipToAddName
        '
        Me.cboShipToAddName.FormattingEnabled = True
        Me.cboShipToAddName.Location = New System.Drawing.Point(570, 118)
        Me.cboShipToAddName.Name = "cboShipToAddName"
        Me.cboShipToAddName.Size = New System.Drawing.Size(261, 21)
        Me.cboShipToAddName.TabIndex = 98
        '
        'cboTaxCodeName
        '
        Me.cboTaxCodeName.FormattingEnabled = True
        Me.cboTaxCodeName.Location = New System.Drawing.Point(133, 84)
        Me.cboTaxCodeName.Name = "cboTaxCodeName"
        Me.cboTaxCodeName.Size = New System.Drawing.Size(83, 21)
        Me.cboTaxCodeName.TabIndex = 97
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(11, 56)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(305, 21)
        Me.cboCustomerName.TabIndex = 96
        '
        'lblTotal
        '
        Me.lblTotal.Location = New System.Drawing.Point(671, 430)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(154, 13)
        Me.lblTotal.TabIndex = 95
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(582, 428)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 18)
        Me.Label22.TabIndex = 94
        Me.Label22.Text = "Total"
        '
        'lblPercentage
        '
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(582, 409)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(47, 13)
        Me.lblPercentage.TabIndex = 93
        Me.lblPercentage.Text = "(0.0%)"
        '
        'cboTaxID
        '
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(441, 406)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(132, 21)
        Me.cboTaxID.TabIndex = 92
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(408, 408)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 18)
        Me.Label25.TabIndex = 91
        Me.Label25.Text = "Tax"
        '
        'cboTaxCodeID
        '
        Me.cboTaxCodeID.FormattingEnabled = True
        Me.cboTaxCodeID.Location = New System.Drawing.Point(133, 84)
        Me.cboTaxCodeID.Name = "cboTaxCodeID"
        Me.cboTaxCodeID.Size = New System.Drawing.Size(83, 21)
        Me.cboTaxCodeID.TabIndex = 88
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 87)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(122, 13)
        Me.Label21.TabIndex = 87
        Me.Label21.Text = "Customer Tax Code"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(83, 449)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(319, 21)
        Me.txtMemo.TabIndex = 86
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(6, 449)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 18)
        Me.Label11.TabIndex = 85
        Me.Label11.Text = "Memo"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.DarkBlue
        Me.Label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label12.Location = New System.Drawing.Point(679, 73)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(152, 19)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "P.O. No."
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPONo
        '
        Me.txtPONo.Location = New System.Drawing.Point(679, 91)
        Me.txtPONo.Multiline = True
        Me.txtPONo.Name = "txtPONo"
        Me.txtPONo.Size = New System.Drawing.Size(152, 21)
        Me.txtPONo.TabIndex = 9
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator2.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'ts_Email
        '
        Me.ts_Email.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ts_Email.Image = CType(resources.GetObject("ts_Email.Image"), System.Drawing.Image)
        Me.ts_Email.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Email.Name = "ts_Email"
        Me.ts_Email.Size = New System.Drawing.Size(23, 22)
        Me.ts_Email.Text = "ToolStripButton3"
        Me.ts_Email.ToolTipText = "Email"
        Me.ts_Email.Visible = False
        '
        'ts_Find
        '
        Me.ts_Find.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ts_Find.Image = CType(resources.GetObject("ts_Find.Image"), System.Drawing.Image)
        Me.ts_Find.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Find.Name = "ts_Find"
        Me.ts_Find.Size = New System.Drawing.Size(23, 22)
        Me.ts_Find.Text = "ToolStripButton4"
        Me.ts_Find.ToolTipText = "Find"
        Me.ts_Find.Visible = False
        '
        'ts_Journal
        '
        Me.ts_Journal.Image = CType(resources.GetObject("ts_Journal.Image"), System.Drawing.Image)
        Me.ts_Journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Journal.Name = "ts_Journal"
        Me.ts_Journal.Size = New System.Drawing.Size(68, 22)
        Me.ts_Journal.Text = "Journal"
        Me.ts_Journal.Visible = False
        '
        'ts_History
        '
        Me.ts_History.Image = CType(resources.GetObject("ts_History.Image"), System.Drawing.Image)
        Me.ts_History.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_History.Name = "ts_History"
        Me.ts_History.Size = New System.Drawing.Size(67, 22)
        Me.ts_History.Text = "History"
        Me.ts_History.Visible = False
        '
        'ts_Spelling
        '
        Me.ts_Spelling.Image = CType(resources.GetObject("ts_Spelling.Image"), System.Drawing.Image)
        Me.ts_Spelling.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Spelling.Name = "ts_Spelling"
        Me.ts_Spelling.Size = New System.Drawing.Size(72, 22)
        Me.ts_Spelling.Text = "Spelling"
        Me.ts_Spelling.Visible = False
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Preview, Me.ts_Print, Me.ts_printShippingLbl, Me.ts_PrintEnvelope})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(32, 22)
        Me.ToolStripSplitButton1.Text = "lope"
        '
        'ts_Preview
        '
        Me.ts_Preview.Name = "ts_Preview"
        Me.ts_Preview.Size = New System.Drawing.Size(198, 22)
        Me.ts_Preview.Text = "Preview"
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.Size = New System.Drawing.Size(198, 22)
        Me.ts_Print.Text = "Print"
        '
        'ts_printShippingLbl
        '
        Me.ts_printShippingLbl.Name = "ts_printShippingLbl"
        Me.ts_printShippingLbl.Size = New System.Drawing.Size(198, 22)
        Me.ts_printShippingLbl.Text = "Print Shipping Label"
        '
        'ts_PrintEnvelope
        '
        Me.ts_PrintEnvelope.Name = "ts_PrintEnvelope"
        Me.ts_PrintEnvelope.Size = New System.Drawing.Size(198, 22)
        Me.ts_PrintEnvelope.Text = "Print Envelope"
        '
        'ts_Next
        '
        Me.ts_Next.Image = CType(resources.GetObject("ts_Next.Image"), System.Drawing.Image)
        Me.ts_Next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Next.Name = "ts_Next"
        Me.ts_Next.Size = New System.Drawing.Size(53, 22)
        Me.ts_Next.Text = "Next"
        '
        'ts_Previous
        '
        Me.ts_Previous.Image = CType(resources.GetObject("ts_Previous.Image"), System.Drawing.Image)
        Me.ts_Previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Previous.Name = "ts_Previous"
        Me.ts_Previous.Size = New System.Drawing.Size(76, 22)
        Me.ts_Previous.Text = "Previous"
        '
        'txtCustomerMsg
        '
        Me.txtCustomerMsg.Location = New System.Drawing.Point(83, 405)
        Me.txtCustomerMsg.Multiline = True
        Me.txtCustomerMsg.Name = "txtCustomerMsg"
        Me.txtCustomerMsg.Size = New System.Drawing.Size(319, 41)
        Me.txtCustomerMsg.TabIndex = 78
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(6, 409)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 34)
        Me.Label8.TabIndex = 77
        Me.Label8.Text = "Customer &Message"
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Location = New System.Drawing.Point(8, 511)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 23)
        Me.btnPreview.TabIndex = 82
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        Me.btnPreview.Visible = False
        '
        'chkEmail
        '
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(119, 488)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(107, 17)
        Me.chkEmail.TabIndex = 81
        Me.chkEmail.Text = "To be emailed"
        Me.chkEmail.UseVisualStyleBackColor = True
        Me.chkEmail.Visible = False
        '
        'chkPrint
        '
        Me.chkPrint.AutoSize = True
        Me.chkPrint.Location = New System.Drawing.Point(7, 488)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(102, 17)
        Me.chkPrint.TabIndex = 80
        Me.chkPrint.Text = "To be printed"
        Me.chkPrint.UseVisualStyleBackColor = True
        Me.chkPrint.Visible = False
        '
        'txtShippingAdd
        '
        Me.txtShippingAdd.Location = New System.Drawing.Point(512, 140)
        Me.txtShippingAdd.Multiline = True
        Me.txtShippingAdd.Name = "txtShippingAdd"
        Me.txtShippingAdd.Size = New System.Drawing.Size(320, 78)
        Me.txtShippingAdd.TabIndex = 74
        '
        'grdSO
        '
        Me.grdSO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSO.Location = New System.Drawing.Point(9, 224)
        Me.grdSO.Name = "grdSO"
        Me.grdSO.Size = New System.Drawing.Size(824, 173)
        Me.grdSO.TabIndex = 76
        '
        'cboShipToAddID
        '
        Me.cboShipToAddID.FormattingEnabled = True
        Me.cboShipToAddID.Location = New System.Drawing.Point(570, 118)
        Me.cboShipToAddID.Name = "cboShipToAddID"
        Me.cboShipToAddID.Size = New System.Drawing.Size(261, 21)
        Me.cboShipToAddID.TabIndex = 75
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.DarkBlue
        Me.Label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label7.Location = New System.Drawing.Point(512, 116)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(321, 25)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Ship To"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(512, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(161, 21)
        Me.Label6.TabIndex = 70
        Me.Label6.Text = "S.O. No."
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(679, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(153, 15)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Date"
        '
        'dteTransDate
        '
        Me.dteTransDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTransDate.Location = New System.Drawing.Point(679, 48)
        Me.dteTransDate.Name = "dteTransDate"
        Me.dteTransDate.Size = New System.Drawing.Size(153, 21)
        Me.dteTransDate.TabIndex = 71
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.DarkBlue
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(10, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(306, 25)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Bill To"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBillingAdd
        '
        Me.txtBillingAdd.Location = New System.Drawing.Point(10, 140)
        Me.txtBillingAdd.Multiline = True
        Me.txtBillingAdd.Name = "txtBillingAdd"
        Me.txtBillingAdd.Size = New System.Drawing.Size(306, 78)
        Me.txtBillingAdd.TabIndex = 68
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(509, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 23)
        Me.Label3.TabIndex = 66
        Me.Label3.Text = "Sales Order"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(11, 56)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(305, 21)
        Me.cboCustomerID.TabIndex = 65
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 64
        Me.Label1.Text = "Customer:Job"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Previous, Me.ts_Next, Me.ToolStripSplitButton1, Me.ts_Email, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.ts_Find, Me.ts_Spelling, Me.ts_History, Me.ts_Journal})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(844, 25)
        Me.ToolStrip1.TabIndex = 63
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'chkClose
        '
        Me.chkClose.AutoSize = True
        Me.chkClose.Location = New System.Drawing.Point(233, 488)
        Me.chkClose.Name = "chkClose"
        Me.chkClose.Size = New System.Drawing.Size(65, 17)
        Me.chkClose.TabIndex = 104
        Me.chkClose.Text = "Closed"
        Me.chkClose.UseVisualStyleBackColor = True
        Me.chkClose.Visible = False
        '
        'frm_cust_CreateSalesOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(844, 536)
        Me.Controls.Add(Me.lblPercentage)
        Me.Controls.Add(Me.chkClose)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtPONo)
        Me.Controls.Add(Me.txtSONo)
        Me.Controls.Add(Me.lblTaxAmt)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboTaxName)
        Me.Controls.Add(Me.cboShipToAddName)
        Me.Controls.Add(Me.cboTaxCodeName)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.cboTaxID)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.cboTaxCodeID)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtCustomerMsg)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.chkEmail)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.txtShippingAdd)
        Me.Controls.Add(Me.grdSO)
        Me.Controls.Add(Me.cboShipToAddID)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteTransDate)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBillingAdd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_cust_CreateSalesOrder"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Sales Order"
        CType(Me.grdSO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtSONo As System.Windows.Forms.TextBox
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents cboShipToAddName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxCodeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cboTaxCodeID As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPONo As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Email As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Find As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_History As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Spelling As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_Preview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_printShippingLbl As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PrintEnvelope As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtCustomerMsg As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents txtShippingAdd As System.Windows.Forms.TextBox
    Friend WithEvents grdSO As System.Windows.Forms.DataGridView
    Friend WithEvents cboShipToAddID As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteTransDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBillingAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents chkClose As System.Windows.Forms.CheckBox
End Class
