Public Class frm_acc_workingtrialbalance

    Private Sub btn_trialbal_makeadj_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_trialbal_makeadj.Click
        With Me.grdWrkngBal
            If Not .CurrentRow.Cells(0).Value.ToString Is Nothing Then
                gAcntCode = .CurrentRow.Cells(0).Value.ToString
            End If
        End With
        frm_acc_makeGeneralJournalEntry.ShowDialog()
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Call load_trialbalance_listofentries()
        Call computetotals()
    End Sub

    Private Sub load_trialbalance_listofentries()
        With grdWrkngBal
            .Columns.Clear()
            ''TERRY SA UPDATE USE ONLY
            '.DataSource = m_GetTransactionsforWorkingBalance(Microsoft.VisualBasic.FormatDateTime(dteEnd.Value, DateFormat.ShortDate)).Tables(0)
            ''TRC UPDATE USE ONLY
            .DataSource = m_GetTransactionsforWorkingBalance(Microsoft.VisualBasic.FormatDateTime(dteFrom.Value, DateFormat.ShortDate), _
                                                                Microsoft.VisualBasic.FormatDateTime(dteEnd.Value, DateFormat.ShortDate)).Tables(0)
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns(2).DefaultCellStyle.Format = "##,##0.00"
            .Columns(3).DefaultCellStyle.Format = "##,##0.00"
            .Columns(4).DefaultCellStyle.Format = "##,##0.00"
            .Columns(5).DefaultCellStyle.Format = "##,##0.00"
            .Columns(6).DefaultCellStyle.Format = "##,##0.00"
            .Columns(7).DefaultCellStyle.Format = "##,##0.00"
            .Columns(0).Width = 180
            .Columns(1).Width = 375
            .Columns(2).Width = 150
            .Columns(3).Width = 150
            .Columns(4).Width = 150
            .Columns(5).Width = 150
            .Columns(6).Width = 150
            .Columns(7).Width = 150

            .Columns(0).HeaderText = "Account Code"
            .Columns(1).HeaderText = "Accounts"
            .Columns(2).HeaderText = "Begin Debit"
            .Columns(3).HeaderText = "Begin Credit"
            .Columns(4).HeaderText = "Current Debit"
            .Columns(5).HeaderText = "Current Credit"
            .Columns(6).HeaderText = "Ending Debit"
            .Columns(7).HeaderText = "Ending Credit"

            .Columns(8).Visible = False
            .Columns(9).Visible = False
        End With
    End Sub

    Private Sub computetotals()
        With grdWrkngBal
            Dim xRow As Integer
            Dim openbaldebit As Decimal = 0.0
            Dim openbalcredit As Decimal = 0.0
            Dim debit As Decimal = 0.0
            Dim credit As Decimal = 0.0
            Dim endbaldebit As Decimal = 0.0
            Dim endbalcredit As Decimal = 0.0
            For xRow = 0 To .Rows.Count - 1
                If .Rows(xRow).Cells(0).Value.ToString IsNot Nothing Then
                    If Not .Rows(xRow).Cells(2).Value Is Nothing Then
                        openbaldebit += CDec(.Rows(xRow).Cells(2).Value)
                    End If
                    If Not .Rows(xRow).Cells(3).Value Is Nothing Then
                        openbalcredit += CDec(.Rows(xRow).Cells(3).Value)
                    End If
                    If Not .Rows(xRow).Cells(4).Value Is Nothing Then
                        debit += CDec(.Rows(xRow).Cells(4).Value)
                    End If
                    If Not .Rows(xRow).Cells(5).Value Is Nothing Then
                        credit += CDec(.Rows(xRow).Cells(5).Value)
                    End If
                    If Not .Rows(xRow).Cells(6).Value Is Nothing Then
                        endbaldebit += CDec(.Rows(xRow).Cells(6).Value)
                    End If
                    If Not .Rows(xRow).Cells(7).Value Is Nothing Then
                        endbalcredit += CDec(.Rows(xRow).Cells(7).Value)
                    End If
                End If
            Next
            'If debit = credit Then
            '    endbal = openbal - debit
            'Else
            '    credit = (debit - credit) + credit
            '    endbal = openbal - debit
            'End If

            txtOBDebit.Text = Format(CDec(openbaldebit), "#,##0.00")
            txtOBCredit.Text = Format(CDec(openbalcredit), "#,##0.00")
          
            txtCURDebit.Text = Format(CDec(debit), "#,##0.00")
            txtCURCredit.Text = Format(CDec(credit), "#,##0.00")

            txtEBDebit.Text = Format(CDec(endbaldebit), "#,##0.00")
            txtEBCredit.Text = Format(CDec(endbalcredit), "#,##0.00")


            txtNetIncome.Text = Format(CDec(m_getNetIncome(Microsoft.VisualBasic.FormatDateTime(dteFrom.Value, DateFormat.ShortDate), _
                                                Microsoft.VisualBasic.FormatDateTime(dteEnd.Value, DateFormat.ShortDate))), "##,##0.00")
        End With
    End Sub

    Private Sub btn_trialbal_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_trialbal_print.Click
        gEndDate = Microsoft.VisualBasic.FormatDateTime(dteEnd.Value, DateFormat.ShortDate)
        gStartDate = Microsoft.VisualBasic.FormatDateTime(dteFrom.Value, DateFormat.ShortDate)
        frmworkingtrialbalance.Show()
    End Sub

    Private Sub grdWrkngBal_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdWrkngBal.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdWrkngBal.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub
End Class