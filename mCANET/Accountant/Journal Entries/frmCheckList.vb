﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmCheckList
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Private Sub frmCheckList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListSelNum()
    End Sub
    Private Sub SearchDocNum(ByVal SelNum As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "Check_Search", _
                                   New SqlParameter("@FX_CheckNumber", SelNum))
        dgvSelectCheck.DataSource = ds.Tables(0)
    End Sub
    Private Sub ListSelNum()
        Try
            Dim i As Integer = frmGeneralJournalEntries.grdListofEntries.CurrentRow.Index
            Dim mycon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "PrepareCheck_List",
                                          New SqlParameter("@BankName", frmGeneralJournalEntries.grdListofEntries.Item("Bank", i).Value.ToString))
            dgvSelectCheck.DataSource = ds.Tables(0)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchDocNum(txtSearch.Text)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Hide()
    End Sub
End Class