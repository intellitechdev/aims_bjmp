Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmDebitMemo

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\debitmemo_individual.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@fxKeyDebit", gKeyDebit)
        rptsummary.SetParameterValue("@printedby", get_preparedby())
        rptsummary.SetParameterValue("@reviewedby", TextBox1.Text)
        rptsummary.SetParameterValue("@approvedby", TextBox2.Text)
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    'Private Function get_preparedby() As String
    '    Dim sSQLCmd As String = "select fcFullName from dbo.tUsers where fcLogName='" & gUserName & "' "
    '    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.sqlconn, CommandType.Text, sSQLCmd)
    '    If rd.Read Then
    '        Return rd.Item(0).ToString
    '    End If
    '    Return Nothing
    'End Function

    Private Sub frmDebitMemo_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub
End Class