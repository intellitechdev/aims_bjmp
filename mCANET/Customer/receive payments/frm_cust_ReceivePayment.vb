Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_cust_ReceivePayment

#Region "Declarations"
    Private gcon As New Clsappconfiguration

    Const cUpdate As Integer = 0
    Const cKeyPaymentInvoice As Integer = 1
    Const cKeyInvoice As Integer = 2
    Const cDateTransact As Integer = 3
    Const cNo As Integer = 4
    Const cOrgAmt As Integer = 5
    Const cAmtDue As Integer = 6
    Const cPayment As Integer = 7
    Const cTaxCredit As Integer = 8

    Private sKeyPaymentMethod As String
    Private sKeyPayment As String
    Private sKeyCustomer As String
    Private sKeyInvoice As String
    Private sKeyAcct As String
    Private sKeyCustomerInitial As String
    Private dAmtDiff As Decimal
    Private bUnderOverPay As Boolean
    Private sSign As String

    Dim checkDate As New clsRestrictedDate
#End Region

#Region "Properties"
    Public Property KeyPayment() As String
        Get
            Return sKeyPayment
        End Get
        Set(ByVal value As String)
            sKeyPayment = value
        End Set
    End Property

    Public Property KeyCustomer() As String
        Get
            Return sKeyCustomer
        End Get
        Set(ByVal value As String)
            sKeyCustomer = value
        End Set
    End Property
#End Region
    ' Private sRemarks As String
#Region "Events"
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If sKeyCustomer <> "" Then
            create_TransactionGridview()
            chkUpdate()
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If cboCustomerID.SelectedItem = "" Then
            MessageBox.Show("Customer field cannot be empty!", "Warning")
        ElseIf grpUnderPay.Visible = True And rbtnLeave.Checked = False _
            And rbtnWriteOff.Checked = False Then
            rbtnLeave.Checked = True
            updatePayment()
        Else
            updatePayment()
        End If
    End Sub
    Private Sub GetClass(ByVal XsKeyPayment As String)
        Dim xClassRefNo As String
        Dim sSqlCmd As String = "SELECT fxKeyClassRefNo FROM dbo.tPayment WHERE fxKeyPayment = '" & XsKeyPayment & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xClassRefNo = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        If xClassRefNo Is Nothing Or xClassRefNo = "" Or xClassRefNo = " " Then
            TxtClassRefNo.Text = ""
            LblClassName.Text = "Class Not Defined"
        Else
            Dim dbClassName As String
            Dim ClassNameSplitter(0) As String
            Dim sSqlClassFinder As String = "SELECT ClassRefnumber, classname FROM dbo.mClasses WHERE ClassRefnumber = '" & xClassRefNo & "'"
            Try
                Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlClassFinder)
                    While rd2.Read
                        TxtClassRefNo.Text = rd2.Item(0).ToString
                        dbClassName = rd2.Item(1).ToString
                        ClassNameSplitter = Split(dbClassName, "���")
                        LblClassName.Text = ClassNameSplitter(0)
                    End While
                End Using
            Catch ex As Exception
            End Try
        End If

    End Sub
    Private Sub frm_cust_ReceivePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_DisplayAccountsAll(cboPayAcnt)
        If sKeyPayment = "" Or sKeyPayment = Nothing Then
            sKeyPayment = Guid.NewGuid.ToString
        Else
            Me.cboPayAcnt.SelectedItem = get_Payaccount()
            GetClass(sKeyPayment)
        End If
        refreshForm()
        IsDateRestricted()

        If frmMain.LblClassActivator.Visible = True Then
            Label11.Visible = True
            LblClassName.Visible = True
            BtnBrowseClass.Visible = True
            BtnBrowseClass.Enabled = True
        Else
            Label11.Visible = False
            LblClassName.Visible = False
            BtnBrowseClass.Visible = False
            BtnBrowseClass.Enabled = False
        End If
    End Sub

    Private Sub grdRcvPayment_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRcvPayment.CellClick
        If e.RowIndex <> grdRcvPayment.Rows.Count - 1 Then
            sKeyInvoice = grdRcvPayment.CurrentRow.Cells(cKeyInvoice).Value.ToString
        End If
    End Sub
    Private Sub grdRcvPayment_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRcvPayment.CellValueChanged
        If e.RowIndex >= 0 Then
            'Try
            Dim dPayment As Decimal = 0
            If e.ColumnIndex = cPayment Then
                If mkDefaultValues(e.RowIndex, cPayment) Is Nothing Then
                    dPayment = grdRcvPayment.CurrentRow.Cells(cPayment).Value

                    If dPayment > grdRcvPayment.CurrentRow.Cells(cAmtDue).Value Then
                        MessageBox.Show("You may not pay more than the amount due.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        dPayment = grdRcvPayment.CurrentRow.Cells(cAmtDue).Value
                        grdRcvPayment.CurrentRow.Cells(cPayment).Value = dPayment
                    End If
                End If
                If dPayment = 0 Then
                    grdRcvPayment.Item(cUpdate, e.RowIndex).Value = 0
                Else
                    grdRcvPayment.Item(cUpdate, e.RowIndex).Value = 1
                End If
            End If

            If e.ColumnIndex = cUpdate Then
                If mkDefaultValues(e.RowIndex, cUpdate) <> Nothing Then
                    If grdRcvPayment.Item(cUpdate, e.RowIndex).Value = 0 Then
                        grdRcvPayment.Item(cPayment, e.RowIndex).Value = 0
                    Else
                        If txtAmount.Text <> 0 Then
                            computePayment()
                        End If
                    End If
                End If
            End If

            getTotal()
            getCurTotal()
            Call getdiscount()

            'Catch ex As Exception
            '    MessageBox.Show(Err.Description, "Data Validation")
            'End Try
        End If
    End Sub
    Private Sub grdRcvPayment_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdRcvPayment.DoubleClick
        If grdRcvPayment.CurrentRow.Index <> grdRcvPayment.Rows.Count - 1 And grdRcvPayment.CurrentCell.ColumnIndex <> cPayment Then
            sKeyInvoice = grdRcvPayment.CurrentRow.Cells(cKeyInvoice).Value.ToString
            If sKeyInvoice <> "" Then
                frm_cust_CreateInvoice.KeyInvoice = sKeyInvoice
                frm_cust_CreateInvoice.IsNew = False
                Me.Text = "Edit Invoice"
                frm_cust_CreateInvoice.ShowDialog()
            End If
        End If
    End Sub
    Private Sub grdRcvPayment_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles grdRcvPayment.MouseClick

    End Sub
    Private Sub grdRcvPayment_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdRcvPayment.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdRcvPayment.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomerName.SelectedIndexChanged
        cboCustomerID.SelectedIndex = cboCustomerName.SelectedIndex
        sKeyCustomer = cboCustomerID.SelectedItem
        If cboCustomerName.SelectedIndex = 1 Then
            Dim x As New frm_cust_masterCustomerAddEdit
            x.ShowDialog()
        End If
        If cboCustomerName.SelectedIndex > 1 Then
            create_TransactionGridview()
        End If
    End Sub
    Private Sub cboPaymentMethodName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMethodName.SelectedIndexChanged
        cboPaymentMethodID.SelectedIndex = cboPaymentMethodName.SelectedIndex
        sKeyPaymentMethod = cboPaymentMethodID.SelectedItem.ToString
        If cboPaymentMethodName.SelectedIndex = 1 Then
            Dim x As New frm_MF_paymentMethodAddEdit
            x.Show()
        End If
    End Sub

    Private Sub dteRcvPaymentBal_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteRcvPaymentBal.ValueChanged
        IsDateRestricted()
    End Sub

#End Region

#Region "Subroutines/Functions"
    'Private Sub updateCustomerBal()
    '    If rbtnLeave.Checked = True And grpUnderPay.Visible = True Then

    '        Dim sSQLCmd As String = " Update mCustomer00Master "
    '        sSQLCmd &= "SET fdBalance = fdBalance " & sSign & dAmtDiff
    '        sSQLCmd &= "WHERE fxKeyCustomer = '" & sKeyCustomer & "'"

    '        Try
    '            SqlHelper.ExecuteDataset(gcon.sqlconn, CommandType.Text, sSQLCmd)
    '        Catch ex As Exception
    '            MessageBox.Show(Err.Description, "Update Customer Balance")
    '        End Try
    '    End If
    'End Sub
    Private Function get_Payaccount() As String
        Dim sresult As String = ""
        Dim sSQLCmd As String

        Try
            sSQLCmd = "select t1.acnt_desc from dbo.mAccounts t1 "
            sSQLCmd &= " inner join dbo.tPayment t2 on t2.fxKeyPayAccount = t1.acnt_id where t2.fxKeyPayment='" & sKeyPayment & "' "

            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sresult = rd.Item(0).ToString
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sresult
    End Function
    Private Function IsPaymentNew(ByVal sKey As String) As Boolean
        Dim sSQLCmd As String = "SELECT * FROM tPayment_Invoice "
        sSQLCmd &= "WHERE fxKeyPayment = '" & sKeyPayment & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.HasRows Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Validation")
            Return True
        End Try
    End Function

    Private Function LoadTransactions(ByVal PaymentStatus As Boolean) As DataSet
        Dim sSQLCmd As String
        If PaymentStatus Then
            sSQLCmd = "usp_t_ReceivedPayments_loadTransactions_new "
            sSQLCmd &= "@fxKeyCustomer = '" & sKeyCustomer & "'"
        Else
            sSQLCmd = "usp_t_ReceivedPayments_loadTransactions_existing "
            sSQLCmd &= "@fxKeyPayment = '" & sKeyPayment & "'"
            sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"

        End If

        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Transactions")
            Return Nothing
        End Try
    End Function
    Private Function updatePaymentInvoice() As Boolean
        Dim iRow As Integer
        Dim sSQLCmd As String
        Dim sKeyPayInvoice As String
        Dim sKeyInvoiceTemp As String

        Dim dOrgAmt As Decimal = 0
        Dim dAmtDue As Decimal = 0
        Dim dPayment As Decimal = 0
        Dim iCtr As Integer = 0

        Try
            If Me.grdRcvPayment.Rows.Count >= 1 Then
                For iRow = 0 To (grdRcvPayment.RowCount - 2)

                    sKeyInvoiceTemp = grdRcvPayment.Rows(iRow).Cells(cKeyInvoice).Value.ToString

                    If mkDefaultValues(iRow, cKeyPaymentInvoice) <> Nothing Then
                        sKeyPayInvoice = grdRcvPayment.Rows(iRow).Cells(cKeyPaymentInvoice).Value.ToString
                    Else
                        sKeyPayInvoice = Guid.NewGuid.ToString
                    End If

                    If mkDefaultValues(iRow, cOrgAmt) <> Nothing Then
                        dOrgAmt = grdRcvPayment.Rows(iRow).Cells(cOrgAmt).Value
                    Else
                        dOrgAmt = 0
                    End If

                    If mkDefaultValues(iRow, cAmtDue) <> Nothing Then
                        dAmtDue = grdRcvPayment.Rows(iRow).Cells(cAmtDue).Value
                    Else
                        dAmtDue = 0
                    End If

                    If mkDefaultValues(iRow, cPayment) <> Nothing Then
                        dPayment = grdRcvPayment.Rows(iRow).Cells(cPayment).Value
                    Else
                        dPayment = 0
                    End If

                    sSQLCmd = "usp_t_paymentInvoice_update "
                    sSQLCmd &= "@fxKeyPaymentItem = '" & sKeyPayInvoice & "'"
                    sSQLCmd &= ",@fxKeyPayment = '" & sKeyPayment & "'"
                    sSQLCmd &= ",@fxKeyInvoice = '" & sKeyInvoiceTemp & "'"
                    sSQLCmd &= ",@fdOrigAmt = " & dOrgAmt
                    sSQLCmd &= ",@fdAmtDue = " & dAmtDue
                    sSQLCmd &= ",@fdPayment = " & dPayment

                    If mkDefaultValues(iRow, cUpdate) <> Nothing Then

                        If grdRcvPayment.Item(cUpdate, iRow).Value <> 0 Then
                            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
                            iCtr += 1
                        End If
                    Else
                        If isInPaymentInvoice(sKeyPayment, sKeyPayInvoice) Then
                            deletePayInvoice(sKeyPayInvoice, sKeyInvoiceTemp)
                        End If
                    End If

                Next
                If iCtr = 0 Then
                    Return False
                Else
                    Return True
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Invoice")
            Return False
        End Try

    End Function
    Private Function isInPaymentInvoice(ByVal sKeyPay As String, ByVal sKeyPayIn As String) As Boolean
        Dim dt As DataSet = m_GetPaymentInvoice(sKeyPay, sKeyPayIn)
        Try
            Using rd As DataTableReader = dt.CreateDataReader
                If rd.Read Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Function
    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdRcvPayment.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdRcvPayment.Rows(irow).Cells(iCol).Value)

    End Function

    Public Sub create_TransactionGridview()
        Try
            With grdRcvPayment
                Dim colUpdate As New DataGridViewCheckBoxColumn
                Dim colPayInvoice As New DataGridViewTextBoxColumn
                Dim colInvoice As New DataGridViewTextBoxColumn
                Dim colDate As New DataGridViewTextBoxColumn
                Dim colNo As New DataGridViewTextBoxColumn
                Dim colOrgAmt As New DataGridViewTextBoxColumn
                Dim colAmtDue As New DataGridViewTextBoxColumn
                Dim colPayment As New DataGridViewTextBoxColumn
                Dim colTaxCredit As New DataGridViewTextBoxColumn

                Dim bIsNew As Boolean = IsPaymentNew(sKeyPayment)
                Dim dtInvoice As DataTable = LoadTransactions(bIsNew).Tables(0)

                .DataSource = dtInvoice.DefaultView

                colUpdate.DataPropertyName = "bUpdate"
                colUpdate.HeaderText = "Update"

                colPayInvoice.DataPropertyName = "fxKeyPaymentItem"
                colPayInvoice.HeaderText = "KeyPaymentItem"

                colInvoice.DataPropertyName = "fxKeyInvoice"
                colInvoice.HeaderText = "KeyInvoice"

                colDate.DataPropertyName = "fdDateTransact"
                colDate.HeaderText = "Date"

                colNo.DataPropertyName = "fcInvoiceNo"
                colNo.HeaderText = "Invoice #"

                colOrgAmt.DataPropertyName = "fdOrigAmt"
                colOrgAmt.HeaderText = "Orig Amt"

                colAmtDue.DataPropertyName = "fdAmtDue"
                colAmtDue.HeaderText = "Amt Due"

                colPayment.DataPropertyName = "fdPayment"
                colPayment.HeaderText = "Payment"

                colTaxCredit.DataPropertyName = "fdTaxCredit"
                colTaxCredit.HeaderText = "Tax Credit"

                .Columns.Clear()
                .Columns.Add(colUpdate)
                .Columns.Add(colPayInvoice)
                .Columns.Add(colInvoice)
                .Columns.Add(colDate)
                .Columns.Add(colNo)
                .Columns.Add(colOrgAmt)
                .Columns.Add(colAmtDue)
                .Columns.Add(colPayment)
                .Columns.Add(colTaxCredit)

                .Columns(cUpdate).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns(cDateTransact).DefaultCellStyle.Format = "MM/dd/yyyy"

                .Columns(cKeyPaymentInvoice).Visible = False
                .Columns(cKeyInvoice).Visible = False

                .Columns(cOrgAmt).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(cAmtDue).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(cPayment).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
                .Columns(cTaxCredit).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight

                .Columns(cDateTransact).ReadOnly = True
                .Columns(cDateTransact).DefaultCellStyle.BackColor = Color.Gainsboro

                .Columns(cNo).ReadOnly = True
                .Columns(cNo).DefaultCellStyle.BackColor = Color.Gainsboro

                .Columns(cOrgAmt).ReadOnly = True
                .Columns(cOrgAmt).DefaultCellStyle.BackColor = Color.Gainsboro
                .Columns(cOrgAmt).DefaultCellStyle.Format = "##,##0.00"

                .Columns(cPayment).DefaultCellStyle.Format = "##,##0.00"

                .Columns(cAmtDue).ReadOnly = True
                .Columns(cAmtDue).DefaultCellStyle.BackColor = Color.Gainsboro
                .Columns(cAmtDue).DefaultCellStyle.Format = "##,##0.00"

                .Columns(cTaxCredit).ReadOnly = True
                .Columns(cTaxCredit).DefaultCellStyle.BackColor = Color.Gainsboro
                .Columns(cTaxCredit).DefaultCellStyle.Format = "##,##0.00"

            End With

            getTotal()
        Catch
            MessageBox.Show(Err.Description, "Customer Transactions")
        End Try
    End Sub
    Private Sub loadPaymethodName()
        m_loadPaymentMethod(cboPaymentMethodName, cboPaymentMethodID)
    End Sub
    Private Sub refreshForm()
        m_loadCustomer(cboCustomerName, cboCustomerID)
        m_loadPaymentMethod(cboPaymentMethodName, cboPaymentMethodID)

        loadPayment()

        m_selectedCboValue(cboCustomerName, cboCustomerID, sKeyCustomerInitial)
        m_selectedCboValue(cboPaymentMethodName, cboPaymentMethodID, sKeyPaymentMethod)

        sKeyCustomer = sKeyCustomerInitial
        If sKeyCustomer <> "" Then
            create_TransactionGridview()
        End If
        getCurTotal()
        chkUpdate()
    End Sub
    Private Sub chkUpdate()
        If grdRcvPayment.Rows.Count > 1 Then
            Dim x As Integer = 0
            Dim bUpdateTemp As Boolean
            For x = 0 To grdRcvPayment.Rows.Count - 1
                If mkDefaultValues(x, cPayment) <> Nothing Then
                    bUpdateTemp = True
                Else
                    bUpdateTemp = False
                End If
                If bUpdateTemp Then
                    grdRcvPayment.Item(cUpdate, x).Value = True
                Else
                    grdRcvPayment.Item(cUpdate, x).Value = False
                    'Else
                    '    grdRcvPayment.Item(cUpdate, x).Value = False
                End If
            Next
        End If
    End Sub
    Private Sub getTotal()
        Try
            '  Dim x As Integer
            Dim dOrgAmt As Decimal = 0
            Dim dAmtDue As Decimal = 0
            Dim dPayment As Decimal = 0

            With Me.grdRcvPayment
                Dim xRow As Integer
                For xRow = 0 To .Rows.Count - 1
                    If Not .Rows(xRow).Cells(cNo).Value Is Nothing Then
                        dOrgAmt += CDec(.Rows(xRow).Cells(cOrgAmt).Value)
                    End If
                Next
                For xRow = 0 To .Rows.Count - 1
                    If Not .Rows(xRow).Cells(cNo).Value Is Nothing Then
                        dAmtDue += CDec(.Rows(xRow).Cells(cAmtDue).Value)
                    End If
                Next
                For xRow = 0 To .Rows.Count - 1
                    If Not .Rows(xRow).Cells(cNo).Value Is Nothing Then
                        dPayment += CDec(.Rows(xRow).Cells(cPayment).Value)
                    End If
                Next
            End With
            'For x = 0 To grdRcvPayment.Rows.Count - 1
            '    If mkDefaultValues(x, 5) <> Nothing Then
            '        ' If grdRcvPayment.Item(cUpdate, x).Value = 1 Then
            '        dOrgAmt = Format(dOrgAmt + grdRcvPayment.Item(cOrgAmt, x).Value, "0.00")
            '        dAmtDue = Format(dAmtDue + grdRcvPayment.Item(cAmtDue, x).Value, "0.00")
            '        'dPayment = Format(dPayment + grdRcvPayment.Item(cPayment, x).Value, "0.00")
            '        dPayment = Format(dAmtDue + grdRcvPayment.Item(cAmtDue, x).Value, "0.00")
            '        'End If
            '    End If
            'Next

            lblTotalOrigAmt.Text = Format(CDec(dOrgAmt), "#,##0.00")
            lblTotalAmtDue.Text = Format(CDec(dAmtDue), "#,##0.00")
            lblTotalPayment.Text = Format(CDec(dPayment), "#,##0.00")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Get Total")
        End Try
    End Sub
    Private Sub loadPayment()
        Dim dtPay As DataSet = m_GetPayment(sKeyPayment)
        Try
            Using rd As DataTableReader = dtPay.CreateDataReader
                If rd.Read Then
                    sKeyCustomerInitial = rd.Item("fxKeyCustomer").ToString
                    txtAmount.Text = rd.Item("fdAmount")
                    txtCardNo.Text = rd.Item("fcCardNo")
                    txtMemo.Text = rd.Item("fcMemo")
                    BtnBrowse.Text = rd.Item("fdBalance").ToString
                    dteRcvPaymentBal.Value = rd.Item("fdDate")
                    txtReferenceNo.Text = rd.Item("fcRefNo")
                    txtMonth.Text = IIf(IsDBNull(rd.Item("fnExpMonth")) = True, " ", rd.Item("fnExpMonth"))
                    txtYear.Text = IIf(IsDBNull(rd.Item("fnExpYear")) = True, "", rd.Item("fnExpYear"))
                    chkProcess.Checked = rd.Item("fbProcessCreditCard")
                    cboPaymentMethodName.SelectedItem = rd.Item("fcPaymentMethodName").ToString
                    cboPaymentMethodID.SelectedIndex = cboPaymentMethodName.SelectedIndex

                    lblAmtDue.Text = rd.Item("fdAmtDue")
                    lblPayment.Text = rd.Item("fdPayment")
                    lblRDAmt.Text = rd.Item("fdDiscount")
                    If rd.Item("fbUnderOverPay") = True Then
                        rbtnLeave.Checked = True
                    Else
                        rbtnWriteOff.Checked = True
                    End If
                Else
                    sKeyCustomerInitial = ""
                    txtAmount.Text = "0.00"
                    txtCardNo.Text = ""
                    txtMemo.Text = ""
                    BtnBrowse.Text = "0.00"
                    dteRcvPaymentBal.Value = Now
                    txtReferenceNo.Text = ""
                    txtMonth.Text = ""
                    txtYear.Text = ""
                    chkProcess.Checked = False
                    lblAmtDue.Text = "0.00"
                    lblPayment.Text = "0.00"
                    lblRDAmt.Text = "0.00"
                    rbtnLeave.Checked = True
                    rbtnWriteOff.Checked = False
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub
    Private Sub getCurTotal()
        If grdRcvPayment.Rows.Count > 0 Then
            Dim x As Integer
            Dim dAmtDue As Decimal = 0
            Dim dPayment As Decimal = 0

            For x = 0 To grdRcvPayment.Rows.Count - 1
                If mkDefaultValues(x, cUpdate) <> Nothing Then
                    If grdRcvPayment.Item(cUpdate, x).Value = 1 Then
                        dAmtDue = dAmtDue + grdRcvPayment.Item(cAmtDue, x).Value
                        dPayment = dPayment + grdRcvPayment.Item(cPayment, x).Value
                    End If
                End If
            Next

            lblAmtDue.Text = Format(dAmtDue, "0.00")
            lblPayment.Text = Format(dPayment, "0.00")
            underOverPay()
        End If
    End Sub
    Private Sub getdiscount()
        Try
            Dim sSDVal As Decimal = 0.0
            Dim sRDVal As Decimal = 0.0
            Dim xRow As Integer
            With grdRcvPayment
                For xRow = 0 To .Rows.Count - 1
                    If Not .Rows(xRow).Cells(cDateTransact).Value Is Nothing Then
                        If .Rows(xRow).Cells(0).Value <> False Then
                            sSDVal += m_GetSalesDiscountRP(.Rows(xRow).Cells(cKeyInvoice).Value.ToString)
                        Else
                            sSDVal += 0
                        End If
                    End If
                    If Not .Rows(xRow).Cells(cDateTransact).Value Is Nothing Then
                        If .Rows(xRow).Cells(0).Value <> False Then
                            sRDVal += m_GetRetailersDiscountRP(.Rows(xRow).Cells(cKeyInvoice).Value.ToString)
                        Else
                            sRDVal += 0
                        End If
                    End If
                Next
            End With
            lblSDAmt.Text = Format(CDec(sSDVal), "##,##0.00")
            lblRDAmt.Text = Format(CDec(sRDVal), "##,##0.00")
        Catch ex As Exception
        End Try
    End Sub
    Private Sub underOverPay()
        Dim dAmt As Decimal = IIf(IsNumeric(txtAmount.Text) = True, txtAmount.Text, "0.00")
        dAmtDiff = Format(dAmt - lblTotalAmtDue.Text, "0.00")

        If dAmt > lblTotalPayment.Text Then
            lblUnderPay.Text = "Overpayment " & dAmtDiff '& " . When you finish, do you want to:"

            grpUnderPay.Visible = True
            rbtnLeave.Text = "Leave the credit to be used later"
            rbtnWriteOff.Text = "Refund the amount to the customer"

            rbtnLeave.Checked = True
            sSign = " + "


        ElseIf dAmt < lblTotalAmtDue.Text Then
            lblUnderPay.Text = "Underpayment " & dAmtDiff '& " . When you finish, do you want to:"

            grpUnderPay.Visible = True
            rbtnLeave.Text = "Leave this as an underpayment"
            rbtnWriteOff.Text = "Write off the extra amount"
            sSign = " - "

        Else
            dAmtDiff = 0
            grpUnderPay.Visible = False
        End If


    End Sub
    Private Sub updatePayment()

        Dim pxPayAccount As String = ""

        If cboPayAcnt.SelectedItem <> " " Then
            pxPayAccount = getAccountID(cboPayAcnt.SelectedItem)
        End If

        Dim dAmt As Decimal = IIf(IsNumeric(txtAmount.Text) = True, txtAmount.Text, "#,##0.00")

        If dAmt = 0 Then
            MessageBox.Show("Amount of payment cannot be blank!", "Data Validation")
            txtAmount.Focus()
            Exit Sub
            'ElseIf dAmt < lblTotalPayment.Text And dAmt <> 0 Then
            '    MessageBox.Show("Transaction is not balance. " & vbCrLf & "Make sure the amounts in detail area is equal to the amount at the top of the form.", "Data Validation")
            '    txtAmount.Focus()
            '    Exit Sub
        End If

        Dim sSQLCmd As String = "usp_t_payment_update "
        sSQLCmd &= "@fxKeyPayment = '" & sKeyPayment & "'"
        sSQLCmd &= ",@fxKeyCustomer = '" & sKeyCustomer & "'"
        sSQLCmd &= ",@fdDate = '" & dteRcvPaymentBal.Value & "'"
        sSQLCmd &= ",@fcRefNo = '" & txtReferenceNo.Text & "'"
        sSQLCmd &= ",@fcMemo = '" & txtMemo.Text & "'"
        sSQLCmd &= ",@fcCardNo = '" & txtCardNo.Text & "'"
        sSQLCmd &= ",@fbProcessCreditCard ='" & IIf(chkProcess.Checked = True, 1, 0) & "' "
        sSQLCmd &= ",@fdAmount ='" & CDec(txtAmount.Text) & "' "
        sSQLCmd &= ",@fdAmtDue ='" & CDec(lblAmtDue.Text) & "' "
        sSQLCmd &= ",@fdPayment ='" & CDec(lblPayment.Text) & "' "
        sSQLCmd &= ",@fdDiscount ='" & CDec(lblRDAmt.Text) & "' "
        sSQLCmd &= ",@fbUnderOverPay ='" & IIf(rbtnLeave.Checked, 1, 0) & "' "
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= ",@createdby ='" & gUserName & "' "
        If cboPaymentMethodName.SelectedItem.ToString <> "Create New" Then
            sSQLCmd &= ",@fcPayMethod='" & cboPaymentMethodName.SelectedItem.ToString & "' "
        Else
            sSQLCmd &= ",@fcPayMethod = '' "
        End If

        sSQLCmd &= ",@fxKeyPayAccnt =" & IIf(pxPayAccount = "", "NULL", "'" & pxPayAccount & "'") & " "

        If Trim(txtMonth.Text) <> "" Then
            sSQLCmd &= ",@fnExpMonth = " & txtMonth.Text
        End If
        If Trim(txtYear.Text) <> "" Then
            sSQLCmd &= ",@fnExpYear = " & txtYear.Text
        End If
        If sKeyAcct <> "" Then
            sSQLCmd &= ",@fxKeyAccount = '" & sKeyAcct & "'"
        End If
        If sKeyPaymentMethod <> "" Then
            sSQLCmd &= ",@fxKeyPaymentMethod = '" & sKeyPaymentMethod & "'"
        End If

        sSQLCmd &= ",@fxKeyClassRefNo = '" & TxtClassRefNo.Text & "'"

        Try
            SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            If updatePaymentInvoice() Then
                MessageBox.Show("Record successfully Updated.", "Receive Payment")
                frm_cust_masterCustomer.loadGrdCustomer2("Received Payments")
                m_customerList(frm_cust_masterCustomer.grdCustomer1, frm_cust_masterCustomer.cboView.SelectedItem)
                'updateCustomerBal()
                Me.Close()
                'Else
                '    sSQLCmd = "DELETE FROM tPayment WHERE fxKeyPayment = '" & sKeyPayment & "'"
                '    SqlHelper.ExecuteDataset(gcon.sqlconn, CommandType.Text, sSQLCmd)
                '    Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(Err.Description, "Receive Payments")
        End Try
    End Sub
    Private Sub deletePayInvoice(ByVal sKeyPay As String, ByVal sKeyIn As String)
        Dim sSQLCmd As String = "usp_t_paymentInvoice_delete "
        sSQLCmd &= "@fxKeyPaymentItem = '" & sKeyPay & "',"
        sSQLCmd &= "@fxKeyInvoice = '" & sKeyIn & "'"

        SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)

        'Dim sSQLCmd As String = "DELETE FROM tPayment_Invoice "
        'sSQLCmd &= "WHERE fxKeyPaymentItem = '" & sKey & "'"
        'Try
        '    SqlHelper.ExecuteDataset(gcon.sqlconn, CommandType.Text, sSQLCmd)

        '    sSQLCmd = "UPDATE tInvoice SET fbPaid = 0, "
        '    sSQLCmd &= "fdBalance = fdBalance +  " & dPay & ", "
        '    sSQLCmd &= "fdPayment = fdPayment - " & dPay & ", "
        '    sSQLCmd &= "WHERE fxKeyInvoice = '" & sKeyIn & "'"

        '    SqlHelper.ExecuteDataset(gcon.sqlconn, CommandType.Text, sSQLCmd)

        'Catch ex As Exception
        '    MessageBox.Show(Err.Description, "Delete Payment in Invoice")
        'End Try
    End Sub
    Private Sub txtAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        e.Handled = Not numeric_Validate(e.KeyChar)
        underOverPay()
    End Sub
    Private Sub txtAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmount.LostFocus
        computePayment()
        chkUpdate()
    End Sub
    Private Sub computePayment()
        'Try

        If grdRcvPayment.Rows.Count > 1 Then
            Dim iRow As Integer
            Dim dDiff As Decimal = IIf(IsNumeric(txtAmount.Text) = True, txtAmount.Text, 0)
            Dim dCurPayment As Decimal
            Dim dCurAmtDue As Decimal

            For iRow = 0 To grdRcvPayment.Rows.Count - 2
                Dim bUpdateTemp As Boolean
                If mkDefaultValues(iRow, cUpdate) <> Nothing Then
                    bUpdateTemp = grdRcvPayment.Item(cUpdate, iRow).Value
                Else
                    bUpdateTemp = False
                End If

                If bUpdateTemp = True Then
                    If mkDefaultValues(iRow, cPayment) <> Nothing Then
                        dCurPayment = grdRcvPayment.Item(cPayment, iRow).Value
                    Else
                        dCurPayment = 0.0
                    End If

                    If mkDefaultValues(iRow, cAmtDue) <> Nothing Then
                        dCurAmtDue = grdRcvPayment.Item(cAmtDue, iRow).Value
                    Else
                        dCurAmtDue = 0.0
                    End If


                    If dDiff >= dCurAmtDue Then
                        grdRcvPayment.Item(cPayment, iRow).Value = dCurAmtDue
                    Else
                        If dDiff > 0 Then
                            grdRcvPayment.Item(cPayment, iRow).Value = dDiff
                        Else
                            grdRcvPayment.Item(cPayment, iRow).Value = "0.00"
                        End If
                    End If
                    dDiff = dDiff - dCurAmtDue
                End If
            Next
        End If
        'Catch ex As Exception
        '    'MessageBox.Show(Err.Description, "Compute Payment")
        'End Try
    End Sub
    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dteRcvPaymentBal.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
        End If
    End Sub
#End Region

    Private Sub cboPaymentMethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click

    End Sub

    Private Sub BtnBrowseClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowseClass.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        TxtClassRefNo.Text = FrmBrowseClass.Data2
        LblClassName.Text = FrmBrowseClass.Data1
    End Sub
End Class