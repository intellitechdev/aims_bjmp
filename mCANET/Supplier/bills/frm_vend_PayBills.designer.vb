<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_PayBills
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim lblCheckNo As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_PayBills))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.grdBills = New System.Windows.Forms.DataGridView()
        Me.grpDiscount = New System.Windows.Forms.GroupBox()
        Me.btnSetCredits = New System.Windows.Forms.Button()
        Me.btnSetDiscount = New System.Windows.Forms.Button()
        Me.lblCredits = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblCreditno = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblDiscount = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblrefno = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblsupplier = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rbtnDue = New System.Windows.Forms.RadioButton()
        Me.rbtnShwAll = New System.Windows.Forms.RadioButton()
        Me.dteDueBefore = New System.Windows.Forms.DateTimePicker()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnSelectAll = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.lblAmt2pay = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblCredUsed = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblDiscUsed = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblAmtdue = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.picLoading = New System.Windows.Forms.PictureBox()
        Me.cboBankAccount = New MTGCComboBox()
        Me.lblendingbal = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtCheckNo = New System.Windows.Forms.TextBox()
        Me.chkCheckPrint = New System.Windows.Forms.CheckBox()
        Me.cboPaymentMethod = New System.Windows.Forms.ComboBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.dtePaymentDate = New System.Windows.Forms.DateTimePicker()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.bgwLoadBalance = New System.ComponentModel.BackgroundWorker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        lblCheckNo = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdBills, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpDiscount.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCheckNo
        '
        lblCheckNo.AutoSize = True
        lblCheckNo.Location = New System.Drawing.Point(6, 54)
        lblCheckNo.Name = "lblCheckNo"
        lblCheckNo.Size = New System.Drawing.Size(66, 13)
        lblCheckNo.TabIndex = 17
        lblCheckNo.Text = "Check No."
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblWarning)
        Me.GroupBox1.Controls.Add(Me.grdBills)
        Me.GroupBox1.Controls.Add(Me.grpDiscount)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(881, 347)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Bills to be Paid"
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblWarning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWarning.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(162, 134)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(537, 55)
        Me.lblWarning.TabIndex = 16
        Me.lblWarning.Text = "The system does not allow different payee in one payment," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "please check your sele" & _
            "ction."
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblWarning.Visible = False
        '
        'grdBills
        '
        Me.grdBills.AllowUserToAddRows = False
        Me.grdBills.AllowUserToDeleteRows = False
        Me.grdBills.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdBills.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdBills.Location = New System.Drawing.Point(3, 68)
        Me.grdBills.Name = "grdBills"
        Me.grdBills.Size = New System.Drawing.Size(875, 199)
        Me.grdBills.TabIndex = 6
        '
        'grpDiscount
        '
        Me.grpDiscount.Controls.Add(Me.btnSetCredits)
        Me.grpDiscount.Controls.Add(Me.btnSetDiscount)
        Me.grpDiscount.Controls.Add(Me.lblCredits)
        Me.grpDiscount.Controls.Add(Me.Label12)
        Me.grpDiscount.Controls.Add(Me.lblCreditno)
        Me.grpDiscount.Controls.Add(Me.Label14)
        Me.grpDiscount.Controls.Add(Me.lblDiscount)
        Me.grpDiscount.Controls.Add(Me.Label8)
        Me.grpDiscount.Controls.Add(Me.lblTerms)
        Me.grpDiscount.Controls.Add(Me.Label10)
        Me.grpDiscount.Controls.Add(Me.lblrefno)
        Me.grpDiscount.Controls.Add(Me.Label5)
        Me.grpDiscount.Controls.Add(Me.lblsupplier)
        Me.grpDiscount.Controls.Add(Me.Label3)
        Me.grpDiscount.Enabled = False
        Me.grpDiscount.Location = New System.Drawing.Point(21, 182)
        Me.grpDiscount.Name = "grpDiscount"
        Me.grpDiscount.Size = New System.Drawing.Size(840, 67)
        Me.grpDiscount.TabIndex = 1
        Me.grpDiscount.TabStop = False
        Me.grpDiscount.Text = "Discount && Credit Information"
        Me.grpDiscount.Visible = False
        '
        'btnSetCredits
        '
        Me.btnSetCredits.Location = New System.Drawing.Point(656, 17)
        Me.btnSetCredits.Name = "btnSetCredits"
        Me.btnSetCredits.Size = New System.Drawing.Size(87, 23)
        Me.btnSetCredits.TabIndex = 15
        Me.btnSetCredits.Text = "Set Credi&ts"
        Me.btnSetCredits.UseVisualStyleBackColor = True
        Me.btnSetCredits.Visible = False
        '
        'btnSetDiscount
        '
        Me.btnSetDiscount.Location = New System.Drawing.Point(399, 16)
        Me.btnSetDiscount.Name = "btnSetDiscount"
        Me.btnSetDiscount.Size = New System.Drawing.Size(96, 23)
        Me.btnSetDiscount.TabIndex = 14
        Me.btnSetDiscount.Text = "Set D&iscount"
        Me.btnSetDiscount.UseVisualStyleBackColor = True
        Me.btnSetDiscount.Visible = False
        '
        'lblCredits
        '
        Me.lblCredits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCredits.Location = New System.Drawing.Point(675, 38)
        Me.lblCredits.Name = "lblCredits"
        Me.lblCredits.Size = New System.Drawing.Size(135, 13)
        Me.lblCredits.TabIndex = 12
        Me.lblCredits.Text = "0.00"
        Me.lblCredits.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblCredits.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(514, 38)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(136, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Total Credits Available"
        Me.Label12.Visible = False
        '
        'lblCreditno
        '
        Me.lblCreditno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditno.Location = New System.Drawing.Point(675, 16)
        Me.lblCreditno.Name = "lblCreditno"
        Me.lblCreditno.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCreditno.Size = New System.Drawing.Size(134, 13)
        Me.lblCreditno.TabIndex = 10
        Me.lblCreditno.Text = "0"
        Me.lblCreditno.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCreditno.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(514, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 13)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Number of Credits"
        Me.Label14.Visible = False
        '
        'lblDiscount
        '
        Me.lblDiscount.AutoSize = True
        Me.lblDiscount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiscount.Location = New System.Drawing.Point(342, 39)
        Me.lblDiscount.Name = "lblDiscount"
        Me.lblDiscount.Size = New System.Drawing.Size(31, 13)
        Me.lblDiscount.TabIndex = 8
        Me.lblDiscount.Text = "0.00"
        Me.lblDiscount.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(245, 39)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Sug. Disc"
        Me.Label8.Visible = False
        '
        'lblTerms
        '
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerms.Location = New System.Drawing.Point(342, 17)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(43, 13)
        Me.lblTerms.TabIndex = 6
        Me.lblTerms.Text = "Terms"
        Me.lblTerms.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(245, 17)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Terms"
        Me.Label10.Visible = False
        '
        'lblrefno
        '
        Me.lblrefno.AutoSize = True
        Me.lblrefno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrefno.Location = New System.Drawing.Point(91, 38)
        Me.lblrefno.Name = "lblrefno"
        Me.lblrefno.Size = New System.Drawing.Size(85, 13)
        Me.lblrefno.TabIndex = 4
        Me.lblrefno.Text = "Reference No."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 38)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "CV Ref. #"
        '
        'lblsupplier
        '
        Me.lblsupplier.AutoSize = True
        Me.lblsupplier.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsupplier.Location = New System.Drawing.Point(91, 16)
        Me.lblsupplier.Name = "lblsupplier"
        Me.lblsupplier.Size = New System.Drawing.Size(88, 13)
        Me.lblsupplier.TabIndex = 2
        Me.lblsupplier.Text = "Supplier Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Supplier"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.rbtnDue)
        Me.Panel3.Controls.Add(Me.rbtnShwAll)
        Me.Panel3.Controls.Add(Me.dteDueBefore)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 17)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(875, 51)
        Me.Panel3.TabIndex = 18
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Show Bills"
        '
        'rbtnDue
        '
        Me.rbtnDue.AutoSize = True
        Me.rbtnDue.Location = New System.Drawing.Point(80, 8)
        Me.rbtnDue.Name = "rbtnDue"
        Me.rbtnDue.Size = New System.Drawing.Size(123, 17)
        Me.rbtnDue.TabIndex = 1
        Me.rbtnDue.TabStop = True
        Me.rbtnDue.Text = "Due on or before"
        Me.rbtnDue.UseVisualStyleBackColor = True
        '
        'rbtnShwAll
        '
        Me.rbtnShwAll.AutoSize = True
        Me.rbtnShwAll.Location = New System.Drawing.Point(80, 31)
        Me.rbtnShwAll.Name = "rbtnShwAll"
        Me.rbtnShwAll.Size = New System.Drawing.Size(99, 17)
        Me.rbtnShwAll.TabIndex = 2
        Me.rbtnShwAll.TabStop = True
        Me.rbtnShwAll.Text = "Show all bills"
        Me.rbtnShwAll.UseVisualStyleBackColor = True
        '
        'dteDueBefore
        '
        Me.dteDueBefore.Enabled = False
        Me.dteDueBefore.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteDueBefore.Location = New System.Drawing.Point(210, 6)
        Me.dteDueBefore.Name = "dteDueBefore"
        Me.dteDueBefore.Size = New System.Drawing.Size(194, 21)
        Me.dteDueBefore.TabIndex = 3
        Me.dteDueBefore.Value = New Date(2008, 3, 28, 0, 0, 0, 0)
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnSelectAll)
        Me.Panel2.Controls.Add(Me.GroupBox6)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(3, 267)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(875, 77)
        Me.Panel2.TabIndex = 17
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSelectAll.Enabled = False
        Me.btnSelectAll.Location = New System.Drawing.Point(11, 12)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(121, 23)
        Me.btnSelectAll.TabIndex = 7
        Me.btnSelectAll.Text = "Select All Bills"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox6.Controls.Add(Me.lblAmt2pay)
        Me.GroupBox6.Controls.Add(Me.Label24)
        Me.GroupBox6.Controls.Add(Me.lblCredUsed)
        Me.GroupBox6.Controls.Add(Me.Label22)
        Me.GroupBox6.Controls.Add(Me.lblDiscUsed)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.lblAmtdue)
        Me.GroupBox6.Controls.Add(Me.Label16)
        Me.GroupBox6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(138, 7)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(728, 64)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Totals"
        '
        'lblAmt2pay
        '
        Me.lblAmt2pay.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmt2pay.Location = New System.Drawing.Point(614, 13)
        Me.lblAmt2pay.Name = "lblAmt2pay"
        Me.lblAmt2pay.Size = New System.Drawing.Size(97, 26)
        Me.lblAmt2pay.TabIndex = 13
        Me.lblAmt2pay.Text = "0.00"
        Me.lblAmt2pay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(530, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(78, 13)
        Me.Label24.TabIndex = 12
        Me.Label24.Text = "Amount to Pay"
        '
        'lblCredUsed
        '
        Me.lblCredUsed.AutoSize = True
        Me.lblCredUsed.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCredUsed.Location = New System.Drawing.Point(344, 42)
        Me.lblCredUsed.Name = "lblCredUsed"
        Me.lblCredUsed.Size = New System.Drawing.Size(31, 13)
        Me.lblCredUsed.TabIndex = 11
        Me.lblCredUsed.Text = "0.00"
        Me.lblCredUsed.Visible = False
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(262, 42)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 13)
        Me.Label22.TabIndex = 10
        Me.Label22.Text = "Credit Used"
        Me.Label22.Visible = False
        '
        'lblDiscUsed
        '
        Me.lblDiscUsed.AutoSize = True
        Me.lblDiscUsed.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiscUsed.Location = New System.Drawing.Point(344, 20)
        Me.lblDiscUsed.Name = "lblDiscUsed"
        Me.lblDiscUsed.Size = New System.Drawing.Size(31, 13)
        Me.lblDiscUsed.TabIndex = 9
        Me.lblDiscUsed.Text = "0.00"
        Me.lblDiscUsed.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(250, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(75, 13)
        Me.Label19.TabIndex = 8
        Me.Label19.Text = "Discount Used"
        Me.Label19.Visible = False
        '
        'lblAmtdue
        '
        Me.lblAmtdue.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmtdue.Location = New System.Drawing.Point(85, 17)
        Me.lblAmtdue.Name = "lblAmtdue"
        Me.lblAmtdue.Size = New System.Drawing.Size(87, 22)
        Me.lblAmtdue.TabIndex = 7
        Me.lblAmtdue.Text = "0.00"
        Me.lblAmtdue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(22, 23)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 13)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Amount Due"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.picLoading)
        Me.GroupBox3.Controls.Add(Me.cboBankAccount)
        Me.GroupBox3.Controls.Add(Me.lblendingbal)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Location = New System.Drawing.Point(252, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(322, 84)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Payment Account"
        '
        'picLoading
        '
        Me.picLoading.Image = Global.CSAcctg.My.Resources.Resources.loading_gif_animation
        Me.picLoading.Location = New System.Drawing.Point(283, 50)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(33, 25)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 15
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'cboBankAccount
        '
        Me.cboBankAccount.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboBankAccount.ArrowColor = System.Drawing.Color.Black
        Me.cboBankAccount.BindedControl = CType(resources.GetObject("cboBankAccount.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.cboBankAccount.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.cboBankAccount.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboBankAccount.ColumnNum = 2
        Me.cboBankAccount.ColumnWidth = "121;0"
        Me.cboBankAccount.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.cboBankAccount.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.cboBankAccount.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.cboBankAccount.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.cboBankAccount.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.cboBankAccount.DisplayMember = "Text"
        Me.cboBankAccount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboBankAccount.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboBankAccount.DropDownForeColor = System.Drawing.Color.Black
        Me.cboBankAccount.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboBankAccount.DropDownWidth = 141
        Me.cboBankAccount.GridLineColor = System.Drawing.Color.LightGray
        Me.cboBankAccount.GridLineHorizontal = False
        Me.cboBankAccount.GridLineVertical = False
        Me.cboBankAccount.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboBankAccount.Location = New System.Drawing.Point(6, 20)
        Me.cboBankAccount.ManagingFastMouseMoving = True
        Me.cboBankAccount.ManagingFastMouseMovingInterval = 30
        Me.cboBankAccount.Name = "cboBankAccount"
        Me.cboBankAccount.SelectedItem = Nothing
        Me.cboBankAccount.SelectedValue = Nothing
        Me.cboBankAccount.Size = New System.Drawing.Size(310, 22)
        Me.cboBankAccount.TabIndex = 14
        '
        'lblendingbal
        '
        Me.lblendingbal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblendingbal.Location = New System.Drawing.Point(104, 54)
        Me.lblendingbal.Name = "lblendingbal"
        Me.lblendingbal.Size = New System.Drawing.Size(212, 15)
        Me.lblendingbal.TabIndex = 2
        Me.lblendingbal.Text = "0.00"
        Me.lblendingbal.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblendingbal.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(7, 54)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(91, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Ending Balance"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(lblCheckNo)
        Me.GroupBox4.Controls.Add(Me.txtCheckNo)
        Me.GroupBox4.Controls.Add(Me.chkCheckPrint)
        Me.GroupBox4.Controls.Add(Me.cboPaymentMethod)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(243, 109)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Payment Method"
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Location = New System.Drawing.Point(79, 50)
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(154, 21)
        Me.txtCheckNo.TabIndex = 16
        '
        'chkCheckPrint
        '
        Me.chkCheckPrint.AutoSize = True
        Me.chkCheckPrint.Location = New System.Drawing.Point(134, 81)
        Me.chkCheckPrint.Name = "chkCheckPrint"
        Me.chkCheckPrint.Size = New System.Drawing.Size(85, 17)
        Me.chkCheckPrint.TabIndex = 1
        Me.chkCheckPrint.Text = "to be printed"
        Me.chkCheckPrint.UseVisualStyleBackColor = True
        '
        'cboPaymentMethod
        '
        Me.cboPaymentMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentMethod.FormattingEnabled = True
        Me.cboPaymentMethod.Items.AddRange(New Object() {"Cash", "Check"})
        Me.cboPaymentMethod.Location = New System.Drawing.Point(6, 19)
        Me.cboPaymentMethod.Name = "cboPaymentMethod"
        Me.cboPaymentMethod.Size = New System.Drawing.Size(227, 21)
        Me.cboPaymentMethod.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dtePaymentDate)
        Me.GroupBox5.Location = New System.Drawing.Point(580, 6)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(281, 58)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Payment Date"
        '
        'dtePaymentDate
        '
        Me.dtePaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtePaymentDate.Location = New System.Drawing.Point(31, 24)
        Me.dtePaymentDate.Name = "dtePaymentDate"
        Me.dtePaymentDate.Size = New System.Drawing.Size(178, 21)
        Me.dtePaymentDate.TabIndex = 4
        Me.dtePaymentDate.Value = New Date(2008, 3, 28, 0, 0, 0, 0)
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(788, 77)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(81, 35)
        Me.btnCancel.TabIndex = 15
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPay
        '
        Me.btnPay.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPay.Enabled = False
        Me.btnPay.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPay.Image = Global.CSAcctg.My.Resources.Resources.money
        Me.btnPay.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPay.Location = New System.Drawing.Point(674, 77)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(108, 35)
        Me.btnPay.TabIndex = 14
        Me.btnPay.Text = "Pay Bills"
        Me.btnPay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'bgwLoadBalance
        '
        Me.bgwLoadBalance.WorkerSupportsCancellation = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.btnPay)
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox5)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 347)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(881, 120)
        Me.Panel1.TabIndex = 16
        '
        'frm_vend_PayBills
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(881, 467)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(889, 494)
        Me.Name = "frm_vend_PayBills"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Check Disbursement"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdBills, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpDiscount.ResumeLayout(False)
        Me.grpDiscount.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbtnShwAll As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDue As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteDueBefore As System.Windows.Forms.DateTimePicker
    Friend WithEvents grdBills As System.Windows.Forms.DataGridView
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents grpDiscount As System.Windows.Forms.GroupBox
    Friend WithEvents lblsupplier As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblrefno As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblDiscount As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblCredits As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblCreditno As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents btnSetCredits As System.Windows.Forms.Button
    Friend WithEvents btnSetDiscount As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblendingbal As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cboPaymentMethod As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents dtePaymentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPay As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAmt2pay As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblCredUsed As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblDiscUsed As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblAmtdue As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboBankAccount As MTGCComboBox
    Friend WithEvents bgwLoadBalance As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents chkCheckPrint As System.Windows.Forms.CheckBox
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
