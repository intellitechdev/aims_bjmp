﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmSelectDocNum2
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Private Sub frmSelectDocNum1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListDocNum()
    End Sub
    Private Sub ListDocNum()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_DocumentNumber_ListDocNo")
        dgvlistDocNumber.DataSource = ds.Tables(0)
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim value As Object = dgvlistDocNumber.Rows(0).Cells(0).Value
        On Error Resume Next
        frmBirthdayBonus.txtDocnum.Text = value
        Me.Close()
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchDocNum(txtSearch.Text)
    End Sub
    Private Sub SearchDocNum(ByVal DocNum As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "usp_m_DocumentNumber_Search_DocNum", _
                                   New SqlParameter("@fcDocNumber", DocNum))
        dgvlistDocNumber.DataSource = ds.Tables(0)
    End Sub

    Private Sub dgvlistDocNumber_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvlistDocNumber.DoubleClick
        Dim value As Object = dgvlistDocNumber.Rows(0).Cells(0).Value
        On Error Resume Next
        frmBirthdayBonus.txtDocnum.Text = value
        Me.Close()
    End Sub
End Class