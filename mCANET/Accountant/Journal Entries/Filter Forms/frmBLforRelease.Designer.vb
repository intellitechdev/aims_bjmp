﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBLforRelease
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvApprovedLoan = New System.Windows.Forms.DataGridView()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.btnselect = New System.Windows.Forms.Button()
        Me.rbVoyage = New System.Windows.Forms.RadioButton()
        Me.rbBL = New System.Windows.Forms.RadioButton()
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(93, 295)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(365, 20)
        Me.txtname.TabIndex = 25
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 298)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Search BL No."
        '
        'dgvApprovedLoan
        '
        Me.dgvApprovedLoan.AllowUserToAddRows = False
        Me.dgvApprovedLoan.AllowUserToDeleteRows = False
        Me.dgvApprovedLoan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvApprovedLoan.BackgroundColor = System.Drawing.Color.White
        Me.dgvApprovedLoan.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvApprovedLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvApprovedLoan.Location = New System.Drawing.Point(13, 35)
        Me.dgvApprovedLoan.Name = "dgvApprovedLoan"
        Me.dgvApprovedLoan.ReadOnly = True
        Me.dgvApprovedLoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvApprovedLoan.Size = New System.Drawing.Size(634, 247)
        Me.dgvApprovedLoan.TabIndex = 24
        '
        'btncancel
        '
        Me.btncancel.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.btncancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btncancel.Location = New System.Drawing.Point(560, 288)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(87, 29)
        Me.btncancel.TabIndex = 23
        Me.btncancel.Text = "Cancel"
        Me.btncancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btncancel.UseVisualStyleBackColor = True
        '
        'btnselect
        '
        Me.btnselect.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnselect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnselect.Location = New System.Drawing.Point(467, 288)
        Me.btnselect.Name = "btnselect"
        Me.btnselect.Size = New System.Drawing.Size(87, 29)
        Me.btnselect.TabIndex = 22
        Me.btnselect.Text = "Select"
        Me.btnselect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnselect.UseVisualStyleBackColor = True
        '
        'rbVoyage
        '
        Me.rbVoyage.AutoSize = True
        Me.rbVoyage.Checked = True
        Me.rbVoyage.Location = New System.Drawing.Point(13, 12)
        Me.rbVoyage.Name = "rbVoyage"
        Me.rbVoyage.Size = New System.Drawing.Size(80, 17)
        Me.rbVoyage.TabIndex = 27
        Me.rbVoyage.TabStop = True
        Me.rbVoyage.Text = "Per Voyage"
        Me.rbVoyage.UseVisualStyleBackColor = True
        '
        'rbBL
        '
        Me.rbBL.AutoSize = True
        Me.rbBL.Location = New System.Drawing.Point(99, 12)
        Me.rbBL.Name = "rbBL"
        Me.rbBL.Size = New System.Drawing.Size(57, 17)
        Me.rbBL.TabIndex = 28
        Me.rbBL.Text = "Per BL"
        Me.rbBL.UseVisualStyleBackColor = True
        '
        'frmBLforRelease
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 331)
        Me.ControlBox = False
        Me.Controls.Add(Me.rbBL)
        Me.Controls.Add(Me.rbVoyage)
        Me.Controls.Add(Me.txtname)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvApprovedLoan)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btnselect)
        Me.Name = "frmBLforRelease"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BL for Release"
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvApprovedLoan As System.Windows.Forms.DataGridView
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents btnselect As System.Windows.Forms.Button
    Friend WithEvents rbVoyage As System.Windows.Forms.RadioButton
    Friend WithEvents rbBL As System.Windows.Forms.RadioButton
End Class
