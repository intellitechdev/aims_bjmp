<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_Cust_CreateSalesReciept_COOP_
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_Cust_CreateSalesReciept_COOP_))
        Me.txtClassName = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.BtnBrowse = New System.Windows.Forms.Button
        Me.cboPaymentMethodName = New System.Windows.Forms.ComboBox
        Me.cboPaymentMethodID = New System.Windows.Forms.ComboBox
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtCheckNo = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtSaleNo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.dteSale = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TxtClassRefno = New System.Windows.Forms.TextBox
        Me.LblSalesDiscountAmount = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblRetDiscount = New System.Windows.Forms.Label
        Me.lblRDPercent = New System.Windows.Forms.Label
        Me.txtRDAmt = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.cboRD = New System.Windows.Forms.ComboBox
        Me.lblSaleDiscount = New System.Windows.Forms.Label
        Me.lblSDpercent = New System.Windows.Forms.Label
        Me.txtSDAmt = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cboSD = New System.Windows.Forms.ComboBox
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.LstSRItem = New System.Windows.Forms.ListView
        Me.fxKeySRItem = New System.Windows.Forms.ColumnHeader
        Me.Item = New System.Windows.Forms.ColumnHeader
        Me.Order = New System.Windows.Forms.ColumnHeader
        Me.Rate = New System.Windows.Forms.ColumnHeader
        Me.Amount = New System.Windows.Forms.ColumnHeader
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddSalesRecieptItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditSalesRecieptItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BtnEditItem = New System.Windows.Forms.Button
        Me.BtnAddItem = New System.Windows.Forms.Button
        Me.btnAccountability = New System.Windows.Forms.Button
        Me.btnSelDel = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.LstDelete = New System.Windows.Forms.ListView
        Me.ColDelete = New System.Windows.Forms.ColumnHeader
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtClassName
        '
        Me.txtClassName.BackColor = System.Drawing.Color.White
        Me.txtClassName.Location = New System.Drawing.Point(12, 101)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.ReadOnly = True
        Me.txtClassName.Size = New System.Drawing.Size(252, 20)
        Me.txtClassName.TabIndex = 160
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 86)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 158
        Me.Label9.Text = "Class:"
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(270, 100)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(62, 23)
        Me.BtnBrowse.TabIndex = 157
        Me.BtnBrowse.Text = "Browse"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        '
        'cboPaymentMethodName
        '
        Me.cboPaymentMethodName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaymentMethodName.FormattingEnabled = True
        Me.cboPaymentMethodName.Location = New System.Drawing.Point(713, 101)
        Me.cboPaymentMethodName.Name = "cboPaymentMethodName"
        Me.cboPaymentMethodName.Size = New System.Drawing.Size(140, 21)
        Me.cboPaymentMethodName.TabIndex = 146
        '
        'cboPaymentMethodID
        '
        Me.cboPaymentMethodID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaymentMethodID.FormattingEnabled = True
        Me.cboPaymentMethodID.Location = New System.Drawing.Point(714, 101)
        Me.cboPaymentMethodID.Name = "cboPaymentMethodID"
        Me.cboPaymentMethodID.Size = New System.Drawing.Size(140, 21)
        Me.cboPaymentMethodID.TabIndex = 153
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(11, 56)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(319, 21)
        Me.cboCustomerName.TabIndex = 137
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(12, 56)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(312, 21)
        Me.cboCustomerID.TabIndex = 151
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.DarkBlue
        Me.Label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label17.Location = New System.Drawing.Point(582, 84)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(128, 19)
        Me.Label17.TabIndex = 147
        Me.Label17.Text = "Check No."
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCheckNo.Location = New System.Drawing.Point(582, 102)
        Me.txtCheckNo.Multiline = True
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(128, 20)
        Me.txtCheckNo.TabIndex = 148
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.BackColor = System.Drawing.Color.DarkBlue
        Me.Label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label16.Location = New System.Drawing.Point(713, 84)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(141, 19)
        Me.Label16.TabIndex = 145
        Me.Label16.Text = "Payment Method"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(582, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 15)
        Me.Label6.TabIndex = 142
        Me.Label6.Text = "Sale No."
        '
        'txtSaleNo
        '
        Me.txtSaleNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSaleNo.Location = New System.Drawing.Point(582, 60)
        Me.txtSaleNo.Name = "txtSaleNo"
        Me.txtSaleNo.Size = New System.Drawing.Size(128, 20)
        Me.txtSaleNo.TabIndex = 144
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(713, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(141, 15)
        Me.Label5.TabIndex = 141
        Me.Label5.Text = "Date"
        '
        'dteSale
        '
        Me.dteSale.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteSale.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSale.Location = New System.Drawing.Point(713, 60)
        Me.dteSale.Name = "dteSale"
        Me.dteSale.Size = New System.Drawing.Size(140, 20)
        Me.dteSale.TabIndex = 143
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(138, 23)
        Me.Label3.TabIndex = 138
        Me.Label3.Text = "Sales Receipt"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 136
        Me.Label1.Text = "Customer:Job"
        '
        'TxtClassRefno
        '
        Me.TxtClassRefno.Enabled = False
        Me.TxtClassRefno.Location = New System.Drawing.Point(12, 101)
        Me.TxtClassRefno.Name = "TxtClassRefno"
        Me.TxtClassRefno.Size = New System.Drawing.Size(115, 20)
        Me.TxtClassRefno.TabIndex = 159
        Me.TxtClassRefno.Visible = False
        '
        'LblSalesDiscountAmount
        '
        Me.LblSalesDiscountAmount.Location = New System.Drawing.Point(448, 338)
        Me.LblSalesDiscountAmount.Name = "LblSalesDiscountAmount"
        Me.LblSalesDiscountAmount.Size = New System.Drawing.Size(105, 22)
        Me.LblSalesDiscountAmount.TabIndex = 200
        Me.LblSalesDiscountAmount.Text = "0.00"
        Me.LblSalesDiscountAmount.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel2.Location = New System.Drawing.Point(711, 452)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(142, 2)
        Me.Panel2.TabIndex = 191
        '
        'lblBalance
        '
        Me.lblBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBalance.Location = New System.Drawing.Point(712, 436)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(143, 13)
        Me.lblBalance.TabIndex = 192
        Me.lblBalance.Text = "0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(711, 338)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 190
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(652, 436)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 189
        Me.Label2.Text = "Total Due"
        '
        'lblRetDiscount
        '
        Me.lblRetDiscount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRetDiscount.Location = New System.Drawing.Point(712, 397)
        Me.lblRetDiscount.Name = "lblRetDiscount"
        Me.lblRetDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblRetDiscount.TabIndex = 188
        Me.lblRetDiscount.Text = "0.00"
        Me.lblRetDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRDPercent
        '
        Me.lblRDPercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRDPercent.AutoSize = True
        Me.lblRDPercent.Location = New System.Drawing.Point(556, 397)
        Me.lblRDPercent.Name = "lblRDPercent"
        Me.lblRDPercent.Size = New System.Drawing.Size(36, 13)
        Me.lblRDPercent.TabIndex = 187
        Me.lblRDPercent.Text = "(0.0%)"
        '
        'txtRDAmt
        '
        Me.txtRDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRDAmt.Location = New System.Drawing.Point(611, 393)
        Me.txtRDAmt.Name = "txtRDAmt"
        Me.txtRDAmt.Size = New System.Drawing.Size(95, 20)
        Me.txtRDAmt.TabIndex = 186
        Me.txtRDAmt.Text = "0.00"
        Me.txtRDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(357, 396)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(88, 13)
        Me.Label27.TabIndex = 185
        Me.Label27.Text = "Retailer Discount"
        '
        'cboRD
        '
        Me.cboRD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboRD.FormattingEnabled = True
        Me.cboRD.Location = New System.Drawing.Point(448, 393)
        Me.cboRD.Name = "cboRD"
        Me.cboRD.Size = New System.Drawing.Size(105, 21)
        Me.cboRD.TabIndex = 184
        '
        'lblSaleDiscount
        '
        Me.lblSaleDiscount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSaleDiscount.Location = New System.Drawing.Point(712, 373)
        Me.lblSaleDiscount.Name = "lblSaleDiscount"
        Me.lblSaleDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblSaleDiscount.TabIndex = 183
        Me.lblSaleDiscount.Text = "0.00"
        Me.lblSaleDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSDpercent
        '
        Me.lblSDpercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSDpercent.AutoSize = True
        Me.lblSDpercent.Location = New System.Drawing.Point(556, 372)
        Me.lblSDpercent.Name = "lblSDpercent"
        Me.lblSDpercent.Size = New System.Drawing.Size(36, 13)
        Me.lblSDpercent.TabIndex = 182
        Me.lblSDpercent.Text = "(0.0%)"
        '
        'txtSDAmt
        '
        Me.txtSDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSDAmt.Location = New System.Drawing.Point(611, 368)
        Me.txtSDAmt.Name = "txtSDAmt"
        Me.txtSDAmt.Size = New System.Drawing.Size(95, 20)
        Me.txtSDAmt.TabIndex = 181
        Me.txtSDAmt.Text = "0.00"
        Me.txtSDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(364, 373)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(78, 13)
        Me.Label18.TabIndex = 180
        Me.Label18.Text = "Sales Discount"
        '
        'cboSD
        '
        Me.cboSD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSD.FormattingEnabled = True
        Me.cboSD.Location = New System.Drawing.Point(448, 368)
        Me.cboSD.Name = "cboSD"
        Me.cboSD.Size = New System.Drawing.Size(105, 21)
        Me.cboSD.TabIndex = 179
        '
        'cboTaxName
        '
        Me.cboTaxName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(505, 287)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(79, 21)
        Me.cboTaxName.TabIndex = 167
        '
        'cboTaxID
        '
        Me.cboTaxID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(505, 287)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(79, 21)
        Me.cboTaxID.TabIndex = 177
        '
        'lblPercentage
        '
        Me.lblPercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(590, 290)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(36, 13)
        Me.lblPercentage.TabIndex = 171
        Me.lblPercentage.Text = "(0.0%)"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(685, 312)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(170, 13)
        Me.lblTotal.TabIndex = 174
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Location = New System.Drawing.Point(590, 313)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 18)
        Me.Label13.TabIndex = 173
        Me.Label13.Text = "Total"
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTaxAmt.Location = New System.Drawing.Point(691, 289)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(164, 14)
        Me.lblTaxAmt.TabIndex = 172
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(12, 330)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemo.Size = New System.Drawing.Size(339, 124)
        Me.txtMemo.TabIndex = 169
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.Location = New System.Drawing.Point(11, 316)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 18)
        Me.Label11.TabIndex = 168
        Me.Label11.Text = "Memo"
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.Location = New System.Drawing.Point(472, 289)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(59, 18)
        Me.Label20.TabIndex = 166
        Me.Label20.Text = "Tax"
        '
        'LstSRItem
        '
        Me.LstSRItem.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.LstSRItem.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LstSRItem.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.fxKeySRItem, Me.Item, Me.Order, Me.Rate, Me.Amount})
        Me.LstSRItem.ContextMenuStrip = Me.ContextMenuStrip1
        Me.LstSRItem.FullRowSelect = True
        Me.LstSRItem.GridLines = True
        Me.LstSRItem.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LstSRItem.HideSelection = False
        Me.LstSRItem.Location = New System.Drawing.Point(11, 127)
        Me.LstSRItem.Name = "LstSRItem"
        Me.LstSRItem.Size = New System.Drawing.Size(842, 152)
        Me.LstSRItem.TabIndex = 201
        Me.LstSRItem.UseCompatibleStateImageBehavior = False
        Me.LstSRItem.View = System.Windows.Forms.View.Details
        '
        'fxKeySRItem
        '
        Me.fxKeySRItem.Text = "fxKeySRItem"
        Me.fxKeySRItem.Width = 0
        '
        'Item
        '
        Me.Item.Text = "Item"
        Me.Item.Width = 517
        '
        'Order
        '
        Me.Order.Text = "Ordered"
        Me.Order.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Rate
        '
        Me.Rate.Text = "Rate"
        Me.Rate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Rate.Width = 120
        '
        'Amount
        '
        Me.Amount.Text = "Amount"
        Me.Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.Amount.Width = 120
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddSalesRecieptItemToolStripMenuItem, Me.EditSalesRecieptItemToolStripMenuItem, Me.DeleteItemToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(142, 70)
        '
        'AddSalesRecieptItemToolStripMenuItem
        '
        Me.AddSalesRecieptItemToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.AddSalesRecieptItemToolStripMenuItem.Name = "AddSalesRecieptItemToolStripMenuItem"
        Me.AddSalesRecieptItemToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.AddSalesRecieptItemToolStripMenuItem.Text = "Add Item"
        '
        'EditSalesRecieptItemToolStripMenuItem
        '
        Me.EditSalesRecieptItemToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditSalesRecieptItemToolStripMenuItem.Name = "EditSalesRecieptItemToolStripMenuItem"
        Me.EditSalesRecieptItemToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.EditSalesRecieptItemToolStripMenuItem.Text = "Edit Item"
        '
        'DeleteItemToolStripMenuItem
        '
        Me.DeleteItemToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.deletebig
        Me.DeleteItemToolStripMenuItem.Name = "DeleteItemToolStripMenuItem"
        Me.DeleteItemToolStripMenuItem.Size = New System.Drawing.Size(141, 22)
        Me.DeleteItemToolStripMenuItem.Text = "Delete Item"
        '
        'BtnEditItem
        '
        Me.BtnEditItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnEditItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnEditItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.BtnEditItem.Location = New System.Drawing.Point(109, 285)
        Me.BtnEditItem.Name = "BtnEditItem"
        Me.BtnEditItem.Size = New System.Drawing.Size(92, 28)
        Me.BtnEditItem.TabIndex = 203
        Me.BtnEditItem.Text = "Edit Item"
        Me.BtnEditItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnEditItem.UseVisualStyleBackColor = True
        '
        'BtnAddItem
        '
        Me.BtnAddItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnAddItem.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAddItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.BtnAddItem.Location = New System.Drawing.Point(12, 285)
        Me.BtnAddItem.Name = "BtnAddItem"
        Me.BtnAddItem.Size = New System.Drawing.Size(92, 28)
        Me.BtnAddItem.TabIndex = 202
        Me.BtnAddItem.Text = "Add Item"
        Me.BtnAddItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnAddItem.UseVisualStyleBackColor = True
        '
        'btnAccountability
        '
        Me.btnAccountability.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccountability.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccountability.Image = Global.CSAcctg.My.Resources.Resources.post
        Me.btnAccountability.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAccountability.Location = New System.Drawing.Point(118, 463)
        Me.btnAccountability.Name = "btnAccountability"
        Me.btnAccountability.Size = New System.Drawing.Size(146, 28)
        Me.btnAccountability.TabIndex = 199
        Me.btnAccountability.Text = "Accountability"
        Me.btnAccountability.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAccountability.UseVisualStyleBackColor = True
        '
        'btnSelDel
        '
        Me.btnSelDel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelDel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelDel.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnSelDel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelDel.Location = New System.Drawing.Point(207, 285)
        Me.btnSelDel.Name = "btnSelDel"
        Me.btnSelDel.Size = New System.Drawing.Size(179, 28)
        Me.btnSelDel.TabIndex = 178
        Me.btnSelDel.Text = "Delete Selected Items"
        Me.btnSelDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSelDel.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(767, 463)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 29)
        Me.btnClose.TabIndex = 176
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(676, 463)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 29)
        Me.btnSave.TabIndex = 175
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(24, 463)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 28)
        Me.btnPreview.TabIndex = 165
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'LstDelete
        '
        Me.LstDelete.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColDelete})
        Me.LstDelete.Location = New System.Drawing.Point(611, 145)
        Me.LstDelete.Name = "LstDelete"
        Me.LstDelete.Size = New System.Drawing.Size(230, 124)
        Me.LstDelete.TabIndex = 204
        Me.LstDelete.UseCompatibleStateImageBehavior = False
        Me.LstDelete.View = System.Windows.Forms.View.Details
        Me.LstDelete.Visible = False
        '
        'ColDelete
        '
        Me.ColDelete.Text = "Deleted"
        Me.ColDelete.Width = 216
        '
        'frm_Cust_CreateSalesReciept_COOP_
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(865, 498)
        Me.ControlBox = False
        Me.Controls.Add(Me.LstDelete)
        Me.Controls.Add(Me.BtnEditItem)
        Me.Controls.Add(Me.BtnAddItem)
        Me.Controls.Add(Me.LstSRItem)
        Me.Controls.Add(Me.btnAccountability)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRetDiscount)
        Me.Controls.Add(Me.lblRDPercent)
        Me.Controls.Add(Me.txtRDAmt)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.cboRD)
        Me.Controls.Add(Me.lblSaleDiscount)
        Me.Controls.Add(Me.lblSDpercent)
        Me.Controls.Add(Me.txtSDAmt)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cboSD)
        Me.Controls.Add(Me.btnSelDel)
        Me.Controls.Add(Me.cboTaxName)
        Me.Controls.Add(Me.cboTaxID)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblPercentage)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lblTaxAmt)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.txtClassName)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.BtnBrowse)
        Me.Controls.Add(Me.cboPaymentMethodName)
        Me.Controls.Add(Me.cboPaymentMethodID)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtCheckNo)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtSaleNo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteSale)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtClassRefno)
        Me.Controls.Add(Me.LblSalesDiscountAmount)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(873, 506)
        Me.Name = "frm_Cust_CreateSalesReciept_COOP_"
        Me.Text = "Enter Sales Receipts"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents cboPaymentMethodName As System.Windows.Forms.ComboBox
    Friend WithEvents cboPaymentMethodID As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSaleNo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dteSale As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtClassRefno As System.Windows.Forms.TextBox
    Friend WithEvents LblSalesDiscountAmount As System.Windows.Forms.Label
    Friend WithEvents btnAccountability As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblRetDiscount As System.Windows.Forms.Label
    Friend WithEvents lblRDPercent As System.Windows.Forms.Label
    Friend WithEvents txtRDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboRD As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaleDiscount As System.Windows.Forms.Label
    Friend WithEvents lblSDpercent As System.Windows.Forms.Label
    Friend WithEvents txtSDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboSD As System.Windows.Forms.ComboBox
    Friend WithEvents btnSelDel As System.Windows.Forms.Button
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents LstSRItem As System.Windows.Forms.ListView
    Friend WithEvents BtnAddItem As System.Windows.Forms.Button
    Friend WithEvents BtnEditItem As System.Windows.Forms.Button
    Friend WithEvents Item As System.Windows.Forms.ColumnHeader
    Friend WithEvents Order As System.Windows.Forms.ColumnHeader
    Friend WithEvents Rate As System.Windows.Forms.ColumnHeader
    Friend WithEvents Amount As System.Windows.Forms.ColumnHeader
    Friend WithEvents fxKeySRItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AddSalesRecieptItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditSalesRecieptItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LstDelete As System.Windows.Forms.ListView
    Friend WithEvents ColDelete As System.Windows.Forms.ColumnHeader
End Class
