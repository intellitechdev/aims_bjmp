Imports Microsoft.ApplicationBlocks.Data
Imports System.data.SqlClient
Public Class frm_Add_SRItems

    '                   ########################################################
    '                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
    '                   ### Date Created: Sept. 04, 2009                     ###
    '                   ### Website: http://www.ionflux.site50.net           ###
    '                   ########################################################
#Region "declaration"
    Public Mode As String = "Edit Mode"
    Public sKeySRItem As String
    Public Result As String
    Public nfxKeyItem As String
    Public nItemName As String
    Public nQty As String
    Public nRate As String
    Public nAmount As String
    Private gCon As New Clsappconfiguration
#End Region
#Region "Subs and Function"
    Private Function getSRItem(Optional ByVal sKey As String = "") As DataSet
        Dim sSQLCmd As String
        If sKey <> "" Then
            sSQLCmd = "usp_m_item_informationList "
            sSQLCmd &= "@fxKeyItem = '" & sKey & "'"
            sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "
        Else
            sSQLCmd = "select distinct cast(t1.fxKeyItem AS VARCHAR(50)) as [fxnKeyItem] , t1.fcDescription+' '+isnull(t3.fcColorDescription, '')+' '+isnull(t1.fcSizeCode, '') as [fcItemName], "
            sSQLCmd &= " isnull(t4.fcItemGroupName,'') as fcItemGroupName, t1.fdUnitPrice as [fdPrice] "
            sSQLCmd &= " from dbo.mItemMaster as t1 left outer join dbo.mItemCode as t2 "
            sSQLCmd &= " on t2.fcItemCode = t1.fcItemCode "
            sSQLCmd &= " left outer join dbo.mItemColor as t3 on t3.fcColorCode = t1.fcColorCode "
            sSQLCmd &= " left outer join dbo.mItem01Group as t4 on t4.fxKeyItemGroup = t1.fxKeyGroupName "
            sSQLCmd &= " WHERE t1.fxKeyCompany = '" & gCompanyID() & "'"
            sSQLCmd &= " ORDER by fcItemName"
        End If
        Try
            Return SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Get Item")
            Return Nothing
        End Try
    End Function
    Private Function computeAmount() As Decimal
        Dim Total As Decimal
        If TxtOrdered.Text = "" Then
            TxtOrdered.Text = "0.0"
        End If
        If txtRate.Text = "" Then
            txtRate.Text = "0.00"
        End If
        Total = CDec(txtRate.Text) * CDec(TxtOrdered.Text)
        Return Total
    End Function
    Private Sub LoadItemName()
        Dim dtItem As New DataTable
        dtItem = getSRItem.Tables(0)
        cboMItem.Items.Clear()
        cboMItem.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        cboMItem.SourceDataString = New String(2) {"fcItemName", "fdPrice", "fxnKeyItem"}
        cboMItem.SourceDataTable = dtItem
    End Sub
    Private Sub Clear_Form()
        sKeySRItem = Nothing
        Result = "Not ready to add"
        nfxKeyItem = Nothing
        nItemName = Nothing
        nQty = 0
        nRate = 0
        nAmount = 0
        cboMItem.Text = "(Pls. Select an item)"
        TxtOrdered.Text = "0.00"
        txtRate.Text = "0.00"
        lblAmout.Text = "0.00"
    End Sub
#End Region

    Private Sub frm_Add_SRItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Result = "Not Ready to add"
        LoadItemName()
        If Mode = "Edit Mode" Then
            cboMItem.SelectedValue = nItemName
            txtRate.Text = nRate
            TxtOrdered.Text = nQty
            lblAmout.Text = nAmount
        Else
            Clear_Form()
        End If
    End Sub
    Private Sub cboMItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMItem.SelectedIndexChanged
        If cboMItem.SelectedValue Is Nothing Or cboMItem.SelectedValue = "" Then
            txtRate.Text = "0.00"
            sKeySRItem = Nothing
            nItemName = ""
        Else
            txtRate.Text = cboMItem.SelectedItem.Col2
            sKeySRItem = cboMItem.SelectedItem.Col3
            nItemName = cboMItem.SelectedItem.Col1
        End If
    End Sub
    Private Sub TxtOrdered_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtOrdered.GotFocus
        TxtOrdered.SelectAll()
    End Sub
    Private Sub TxtOrdered_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtOrdered.KeyPress
        If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
        If e.KeyChar = Chr(8) Then e.Handled = False
        If e.KeyChar = Chr(13) Then TxtOrdered.Focus()
    End Sub
    Private Sub txtRate_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRate.GotFocus
        txtRate.SelectAll()
    End Sub
    Private Sub txtRate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRate.KeyPress
        If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
        If e.KeyChar = Chr(8) Then e.Handled = False
        If e.KeyChar = Chr(13) Then txtRate.Focus()
    End Sub
    Private Sub TxtOrdered_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtOrdered.TextChanged
        lblAmout.Text = computeAmount.ToString
    End Sub
    Private Sub txtRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRate.TextChanged
        lblAmout.Text = computeAmount.ToString
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click, BtnEdt.Click
        If cboMItem.Text = "(Pls. Select an item)" Or cboMItem.Text = "" Or cboMItem.Text Is Nothing Then
            MsgBox("Please select an item.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "No Item Detected")
            cboMItem.SelectAll()
        Else
            nfxKeyItem = sKeySRItem
            nRate = txtRate.Text
            nQty = TxtOrdered.Text
            nAmount = lblAmout.Text
            Result = "Ready to add"
            Me.Close()
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Result = "Not ready to add"
        Me.Close()
    End Sub
End Class