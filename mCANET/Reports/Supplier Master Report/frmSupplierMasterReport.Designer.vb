<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmsupplierMasterReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmsupplierMasterReport))
        Me.crvSupplierMaster = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'crvSupplierMaster
        '
        Me.crvSupplierMaster.ActiveViewIndex = -1
        Me.crvSupplierMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvSupplierMaster.DisplayGroupTree = False
        Me.crvSupplierMaster.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvSupplierMaster.Location = New System.Drawing.Point(0, 0)
        Me.crvSupplierMaster.Name = "crvSupplierMaster"
        Me.crvSupplierMaster.SelectionFormula = ""
        Me.crvSupplierMaster.ShowCloseButton = False
        Me.crvSupplierMaster.ShowGroupTreeButton = False
        Me.crvSupplierMaster.ShowRefreshButton = False
        Me.crvSupplierMaster.Size = New System.Drawing.Size(922, 529)
        Me.crvSupplierMaster.TabIndex = 1
        Me.crvSupplierMaster.ViewTimeSelectionFormula = ""
        '
        'frmsupplierMasterReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(922, 529)
        Me.Controls.Add(Me.crvSupplierMaster)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmsupplierMasterReport"
        Me.Text = "Supplier Master Report"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvSupplierMaster As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
