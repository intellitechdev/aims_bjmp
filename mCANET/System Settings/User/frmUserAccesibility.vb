﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading

Public Class frmUserAccesibility
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        
    End Sub

    Private Function getUserID()
        Dim mycon As New Clsappconfiguration
        Dim sSqlCmd As String = "SELECT fxKeyUser FROM dbo.tUsers WHERE fcLogName ='" & txtUsername.Text & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.Text, sSqlCmd)
                If rd.Read = True Then
                    Return rd.Item(0).ToString
                End If
            End Using
        Catch ex As Exception

        End Try
    End Function

    Private Sub frmUserAccesibility_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadusers()
    End Sub

    Private Sub loadusers()
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "SYS_User_List")

        grdUsers.DataSource = ds.Tables(0)

        With grdUsers
            .Columns("fxKeyUser").Visible = False
            .Columns("fxKeyGroup").Visible = False
            .Columns("fcGroupName").Visible = False
            .Columns("fdCreated").Visible = False
            .Columns("fcCreator").Visible = False
            .Columns("fcFullName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("fcDescription").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        End With
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Call updateUserModules()

    End Sub

    Private Sub updateUserModules()
        Dim mycon As New Clsappconfiguration
        For xRow As Integer = 0 To grdUserAccess.RowCount - 1
            Dim fxModuleID As String = grdUserAccess.Item("pk_UserModule", xRow).Value.ToString
            Dim isAllowed As Boolean = grdUserAccess.Item("fbIsAllowed", xRow).Value
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "spu_UserAccess_Update", _
                                          New SqlParameter("@pk_UserModule", fxModuleID), _
                                          New SqlParameter("@fbIsAllowed", isAllowed))
        Next
        MsgBox("User Access Modules Successfully Updated")
        AuditTrail_Save("User Access Modules", "Update user modules of " & txtUsername.Text)
        Call SetAccesibility()
    End Sub

    Private Sub grdUsers_Click(sender As System.Object, e As System.EventArgs) Handles grdUsers.Click
        Try
            Dim ds As DataSet

            ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "spu_UserAccess_List",
                                          New SqlParameter("@fxKeyUser", CInt(grdUsers.Item(0, grdUsers.CurrentRow.Index).Value)))

            grdUserAccess.DataSource = ds.Tables(0)

            With grdUserAccess
                .Columns("pk_UserModule").Visible = False
                .Columns("fcModuleName").HeaderText = "Module Name"
                .Columns("fcModuleName").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("fcModuleName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("fcModuleName").ReadOnly = True
                .Columns("fbIsAllowed").HeaderText = "Allowed"
            End With
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub
End Class