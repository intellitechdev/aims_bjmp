﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmBLforRelease

    Public RecordedInAccounting As Boolean
    Private gCon As New Clsappconfiguration()
    Public fxKeyAccount As String
    Dim Loanref As String
    Public _IsBL As Boolean

    Private Sub btnselect_Click(sender As System.Object, e As System.EventArgs) Handles btnselect.Click
        If Me.dgvApprovedLoan.SelectedRows.Count <> 0 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            MessageBox.Show("Please Select Entry!", "BL for Release", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btncancel_Click(sender As System.Object, e As System.EventArgs) Handles btncancel.Click
        Me.Close()
    End Sub

    Private Sub SearchBL()
        'Try
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "[spu_00BillofLading_List]", _
                                      New SqlParameter("@FcBLNo", txtname.Text))
        dgvApprovedLoan.DataSource = ds.Tables(0)
        With dgvApprovedLoan
            .Columns("FcIDno").Width = 80
            .Columns("FcIDno").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FcIDno").ReadOnly = True
            .Columns("FcIDno").HeaderText = "ID No."

            .Columns("fullname").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns("fullname").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fullname").ReadOnly = True
            .Columns("fullname").HeaderText = "Name"

            .Columns("FcVoyageNo").Width = 80
            .Columns("FcVoyageNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FcVoyageNo").ReadOnly = True
            .Columns("FcVoyageNo").HeaderText = "Voyage No."

            .Columns("FcBLNo").Width = 80
            .Columns("FcBLNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FcBLNo").ReadOnly = True
            .Columns("FcBLNo").HeaderText = "BL No."

            .Columns("FnTotalFreight").Width = 80
            .Columns("FnTotalFreight").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FnTotalFreight").ReadOnly = True
            .Columns("FnTotalFreight").HeaderText = "Amount"
            .Columns("FnTotalFreight").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("FnTotalFreight").DefaultCellStyle.Format = "##,##0.00"

            .Columns("FdBLDate").Width = 80
            .Columns("FdBLDate").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("FdBLDate").ReadOnly = True
            .Columns("FdBLDate").HeaderText = "BL Date"
        End With
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Critical, "SearchBL")
        'End Try
    End Sub

    Private Sub SearchVoyage()
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As New DataSet
            frmGeneralJournalEntries.grdListofEntries.Rows.Clear()
            ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "[spu_00Voyage_List]", _
                                          New SqlParameter("@FcVoyageNo", txtname.Text))
            If dgvApprovedLoan.DataSource IsNot Nothing Then
                dgvApprovedLoan.DataSource = ds.Tables(0)
            Else
                dgvApprovedLoan.DataSource = Nothing
                dgvApprovedLoan.DataSource = ds.Tables(0)
            End If
            With dgvApprovedLoan
                .Columns("PkVoyageID").Visible = False

                .Columns("FcVoyageNo").Width = 150
                .Columns("FcVoyageNo").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("FcVoyageNo").ReadOnly = True
                .Columns("FcVoyageNo").HeaderText = "Voyage No."

                .Columns("FcVoyageType").Width = 150
                .Columns("FcVoyageType").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("FcVoyageType").ReadOnly = True
                .Columns("FcVoyageType").HeaderText = "Voyage Type"

                .Columns("FdDeparture").Width = 80
                .Columns("FdDeparture").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("FdDeparture").ReadOnly = True
                .Columns("FdDeparture").HeaderText = "Voyage Date"

                .Columns("FcDescription").Width = 80
                .Columns("FcDescription").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("FcDescription").ReadOnly = True
                .Columns("FcDescription").HeaderText = "Vessel"
            End With
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "SearchVoyage")
        End Try
    End Sub

    Private Sub txtname_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtname.TextChanged
        If Me.rbBL.Checked Then
            SearchBL()
        ElseIf Me.rbVoyage.Checked Then
            SearchVoyage()
        End If
    End Sub

    Public Sub AvoidDuplicate()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        Dim fcFilter As String = dgvApprovedLoan.SelectedRows(0).Cells(1).Value.ToString()
        ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "spu_t_tJVEntry_List",
                                      New SqlParameter("@docType", frmGeneralJournalEntries.cboDoctype.Text),
                                      New SqlParameter("@filter", fcFilter),
                                      New SqlParameter("@coid", gCompanyID()))
        'MsgBox(ds.Tables(0).Rows.Count)
        If ds.Tables(0).Rows.Count <> 0 Then
            RecordedInAccounting = True
        Else
            RecordedInAccounting = False
        End If
    End Sub

    Public Sub EntryDetails()
        Try
            fxKeyAccount = dgvApprovedLoan.SelectedRows(0).Cells(0).Value.ToString()
            frmGeneralJournalEntries.keyID = fxKeyAccount
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub frmBLforRelease_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Me.rbBL.Checked Then
            SearchBL()
        ElseIf Me.rbVoyage.Checked Then
            SearchVoyage()
        End If
    End Sub

    Private Sub rbVoyage_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbVoyage.CheckedChanged
        
    End Sub

    Private Sub rbBL_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbBL.CheckedChanged
            SearchBL()
    End Sub

    Private Sub rbVoyage_Click(sender As System.Object, e As System.EventArgs) Handles rbVoyage.Click
        SearchVoyage()
    End Sub

    Private Sub rbBL_Click(sender As System.Object, e As System.EventArgs) Handles rbBL.Click
            SearchBL()
    End Sub

    Private Sub dgvApprovedLoan_DoubleClick(sender As System.Object, e As System.EventArgs) Handles dgvApprovedLoan.DoubleClick
        btnselect_Click(sender, e)
    End Sub
End Class